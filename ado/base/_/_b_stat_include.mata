*! version 1.1.0  14jan2013

local diparm_sep	-1
local diparm_bot	-2
local diparm_label	-3
local diparm_eqlab	-4
local diparm_aux	1
local diparm_trans	2
local diparm_vlabel	3
local diparm_veqlab	4

local ci_normal		0
local ci_logit		1
local ci_default	`ci_normal'

local varcov	`"tokens("var cov")"'

local chibar_eps	1e-5

local reldif_tol	1e-7

local pclass_var	100

