*! version 1.0.9  04aug2014
program _me_display
	version 13
	
	syntax [, noLog noHEADer noGRoup grouponly grouptable	///
		noTABle noLRtest mi or irr eform EFORM1(string) ///
		noESTimate COEFLegend * ]
	
	if "`or'"!="" {
		if !inlist("`e(model)'","logistic","ologit") {
			di "{err}option {bf:or} not allowed"
			exit 198
		}
	}
	if "`irr'"!="" {
		if !inlist("`e(model)'","nbinomial","Poisson") {
			di "{err}option {bf:irr} not allowed"
			exit 198
		}
	}
	
	if "`irr'"!="" local eform eform(IRR)
	if "`or'"!="" local or eform(Odds Ratio)
	if `"`eform1'"'!="" local eform eform(`eform1')
	if "`estimate'" == "noestimate" local coeflegend coeflegend
	
	_get_diopts diopts, `options'
	local diopts `diopts' `coeflegend'
	
	if "`header'" == "" 	_header, `group' `grouponly' `grouptable'
	if "`grouponly'`grouptable'" != "" {
		exit
	}
	if "`table'" == "" {
		_prefix_display, noheader nofootnote `or' `eform' `diopts'
		local lsizeopt linesize(`s(width)')
	}
	if "`lrtest'"=="" 	_LR_test
	if "`table'" == "" {
		_prefix_footnote, `lsizeopt'
	}
	
end

program _header

	syntax [, noGRoup grouponly grouptable mi ]
	
	if "`e(cmd)'"=="meglm" local flink 1
	else local flink 0
	
	if "`grouponly'" != "" {
		if "`e(ivars)'" == "" {
			di "{err}{p 0 4 2}model is `e(model)' regression; "
			di "no group information available{p_end}"
			exit 459
		}
		_group_table, table `mi'
		exit
	}
	
	if "`grouptable'" != "" {
		if "`e(ivars)'" == "" {
			di "{err}{p 0 4 2}model is `e(model)' regression; "
			di "no group information available{p_end}"
			exit 459
		}
		_group_table, table `mi'
		exit
	}
	
	di
	local crtype = upper(substr(`"`e(crittype)'"',1,1)) + /*
                */ substr(`"`e(crittype)'"',2,.)
	
	di "{txt}`e(title)'" _c
	di _col(49) "{txt}Number of obs" _col(68) "={res}" _col(70) %9.0g e(N)
	
	if `flink' {
		local family `e(family)'
		if inlist("`family'","gaussian","bernoulli","poisson") {
			local family = proper("`family'")
		}
		if "`family'"=="nbinomial" local family "negative binomial"
		local col1 = 32 - length("`family'")
		di "{txt}Family: " _col(`col1') "{res}`family'"
		
		local link `e(link)'
		if "`link'"=="cloglog" local link "complementary log-log"
		local col1 = 32 - length("`link'")
		di "{txt}Link: " _col(`col1') "{res}`link'"
	}
	
	if "`e(binomial)'" != "" {
		local bin `e(binomial)'
		cap confirm number `bin'
		if _rc {
			local abbr = abbrev("`bin'",12)
			local col1 = 32 - length("`abbr'")
			di "{txt}Binomial variable: " _col(`col1') "{res}`abbr'"
		}
		else {
			local col1 = 32 - length("`bin'")
			di "{txt}Binomial trials: " _col(`col1') %9.0g "{res}`bin'"
		}
	}
	if "`e(dispersion)'" != "" {
		local col1 = 32 - length("`e(dispersion)'")
		di "{txt}Overdispersion: " _col(`col1') "{res}`e(dispersion)'"
	}
	
	local intm `e(intmethod)'
	if "`intm'"=="laplace" local intp 0
	else local intp `e(n_quad)'
		
	if "`group'" == "" {
 		_group_table, `mi'
		*di
	}
	else di
	
	local col1 = 32 - length("`intm'")
	local col2 = 79 - length("`e(n_quad)'")
	
	if e(k_r) {
		if `intp' {
			di "{txt}Integration method: " 		///
				_col(`col1') "{res}`intm'" _c
			di _col(49) "{txt}Integration points =" ///
				_col(`col2') "{res}`e(n_quad)'"
		}
		else di "{txt}Integration method: " _col(`col1') "{res}`intm'"
	}
		
	di
	if !e(estimates) {
		exit
	}
	di _col(49) "{txt}`e(chi2type)' chi2({res}`e(df_m)'{txt})" ///
		_col(68) "={res}" _col(70) %9.2f e(chi2)
	di "{txt}`crtype'" " = {res}" %10.0g e(ll) ///
		_col(49) "{txt}Prob > chi2" _col(68) "={res}" _col(73) ///
		%6.4f chiprob(e(df_m), e(chi2))
end

program _group_table
	
	if !e(k_r) {
		exit
	}
	
	syntax [, table mi]
	
	local ivars `e(ivars)'
	local levels : list uniq ivars
	tempname Ng min avg max
	if ("`e(cmd)'"=="mi estimate" | "`mi'"!="") {
		local sfx _mi
	}
	mat `Ng' = e(N_g`sfx')
	mat `min' = e(g_min`sfx')
	mat `avg' = e(g_avg`sfx')
	mat `max' = e(g_max`sfx')
	local w : word count `levels'
	if `w' == 0 {
		exit
	}
	
	if `w' == 1 & "`table'" == "" {
		local abbr = abbrev("`levels'",14)
		local col1 = 32 - length("`abbr'")
		
		di "{txt}Group variable: " _col(`col1') "{res}`abbr'"  ///
		     _col(49) "{txt}Number of groups" _col(68) "={res}" ///
		     _col(70) %9.0g `Ng'[1,1] _n
		di _col(49) "{txt}Obs per group: min" _col(68) "={res}" ///
		     _col(70) %9.0g `min'[1,1]
		di _col(64) "{txt}avg" _col(68) "={res}" ///
		     _col(70) %9.1f `avg'[1,1]
		di _col(64) "{txt}max" _col(68) "={res}" ///
		     _col(70) %9.0g `max'[1,1] _n
	}
	//         1         2         3         4         5         6
	//123456789012345678901234567890123456789012345678901234567890
	//                    No. of       Observations per Group
	// Group Variable |   Groups    Minimum    Average    Maximum
	//        level1  | ########  #########  #########  #########
	else {
		di
		di "{txt}{hline 16}{c TT}{hline 42}
		di _col(17) "{txt}{c |}" _col(21) "No. of" ///
	          _col(34) "Observations per Group"
		
		di _col(2) "{txt}Group Variable" _col(17) "{c |}" ///
		  _col(21) "Groups" _col(31) "Minimum" ///
		  _col(42) "Average" _col(53) "Maximum"
		
		di "{txt}{hline 16}{c +}{hline 42}"
		local i 1
		foreach k of local levels {
			local lev = abbrev("`k'",12)
			local p = 16 - length("`lev'")
			di _col(`p') "{res}`lev'" /// 
			  _col(17) "{txt}{c |}{res}" ///
			  _col(19) %8.0g `Ng'[1,`i'] ///
                          _col(29) %9.0g `min'[1,`i'] ///
                          _col(40) %9.1f `avg'[1,`i'] ///
                          _col(51) %9.0g `max'[1,`i']
			local ++i
		}
		di "{txt}{hline 16}{c BT}{hline 42}"
		di
	}
end

program _LR_test, sclass
	
	// skip LR test if certain conditions are met
	if !e(k_r) | "`e(vce)'"!="oim" | e(has_cns) | !e(estimates) {
		exit
	}
	if !e(converged) {
		exit
	}
	
	// We have already established that e(chi2_c) exists
	if ((e(chi2_c) > 0.005) & (e(chi2_c)<1e5)) | (e(chi2_c)==0) {
  		local fmt "%8.2f"
        }
        else    local fmt "%8.2e"

	di "{txt}LR test vs. `e(model)' regression:" _c
	if `e(df_c)' == 1 {    		// chibar2(01)
		di _col(34) "{txt}{help j_chibar##|_new:chibar2(01) =}{res}" ///
                   _col(48) `fmt' e(chi2_c) ///
		   _col(57) "{txt}Prob>=chibar2 = {res}" ///
		   _col(73) %6.4f e(p_c)
	}
	else {
		local k = length("`e(df_c)'")
		di _col(`=39-`k'') "{txt}chi2({res}`e(df_c)'{txt}) ={res}" ///
		   _col(48) `fmt' e(chi2_c) ///
		   _col(59) "{txt}Prob > chi2 ={res}" ///
		   _col(73) %6.4f e(p_c)
		local conserve conserve
	}
	
	if "`conserve'" != "" {
		di _n "{txt}{p 0 6 4 79}Note: "	///
			"{help j_mixedlr##|_new:LR test is conservative} " ///
			"and provided only for reference.{p_end}"
		sreturn local note note
	}

end
exit
