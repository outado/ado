/*
	_power_list.idlg

	column lists for power dialogs

*!  VERSION 1.0.2  01jun2013

*/

LIST main_power_beta_list
BEGIN
	"Power"
	"Type II error"
END

LIST main_power_beta_values
BEGIN
	"power"
	"beta"
END

LIST main_fpc_list
BEGIN
	"None"
	"Population size"
	"Sampling rate"
END

LIST main_fpc_values
BEGIN
	"none"
	"pop_size"
	"sample_rate"
END

LIST onemean_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null mean"
	"Alternative mean"
	"Difference between the alternative mean and the null mean"
	"Standard deviation"
	"Population size or sampling rate"
	"Target parameter"
END

LIST onemean_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"m0"
	"ma"
	"diff"
	"sd"
	"fpc"
	"target"
END

LIST onecorr_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null correlation"
	"Alternative correlation"
	"Difference between the alternative correlation and the null correlation"
	"Target parameter"
END

LIST onecorr_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"r0"
	"ra"
	"diff"
	"target"
END

LIST oneprop_score_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null proportion"
	"Alternative proportion"
	"Difference between the alternative proportion and the null proportion"
	"Target parameter"
END

LIST oneprop_score_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"p0"
	"pa"
	"diff"
	"target"
END

LIST oneprop_bin_dimlist
BEGIN
	"Significance level"
	"Actual significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null proportion"
	"Alternative proportion"
	"Difference between the alternative proportion and the null proportion"
	"Lower critical value"
	"Upper critical value"
	"Target parameter"
END

LIST oneprop_bin_dimvalues
BEGIN
	"alpha"
	"alpha_a"
	"power"
	"beta"
	"N"
	"delta"
	"p0"
	"pa"
	"diff"
	"C_l"
	"C_u"
	"target"
END

LIST oneprop_wald_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null proportion"
	"Alternative proportion"
	"Difference between the alternative proportion and the null proportion"
	"Target parameter"
END

LIST oneprop_wald_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"p0"
	"pa"
	"diff"
	"target"
END

LIST onevar_var_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Null variance"
	"Alternative variance"
	"Ratio of the alternative variance to the null variance"
	"Effect size"
	"Target parameter"
END

LIST onevar_var_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"v0"
	"va"
	"ratio"
	"delta"
	"target"
END

LIST onevar_sd_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null standard deviation"
	"Alternative standard deviation"
	"Ratio of the alternative standard deviation to the null standard deviation"
	"Target parameter"
END

LIST onevar_sd_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"s0"
	"sa"
	"ratio"
	"target"
END

LIST twomeans_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Total number of subjects"
	"Number of subjects in the control group"
	"Number of subjects in the experimental group"
	"Ratio of sample sizes, experimental to control"
	"Effect size"
	"Control-group mean"
	"Experimental-group mean"
	"Common standard deviation"
	"Control-group standard deviation"
	"Experimental-group standard deviation"
	"Difference between the experimental-group and control-group means"
	"Target parameter"
END

LIST twomeans_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"N1"
	"N2"
	"nratio"
	"delta"
	"m1"
	"m2"
	"sd"
	"sd1"
	"sd2"
	"diff"
	"target"
END

LIST twocorr_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Total number of subjects"
	"Number of subjects in the control group"
	"Number of subjects in the experimental group"
	"Ratio of sample sizes, experimental to control"
	"Effect size"
	"Control-group correlation"
	"Experimental-group correlation"
	"Difference between the experimental-group and control-group correlations"
	"Target parameter"
END

LIST twocorr_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"N1"
	"N2"
	"nratio"
	"delta"
	"r1"
	"r2"
	"diff"
	"target"
END

LIST twovar_var_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Total number of subjects"
	"Number of subjects in the control group"
	"Number of subjects in the experimental group"
	"Ratio of sample sizes, experimental to control"
	"Effect size"
	"Control-group variance"
	"Experimental-group variance"
	"Ratio of the experimental-group variance to the control-group variance"
	"Target parameter"
END

LIST twovar_var_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"N1"
	"N2"
	"nratio"
	"delta"
	"v1"
	"v2"
	"ratio"
	"target"
END

LIST twovar_sd_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Total number of subjects"
	"Number of subjects in the control group"
	"Number of subjects in the experimental group"
	"Ratio of sample sizes, experimental to control"
	"Effect size"
	"Control-group standard deviation"
	"Experimental-group standard deviation"
	"Ratio of the experimental-group standard deviation to the control-group standard deviation"
	"Target parameter"
END

LIST twovar_sd_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"N1"
	"N2"
	"nratio"
	"delta"
	"s1"
	"s2"
	"ratio"
	"target"
END

LIST twoprop_chi_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Total number of subjects"
	"Number of subjects in the control group"
	"Number of subjects in the experimental group"
	"Ratio of sample sizes, experimental to control"
	"Effect size"
	"Control-group proportion"
	"Experimental-group proportion"
	"Difference between the experimental-group and control-group proportions"
	"Risk difference"
	"Ratio of the experimental-group proportion to the control-group proportion"
	"Relative risk"
	"Odds ratio"
	"Target parameter"
END

LIST twoprop_chi_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"N1"
	"N2"
	"nratio"
	"delta"
	"p1"
	"p2"
	"diff"
	"rdiff"
	"ratio"
	"rrisk"
	"oratio"
	"target"
END

LIST twoprop_fisher_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Total number of subjects"
	"Number of subjects in the control group"
	"Number of subjects in the experimental group"
	"Ratio of sample sizes, experimental to control"
	"Effect size"
	"Control-group proportion"
	"Experimental-group proportion"
	"Difference between the experimental-group and control-group proportions"
	"Risk difference"
	"Ratio of the experimental-group proportion to the control-group proportion"
	"Relative risk"
	"Odds ratio"
	"Observed significance level"
	"Target parameter"
END

LIST twoprop_fisher_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"N1"
	"N2"
	"nratio"
	"delta"
	"p1"
	"p2"
	"diff"
	"rdiff"
	"ratio"
	"rrisk"
	"oratio"
	"alpha_a"
	"target"
END

LIST pairedprop_discord_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Success-failure proportion"
	"Failure-success proportion"
	"Difference between discordant proportions"
	"Ratio of discordant proportions"
	"Proportion of discordant pairs"
	"Sum of discordant proportions"
	"Target parameter"
END

LIST pairedprop_discord_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"p12"
	"p21"
	"diff"
	"ratio"
	"prdiscordant"
	"sum"
	"target"
END

LIST pairedprop_marg_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Success proportion at occasion 1"
	"Success proportion at occasion 2"
	"Correlation between paired observations"
	"Difference between marginal proportions"
	"Ratio of marginal proportions"
	"Relative risk for marginal proportions"
	"Odds ratio for marginal proportions"
	"Target parameter"
END

LIST pairedprop_marg_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"pmarg1"
	"pmarg2"
	"corr"
	"diff"
	"ratio"
	"rrisk"
	"oratio"
	"target"
END

LIST pairedmeans_sddiff_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null mean difference"
	"Alternative mean difference"
	"Standard deviation of the differences"
	"Alternative pretreatment mean"
	"Alternative posttreatment mean"
	"Population size or sampling rate"
	"Target parameter"
END

LIST pairedmeans_sddiff_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"d0"
	"da"
	"sd_d"
	"ma1"
	"ma2"
	"fpc"
	"target"
END

LIST pairedmeans_corr_dimlist
BEGIN
	"Significance level"
	"Power"
	"Type-II-error probability"
	"Number of subjects"
	"Effect size"
	"Null mean difference"
	"Alternative mean difference"
	"Standard deviation of the differences"
	"Alternative pretreatment mean"
	"Alternative posttreatment mean"
	"Common standard deviation"
	"Standard deviation of the pretreatment group"
	"Standard deviation of the posttreatment group"
	"Correlation between paired observations"
	"Population size or sampling rate"
	"Target parameter"
END

LIST pairedmeans_corr_dimvalues
BEGIN
	"alpha"
	"power"
	"beta"
	"N"
	"delta"
	"d0"
	"da"
	"sd_d"
	"ma1"
	"ma2"
	"sd"
	"sd1"
	"sd2"
	"corr"
	"fpc"
	"target"
END













