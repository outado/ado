*! version 1.0.0  25oct2013

/* parse power options -grweights()- -npergroup()- -n()- -n#()-		*/
program define _pss_chk_multisample, rclass
	args nlev minlev levtype solvefor colon options

	local 0, `options'
	syntax, [ n(string) GRWeights(string) NPERGroup(string) NFRACtional * ]

	local 0, `options'
	if `nlev' {
		forvalues i=1/`nlev' {
			local nsyntax `"`nsyntax' n`i'(string)"'
		}
		syntax, [ `nsyntax' * ]
		local 0, `options'
	}
	else {
		local n0 = "x"
		while ("`n`nlev''"!="") {
			local nsyntax "n`++nlev'(string)"
			cap noi syntax, [ `nsyntax' * ]
			local 0, `options'
		}
		local nlev = `nlev'-1
		local n0
	}

	if ("`solvefor'"=="esize") local sflab effect size
	else local sflab `solvefor'

	local kn = 0
	forvalues i=1/`nlev' {
		if "`n`i''" != "" {
			local `++kn'
			local nlist `"`nlist' n`i'(`n`i'')"'
		}
	}
	if "`n'" != "" {
		if (`kn') local which {bf:n}{it:#}{bf:()} 	
		else if ("`npergroup'"!="") local which {bf:npergroup()}

		if "`which'" != "" {
			di as err "{p}options {bf:n()} and `which' cannot " ///
			 "be specified together{p_end}"
			exit 184
		}
		/* already checked in lower level parsing		*/
		numlist `"`n'"', range(>0) integer
		local which n
		local n `"`r(numlist)'"'
		local nlist n(`n')
	}
	if "`grweights'" != "" {
		if `kn' > 0 {
			di as err "{p}options {bf:n}{it:#}{bf:()} and " ///
			 "{bf:grweights()} cannot be specified together{p_end}"
			exit 184
		}
		if "`npergroup'" != "" {
			di as err "{p}options {bf:npergroup()} and " ///
			 "{bf:grweights()} cannot be specified together{p_end}"
			exit 184
		}
		/* grweights(multiple_lists)			*/
		if ("`nfractional'"=="") local integer integer

		local nlev0 = `nlev'
		/* set the minimum number of levels			*/
		if (!`nlev0') local nlev0 = -`minlev'

                _pss_chk_multilist `grweights', option({bf:grweights()}) ///
			levels(`levtype') nlevels(`nlev0') range(>0) `integer'

		if (!`nlev') local nlev = `s(nlevels)'

                forvalues i=1/`nlev' {
       	                local grwgh `"grwgt`i'(`s(numlist`i')')"'
       	                local gwlist `"`gwlist' `grwgh'"'
               	}
		local nlist `"`nlist' `gwlist'"'
		local nlist : list retokenize nlist
		local which grweights
	}
	else if "`npergroup'" != "" {
		if `kn' {
			di as err "{p}options {bf:npergroup()} and "        ///
			 "{bf:n}{it:#}{bf:()} cannot be specified together" ///
			 "{p_end}"
			exit 184
		}
		cap numlist `"`npergroup'"', range(>0) integer
		local rc = c(rc)
		if `rc' {
			di as err "{p}invalid {bf:npergroup()}: a " ///
			 "{help numlist} of positive integers is "  ///
			 "required {p_end}"
			exit `rc'
		}
		local npergroup `"`r(numlist)'"'
		local nlist npergroup(`npergroup')
		local which npergr
	}
	else if `kn' {
		local nlist
		forvalues i=1/`nlev' {
			CheckNi, i(`i') nlev(`nlev') ni(`n`i'')
			local n`i' `"`r(numlist)'"'
			local nlist `"`nlist' n`i'(`n`i'')"'
		}
		local nlist : list retokenize nlist
		local which n#
	}
	else if "`solvefor'"!="n" & "`n'"=="" {
		di as err "{p}one of {bf:n()} with, optionally, "         ///
		 "{bf:grweights()}; {bf:npergroup()}; or "                ///
		 "{bf:n}{it:#}{bf:()}, {it:#}=1,...,`nlev', is required " ///
		 "when solving for `sflab'{p_end}"
		exit 198
	}
	return local which `which'
	return local options `"`options'"'
	return local nlist `"`nlist'"'
	return local nlevels = `nlev'
end

program define CheckNi, rclass
	syntax, i(integer) nlev(integer) [ ni(string) ]

	if "`ni'" == "" {
		di as err "{p}option {bf:n`i'()} is missing;{p_end}"
		di as err "{p 4 4 2}Sample size is specified using either " ///
		 "option {bf:n()} with, optionally, {bf:grweights()}; "    ///
		 "{bf:npergroup()}; or options {bf:n}{it:#}{bf:()} for "   ///
		 "{it:#}=1,...,`nlev'.{p_end}"
		exit 198
	}
	cap numlist `"`ni'"', range(>0) integer
	local rc = c(rc)
	if `rc' {
		di as err "{p}invalid {bf:n`i'()}: a {help numlist} of " ///
		 "positive integers is required{p_end}"
		exit `rc'
	}
	return add
end
exit
