*! version 1.0.2  07may2014

program _teffects_parse_dvarlist, sclass
	version 13
	cap noi syntax varlist(numeric fv), touse(varname) [ wtype(string) ///
		wvar(string) noCONstant linear logit probit                ///
		hetprobit(string) poisson rmcoll noMARKout ]
	/* assumption: -marksample touse- by calling program		*/
	local rc = c(rc)
	if `rc' {
		di as txt "{phang}The outcome model is misspecified.{p_end}"
		exit `rc'
	}
	if "`wtype'" != "" {
		local wts [`wtype'=`wvar']
		if ("`wtype'"=="fweight") local freq `wvar'
	}
	if "`markout'" == "" {
		markout `touse' `varlist'
		_teffects_count_obs `touse', freq(`freq') ///
			why(observations with missing values)
	}

	gettoken depvar varlist : varlist, bind
	_fv_check_depvar `depvar'

	if "`varlist'"=="" & "`constant'"!="" {
		di as err "{p}The outcome model is misspecified; there " ///
		 "must be at least a constant term in the model{p_end}"
		exit 100
	}

	/* check for collinearity among variables 			*/
	if ("`rmcoll'"=="") local rmcoll _rmdcoll `depvar'
	else local rmcoll _rmcoll

	`rmcoll' `varlist' if `touse' `wts', `constant' 
	local k_omitted = r(k_omitted)
	if (`k_omitted'!=0) local varlist `r(varlist)'

	fvrevar `varlist', list
	local dvarlist `r(varlist)'
	local dvarlist : list uniq dvarlist

	fvexpand `varlist' if `touse'
	local rc = c(rc)
	if `rc' {
		di as err "{p}unable to expand factor variables in " ///
		 "outcome-model varlist{p_end}"
		exit `rc'
	}
	local fvops `r(fvops)'
	local fvdvarlist `r(varlist)'
	if ("`fvops'"=="") local fvops false

	if `"`hetprobit'"' != "" {
		local hetprobit `"hetprobit(`hetprobit')"'
	}
	ParseDModel, `linear' `logit' `probit' `hetprobit' `poisson'

	local omodel `s(omodel)'
	if "`omodel'" == "hetprobit" {
		local hvarlist `s(hvarlist)'

		markout `touse' `hvarlist'

		_teffects_count_obs `touse', freq(`freq') ///
			why(observations with missing values)

	}
	if "`omodel'"=="logit" | "`omodel'"=="probit"| "`omodel'"=="hetprobit" {
		_teffects_validate_catvar `depvar', argname(outcome variable) ///
			touse(`touse') binary
		qui count if `depvar'==0 & `touse'
		local n0 = r(N)
		qui count if `depvar'==1 & `touse'
		local n1 = r(N)
		if !`n0' | !`n1' {
			if (!`n0') local extra "zeros"
			if !`n1' {
				if (!`n0') local extra `extra' or
				local extra `extra' ones
			}
			di as err "the outcome model is misspecified"
			di as txt "{phang}{bf:`depvar'} is not a "	///
			   "binary variable, as is required when "	///
			   "{bf:`omodel'} is the outcome model.{p_end}"
			exit 450
		}
	}
	else if "`omodel'" == "poisson" {
		qui count if `depvar'<0 & `touse'
		if r(N) > 0 {
			di as err "{p}Poisson data must be nonnegative; " ///
			 "outcome `depvar' is not nonnegative{p_end}"
			exit 459
		}
	}
	sreturn local omodel `omodel'
	sreturn local k_omitted = `k_omitted'
	sreturn local fvdvarlist `fvdvarlist'
	sreturn local kfv : word count `fvdvarlist'
	sreturn local dvarlist `dvarlist'
	sreturn local k : word count `dvarlist'
	sreturn local depvar `depvar'
	sreturn local fvops `fvops'
	sreturn local constant `constant'
	if "`omodel'" == "hetprobit" {
		sreturn local hvarlist `hvarlist'
	}
end

program define ParseDModel, sclass
	cap noi syntax, [ linear logit probit hetprobit(string) ///
		poisson ]
	local rc = c(rc)
	if `rc' {
		di as txt "{phang}The outcome model is misspecified.{p_end}"
		exit `rc'
	}
	local k : word count `linear' `logit' `probit' `poisson' 
	if `k'>1 | ("`hetprobit'"!="" & `k'==1) {
		di as err "{p}The outcome model is misspecified{p_end}"
		di as txt "{p}You can only specify " ///
		 "one of {bf:linear}, {bf:logit}, {bf:probit}, "        ///
		 "{bf:poisson}, or {bf:hetprobit()}.{p_end}"
		exit 184
	}
	if "`hetprobit'"!="" {
		local omodel hetprobit

		ParseHetprob `hetprobit'
	}
	else if (!`k') local omodel linear
	else local omodel `linear'`logit'`probit'`poisson'

	sreturn local omodel `omodel'
end

program define ParseHetprob, sclass
	cap noi syntax varlist(numeric fv)
	local rc = c(rc)
	if `rc' {
		di as txt "{phang}The outcome model is misspecified; "	///
			"the option {bf:hetprobit()} is misspecified.{p_end}"
		exit `rc'
	}
	sreturn local hvarlist `varlist'
end

exit

