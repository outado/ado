*! version 1.0.4  10apr2013
program define _xt_robust, eclass

	version 13
	
	local 0 : subinstr local 0 "[]" "", all
	
	syntax [anything] [if] [in] , 	///
		relevel(varname) 	/// intercept and discard
		touse(string)		///
		[			///
		vce(passthru) 		///
		quad(string)		/// change to intpoints()
		iter(string)		///
		from(string) CONSTraints(string) RE noSKIP I(varname) ///
		noREFINE Level(passthru) noDISplay DOOPT call(string) ///
		GAUSSian NORMAL IRr EForm noCONstant OFFset(passthru) ///
		EXPosure(passthru) * ]
	
	gettoken cmd anything : anything
	local f_eq `anything' if `touse', `constant' `offset' `exposure'
	
	local coef lnsig2u:_cons
	
	tempname hold b V C J V_modelbased
	
	di
	di "{txt}Calculating robust standard errors:"
	
	mat `b' = e(b)
	mat `V_modelbased' = e(V)
	
	if "`e(Cns)'"=="matrix" {
		matrix `C' = e(Cns)
		local j = colsof(`C')
		forvalues i=1/`=rowsof(`C')' {
			if `C'[`i',`j'-1] != 0 {
				mat `C'[`i',`j'] = exp(`C'[`i',`j'])
			}
		}
		local cns constraints(`C')
	}
	
	_est hold `hold', nullok restore
	
	local NAMES : colfullnames `b'
	local NAMES_O `NAMES'
	local k = colsof(`b')
	mat `b'[1,`k'] = exp(`b'[1,`k'])
	local NAMES: subinstr local NAMES "`coef'" "var(_cons[`relevel']):_cons"
	mat colnames `b' = `NAMES'
	
	local opts intp(`quad') from(`b') iter(0) startv(zero) `options'
	
	capture me`cmd' `f_eq' || `relevel':, `vce' `cns' `opts'
	if _rc {
		di "{err}calculation of robust standard errors failed"
		exit 198
	}
	
	mat `V' = e(V)
	mat `J' = I(`k')
	mat `J'[`k',`k'] = 1/_b[var(_cons[`relevel']):_cons]
	
	mat `V' = `J'*`V'*`J'
	
	mat colnames `V' = `NAMES_O'
	mat rownames `V' = `NAMES_O'
	
	_est unhold `hold'
	
	ereturn repost V=`V'
	ereturn matrix V_modelbased = `V_modelbased'
	ereturn local vce "robust"
	ereturn local vcetype "Robust"
	
end
exit

