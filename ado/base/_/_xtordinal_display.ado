*! version 1.0.2  14may2013
program define _xtordinal_display

	syntax [, Level(cilevel) OR noLRtest noDISplay *]
        
        if "`or'" != "" local earg "eform(Odds Ratio)" 
	_get_diopts diopts, `options'
	
	if "`display'"=="" {
		_crcphdr
		_coef_table, `earg' level(`level') `diopts' cmdextras
		if "`lrtest'"=="" {
			_LR_test
		}
	}
	
end

program _LR_test, sclass
	
	// skip LR test if certain conditions are met
	if !e(k_r) | "`e(vce)'"!="oim" | e(has_cns) | !e(estimates) {
		exit
	}
	if !e(converged) {
		exit
	}
	
	// We have already established that e(chi2_c) exists
	if ((e(chi2_c) > 0.005) & (e(chi2_c)<1e5)) | (e(chi2_c)==0) {
  		local fmt "%8.2f"
        }
        else    local fmt "%8.2e"

	di "{txt}LR test vs. `e(model)' regression:" _c
	if `e(df_c)' == 1 {    		// chibar2(01)
		di _col(34) "{txt}{help j_chibar##|_new:chibar2(01) =}{res}" ///
                   _col(48) `fmt' e(chi2_c) ///
		   _col(57) "{txt}Prob>=chibar2 = {res}" ///
		   _col(73) %6.4f e(p_c)
	}
	else {
		local k = length("`e(df_c)'")
		di _col(`=39-`k'') "{txt}chi2({res}`e(df_c)'{txt}) ={res}" ///
		   _col(48) `fmt' e(chi2_c) ///
		   _col(59) "{txt}Prob > chi2 ={res}" ///
		   _col(73) %6.4f e(p_c)
	}
end

program define _display_LR
	if "`e(ll_c)'" != "" & "`e(vce)'" != "robust" {
		tempname pval
                scalar `pval' =  chiprob(1, e(chi2_c))*0.5
                if e(chi2_c)==0 scalar `pval'= 1
		if ((e(chi2_c) > 0.005) & (e(chi2_c)<1e4)) | (e(chi2_c)==0) {
                        local fmt "%8.2f"
                }
                else    local fmt "%8.2e"
                di in green "Likelihood-ratio test of rho=0: " _c
                di in green in smcl "{help j_chibar##|_new:chibar2(01) = }" /*
                        */ in ye `fmt' e(chi2_c) _c
                di in green " Prob >= chibar2 = " in ye %5.3f /*
                        */ `pval'
	}

	if e(N_cd) < . {
		if e(N_cd) > 1 { local s "s" }
		di in gr _n /*
		*/ "Note: `e(N_cd)' completely determined panel`s'"
	}
end
exit

