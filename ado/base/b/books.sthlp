{smcl}
{* *! version 1.0.1  05apr2013}{...}
{findalias asfrbooks}{...}
{title:Title}

{title:{findalias frbooks}}


{title:{findalias frbookreaders}}

{pstd}
There are books published about Stata, both by us and by others.  Visit the
Stata bookstore at
{browse "http://www.stata.com/bookstore/"}.  For the books that we
carry, we include the table of contents and comments written by a member of
our technical staff, explaining why we think this book might interest you.


{title:{findalias frbookauthors}}

{pstd}
If you have written a book related to Stata and would like us to consider
carrying it in our bookstore, email
{browse "mailto:bookstore@stata.com":bookstore@stata.com}.

{pstd}
If you are writing a book, join our free Author Support
Program.  Stata professionals are available to review your Stata code to
ensure that it is efficient and reflects modern usage, production specialists
are available to help format Stata output, and editors and statisticians are
available to ensure the accuracy of Stata-related content.  Visit
{browse "http://www.stata.com/authorsupport/"}.

{pstd}
If you are thinking about writing a Stata-related book, consider 
publishing it with Stata Press.  Email
{browse "mailto:submissions@stata-press.com":submissions@stata-press.com}.
{p_end}
