{smcl}
{* *! version 1.0.1  19dec2012}{...}
{vieweralsosee "[R] cls" "mansection R cls"}{...}
{viewerjumpto "Syntax" "cls##syntax"}{...}
{viewerjumpto "Description" "cls##description"}{...}
{title:Title}

{p2colset 5 16 18 2}{...}
{p2col :{manlink R cls} {hline 2}}Clear Results window{p_end}
{p2colreset}{...}


{marker syntax}{...}
{title:Syntax}

{p 8 13 2}
{opt cls}


{marker description}{...}
{title:Description}

{pstd}
{cmd:cls} clears the Results window, causing all text to be removed.
This operation cannot be undone.

