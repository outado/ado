{smcl}
{p 0 4}
{help contents:Top}
> {help contents_data:Data manipulation and management}
{bind:> {bf:Basic data commands}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help codebook:codebook - Describe data contents}}

{p 4 8 4}
{bf:{help data_management:data management - Introduction to data management commands}}

{p 4 8 4}
{bf:{help data_types:data types - Quick reference for data types}}

{p 4 8 4}
{bf:{help datetime:datetime - Date and time values and variables}}

{p 4 8 4}
{bf:{help describe:describe - Describe data in memory or in file}}

{p 4 8 4}
{bf:{help edit:edit - Browse or edit data with Data Editor}}

{p 4 8 4}
{bf:{help format:format - Set variables' output format}}

{p 4 8 4}
{bf:{help inspect:inspect - Display simple summary of data's attributes}}

{p 4 8 4}
{bf:{help label:label - Manipulate labels}}

{p 4 8 4}
{bf:{help list:list - List values of variables}}

{p 4 8 4}
{bf:{help missing:missing values - Quick reference for missing values}}

{p 4 8 4}
{bf:{help rename:rename - Rename variable}}

{p 4 8 4}
{bf:{help save:save - Save Stata dataset}}

{p 4 8 4}
{bf:{help sort:sort - Sort data}}

{p 4 8 4}
{bf:{help use:use - Load Stata dataset}}

{p 4 8 4}
{bf:{help varmanage:varmanage - Manage variable labels, formats, and other properties}}

{hline}
