{smcl}
{p 0 4}
{help contents:Top}
> {help contents_data:Data manipulation and management}
{bind:> {bf:Dates and times}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection U 12.5.3Dateandtimeformats:[U] 12.5.3 Date and time formats (pdf)}}

{p 4 8 4}
{bf:{mansection U 24Workingwithdatesandtimes:[U] 24 Working with dates and times (pdf)}}

{p 4 8 4}
{bf:{help bcal:bcal - Business calendar file manipulation}}

{p 4 8 4}
{bf:{help datetime:datetime - Date and time values and variables}}

{p 4 8 4}
{bf:{help datetime_business_calendars:datetime business calendars - Business calendars}}

{p 4 8 4}
{bf:{help datetime_business_calendars_creation:datetime business calendars creation - Business calendars creation}}

{p 4 8 4}
{bf:{help datetime_display_formats:datetime display formats - Display formats for dates and times}}

{p 4 8 4}
{bf:{help datetime_translation:datetime translation - String to numeric date translation functions}}

{hline}
