{smcl}
{p 0 4}
{help contents:Top}
> {help contents_data:Data manipulation and management}
{bind:> {bf:File manipulation}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help cd:cd - Change directory}}

{p 4 8 4}
{bf:{help cf:cf - Compare two datasets}}

{p 4 8 4}
{bf:{help changeeol:changeeol - Convert end-of-line characters of text file}}

{p 4 8 4}
{bf:{help checksum:checksum - Calculate checksum of file}}

{p 4 8 4}
{bf:{help copy:copy - Copy file from disk or URL}}

{p 4 8 4}
{bf:{help dir:dir - Display filenames}}

{p 4 8 4}
{bf:{help erase:erase - Erase a disk file}}

{p 4 8 4}
{bf:{help filefilter:filefilter - Convert text or binary patterns in a file}}

{p 4 8 4}
{bf:{help mkdir:mkdir - Create directory}}

{p 4 8 4}
{bf:{help rmdir:rmdir - Remove directory}}

{p 4 8 4}
{bf:{help type:type - Display contents of a file}}

{p 4 8 4}
{bf:{help zipfile:zipfile - Compress and uncompress files and directories in zip archive format}}

{hline}
