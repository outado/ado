{smcl}
{p 0 4}
{help contents:Top}
> {help contents_data:Data manipulation and management}
{bind:> {bf:Loading, saving, importing, and exporting data}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection GSM 6UsingtheDataEditor:[GSM] 6 Using the Data Editor (pdf)}}

{p 4 8 4}
{bf:{mansection GSU 6UsingtheDataEditor:[GSU] 6 Using the Data Editor (pdf)}}

{p 4 8 4}
{bf:{mansection GSW 6UsingtheDataEditor:[GSW] 6 Using the Data Editor (pdf)}}

{p 4 8 4}
{bf:{mansection U 21Enteringandimportingdata:[U] 21 Entering and importing data (pdf)}}

{p 4 8 4}
{bf:{help edit:edit - Browse or edit data with Data Editor}}

{p 4 8 4}
{bf:{help export:export - Overview of exporting data from Stata}}

{p 4 8 4}
{bf:{help import:import - Overview of importing data into Stata}}

{p 4 8 4}
{bf:{help import_delimited:import delimited - Import delimited text files}}

{p 4 8 4}
{bf:{help import_excel:import excel - Import and export Excel files}}

{p 4 8 4}
{bf:{help import_haver:import haver - Import data from Haver Analytics databases}}

{p 4 8 4}
{bf:{help import_sasxport:import sasxport - Import and export datasets in SAS XPORT format}}

{p 4 8 4}
{bf:{help infile1:infile (fixed format) - Read text data in fixed format with a dictionary}}

{p 4 8 4}
{bf:{help infile2:infile (free format) - Read unformatted text data}}

{p 4 8 4}
{bf:{help infix:infix (fixed format) - Read text data in fixed format}}

{p 4 8 4}
{bf:{help input:input - Enter data from keyboard}}

{p 4 8 4}
{bf:{help odbc:odbc - Load, write, or view data from ODBC sources}}

{p 4 8 4}
{bf:{help outfile:outfile - Export dataset in text format}}

{p 4 8 4}
{bf:{help putexcel:putexcel - Export results to an Excel file}}

{p 4 8 4}
{bf:{help save:save - Save Stata dataset}}

{p 4 8 4}
{bf:{help sysuse:sysuse - Use shipped dataset}}

{p 4 8 4}
{bf:{help use:use - Load Stata dataset}}

{p 4 8 4}
{bf:{help webuse:webuse - Use dataset from Stata website}}

{p 4 8 4}
{bf:{help xmlsave:xmlsave - Export or import dataset in XML format}}

{hline}
