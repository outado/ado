{smcl}
{p 0 4}
{help contents:Top}
> {help contents_data:Data manipulation and management}
{bind:> {bf:Miscellaneous data commands}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help corr2data:corr2data - Create dataset with specified correlation structure}}

{p 4 8 4}
{bf:{help drawnorm:drawnorm - Draw sample from multivariate normal distribution}}

{p 4 8 4}
{bf:{help dydx:dydx - Calculate numeric derivatives and integrals}}

{p 4 8 4}
{bf:{help icd9:icd9 - ICD-9-CM diagnostic and procedure codes}}

{p 4 8 4}
{bf:{help ipolate:ipolate - Linearly interpolate (extrapolate) values}}

{p 4 8 4}
{bf:{help range:range - Generate numerical range}}

{p 4 8 4}
{bf:{help sample:sample - Draw random sample}}

{hline}
