{smcl}
{p 0 4}
{help contents:Top}
> {help contents_data:Data manipulation and management}
{bind:> {bf:Creating and dropping variables}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help clear:clear - Clear memory}}

{p 4 8 4}
{bf:{help compress:compress - Compress data in memory}}

{p 4 8 4}
{bf:{help drop:drop - Drop variables or observations}}

{p 4 8 4}
{bf:{help egen:egen - Extensions to generate}}

{p 4 8 4}
{bf:{help functions:functions - Functions}}

{p 4 8 4}
{bf:{help generate:generate - Create or change contents of variable}}

{p 4 8 4}
{bf:{help orthog:orthog - Orthogonalize variables and compute orthogonal polynomials}}

{hline}
