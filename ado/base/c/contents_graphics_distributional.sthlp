{smcl}
{p 0 4}
{help contents:Top}
> {help contents_graphics:Graphics}
{bind:> {bf:Distributional graphs}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help cumul:cumul - Cumulative distribution}}

{p 4 8 4}
{bf:{help diagnostic_plots:diagnostic plots - Distributional diagnostic plots}}

{p 4 8 4}
{bf:{help ladder:ladder - Ladder of powers}}

{p 4 8 4}
{bf:{help spikeplot:spikeplot - Spike plots and rootograms}}

{hline}
