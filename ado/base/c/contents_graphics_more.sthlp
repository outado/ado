{smcl}
{p 0 4}
{help contents:Top}
> {help contents_graphics:Graphics}
{bind:> {bf:More statistical graphs}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help dotplot:dotplot - Comparative scatterplots}}

{p 4 8 4}
{bf:{help epitab:epitab - Tables for epidemiologists}}

{p 4 8 4}
{bf:{help fp_postestimation:fp postestimation - Postestimation tools for fp}}

{p 4 8 4}
{bf:{help grmeanby:grmeanby - Graph means and medians by categorical variables}}

{p 4 8 4}
{bf:{help pkexamine:pkexamine - Calculate pharmacokinetic measures}}

{p 4 8 4}
{bf:{help pksumm:pksumm - Summarize pharmacokinetic data}}

{p 4 8 4}
{bf:{help stem:stem - Stem-and-leaf displays}}

{p 4 8 4}
{bf:{help xtline:xtline - Panel-data line plots}}

{hline}
