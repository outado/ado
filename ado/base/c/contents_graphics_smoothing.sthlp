{smcl}
{p 0 4}
{help contents:Top}
> {help contents_graphics:Graphics}
{bind:> {bf:Smoothing and densities}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help histogram:histogram - Histograms for continuous and categorical variables}}

{p 4 8 4}
{bf:{help kdensity:kdensity - Univariate kernel density estimation}}

{p 4 8 4}
{bf:{help lowess:lowess - Lowess smoothing}}

{p 4 8 4}
{bf:{help lpoly:lpoly - Kernel-weighted local polynomial smoothing}}

{p 4 8 4}
{bf:{help sunflower:sunflower - Density-distribution sunflower plots}}

{hline}
