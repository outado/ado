{smcl}
{p 0 4}
{help contents:Top}
> {help contents_program:Programming}
{bind:> {bf:Advanced programming commands}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help mf__docx:_docx*() - Generate Office Open XML (.docx) file}}

{p 4 8 4}
{bf:{help automation:automation - Automation}}

{p 4 8 4}
{bf:{help break:break - Suppress Break key}}

{p 4 8 4}
{bf:{help char:char - Characteristics}}

{p 4 8 4}
{bf:{help m2_class:class - Object-oriented programming (classes)}}

{p 4 8 4}
{bf:{help class:class - Class programming}}

{p 4 8 4}
{bf:{help class_exit:class exit - Exit class-member program and return result}}

{p 4 8 4}
{bf:{help classutil:classutil - Class programming utility}}

{p 4 8 4}
{bf:{help estat_programming:estat programming - Controlling estat after user-written commands}}

{p 4 8 4}
{bf:{help _estimates:_estimates - Manage estimation results}}

{p 4 8 4}
{bf:{help file:file - Read and write ASCII text and binary files}}

{p 4 8 4}
{bf:{help findfile:findfile - Find file in path}}

{p 4 8 4}
{bf:{help include:include - Include commands from file}}

{p 4 8 4}
{bf:{help java:java - Java plugins}}

{p 4 8 4}
{bf:{help javacall:javacall - Call a static Java method}}

{p 4 8 4}
{bf:{help macro:macro - Macro definition and manipulation}}

{p 4 8 4}
{bf:{help macrolists:macro lists - Manipulate lists}}

{p 4 8 4}
{bf:{help ml:ml - Maximum likelihood estimation}}

{p 4 8 4}
{bf:{help mf_moptimize:moptimize() - Model optimization}}

{p 4 8 4}
{bf:{help mf_optimize:optimize() - Function optimization}}

{p 4 8 4}
{bf:{help plugin:plugin - Load a plugin}}

{p 4 8 4}
{bf:{help post:postfile - Post results in Stata dataset}}

{p 4 8 4}
{bf:{help _predict:_predict - Obtain predictions, residuals, etc., after estimation programming command}}

{p 4 8 4}
{bf:{help program_properties:program properties - Properties of user-defined programs}}

{p 4 8 4}
{bf:{help putexcel:putexcel - Export results to an Excel file}}

{p 4 8 4}
{bf:{help putmata:putmata - Put Stata variables into Mata and vice versa}}

{p 4 8 4}
{bf:{help _return:_return - Preserve stored results}}

{p 4 8 4}
{bf:{help _rmcoll:_rmcoll - Remove collinear variables}}

{p 4 8 4}
{bf:{help _robust:_robust - Robust variance estimates}}

{p 4 8 4}
{bf:{help serset:serset - Create and manipulate sersets}}

{p 4 8 4}
{bf:{help snapshot:snapshot - Save and restore data snapshots}}

{p 4 8 4}
{bf:{help unab:unab - Unabbreviate variable list}}

{p 4 8 4}
{bf:{help unabcmd:unabcmd - Unabbreviate command name}}

{p 4 8 4}
{bf:{help set_varabbrev:varabbrev - Control variable abbreviation}}

{p 4 8 4}
{bf:{help viewsource:viewsource - View source code}}

{p 4 8 4}
{bf:{help mf_xl:xl() - Excel file I/O class}}

{hline}
