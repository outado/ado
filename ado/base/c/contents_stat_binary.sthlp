{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Binary outcomes}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help estcom:[U] 20 Estimation and postestimation commands}}

{p 4 8 4}
{bf:{mansection U 26.7Binary-outcomequalitativedependent-variablemodels:[U] 26.7 Binary-outcome qualitative dependent-variable models (pdf)}}

{p 4 8 4}
{bf:{help binreg:binreg - Generalized linear models: Extensions to the binomial family}}

{p 4 8 4}
{bf:{help biprobit:biprobit - Bivariate probit regression}}

{p 4 8 4}
{bf:{help cloglog:cloglog - Complementary log-log regression}}

{p 4 8 4}
{bf:{help exlogistic:exlogistic - Exact logistic regression}}

{p 4 8 4}
{bf:{help glm:glm - Generalized linear models}}

{p 4 8 4}
{bf:{help glogit:glogit - Logit and probit regression for grouped data}}

{p 4 8 4}
{bf:{help heckoprobit:heckoprobit - Ordered probit model with sample selection}}

{p 4 8 4}
{bf:{help heckprobit:heckprobit - Probit model with sample selection}}

{p 4 8 4}
{bf:{help hetprobit:hetprobit - Heteroskedastic probit model}}

{p 4 8 4}
{bf:{help ivprobit:ivprobit - Probit model with continuous endogenous regressors}}

{p 4 8 4}
{bf:{help logistic:logistic - Logistic regression, reporting odds ratios}}

{p 4 8 4}
{bf:{help logit:logit - Logistic regression, reporting coefficients}}

{p 4 8 4}
{bf:{help mecloglog:mecloglog - Multilevel mixed-effects complementary log-log regression}}

{p 4 8 4}
{bf:{help melogit:melogit - Multilevel mixed-effects logistic regression}}

{p 4 8 4}
{bf:{help meprobit:meprobit - Multilevel mixed-effects probit regression}}

{p 4 8 4}
{bf:{help meqrlogit:meqrlogit - Multilevel mixed-effects logistic regression (QR decomposition)}}

{p 4 8 4}
{bf:{help probit:probit - Probit regression}}

{p 4 8 4}
{bf:{help rocfit:rocfit - Parametric ROC models}}

{p 4 8 4}
{bf:{help rocreg:rocreg - Receiver operating characteristic (ROC) regression}}

{p 4 8 4}
{bf:{help scobit:scobit - Skewed logistic regression}}

{p 4 8 4}
{bf:{help xtcloglog:xtcloglog - Random-effects and population-averaged cloglog models}}

{p 4 8 4}
{bf:{help xtlogit:xtlogit - Fixed-effects, random-effects, and population-averaged logit models}}

{p 4 8 4}
{bf:{help xtprobit:xtprobit - Random-effects and population-averaged probit models}}

{hline}
