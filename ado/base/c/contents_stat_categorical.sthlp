{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Categorical outcomes}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help estcom:[U] 20 Estimation and postestimation commands}}

{p 4 8 4}
{bf:{mansection U 26.10Multiple-outcomequalitativedependent-variablemodels:[U] 26.10 Multiple-outcome qualitative dependent-variable models (pdf)}}

{p 4 8 4}
{bf:{help asclogit:asclogit - Alternative-specific conditional logit (McFadden's choice) model}}

{p 4 8 4}
{bf:{help asmprobit:asmprobit - Alternative-specific multinomial probit regression}}

{p 4 8 4}
{bf:{help clogit:clogit - Conditional (fixed-effects) logistic regression}}

{p 4 8 4}
{bf:{help mlogit:mlogit - Multinomial (polytomous) logistic regression}}

{p 4 8 4}
{bf:{help mprobit:mprobit - Multinomial probit regression}}

{p 4 8 4}
{bf:{help nlogit:nlogit - Nested logit regression}}

{p 4 8 4}
{bf:{help slogit:slogit - Stereotype logistic regression}}

{hline}
