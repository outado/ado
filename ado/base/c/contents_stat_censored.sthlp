{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Censored and truncated regression models}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help heckman:heckman - Heckman selection model}}

{p 4 8 4}
{bf:{help heckoprobit:heckoprobit - Ordered probit model with sample selection}}

{p 4 8 4}
{bf:{help heckprobit:heckprobit - Probit model with sample selection}}

{p 4 8 4}
{bf:{help intreg:intreg - Interval regression}}

{p 4 8 4}
{bf:{help tnbreg:tnbreg - Truncated negative binomial regression}}

{p 4 8 4}
{bf:{help tobit:tobit - Tobit regression}}

{p 4 8 4}
{bf:{help tpoisson:tpoisson - Truncated Poisson regression}}

{p 4 8 4}
{bf:{help truncreg:truncreg - Truncated regression}}

{p 4 8 4}
{bf:{help xtintreg:xtintreg - Random-effects interval-data regression models}}

{p 4 8 4}
{bf:{help xttobit:xttobit - Random-effects tobit models}}

{hline}
