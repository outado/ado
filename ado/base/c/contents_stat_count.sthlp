{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Count outcomes}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help estcom:[U] 20 Estimation and postestimation commands}}

{p 4 8 4}
{bf:{mansection U 26.11Countdependent-variablemodels:[U] 26.11 Count dependent-variable models (pdf)}}

{p 4 8 4}
{bf:{mansection U 26.18.5Countdependent-variablemodelswithpaneldata:[U] 26.18.5 Count dependent-variable models with panel data (pdf)}}

{p 4 8 4}
{bf:{help etpoisson:etpoisson - Poisson regression with endogenous treatment effects}}

{p 4 8 4}
{bf:{help expoisson:expoisson - Exact Poisson regression}}

{p 4 8 4}
{bf:{help menbreg:menbreg - Multilevel mixed-effects negative binomial regression}}

{p 4 8 4}
{bf:{help mepoisson:mepoisson - Multilevel mixed-effects Poisson regression}}

{p 4 8 4}
{bf:{help meqrpoisson:meqrpoisson - Multilevel mixed-effects Poisson regression (QR decomposition)}}

{p 4 8 4}
{bf:{help nbreg:nbreg - Negative binomial regression}}

{p 4 8 4}
{bf:{help poisson:poisson - Poisson regression}}

{p 4 8 4}
{bf:{help tnbreg:tnbreg - Truncated negative binomial regression}}

{p 4 8 4}
{bf:{help tpoisson:tpoisson - Truncated Poisson regression}}

{p 4 8 4}
{bf:{help xtnbreg:xtnbreg - Fixed-effects, random-effects, & population-averaged negative binomial models}}

{p 4 8 4}
{bf:{help xtpoisson:xtpoisson - Fixed-effects, random-effects, and population-averaged Poisson models}}

{p 4 8 4}
{bf:{help zinb:zinb - Zero-inflated negative binomial regression}}

{p 4 8 4}
{bf:{help zip:zip - Zero-inflated Poisson regression}}

{hline}
