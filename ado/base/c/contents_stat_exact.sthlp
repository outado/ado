{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Exact statistics}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection U 26.12Exactestimators:[U] 26.12 Exact estimators (pdf)}}

{p 4 8 4}
{bf:{help bitest:bitest - Binomial probability test}}

{p 4 8 4}
{bf:{help centile:centile - Report centile and confidence interval}}

{p 4 8 4}
{bf:{help ci:ci - Confidence intervals for means, proportions, and counts}}

{p 4 8 4}
{bf:{help dstdize:dstdize - Direct and indirect standardization}}

{p 4 8 4}
{bf:{help epitab:epitab - Tables for epidemiologists}}

{p 4 8 4}
{bf:{help exlogistic:exlogistic - Exact logistic regression}}

{p 4 8 4}
{bf:{help expoisson:expoisson - Exact Poisson regression}}

{p 4 8 4}
{bf:{help ksmirnov:ksmirnov - Kolmogorov-Smirnov equality-of-distributions test}}

{p 4 8 4}
{bf:{help loneway:loneway - Large one-way ANOVA, random effects, and reliability}}

{p 4 8 4}
{bf:{help ranksum:ranksum - Equality tests on unmatched data}}

{p 4 8 4}
{bf:{help roctab:roctab - Nonparametric ROC analysis}}

{p 4 8 4}
{bf:{help symmetry:symmetry - Symmetry and marginal homogeneity tests}}

{p 4 8 4}
{bf:{help tabulate_twoway:tabulate twoway - Two-way table of frequencies}}

{p 4 8 4}
{bf:{help tetrachoric:tetrachoric - Tetrachoric correlations for binary variables}}

{hline}
