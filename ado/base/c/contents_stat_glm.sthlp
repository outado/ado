{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Generalized linear models}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help estcom:[U] 20 Estimation and postestimation commands}}

{p 4 8 4}
{bf:{mansection U 26.6Generalizedlinearmodels:[U] 26.6 Generalized linear models (pdf)}}

{p 4 8 4}
{bf:{help binreg:binreg - Generalized linear models: Extensions to the binomial family}}

{p 4 8 4}
{bf:{help glm:glm - Generalized linear models}}

{p 4 8 4}
{bf:{help xtgee:xtgee - Fit population-averaged panel-data models by using GEE}}

{hline}
