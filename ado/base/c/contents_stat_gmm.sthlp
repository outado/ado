{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Do-it-yourself generalized method of moments}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection U 26.22Generalizedmethodofmoments(GMM):[U] 26.22 Generalized method of moments (GMM)}}

{p 4 8 4}
{bf:{help gmm:gmm - Generalized method of moments estimation}}

{p 4 8 4}
{bf:{help matrix:matrix - Introduction to matrix commands}}

{hline}
