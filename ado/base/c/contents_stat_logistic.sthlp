{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Logistic and probit regression}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help estcom:[U] 20 Estimation and postestimation commands}}

{p 4 8 4}
{bf:{mansection U 26OverviewofStataestimationcommands:[U] 26 Overview of Stata estimation commands (pdf)}}

{p 4 8 4}
{bf:{help asclogit:asclogit - Alternative-specific conditional logit (McFadden's choice) model}}

{p 4 8 4}
{bf:{help asmprobit:asmprobit - Alternative-specific multinomial probit regression}}

{p 4 8 4}
{bf:{help asroprobit:asroprobit - Alternative-specific rank-ordered probit regression}}

{p 4 8 4}
{bf:{help biprobit:biprobit - Bivariate probit regression}}

{p 4 8 4}
{bf:{help clogit:clogit - Conditional (fixed-effects) logistic regression}}

{p 4 8 4}
{bf:{help cloglog:cloglog - Complementary log-log regression}}

{p 4 8 4}
{bf:{help exlogistic:exlogistic - Exact logistic regression}}

{p 4 8 4}
{bf:{help glogit:glogit - Logit and probit regression for grouped data}}

{p 4 8 4}
{bf:{help heckoprobit:heckoprobit - Ordered probit model with sample selection}}

{p 4 8 4}
{bf:{help heckprobit:heckprobit - Probit model with sample selection}}

{p 4 8 4}
{bf:{help hetprobit:hetprobit - Heteroskedastic probit model}}

{p 4 8 4}
{bf:{help ivprobit:ivprobit - Probit model with continuous endogenous regressors}}

{p 4 8 4}
{bf:{help logistic:logistic - Logistic regression, reporting odds ratios}}

{p 4 8 4}
{bf:{help logit:logit - Logistic regression, reporting coefficients}}

{p 4 8 4}
{bf:{help melogit:melogit - Multilevel mixed-effects logistic regression}}

{p 4 8 4}
{bf:{help meologit:meologit - Multilevel mixed-effects ordered logistic regression}}

{p 4 8 4}
{bf:{help meoprobit:meoprobit - Multilevel mixed-effects ordered probit regression}}

{p 4 8 4}
{bf:{help meprobit:meprobit - Multilevel mixed-effects probit regression}}

{p 4 8 4}
{bf:{help meqrlogit:meqrlogit - Multilevel mixed-effects logistic regression (QR decomposition)}}

{p 4 8 4}
{bf:{help mlogit:mlogit - Multinomial (polytomous) logistic regression}}

{p 4 8 4}
{bf:{help mprobit:mprobit - Multinomial probit regression}}

{p 4 8 4}
{bf:{help nlogit:nlogit - Nested logit regression}}

{p 4 8 4}
{bf:{help ologit:ologit - Ordered logistic regression}}

{p 4 8 4}
{bf:{help oprobit:oprobit - Ordered probit regression}}

{p 4 8 4}
{bf:{help probit:probit - Probit regression}}

{p 4 8 4}
{bf:{help rologit:rologit - Rank-ordered logistic regression}}

{p 4 8 4}
{bf:{help scobit:scobit - Skewed logistic regression}}

{p 4 8 4}
{bf:{help slogit:slogit - Stereotype logistic regression}}

{p 4 8 4}
{bf:{help xtcloglog:xtcloglog - Random-effects and population-averaged cloglog models}}

{p 4 8 4}
{bf:{help xtgee:xtgee - Fit population-averaged panel-data models by using GEE}}

{p 4 8 4}
{bf:{help xtlogit:xtlogit - Fixed-effects, random-effects, and population-averaged logit models}}

{p 4 8 4}
{bf:{help xtologit:xtologit - Random-effects ordered logistic models}}

{p 4 8 4}
{bf:{help xtoprobit:xtoprobit - Random-effects ordered probit models}}

{p 4 8 4}
{bf:{help xtprobit:xtprobit - Random-effects and population-averaged probit models}}

{hline}
