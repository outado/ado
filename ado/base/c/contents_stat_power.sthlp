{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Power and sample size}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection U 26.29Powerandsample-sizeanalysis:[U] 26.29 Power and sample-size analysis (pdf)}}

{p 4 8 4}
{bf:{help pss_gui:GUI - Graphical user interface for power and sample-size analysis}}

{p 4 8 4}
{bf:{help power:power - Power and sample-size analysis for hypothesis tests}}

{p 4 8 4}
{bf:{help power_onecorrelation:power onecorrelation - Power analysis for a one-sample correlation test}}

{p 4 8 4}
{bf:{help power_onemean:power onemean - Power analysis for a one-sample mean test}}

{p 4 8 4}
{bf:{help power_oneproportion:power oneproportion - Power analysis for a one-sample proportion test}}

{p 4 8 4}
{bf:{help power_onevariance:power onevariance - Power analysis for a one-sample variance test}}

{p 4 8 4}
{bf:{help power_oneway:power oneway - Power analysis for one-way analysis of variance}}

{p 4 8 4}
{bf:{help power_pairedmeans:power pairedmeans - Power analysis for a two-sample paired-means test}}

{p 4 8 4}
{bf:{help power_pairedproportions:power pairedproportions - Power analysis for a two-sample paired-proportions test}}

{p 4 8 4}
{bf:{help power_repeated:power repeated - Power analysis for repeated-measures analysis of variance}}

{p 4 8 4}
{bf:{help power_twocorrelations:power twocorrelations - Power analysis for a two-sample correlations test}}

{p 4 8 4}
{bf:{help power_twomeans:power twomeans - Power analysis for a two-sample means test}}

{p 4 8 4}
{bf:{help power_twoproportions:power twoproportions - Power analysis for a two-sample proportions test}}

{p 4 8 4}
{bf:{help power_twovariances:power twovariances - Power analysis for a two-sample variances test}}

{p 4 8 4}
{bf:{help power_twoway:power twoway - Power analysis for two-way analysis of variance}}

{p 4 8 4}
{bf:{help stpower:stpower - Sample size, power, and effect size for survival analysis}}

{p 4 8 4}
{bf:{help stpower_cox:stpower cox - Sample size, power, and effect size for the Cox proportional hazards model}}

{p 4 8 4}
{bf:{help stpower_exponential:stpower exponential - Sample size and power for the exponential test}}

{p 4 8 4}
{bf:{help stpower_logrank:stpower logrank - Sample size, power, and effect size for the log-rank test}}

{p 4 8 4}
{bf:{mansection PSS unbalanceddesigns:unbalanced designs - Specifications for unbalanced designs}}

{hline}
