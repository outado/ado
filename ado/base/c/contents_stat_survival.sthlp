{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Survival analysis}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help estcom:[U] 20 Estimation and postestimation commands}}

{p 4 8 4}
{bf:{mansection U 26.20Survival-time(failure-time)models:[U] 26.20 Survival-time (failure-time) models (pdf)}}

{p 4 8 4}
{bf:{mansection U 26.29Powerandsample-sizeanalysis:[U] 26.29 Power and sample-size analysis (pdf)}}

{p 4 8 4}
{bf:{help survival_analysis:survival analysis - Introduction to survival analysis & epidemiological tables commands}}

{p 4 8 4}
{bf:{help ct:ct - Count-time data}}

{p 4 8 4}
{bf:{help ctset:ctset - Declare data to be count-time data}}

{p 4 8 4}
{bf:{help cttost:cttost - Convert count-time data to survival-time data}}

{p 4 8 4}
{bf:{mansection ST discrete:discrete - Discrete-time survival analysis (pdf)}}

{p 4 8 4}
{bf:{help ltable:ltable - Life tables for survival data}}

{p 4 8 4}
{bf:{help snapspan:snapspan - Convert snapshot data to time-span data}}

{p 4 8 4}
{bf:{help st:st - Survival-time data}}

{p 4 8 4}
{bf:{help st_is:st_is - Survival analysis subroutines for programmers}}

{p 4 8 4}
{bf:{help stbase:stbase - Form baseline dataset}}

{p 4 8 4}
{bf:{help stci:stci - Confidence intervals for means and percentiles of survival time}}

{p 4 8 4}
{bf:{help stcox:stcox - Cox proportional hazards model}}

{p 4 8 4}
{bf:{help stcox_diagnostics:stcox PH-assumption tests - Tests of proportional-hazards assumption}}

{p 4 8 4}
{bf:{help stcrreg:stcrreg - Competing-risks regression}}

{p 4 8 4}
{bf:{help stcurve:stcurve - Plot survivor, hazard, cumulative hazard, or cumulative incidence function}}

{p 4 8 4}
{bf:{help stdescribe:stdescribe - Describe survival-time data}}

{p 4 8 4}
{bf:{help stepwise:stepwise - Stepwise estimation}}

{p 4 8 4}
{bf:{help stfill:stfill - Fill in by carrying forward values of covariates}}

{p 4 8 4}
{bf:{help stgen:stgen - Generate variables reflecting entire histories}}

{p 4 8 4}
{bf:{help stir:stir - Report incidence-rate comparison}}

{p 4 8 4}
{bf:{help stpower:stpower - Sample size, power, and effect size for survival analysis}}

{p 4 8 4}
{bf:{help stpower_cox:stpower cox - Sample size, power, and effect size for the Cox proportional hazards model}}

{p 4 8 4}
{bf:{help stpower_exponential:stpower exponential - Sample size and power for the exponential test}}

{p 4 8 4}
{bf:{help stpower_logrank:stpower logrank - Sample size, power, and effect size for the log-rank test}}

{p 4 8 4}
{bf:{help stptime:stptime - Calculate person-time, incidence rates, and SMR}}

{p 4 8 4}
{bf:{help strate:strate - Tabulate failure rates and rate ratios}}

{p 4 8 4}
{bf:{help streg:streg - Parametric survival models}}

{p 4 8 4}
{bf:{help sts:sts - Generate, graph, list, and test the survivor and cumulative hazard functions}}

{p 4 8 4}
{bf:{help sts_generate:sts generate - Create variables containing survivor and related functions}}

{p 4 8 4}
{bf:{help sts_graph:sts graph - Graph the survivor, hazard, or cumulative hazard function}}

{p 4 8 4}
{bf:{help sts_list:sts list - List the survivor or cumulative hazard function}}

{p 4 8 4}
{bf:{help sts_test:sts test - Test equality of survivor functions}}

{p 4 8 4}
{bf:{help stset:stset - Declare data to be survival-time data}}

{p 4 8 4}
{bf:{help mi_xxxset:mi XXXset - Declare mi data to be svy, st, ts, xt, etc.}}

{p 4 8 4}
{bf:{help stsplit:stsplit - Split and join time-span records}}

{p 4 8 4}
{bf:{help mi_stsplit:mi stsplit - Stsplit and stjoin mi data}}

{p 4 8 4}
{bf:{help stsum:stsum - Summarize survival-time data}}

{p 4 8 4}
{bf:{help sttocc:sttocc - Convert survival-time data to case-control data}}

{p 4 8 4}
{bf:{help sttoct:sttoct - Convert survival-time data to count-time data}}

{p 4 8 4}
{bf:{help stvary:stvary - Report variables that vary over time}}

{hline}
