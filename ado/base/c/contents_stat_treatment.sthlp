{smcl}
{p 0 4}
{help contents:Top}
> {help contents_stat:Statistics}
{bind:> {bf:Treatment effects}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection U 26.21Treatment-effectmodels:[U] 26.21 Treatment-effect models (pdf)}}

{p 4 8 4}
{bf:{help etpoisson:etpoisson - Poisson regression with endogenous treatment effects}}

{p 4 8 4}
{bf:{help etregress:etregress - Linear regression with endogenous treatment effects}}

{p 4 8 4}
{bf:{help teffects:teffects - Treatment-effects estimation for observational data}}

{p 4 8 4}
{bf:{help teffects_aipw:teffects aipw - Augmented inverse-probability weighting}}

{p 4 8 4}
{bf:{mansection TE teffectsintro:teffects intro - Introduction to treatment effects for observational data (pdf)}}

{p 4 8 4}
{bf:{mansection TE teffectsintroadvanced:teffects intro advanced - Advanced introduction to treatment effects for observational data (pdf)}}

{p 4 8 4}
{bf:{help teffects_ipw:teffects ipw - Inverse-probability weighting}}

{p 4 8 4}
{bf:{help teffects_ipwra:teffects ipwra - Inverse-probability-weighted regression adjustment}}

{p 4 8 4}
{bf:{mansection TE teffectsmultivalued:teffects multivalued - Multivalued treatment effects (pdf)}}

{p 4 8 4}
{bf:{help teffects_nnmatch:teffects nnmatch - Nearest-neighbor matching}}

{p 4 8 4}
{bf:{help teffects_overlap:teffects overlap - Overlap plots}}

{p 4 8 4}
{bf:{help teffects_psmatch:teffects psmatch - Propensity-score matching}}

{p 4 8 4}
{bf:{help teffects_ra:teffects ra - Regression adjustment}}

{p 4 8 4}
{bf:{mansection TE treatmenteffects:treatment effects - Introduction to treatment-effects commands (pdf)}}

{hline}
