{smcl}
{p 0 4}
{help contents:Top}
> {help contents_utilities:Utilities}
{bind:> {bf:Advanced utilities}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{help assert:assert - Verify truth of claim}}

{p 4 8 4}
{bf:{help cd:cd - Change directory}}

{p 4 8 4}
{bf:{help changeeol:changeeol - Convert end-of-line characters of text file}}

{p 4 8 4}
{bf:{help checksum:checksum - Calculate checksum of file}}

{p 4 8 4}
{bf:{help copy:copy - Copy file from disk or URL}}

{p 4 8 4}
{bf:{help _datasignature:_datasignature - Determine whether data have changed}}

{p 4 8 4}
{bf:{help datasignature:datasignature - Determine whether data have changed}}

{p 4 8 4}
{bf:{help db:db - Launch dialog}}

{p 4 8 4}
{bf:{help dialog_programming:dialog programming - Dialog programming}}

{p 4 8 4}
{bf:{help dir:dir - Display filenames}}

{p 4 8 4}
{bf:{help discard:discard - Drop automatically loaded programs}}

{p 4 8 4}
{bf:{help erase:erase - Erase a disk file}}

{p 4 8 4}
{bf:{help file:file - Read and write ASCII text and binary files}}

{p 4 8 4}
{bf:{help filefilter:filefilter - Convert text or binary patterns in a file}}

{p 4 8 4}
{bf:{help hexdump:hexdump - Display hexadecimal report on file}}

{p 4 8 4}
{bf:{help mkdir:mkdir - Create directory}}

{p 4 8 4}
{bf:{help more:more - The -more- message}}

{p 4 8 4}
{bf:{help query:query - Display system parameters}}

{p 4 8 4}
{bf:{help quietly:quietly - Quietly and noisily perform Stata command}}

{p 4 8 4}
{bf:{help rmdir:rmdir - Remove directory}}

{p 4 8 4}
{bf:{help query:set - Overview of system parameters}}

{p 4 8 4}
{bf:{help set_cformat:set cformat - Format settings for coefficient tables}}

{p 4 8 4}
{bf:{help set_defaults:set defaults - Reset system parameters to original Stata defaults}}

{p 4 8 4}
{bf:{help set_emptycells:set emptycells - Set what to do with empty cells in interactions}}

{p 4 8 4}
{bf:{help set_seed:set seed - Specify initial value of random-number seed}}

{p 4 8 4}
{bf:{help set_showbaselevels:set showbaselevels - Display settings for coefficient tables}}

{p 4 8 4}
{bf:{help shell:shell - Temporarily invoke operating system}}

{p 4 8 4}
{bf:{help signestimationsample:signestimationsample - Determine whether the estimation sample has changed}}

{p 4 8 4}
{bf:{help smcl:smcl - Stata Markup and Control Language}}

{p 4 8 4}
{bf:{help sysdir:sysdir - Query and set system directories}}

{p 4 8 4}
{bf:{help type:type - Display contents of a file}}

{p 4 8 4}
{bf:{help which:which - Display location and version for an ado-file}}

{hline}
