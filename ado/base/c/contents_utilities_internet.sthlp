{smcl}
{p 0 4}
{help contents:Top}
> {help contents_utilities:Utilities}
{bind:> {bf:Internet}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection U 28UsingtheInternettokeepuptodate:[U] 28 Using the Internet to keep up to date (pdf)}}

{p 4 8 4}
{bf:{help adoupdate:adoupdate - Update user-written ado-files}}

{p 4 8 4}
{bf:{help checksum:checksum - Calculate checksum of file}}

{p 4 8 4}
{bf:{help copy:copy - Copy file from disk or URL}}

{p 4 8 4}
{bf:{help net:net - Install and manage user-written additions from the Internet}}

{p 4 8 4}
{bf:{help net:net search - Search the Internet for installable packages}}

{p 4 8 4}
{bf:{help netio:netio - Control Internet connections}}

{p 4 8 4}
{bf:{help news:news - Report Stata news}}

{p 4 8 4}
{bf:{help sj:sj - Stata Journal and STB installation instructions}}

{p 4 8 4}
{bf:{help ssc:ssc - Install and uninstall packages from SSC}}

{p 4 8 4}
{bf:{help update:update - Check for official updates}}

{p 4 8 4}
{bf:{help use:use - Load Stata dataset}}

{hline}
