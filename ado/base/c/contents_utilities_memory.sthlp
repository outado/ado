{smcl}
{p 0 4}
{help contents:Top}
> {help contents_utilities:Utilities}
{bind:> {bf:Data types and memory}}
{p_end}
{hline}

{title:Help file listings}

{p 4 8 4}
{bf:{mansection U 6Managingmemory:[U] 6 Managing memory (pdf)}}

{p 4 8 4}
{bf:{mansection U 12.2.2Numericstoragetypes:[U] 12.2.2 Numeric storage types (pdf)}}

{p 4 8 4}
{bf:{mansection U 12.4Strings:[U] 12.4 Strings (pdf)}}

{p 4 8 4}
{bf:{mansection U 13.11Precisionandproblemstherein:[U] 13.11 Precision and problems therein (pdf)}}

{p 4 8 4}
{bf:{mansection U 23Workingwithstrings:[U] 23 Working with strings (pdf)}}

{p 4 8 4}
{bf:{help compress:compress - Compress data in memory}}

{p 4 8 4}
{bf:{help data_types:data types - Quick reference for data types}}

{p 4 8 4}
{bf:{help matsize:matsize - Set the maximum number of variables in a model}}

{p 4 8 4}
{bf:{help memory:memory - Memory management}}

{p 4 8 4}
{bf:{help missing:missing values - Quick reference for missing values}}

{p 4 8 4}
{bf:{help recast:recast - Change storage type of variable}}

{hline}
