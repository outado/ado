{smcl}
{* *! version 1.1.4  31oct2012}{...}
{findalias asfrtutorials}
{title:Title}

{pstd}
{findalias frtutorials}


{title:Description}

{pstd}
{help dta_examples:Example datasets installed with Stata}

{pin}
This page contains links enabling you to describe or use the datasets that
were installed with Stata.


{pstd}
{help dta_manuals:Stata 13 manual datasets}

{pin}
This page provides web access to all the datasets referred to in the Stata
documentation.
{p_end}
