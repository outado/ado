{smcl}
{* *! version 1.1.1  20may2013}{...}
{bf:Stata 13 manual datasets}
{hline}

{pstd}
The following links to Stata manual datasets require web access.

{p 4 8 4}
{bf:{help q_base:Base Reference Manual [R]}}{break}

{p 4 8 4}
{bf:{help q_data:Data-Management Reference Manual [D]}}{break}

{p 4 8 4}
{bf:{help q_graphics:Graphics Reference Manual [G]}}{break}

{p 4 8 4}
{bf:{help q_cross:Longitudinal-Data/Panel-Data Reference Manual [XT]}}{break}

{p 4 8 4}
{bf:{help q_me:Multilevel Mixed-Effects Reference Manual [ME]}}{break}

{p 4 8 4}
{bf:{help q_mi:Multiple-Imputation Reference Manual [MI]}}{break}

{p 4 8 4}
{bf:{help q_multivariate:Multivariate Statistics Reference Manual [MV]}}{break}

{p 4 8 4}
{bf:{help q_power:Power and Sample-Size Reference Manual [PSS]}}{break}

{p 4 8 4}
{bf:{help q_prog:Programming Reference Manual [P]}}{break}

{p 4 8 4}
{bf:{help q_sem:Structural Equation Modeling Reference Manual [SEM]}}{break}

{p 4 8 4}
{bf:{help q_survey:Survey Data Reference Manual [SVY]}}{break}

{p 4 8 4}
{bf:{help q_survival:Survival Analysis and Epidemiological Tables Reference Manual [ST]}}{break}

{p 4 8 4}
{bf:{help q_time:Time-Series Reference Manual [TS]}}{break}

{p 4 8 4}
{bf:{help q_teffects:Treatment-Effects Reference Manual [TE]}}{break}

{p 4 8 4}
{bf:{help q_user:User's Guide [U]}}{break}

{hline}
