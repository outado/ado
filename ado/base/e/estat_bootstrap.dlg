/*
  estat bootstrap

*!  VERSION 1.0.2  31may2013

*/

VERSION 9.0

INCLUDE _std_large
INCLUDE _ht320
INCLUDE header
HELP hlp1, view("help estat bootstrap")
RESET res1

DIALOG main, label("estat bootstrap - Bootstrap confidence intervals") /*
	*/ tabtitle("Main")
BEGIN
  GROUPBOX gb_ci     _lft      _top      _iwd      _ht12, 		///
		label("Bootstrap confidence intervals")
  CHECKBOX ck_bc     _indent   _ss       _ibwd         .,		///
		label("Display bias-corrected CIs (default)")		///
		option(bc)
  CHECKBOX ck_bca    @         _ms       @         .,			///
		label("Display bias-corrected and accelerated CIs")	///
		option(bca)
  CHECKBOX ck_norm   @         _ms       @         .,			///
		label("Display normal based CIs")			///
		option("normal")
  CHECKBOX ck_p      @         _ms       @         .,			///
		label("Display percentile CIs")				///
		option(percentile)
  CHECKBOX ck_all    @   	_ms       @	     .,			///
		label("Display all available CIs")			///
		onclickon(program check_all)				///
		onclickoff(program check_all)				///
		option(all)

  CHECKBOX ck_noheader _lft    _xls      _iwd      .,			///
		label("Suppress the table header")			///
		onclickon(program check_header)				///
		onclickoff(program check_header)			///
		option(noheader)
  CHECKBOX ck_nolegend  @      _ms       @         .,			///
		label("Suppress the table legend")			///
		onclickon(program check_legend)				///
		onclickoff(program check_legend)			///
		option(nolegend)
  CHECKBOX ck_verbose @        _ms       @         .,			///
		label("Display the full table legend")			///
		option(verbose)

  TEXT     tx_title  _lft      _ls       _iwd      .,			///
		label("Title:")
  EDIT     ed_title  @         _ss       @         .,			///
		label("Title")						///
		option(title)
END

PROGRAM check_all
BEGIN
	if main.ck_all {
		call gaction main.ck_bc.disable
		call gaction main.ck_bca.disable
		call gaction main.ck_norm.disable
		call gaction main.ck_p.disable
	}
	if !main.ck_all {
		call gaction main.ck_bc.enable
		call gaction main.ck_bca.enable
		call gaction main.ck_norm.enable
		call gaction main.ck_p.enable
	}
END

PROGRAM check_header
BEGIN
	if main.ck_noheader {
		call gaction main.ck_nolegend.disable
		call gaction main.ck_verbose.disable
	}
	if !main.ck_noheader {
		call gaction main.ck_nolegend.enable
		call program check_legend
	}
END

PROGRAM check_legend
BEGIN
	if main.ck_nolegend {
		call gaction main.ck_verbose.disable
	}
	if !main.ck_nolegend {
		call gaction main.ck_verbose.enable
	}
END

PROGRAM command
BEGIN
	put "estat bootstrap"
	beginoptions
		option main.ck_all
		option main.ck_bc
		option main.ck_bca
		option main.ck_norm
		option main.ck_p
		option main.ck_noheader
		option main.ck_nolegend
		option main.ck_verbose
		optionarg main.ed_title
	endoptions
END
/* end */
