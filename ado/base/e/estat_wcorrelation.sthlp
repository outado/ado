{smcl}
{* *! version 1.0.0  12sep2013}{...}

{pstd}
Help for {cmd:estat wcorrelation} is found in:

{p2colset 9 28 30 2}{...}
{p2col:After command}See{p_end}
{p2line}
{p2col:{helpb mixed}}{help mixed postestimation}{p_end}
{p2col:{helpb xtgee}}{help xtgee postestimation}{p_end}
{p2line}
{p2colreset}{...}
