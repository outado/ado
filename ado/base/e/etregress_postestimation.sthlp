{smcl}
{* *! version 1.0.4  14apr2013}{...}
{viewerdialog predict "dialog treatr_p"}{...}
{vieweralsosee "[TE] etregress postestimation" "mansection TE etregresspostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[TE] etregress" "help etregress"}{...}
{viewerjumpto "Description" "etregress postestimation##description"}{...}
{viewerjumpto "Syntax for predict" "etregress postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "etregress postestimation##menu_predict"}{...}
{viewerjumpto "Options for predict" "etregress postestimation##options_predict"}{...}
{viewerjumpto "Examples" "etregress postestimation##examples"}{...}
{title:Title}

{p2colset 5 38 40 2}{...}
{p2col:{manlink TE etregress postestimation} {hline 2}}Postestimation
tools for etregress{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation commands are available after {opt etregress}:

{synoptset 17 notes}{...}
{p2coldent:Command}Description{p_end}
{synoptline}
INCLUDE help post_contrast
{p2coldent:(1) {bf:{help estat ic}}}Akaike's and Schwarz's Bayesian information criteria (AIC and BIC){p_end}
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_svy_estat
INCLUDE help post_estimates
INCLUDE help post_lincom
INCLUDE help post_lrtest_twostep
INCLUDE help post_margins
INCLUDE help post_marginsplot
INCLUDE help post_nlcom
{p2col :{helpb etregress postestimation##predict:predict}}predictions, residuals, influence statistics, and other diagnostic measures{p_end}
INCLUDE help post_predictnl
INCLUDE help post_pwcompare
{p2coldent:(1) {helpb suest}}seemingly unrelated estimation{p_end}
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{phang}
(1) {cmd:estat ic} and {cmd:suest} are not appropriate after
      {cmd:etregress, twostep}.
{p_end}
INCLUDE help post_lrtest_twostep_msg


{marker syntax_predict}{...}
{marker predict}{...}
{title:Syntax for predict}

{pstd}
After ML or twostep

{p 8 16 2}
{cmd:predict}
{dtype}
{newvar}
{ifin}
[{cmd:,} {it:statistic}]

{pstd}
After ML

{p 8 16 2}
{cmd:predict}
{dtype}
{c -(}{it:stub*}{c |}{it:{help newvar:newvar_reg}}
    {it:{help newvar:newvar_treat}} {it:{help newvar:newvar_athrho}}
    {it:{help newvar:newvar_lnsigma}}{c )-}
{ifin}
{cmd:,}
{opt sc:ores}

{marker statistic}{...}
{synoptset 17 tabbed}{...}
{synopthdr:statistic}
{synoptline}
{syntab:Main}
{synopt:{opt xb}}linear prediction; the default{p_end}
{synopt:{opt stdp}}standard error of the prediction{p_end}
{synopt:{opt stdf}}standard error of the forecast{p_end}
{synopt:{opt yct:rt}}{it:E}(y | treatment = 1){p_end}
{synopt:{opt ycnt:rt}}{it:E}(y | treatment = 0){p_end}
{synopt:{opt pt:rt}}Pr(treatment = 1){p_end}
{synopt:{opt xbt:rt}}linear prediction for treatment equation{p_end}
{synopt:{opt stdpt:rt}}standard error of the linear prediction for treatment
equation{p_end}
{synoptline}
{p2colreset}{...}
INCLUDE help esample
{p 4 6 2}
{opt stdf} is not allowed with {cmd:svy} estimation results.
{p_end}


INCLUDE help menu_predict


{marker options_predict}{...}
{title:Options for predict}

{dlgtab:Main}

{phang}
{opt xb}, the default, calculates the linear predictions, xb.

{phang}
{opt stdp} calculates the standard error of the prediction, which can be
   thought of as the standard error of the predicted expected value or mean
   for the observation's covariate pattern.  The standard error of the
   prediction is also referred to as the standard error of the fitted value.

{phang}
{opt stdf} calculates the standard error of the forecast, which is the
   standard error of the point prediction for one observation.  It is
   commonly referred to as the standard error of the future or forecast value.
   By construction, the standard errors produced by {opt stdf} are always
   larger than those produced by {opt stdp}; see
   {it:{mansection R regressMethodsandformulas:Methods and formulas}} in
   {hi:[R] regress}.

{phang}
{opt yctrt} calculates the expected value of the dependent variable
   conditional on the presence of the treatment: {it:E}(y | treatment=1).

{phang}
{opt ycntrt} calculates the expected value of the dependent variable
   conditional on the absence of the treatment: {it:E}(y | treatment=0).

{phang}
{opt ptrt} calculates the probability of the presence of the treatment:
   Pr(treatment=1) = Pr(w_j*g + u_j > 0).

{phang}
{opt xbtrt} calculates the linear prediction for the treatment equation.

{phang}
{opt stdptrt} calculates the standard error of the linear prediction for the
   treatment equation.

{phang}
{opt scores}, not available with {opt twostep}, calculates equation-level
score variables.

{pmore}
The first new variable will contain the derivative of the log likelihood with
respect to the regression equation.

{pmore}
The second new variable will contain the derivative of the log likelihood with
respect to the treatment equation.

{pmore}
The third new variable will contain the derivative of the log likelihood with
respect to the third equation ({hi:athrho}).

{pmore}
The fourth new variable will contain the derivative of the log likelihood with
respect to the fourth equation ({hi:lnsigma}).


{marker examples}{...}
{title:Examples}

{pstd}Setup{p_end}
{phang2}{cmd:. webuse union3}{p_end}
{p 8 12 2}{cmd:. etregress wage age grade smsa i.union#c.(black tenure),}
           {cmd:treat(union = south black tenure) vce(robust) vsquish}
	   {cmd:nolstretch}

{pstd}Estimate average treatment effect{p_end}
{phang2}{cmd:. margins r.union, vce(unconditional) contrast(nowald)}

{pstd}Estimate average treatment effect on the treated{p_end}
{phang2}{cmd:. margins r.union, vce(unconditional) contrast(nowald)}
          {cmd:subpop(union)}
{p_end}
