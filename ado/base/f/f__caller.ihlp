{* *! version 1.2.1  17sep2013}{...}
    {cmd:_caller()}
{p2colset 8 22 26 2}{...}
{p2col: Range:}1 to 13.1{p_end}
{p2col: Description:}returns {cmd:version} of
	   the program or session that invoked the currently running
	   program; see {manhelp version P}.  The current version at the time
	   of this writing is 13.1, so 13.1 is the upper end of this range. If
	   Stata 13.2 were the current version, 13.2 would be the upper end of
	   this range, and likewise, if Stata 14 were the current version,
	   14 would be the upper end of this range.  This is a function for
	   use by programmers.{p_end}
{p2colreset}{...}
