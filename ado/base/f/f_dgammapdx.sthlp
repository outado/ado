{smcl}
{* *! version 1.1.4  25mar2013}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help density functions" "help density_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Partial derivative wrt x of the cumulative gamma distribution function}

INCLUDE help f_dgammapdx
