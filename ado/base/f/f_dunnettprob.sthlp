{smcl}
{* *! version 1.0.1  25feb2013}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help density functions" "help density_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Cumulative Dunnett's multiple range distribution function}

INCLUDE help f_dunnettprob


{title:Reference}

{phang}
Miller, R. G.  1981.
{it:Simultaneous Statistical Inference}.  2nd ed.  New York: Springer.
{p_end}
