{* *! version 1.0.2  21aug2012}{...}
    {cmd:Fden(}{it:df1}{cmd:,}{it:df2}{cmd:,}{it:f}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df1}:}1e-323 to 8e+307 (may be nonintegral){p_end}
{p2col: Domain {it:df2}:}1e-323 to 8e+307 (may be nonintegral){p_end}
{p2col: Domain {it:f}:}-8e+307 to 8e+307{p_end}
{p2col: }Interesting domain is {it:f} {ul:>} 0{p_end}
{p2col: Range:}0 to 8e+307{p_end}
{p2col: Description:}returns the
	probability density function for the F distribution with {it:df1}
	numerator and {it:df2} denominator degrees of freedom.{p_end}
{p2col: }returns {cmd:0} if {it:f} < 0.{p_end}
{p2colreset}{...}
