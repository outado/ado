{* *! version 1.0.1  23may2013}{...}
    {cmd:fileexists(}{it:f}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain:}filenames{p_end}
{p2col: Range:}0 and 1{p_end}
{p2col: Description:}returns {cmd:1} if the file specified by {it:f} exists;
returns {cmd:0} otherwise.
{p_end}

{p2col 8 22 22 2:}If the file exists but is not readable, {cmd:fileexists()}
will still return {cmd:1}, because it does exist.  If the "file" is a
directory, {cmd:fileexists()} will return {cmd:0}.
{p_end}
{p2colreset}{...}
