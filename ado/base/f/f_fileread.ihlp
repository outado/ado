{* *! version 1.0.1  13may2013}{...}
    {cmd:fileread(}{it:f}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain:}filenames{p_end}
{p2col: Range:}strings{p_end}
{p2col: Description:}returns the contents of the file specified by {it:f}.
{p_end}

{p2col 8 22 22 2:}If the file does not exist or an I/O error occurs while
reading the file, then "{cmd:fileread() error} {it:#}" is returned, where
{it:#} is a standard Stata error return code.
{p_end}
{p2colreset}{...}
