{* *! version 1.0.1  07oct2013}{...}
    {cmd:invnchi2tail(}{it:df}{cmd:,}{it:np}{cmd:,}{it:p}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}2e-10 to 1e+6 (may be nonintegral){p_end}
{p2col: Domain {it:np}:}0 to 10,000{p_end}
{p2col: Domain {it:p}:}0 to 1{p_end}
{p2col: Range:}0 to 8e+307{p_end}
{p2col: Description:}returns the inverse reverse cumulative (upper tail or
survivor) noncentral chi-squared distribution: if
{cmd:nchi2tail(}{it:df}{cmd:,}{it:np}{cmd:,}{it:x}{cmd:)} = {it:p}, then
        {cmd:invnchi2tail(}{it:df}{cmd:,}{it:np}{cmd:,}{it:p}{cmd:)} = {it:x}.{p_end}
{p2colreset}{...}
