{* *! version 1.0.3  29apr2013}{...}
    {cmd:invt(}{it:df}{cmd:,}{it:p}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}2e-10 to 2e+17 (may be nonintegral){p_end}
{p2col: Domain {it:p}:}0 to 1{p_end}
{p2col: Range:}-8e+307 to 8e+307{p_end}
{p2col: Description:}returns the inverse cumulative Student's t distribution: if
	{cmd:t(}{it:df}{cmd:,}{it:t}{cmd:)} = {it:p}, then
	{cmd:invt(}{it:df}{cmd:,}{it:p}{cmd:)} = {it:t}.{p_end}
{p2colreset}{...}
