{* *! version 1.0.6  05dec2012}{...}
    {cmd:invttail(}{it:df}{cmd:,}{it:p}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}2e-10 to 2e+17 (may be nonintegral){p_end}
{p2col: Domain {it:p}:}0 to 1{p_end}
{p2col: Range:}-8e+307 to 8e+307{p_end}
{p2col: Description:}returns the inverse reverse
	cumulative (upper tail or survivor) Student's t distribution: if
	{cmd:ttail(}{it:df}{cmd:,}{it:t}{cmd:)} = {it:p}, then
	{cmd:invttail(}{it:df}{cmd:,}{it:p}{cmd:)} = {it:t}.{p_end}
{p2colreset}{...}
