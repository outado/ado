{* *! version 1.0.4  07may2013}{...}
    {cmd:length(}{it:s}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain:}strings{p_end}
{p2col: Range:}integers >= 0{p_end}
{p2col: Description:}returns the length of {it:s}.{break}
                    {cmd:length("ab")} = {cmd:2}{p_end}
{p2colreset}{...}
