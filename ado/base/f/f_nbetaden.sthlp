{smcl}
{* *! version 1.1.4  25feb2013}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help density functions" "help density_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Probability density function of the noncentral beta distribution}

INCLUDE help f_nbetaden


{title:Reference}

{marker JKB1995}{...}
{phang}
Johnson, N. L., S. Kotz, and N. Balakrishnan.  1995.
{it:Continuous Univariate Distributions, Vol. 2}. 2nd ed.
New York: Wiley.
{p_end}
