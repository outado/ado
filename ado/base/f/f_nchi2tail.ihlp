{* *! version 1.0.0  26sep2013}{...}
    {cmd:nchi2tail(}{it:df}{cmd:,}{it:np}{cmd:,}{it:x}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}2e-10 to 1e+6 (may be nonintegral){p_end}
{p2col: Domain {it:np}:}0 to 10,000{p_end}
{p2col: Domain {it:x}:}-8e+307 to 8e+307{p_end}
{p2col: Range:}0 to 1{p_end}
{p2col: Description:}returns the reverse cumulative (upper tail or survivor)
	noncentral chi-squared distribution, where {it:df} denotes the degrees
	of freedom, {it:np} is the noncentrality parameter, and {it:x} is the
	value of chi-squared.
	{p_end}
{p2col: }returns {cmd:1} if {it:x} < 0.{p_end}
{p2colreset}{...}
