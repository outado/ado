{* *! version 1.0.6  12sep2013}{...}
    {cmd:npnchi2(}{it:df}{cmd:,}{it:x}{cmd:,}{it:p}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}2e-10 to 1e+6 (may be nonintegral){p_end}
{p2col: Domain {it:x}:}0 to 8e+307{p_end}
{p2col: Domain {it:p}:}0 to 1{p_end}
{p2col: Range:}0 to 10,000{p_end}
{p2col: Description:}returns the
	noncentrality parameter, {it:np}, for the noncentral chi-squared:
	if {cmd:nchi2(}{it:df}{cmd:,}{it:np}{cmd:,}{it:x}{cmd:)} =
	{it:p}, then {cmd:npnchi2(}{it:df}{cmd:,}{it:x}{cmd:,}{it:p}{cmd:)}
	= {it:np}.
	{p_end}
{p2colreset}{...}
