{* *! version 1.0.5  13may2013}{...}
    {cmd:npnt(}{it:df}{cmd:,}{it:t}{cmd:,}{it:p}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}1e-100 to 1e+8 (may be nonintegral){p_end}
{p2col: Domain {it:t}:}-8e+307 to 8e+307{p_end}
{p2col: Domain {it:p}:}0 to 1{p_end}
{p2col: Range:}-1,000 to 1,000{p_end}
{p2col: Description:}returns the
	noncentrality parameter, {it:np}, for the noncentral Student's t
	distribution:
	if {cmd:nt(}{it:df}{cmd:,}{it:np}{cmd:,}{it:t}{cmd:)} =
	{it:p}, then {cmd:npnt(}{it:df}{cmd:,}{it:t}{cmd:,}{it:p}{cmd:)}
	= {it:np}.
	{p_end}
{p2colreset}{...}
