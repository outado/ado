{* *! version 1.0.2  20feb2013}{...}
    {cmd:nt(}{it:df}{cmd:,}{it:np}{cmd:,}{it:t}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}1e-100 to 1e+10 (may be nonintegral){p_end}
{p2col: Domain {it:np}:}-1,000 to 1,000{p_end}
{p2col: Domain {it:t}:}-8e+307 to 8e+307{p_end}
{p2col: Range:}0 to 1{p_end}
{p2col: Description:}returns the cumulative noncentral Student's t distribution
	with {it:df} degrees of freedom and noncentrality parameter {it:np}.
	{cmd:nt(}{it:df}{cmd:,0,}{it:t}{cmd:)} = {cmd:t(}{it:df}{cmd:,}{it:t}{cmd:)}.
	{p_end}
{p2colreset}{...}
