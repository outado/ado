{* *! version 1.0.3  25feb2013}{...}
    {cmd:ntden(}{it:df}{cmd:,}{it:np}{cmd:,}{it:t}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}1e-100 to 1e+10 (may be nonintegral){p_end}
{p2col: Domain {it:np}:}-1,000 to 1,000{p_end}
{p2col: Domain {it:t}:}-8e+307 to 8e+307{p_end}
{p2col: Range:}0 to 0.39894 ...{p_end}
{p2col: Description:}returns the probability density function
	of the noncentral Student's t distribution with {it:df} degrees of
	freedom and noncentrality parameter {it:np}.{p_end}
{p2colreset}{...}
