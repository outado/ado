{smcl}
{* *! version 1.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help random number functions" "help random_number_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Random-number function}

INCLUDE help f_rbeta


{marker references}{...}
{title:References}

{marker AW1976}{...}
{phang}
Atkinson, A. C., and J. Whittaker.  1976.  A switching algorithm for the 
generation of beta random variables with at least one parameter less
than 1.  {it:Journal of the Royal Statistical Society, Series A}
139: 462-467.

{marker AW1970}{...}
{phang}
------. 1970. Algorithm AS 134: The generation of beta random
variables with one parameter greater than and one parameter less 
than 1.  {it:Applied Statistics} 28: 90-93.

{marker D1986}{...}
{phang}
Devroye, L.  1986.  {it:Non-uniform Random Variate Generation}.
New York: Springer.

{marker G2003}{...}
{phang}
Gentle, J. E.  2003.  {it:Random Number Generation and Monte Carlo Methods}. 
2nd ed.  New York: Springer.

{marker SB1980}{...}
{phang}
Schmeiser, B. W., and A. J. G. Babu.  1980.  Beta variate generation via 
exponential majorizing functions.  {it:Operations Research} 28: 917-926.
{p_end}
