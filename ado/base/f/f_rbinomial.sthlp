{smcl}
{* *! version 1.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help random number functions" "help random_number_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Random-number function}

INCLUDE help f_rbinomial


{marker references}{...}
{title:References}

{marker K1982}{...}
{phang}
Kachitvichyanukul, V.  1982.
Computer Generation of Poisson, Binomial, and Hypergeometric Random Variables.
PhD thesis, Purdue University.

{marker KS1988}{...}
{phang}
Kachitvichyanukul, V., and B. Schmeiser.  1988.
Binomial random variate generation.
{it:Communications of the Association for Computing Machinery} 31: 216-222.

{marker K1986}{...}
{phang} 
Kemp, C. D.  1986.  A modal method for generating binomial variates.  
{it:Communications in Statistics: Theory and Methods} 15: 805-813.
{p_end}
