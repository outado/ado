{smcl}
{* *! version 1.0.0  10aug2012}{...}
    {cmd:rchi2(}{it:df}{cmd:)}
{p2colset 8 22 22 2}{...}
{p2col: Domain {it:df}:}2e-4 to 2e+8{p_end}
{p2col: Range:}0 to {cmd:c(maxdouble)}{p_end}
{p2col: Description:}returns chi-squared, with {it:df} degrees of 
	freedom, random variates.
{p2colreset}{...}
