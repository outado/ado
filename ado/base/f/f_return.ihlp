{* *! version 1.0.4  25feb2013}{...}
    {cmd:return(}{it:name}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain:}names{p_end}
{p2col: Range:}strings, scalars, matrices, and {it:missing}{p_end}
{p2col: Description:}returns the value of the to-be-stored result
             {cmd:return(}{it:name}{cmd:)}; see {manhelp return P}.{p_end}
{p2col 8 26 45 2:}{cmd:return(}{it:name}{cmd:)} = scalar missing if the stored
                     result does not exist{p_end}
{p2col 8 26 45 2:}{cmd:return(}{it:name}{cmd:)} = specified matrix if the stored
                     result is a matrix{p_end}
{p2col 8 26 45 2:}{cmd:return(}{it:name}{cmd:)} = scalar numeric value if the stored
                     result is a scalar{p_end}
{p2col 8 26 45 2:}{cmd:return(}{it:name}{cmd:)} = a string containing the first
                     2,045 characters if the stored result is a string{p_end}
{p2colreset}{...}
