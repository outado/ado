{smcl}
{* *! version 1.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help random number functions" "help random_number_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Random-number function}

INCLUDE help f_rgamma


{marker references}{...}
{title:References}

{marker AD1974}{...}
{phang}
Ahrens, J. H., and U. Dieter.  1974.  Computer methods for sampling from gamma,
beta, Poisson, and binomial distributions.  {it:Computing} 12: 223-246.

{marker B1983}{...}
{phang}
Best, D. J.  1983.  A note on gamma variate generators with shape parameters
less than unity.  {it:Computing} 30: 185-188.

{marker SL1980}{...}
{phang}
Schmeiser, B. W., and R. Lal. 1980.
Squeeze methods for generating gamma variates.
{it:Journal of the American Statistical Association} 75: 679-682.
{p_end}
