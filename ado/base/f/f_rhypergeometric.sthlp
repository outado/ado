{smcl}
{* *! version 1.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help random number functions" "help random_number_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Random-number function}

INCLUDE help f_rhypergeometric


{marker references}{...}
{title:References}

{marker K1982}{...}
{phang}
Kachitvichyanukul, V.  1982.
Computer Generation of Poisson, Binomial, and Hypergeometric Random Variables.
PhD thesis, Purdue University.

{marker KS1985}{...}
{phang}
Kachitvichyanukul, V., and B. Schmeiser.  1985.  Computer generation
of hypergeometric random variates.
{it:Journal of Statistical Computation and Simulation} 22: 127-145.
{p_end}
