{smcl}
{* *! version 1.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help random number functions" "help random_number_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Random-number function}

INCLUDE help f_rnormal


{marker references}{...}
{title:References}

{marker K1998}{...}
{phang} 
Knuth, D.  1998.
{it:The Art of Computer Programming, Volume 2: Seminumerical Algorithms}.
3rd ed.  Reading, MA: Addison Wesley.

{marker MMB1964}{...}
{phang} 
Marsaglia, G., M. D. MacLaren, and T. A. Bray.  1964.  A fast procedure for
generating normal random variables.  {it:Communications of the ACM} 7: 4-10.

{marker W1977}{...}
{phang}
Walker, A. J.  1977.  An efficient method for generating discrete random
variables with general distributions.
{it:ACM Transactions on Mathematical Software} 3: 253-256.
{p_end}
