{smcl}
{* *! version 1.0.0  10aug2012}{...}
    {cmd:rpoisson(}{it:m}{cmd:)}
{p2colset 8 22 22 2}{...}
{p2col: Domain {it:m}:}1e-6 to 1e+11{p_end}
{p2col: Range:}0 to 2^53-1{p_end}
{p2col: Description:}returns Poisson({it:m}) random variates, where {it:m} 
	is the distribution mean.  

{p2col 8 22 22 2:}Poisson variates are generated using the probability 
	integral transform methods of Kemp and Kemp
         ({help rpoisson()##KK1990:1990},
          {help rpoisson()##KK1991:1991}), as well as 
	the method of 
        {help rpoisson()##K1982:Kachitvichyanukul (1982)}.
{p2colreset}{...}
