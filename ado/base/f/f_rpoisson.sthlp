{smcl}
{* *! version 1.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help random number functions" "help random_number_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Random-number function}

INCLUDE help f_rpoisson


{marker references}{...}
{title:References}

{marker K1982}{...}
{phang}
Kachitvichyanukul, V.  1982.
Computer Generation of Poisson, Binomial, and Hypergeometric Random Variables.
PhD thesis, Purdue University.

{marker KK1990}{...}
{phang} 
Kemp, A. W., and C. D. Kemp.  1990.  A composition-search algorithm for 
low-parameter Poisson generation.
{it:Journal of Statistical Computation and Simulation} 35: 239-244.

{marker KK1991}{...}
{phang} 
Kemp, C. D., and A. W. Kemp.  1991.  Poisson random variate generation. 
{it:Applied Statistics} 40: 143-158.
{p_end}
