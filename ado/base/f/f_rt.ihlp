{smcl}
{* *! version 1.0.0  10aug2012}{...}
    {cmd:rt(}{it:df}{cmd:)}
{p2colset 8 22 22 2}{...}
{p2col: Domain {it:df}:}1 to 2^53-1{p_end}
{p2col: Range:}{cmd:c(mindouble)} to {cmd:c(maxdouble)}{p_end}
{p2col: Description:}returns Student's t random variates, where {it:df}
	is the degrees of freedom.  

{p2col 8 22 22 2:}Student's t variates are generated using the method 
	of Kinderman and Monahan
        ({help rt()##KM1977:1977},
         {help rt()##KM1980:1980}).
{p2colreset}{...}
