{smcl}
{* *! version 1.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "help functions" "help functions"}{...}
{vieweralsosee "help random number functions" "help random_number_functions"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{title:Random-number function}

INCLUDE help f_rt


{marker references}{...}
{title:References}

{marker KM1977}{...}
{phang}
Kinderman, A. J., and J. F. Monahan.  1977.  Computer generation of random
variables using the ratio of uniform deviates.
{it:Association for Computing Machinery Transactions on Mathematical Software}
3: 257-260.

{marker KM1980}{...}
{phang}
------.  1980.  New methods for generating
Student's t and gamma variables.  {it:Computing} 25: 369-377.
{p_end}
