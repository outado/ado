{smcl}
{* *! version 1.0.0  10aug2012}{...}
    {cmd:runiform()}
{p2colset 8 22 26 2}{...}
{p2col: Range:}0 to nearly 1 (0 to 1 - 2^(-32)){p_end}
{p2col: Description:}returns uniform random variates.{p_end}

{p2col 8 22 22 2:}{cmd:runiform()} returns uniformly distributed random
variates on the interval [0,1).  {cmd:runiform()} takes no arguments, but the
parentheses must be typed.  {cmd:runiform()} can be seeded with the 
{cmd:set seed} command. (See {help matrix functions} for the related
{cmd:matuniform()} matrix function.){p_end}

{p2col 8 22 22 2:}To generate random variates over the interval
[{it:a},{it:b}), use {it:a}{cmd:+(}{it:b}{cmd:-}{it:a}{cmd:)*runiform()}.{p_end}

{p2col 8 22 22 2:}To generate random integers over [{it:a},{it:b}],
use {it:a}{cmd:+int((}{it:b}{cmd:-}{it:a}{cmd:+1)*runiform())}.{p_end}
{p2colreset}{...}
