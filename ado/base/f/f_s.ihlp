{* *! version 1.0.5  25feb2013}{...}
    {cmd:s(}{it:name}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain:}names{p_end}
{p2col: Range:}strings and {it:missing}{p_end}
{p2col: Description:}returns the value of stored result
             {cmd:s(}{it:name}{cmd:)}; see {findalias frresult}.{p_end}
{p2col 8 26 40 2:}{cmd:s(}{it:name}{cmd:)} = {cmd:.} if the stored
                     result does not exist{p_end}
{p2col 8 26 40 2:}{cmd:s(}{it:name}{cmd:)} = a string containing the first
                     2,045 characters if the stored result is a string{p_end}
{p2colreset}{...}
