{* *! version 1.0.0  19mar2013}{...}
    {cmd:strcat(}{it:s1}{cmd:,}{it:s2}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:s1}:}strings{p_end}
{p2col: Domain {it:s2}:}strings{p_end}
{p2col: Range:}strings{p_end}
{p2col: Description:}There is no {cmd:strcat()} function.  Instead the
        addition operator is used to concatenate strings:{break}
	{cmd:"hello " + "world"} =
	{cmd:"hello world"}{break}
	{cmd:"a" + "b"} =
	{cmd:"ab"}
{p_end}
{p2colreset}{...}
