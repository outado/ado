{* *! version 1.0.1  22mar2013}{...}
    {cmd:strdup(}{it:s1}{cmd:,}{it:n}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:s1}:}strings{p_end}
{p2col: Domain {it:n}:}nonnegative integers 0, 1, 2, ...{p_end}
{p2col: Range:}strings{p_end}
{p2col: Description:}There is no {cmd:strdup()} function.  Instead the
        multiplication operator is used to create multiple copies
        of strings:{break}
	{cmd:"hello" * 3} =
	{cmd:"hellohellohello"}{break}
	{cmd:3 * "hello"} =
	{cmd:"hellohellohello"}{break}
	{cmd:0 * "hello"} =
	{cmd:""}{break}
	{cmd:"hello" * 1} =
	{cmd:"hello"}{break}
{p_end}
{p2colreset}{...}
