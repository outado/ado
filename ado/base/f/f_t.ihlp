{* *! version 1.0.3  24apr2013}{...}
    {cmd:t(}{it:df}{cmd:,}{it:t}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}2e-10 to 2e+17 (may be nonintegral){p_end}
{p2col: Domain {it:t}:}-8e+307 to 8e+307{p_end}
{p2col: Range:}0 to 1{p_end}
{p2col: Description:}returns the cumulative Student's t distribution with {it:df} degrees of freedom.
	{p_end}
{p2colreset}{...}
