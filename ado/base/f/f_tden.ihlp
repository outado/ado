{* *! version 1.0.4  05nov2012}{...}
    {cmd:tden(}{it:df}{cmd:,}{it:t}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}1e-323 to 8e+307 (may be nonintegral){p_end}
{p2col: Domain {it:t}:}-8e+307 to 8e+307{p_end}
{p2col: Range:}0 to 0.39894 ...{p_end}
{p2col: Description:}returns the probability density function
	of Student's t distribution.{p_end}
{p2colreset}{...}
