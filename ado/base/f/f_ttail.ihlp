{* *! version 1.0.5  21aug2012}{...}
    {cmd:ttail(}{it:df}{cmd:,}{it:t}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:df}:}2e-10 to 2e+17 (may be nonintegral){p_end}
{p2col: Domain {it:t}:}-8e+307 to 8e+307{p_end}
{p2col: Range:}0 to 1{p_end}
{p2col: Description:}returns the reverse
	cumulative (upper tail or survivor) Student's t distribution;
	it returns the probability T > {it:t}.
	{p_end}
{p2colreset}{...}
