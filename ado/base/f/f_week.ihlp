{* *! version 1.0.1  11feb2013}{...}
    {cmd:week(}{it:e_d}{cmd:)}
{p2colset 8 22 26 2}{...}
{p2col: Domain {it:e_d}:}{cmd:%td} dates 01jan0100 to 31dec9999
           (integers -679,350 to 2,936,549){p_end}
{p2col: Range:}integers 1 to 52 and {it:missing}{p_end}
{p2col: Description:}returns the numeric week of the year corresponding to date
           {it:e_d}, the {cmd:%td} encoded date (days since 01jan1960).
	   Note: The first week of a year is the first 7-day period
           of the year.{p_end}
{p2colreset}{...}
