*! version 1.0.3  10nov2013
program forecast_wrk_dlg
	version 13

	.forecast_dlg.TimeFormat.setvalue ""
	.forecast_dlg.ModelInMemory.setfalse
	.forecast_dlg.ModelName.setvalue "Model = No model in memory"
	.forecast_dlg.PeriodRangeMax.setvalue 0

	qui forecast query
	local model_found = r(found)
	local model_name  "`r(name)'"

	if "`r_unit'" != "." & "`r_unit'" != "" & "`r_unit'" != "y" {
		local time_format = "t" + "`r_unit'" + "("
		.forecast_dlg.TimeFormat.setvalue "`time_format'"
	}

	if `model_found' {
		.forecast_dlg.ModelInMemory.settrue
	}
	else {
		exit
	}

	if "`model_name'" != "" {
		.forecast_dlg.ModelName.setvalue "Model = `model_name'"
	}
	else {
		.forecast_dlg.ModelName.setvalue "Model = Forecast model started"
	}

	qui cap xtset

	if !_rc {
		local r_unit = "`r(unit1)'"
		if "`r_unit'" != "." & "`r_unit'" != "" & "`r_unit'" != "y" {
			local time_format = "t" + "`r_unit'" + "("
			.forecast_dlg.TimeFormat.setvalue "`time_format'"
		}
		local num = (`r(tmax)'-`r(tmin)'+1)/`r(tdelta)'
		.forecast_dlg.PeriodRangeMax.setvalue `num'
	}
end
