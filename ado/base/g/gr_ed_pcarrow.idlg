/*
  gr_ed_pcarrow.idlg

*!  VERSION 1.0.0  02jan2007

*/


SCRIPT main_PREINIT
BEGIN
	create STRING color
	create STRING rgb
	create STRING width
	create STRING pattern
	program main_get_defaults
END

DIALOG main, title("Paired-coordinate arrow properties") tabtitle("Lines")
BEGIN
  GROUPBOX gb_lines	_lft	_top	_iwd	_ht8,		///
	label("Line properties")
  TEXT     tx_clr	_ilft	_ss	_wd	.,		///
	label("Color:") right
  COMBOBOX cb_clr	_sep	@	_cbwd	.,		///
	dropdownlist contents(_colors) 				///
	onselchangelist(main_cl_clr_sel)
  COLOR    cl_clr	_cbsep	@	_clwd	.,
  
  TEXT     tx_width	_ilft	 _mls	_wd	.,		///
	label("Width:") right
  COMBOBOX cb_width	_sep	@	_cbwd	.,		///
	dropdown 						///
	contents(_linewidths)

  TEXT     tx_pattern	_ilft	_mls	_wd	.,		///
	label("Pattern:") right
  COMBOBOX cb_pattern	_sep	@	_cbwd	.,		///
	dropdownlist						///
	contents(_linepatterns)
END

SCRIPT main_POSTINIT
BEGIN
	program main_initialize_controls
END

LIST main_cl_clr_sel
BEGIN
	main.cl_clr.show
	main.cl_clr.hide
END

PROGRAM main_get_defaults
BEGIN	
	call Gstyle.withvalue index.setvalue class @.area.linestyle.color.styledex
	call index.withvalue color.setstring class .__STYLES.color[@]
	call main.cb_clr.setdefault class color.value

	call Gstyle.withvalue rgb.setstring class @.area.linestyle.color.rgb
	call main.cl_clr.setdefault class rgb.value

	call Gstyle.withvalue index.setvalue class @.area.linestyle.width.styledex
	call index.withvalue width.setstring class .__STYLES.linewidth[@]
	if ! width {
		call G.withvalue width.setvalue class @.area.linestyle.width.val
	}
	call main.cb_width.setdefault class width.value

	call Gstyle.withvalue index.setvalue class @.area.linestyle.pattern.styledex
	call index.withvalue pattern.setstring class .__STYLES.linepattern[@]
	call main.cb_pattern.setdefault class pattern.value
END

PROGRAM main_initialize_controls
BEGIN
	if color {
		call main.cb_clr.setvalue class color.value
	}
	if ! color {
		call main.cb_clr.setvalue "custom"
	}
	call main.cl_clr.setvalue class rgb.value

	call main.cb_width.setvalue class width.value
	call main.cb_pattern.setvalue class pattern.value
END

PROGRAM main_output
BEGIN
	call program main_get_defaults

	if ! main.cl_clr.isvisible() & ! main.cb_clr.isdefault() {
		put "style(area(linestyle(color(" 
		put main.cb_clr 
		put ")))) "
	}
	if main.cl_clr.isvisible() & ! main.cl_clr.isdefault() {
		put "style(area(linestyle(color("  `"""'
		put main.cl_clr
		put `"""'
		put ")))) "
	}
	if ! main.cb_width.isdefault() {
		put "style(area(linestyle(width("
		put main.cb_width
		put ")))) "
	}
	if ! main.cb_pattern.isdefault() {
		put "style(area(linestyle(pattern("
		put main.cb_pattern
		put ")))) "
	}
END

//*****************************************************************************

DIALOG arrow, tabtitle("Arrowheads")
BEGIN
  GROUPBOX gb_basic	_lft	_top	_iwd	_ht21,		///
	label("Arrowhead properties")
	
  TEXT     tx_size	_ilft	_ss	_wd	.,		///
	label("Size:") right
  COMBOBOX cb_size	_sep	@	_cbwd	.,		///
	dropdown						///
	contents(_symbolsizes)
  
  TEXT     tx_bsize	_ilft	_mls	_wd	.,		///
	label("Barb size:") right
  COMBOBOX cb_bsize	_sep	@	_cbwd	.,		///
	dropdown 						///
	contents(_symbolsizes)

  TEXT     tx_angle	_ilft	_mls	_wd	.,		///
	label("Angle:") right
  COMBOBOX cb_angle	_sep	@	_cbwd	.,		///
	dropdown						///
	contents(_angles)					///
	nomemory

  TEXT     tx_clr	_ilft	_mls	_wd	.,		///
	label("Color:") right
  COMBOBOX cb_clr	_sep	@	_cbwd	.,		///
	dropdownlist contents(_colors) 				///
	onselchangelist(arrow_cl_clr_sel)
  COLOR    cl_clr	_cbsep	@	_clwd	.,

  CHECKBOX ck_outline	_ilft	+35	_ibwd	_ht6,		///
	groupbox						///
	label("Different outline color")			///
	onclickon(script arrow_outline_on)			///
	onclickoff(script arrow_outline_off) nomemory
  
  TEXT     tx_oclr	_iilft	_ms	_wd2	.,		///
	label("Outline color:") right
  COMBOBOX cb_oclr	_sep2	@	_cbwd	.,		///
	dropdownlist contents(_colors) 				///
	onselchangelist(arrow_cl_oclr_sel)
  COLOR    cl_oclr	_cbsep	@	_clwd	.,

  TEXT     tx_owidth	_iilft	_mls	_wd2	.,		///
	label("Outline width:") right
  COMBOBOX cb_owidth	_sep2	@	_cbwd	.,		///
	dropdown 						///
	contents(_linewidths)

END

SCRIPT arrow_outline_on
BEGIN
	arrow.tx_oclr.enable
	arrow.cb_oclr.enable
	arrow.cl_oclr.enable
	arrow.tx_owidth.enable
	arrow.cb_owidth.enable
END

SCRIPT arrow_outline_off
BEGIN
	arrow.tx_oclr.disable
	arrow.cb_oclr.disable
	arrow.cl_oclr.disable
	arrow.tx_owidth.disable
	arrow.cb_owidth.disable
END

LIST arrow_cl_clr_sel
BEGIN
	arrow.cl_clr.show
	arrow.cl_clr.hide
END

LIST arrow_cl_oclr_sel
BEGIN
	arrow.cl_oclr.show
	arrow.cl_oclr.hide
END

SCRIPT arrow_PREINIT
BEGIN
	create STRING  arrowSize
	create STRING  arrowBSize
	create STRING  arrowAngle
	create STRING  arrowColor
	create STRING  arrowRGB
	create STRING  arrowOColor
	create STRING  arrowORGB
	create STRING  arrowOWidth
	program arrow_get_defaults
END

SCRIPT arrow_POSTINIT
BEGIN
	program arrow_initialize_controls
END

PROGRAM arrow_get_defaults
BEGIN
	call G.withvalue index.setvalue class @.style.marker.size.styledex
	call index.withvalue arrowSize.setstring class .__STYLES.symbolsize[@]
	if ! arrowSize {
		call G.withvalue arrowSize.setstring class @.style.marker.size.val
	}
	call arrow.cb_size.setdefault class arrowSize.value
	
	call G.withvalue index.setvalue class @.style.marker.backsize.styledex
	call index.withvalue arrowBSize.setstring class .__STYLES.symbolsize[@]
	if ! arrowBSize {
		call G.withvalue arrowBSize.setstring class @.style.marker.backsize.val
	}
	call  arrow.cb_bsize.setdefault class arrowBSize.value

	call G.withvalue index.setvalue class @.style.marker.angle.styledex
	call index.withvalue arrowAngle.setstring class .__STYLES.anglestyle[@]
	if ! arrowAngle {
		call G.withvalue arrowAngle.setstring class @.style.marker.angle.val
	}
	call arrow.cb_angle.setdefault class arrowAngle.value

	call G.withvalue index.setvalue class @.style.marker.fillcolor.styledex
	call index.withvalue arrowColor.setstring class .__STYLES.color[@]
	call arrow.cb_clr.setdefault class arrowColor.value
	
	call G.withvalue arrowRGB.setstring class @.style.marker.fillcolor.rgb
	call arrow.cl_clr.setdefault class arrowRGB.value

	call G.withvalue index.setvalue class @.style.marker.linestyle.color.styledex
	call index.withvalue arrowOColor.setstring class .__STYLES.color[@]
	call arrow.cb_oclr.setdefault class arrowOColor.value

	call G.withvalue arrowORGB.setstring class @.style.marker.linestyle.color.rgb
	call arrow.cl_oclr.setdefault class arrowORGB.value

	call G.withvalue index.setvalue class @.style.marker.linestyle.width.styledex
	call index.withvalue arrowOWidth.setstring class .__STYLES.linewidth[@]
	if ! arrowOWidth {
		call G.withvalue arrowOWidth.setvalue class @.style.marker.linestyle.width.val
	}
	call arrow.cb_owidth.setdefault class arrowOWidth.value
END

PROGRAM arrow_initialize_controls
BEGIN
	call arrow.cb_size.setvalue class arrowSize.value
	call arrow.cb_bsize.setvalue class arrowBSize.value
	call arrow.cb_angle.setvalue class arrowAngle.value
	if arrowColor {
		call arrow.cb_clr.setvalue class arrowColor.value
	}
	else {
		call arrow.cb_clr.setvalue "custom"
	}
	call arrow.cl_clr.setvalue class arrowRGB.value
	if arrowOColor {
		call arrow.cb_oclr.setvalue class arrowOColor.value
	}
	else {
		call arrow.cb_oclr.setvalue "custom"
	}
	call arrow.cl_oclr.setvalue class arrowORGB.value
	call arrow.cb_owidth.setvalue class arrowOWidth.value
	
	if arrowRGB.iseq(arrowORGB) {
		call arrow.ck_outline.setoff
	}
	else {
		call arrow.ck_outline.seton
	}
END

PROGRAM arrow_output
BEGIN
	call program arrow_get_defaults

	if ! arrow.cb_size.isdefault() {
		put "style(marker(size("
		put arrow.cb_size
		put "))) "
	}

	if ! arrow.cb_bsize.isdefault() {
		put "style(marker(backsize("
		put arrow.cb_bsize
		put "))) "
	}

	if ! arrow.cb_angle.isdefault() {
		put "style(marker(angle("
		put arrow.cb_angle
		put "))) "
	}


	// always output fill color when not the default
	if ! arrow.cl_clr.isvisible() & ! arrow.cb_clr.isdefault() {
		put "style(marker(fillcolor(" 
		put arrow.cb_clr 
		put "))) "
	}
	if arrow.cl_clr.isvisible() & ! arrow.cl_clr.isdefault() {
		put "style(marker(fillcolor("  `"""'
		put arrow.cl_clr
		put `"""'
		put "))) "
	}
	
	// determine when outline color should be output
	if ! arrow.ck_outline {
		// force the outline color to the same when necessary
		if ! arrow.cb_clr.iseq(arrowOColor) & ! arrow.cl_clr.isvisible() {
			// they are different... force them to be the same
			put "style(marker(linestyle(color(" 
			put arrow.cb_clr 
			put ")))) "
		}
		if ! arrow.cl_clr.iseq(arrowORGB) & arrow.cl_clr.isvisible() {
			// they are different... force them to be the same
			put "style(marker(linestyle(color("  `"""'
			put arrow.cl_clr
			put `"""'
			put ")))) "
		}
		
	}
	else {
		// request that outline and fill are different
		if ! arrow.cl_oclr.isvisible() & ! arrow.cb_oclr.isdefault() {
			put "style(marker(linestyle(color(" 
			put arrow.cb_oclr 
			put ")))) "
		}
		if arrow.cl_oclr.isvisible() & ! arrow.cl_oclr.isdefault() {
			put "style(marker(linestyle(color("  `"""'
			put arrow.cl_oclr
			put `"""'
			put ")))) "			
		}
	}

	if ! arrow.cb_owidth.isdefault() {
		put "style(marker(linestyle(width("
		put /hidden arrow.cb_owidth
		put ")))) "
	}
END

PROGRAM pcarrow_output
BEGIN
	put " " /program main_output
	put " " /program arrow_output
END
