/*
  gr_ed_rectangle.idlg

*!  VERSION 1.0.0  23jan2007

*/

SCRIPT rect_PREINIT
BEGIN
	create DOUBLE plotCustomLabel
	create DOUBLE plotCustomStyle

	create STRING fill_color
	create STRING fill_rgb
	create STRING intensity
	create STRING line_color
	create STRING line_rgb
	create STRING width
	create STRING pattern
	create DOUBLE drawview
	program rect_get_defaults
END

SCRIPT rect_POSTINIT
BEGIN
	program rect_initialize_controls
END

DIALOG rect, label("Rectangle properties") tabtitle("Main")
BEGIN
  GROUPBOX gb_rect	_lft	_top	_iwd	_ht17,		///
	label("Rectangle properties")
  TEXT     tx_fclr	_ilft	_ss	_wd	.,		///
	label("Color:") right
  COMBOBOX cb_fclr	_sep	@	_cbwd	.,		///
	dropdownlist contents(_colors) 				///
	onselchangelist(rect_cl_fclr_sel)
  COLOR    cl_fclr	_cbsep	@	_clwd	.,

  TEXT     tx_intensity	_ilft	_mls	_wd	.,		///
	label("Fill intensity:") right
  COMBOBOX cb_intensity	_sep	@	_cbwd	.,		///
	dropdown contents(_intensity)
  
  TEXT     tx_width	_ilft	 _mls	_wd	.,		///
	label("Outline width:") right
  COMBOBOX cb_width	_sep	@	_cbwd	.,		///
	dropdown 						///
	contents(_linewidths)

  TEXT     tx_pattern	_ilft	_mls	_wd	.,		///
	label("Outline pattern:") right
  COMBOBOX cb_pattern	_sep	@	_cbwd	.,		///
	dropdownlist						///
	contents(_linepatterns)

  CHECKBOX ck_outline	_ilft	_ls	_ibwd	_ht2h,		///
	groupbox						///
	label("Different outline color")			///
	onclickon(script rect_outline_on)			///
	onclickoff(script rect_outline_off) nomemory

  TEXT     tx_lclr	_iilft	_ss	_wd2	.,		///
	label("Outline color:") right
  COMBOBOX cb_lclr	_sep2	@	_cbwd	.,		///
	dropdownlist contents(_colors) 				///
	onselchangelist(rect_cl_lclr_sel)
  COLOR    cl_lclr	_cbsep	@	_clwd	.,

  // public scope (do not change name)
  CHECKBOX ck_hide	_lft	_xxxls	_iwd	.,		///
	label("Hide selected rectangle")
END


SCRIPT rect_outline_on
BEGIN
	rect.tx_lclr.enable
	rect.cb_lclr.enable
END

SCRIPT rect_outline_off
BEGIN
	rect.tx_lclr.disable
	rect.cb_lclr.disable
END

LIST rect_cl_fclr_sel
BEGIN
	rect.cl_fclr.show
	rect.cl_fclr.hide
END
LIST rect_cl_lclr_sel
BEGIN
	rect.cl_lclr.show
	rect.cl_lclr.hide
END

PROGRAM rect_get_defaults
BEGIN
	call Gstyle.withvalue index.setvalue class @.shadestyle.color.styledex
	call index.withvalue fill_color.setvalue class .__STYLES.color[@]
	call rect.cb_fclr.setdefault class fill_color.value
	call Gstyle.withvalue fill_rgb.setvalue class @.shadestyle.color.rgb
	call rect.cl_fclr.setdefault class fill_rgb.value

	call Gstyle.withvalue index.setvalue  class @.shadestyle.intensity.styledex
	call index.withvalue intensity.setvalue class .__STYLES.intensity[@]
	if ! intensity {
		call Gstyle.withvalue intensity.setvalue class @.shadestyle.intensity.val
	}
	call rect.cb_intensity.setdefault class intensity.value

	call Gstyle.withvalue index.setvalue class @.linestyle.color.styledex
	call index.withvalue line_color.setvalue class .__STYLES.color[@]
	call rect.cb_lclr.setdefault class line_color.value
	call Gstyle.withvalue line_rgb.setvalue class @.linestyle.color.rgb
	call rect.cl_lclr.setdefault class line_rgb.value

	call Gstyle.withvalue index.setvalue class @.linestyle.width.styledex
	call index.withvalue width.setvalue class .__STYLES.linewidth[@]
	if ! width {
		call Gstyle.withvalue width.setvalue class @.linestyle.width.val
	}
	call rect.cb_width.setdefault class width.value

	call Gstyle.withvalue index.setvalue class @.linestyle.pattern.styledex
	call index.withvalue pattern.setvalue class .__STYLES.linepattern[@]
	call rect.cb_pattern.setdefault class pattern.value

	call G.withvalue drawview.setvalue class @.draw_view.istrue
	if drawview {
		call rect.ck_hide.setdefault 0
	}
	else {
		call rect.ck_hide.setdefault 1
	}
END

PROGRAM rect_initialize_controls
BEGIN
	if fill_color {
		call rect.cb_fclr.setvalue class fill_color.value
	}
	else {
		call rect.cb_fclr.setvalue "custom"
	}
	call rect.cl_fclr.setvalue class fill_rgb.value
	call rect.cb_intensity.setvalue class intensity.value

	if line_color {
		call rect.cb_lclr.setvalue class line_color.value
	}
	else {
		call rect.cb_lclr.setvalue "custom"
	}
	call rect.cl_lclr.setvalue class line_rgb.value

	call rect.cb_width.setvalue class width.value
	call rect.cb_pattern.setvalue class pattern.value

	if fill_rgb.isneq(line_rgb) {
		call rect.ck_outline.seton
	}
	else {
		call rect.ck_outline.setoff
	}

	if drawview {
		call rect.ck_hide.setoff
	}
	else {
		call rect.ck_hide.seton
	}
END

PROGRAM rect_output
BEGIN
	call program rect_get_defaults

	if ! rect.cl_fclr.isvisible() & ! rect.cb_fclr.isdefault() {
		put "style(shadestyle(color(" 
		put rect.cb_fclr 
		put "))) "
	}
	if rect.cl_fclr.isvisible() & ! rect.cl_fclr.isdefault() {
		put "style(shadestyle(color("  `"""'
		put rect.cl_fclr
		put `"""'
		put "))) "
	}
	if ! rect.ck_outline & rect.cb_fclr.isneq(line_color) {
		if ! rect.cl_fclr.isvisible() {
			put "style(linestyle(color(" 
			put rect.cb_fclr 
			put "))) "
		}
		if rect.cl_fclr.isvisible() & rect.cl_fclr.isneq(line_rgb) {
			put "style(linestyle(color("  `"""'
			put rect.cl_fclr
			put `"""'
			put "))) "
		}
	}
	if rect.cb_lclr & rect.ck_outline {
		if ! rect.cl_lclr.isvisible() & ! rect.cb_lclr.isdefault() {
			put "style(linestyle(color(" 
			put rect.cb_lclr 
			put "))) "
		}
		if rect.cl_lclr.isvisible() & ! rect.cl_lclr.isdefault() {
			put "style(linestyle(color("  `"""'
			put rect.cl_lclr
			put `"""'
			put "))) "
		}
	}

	if ! rect.cb_intensity.isdefault() & rect.cb_intensity {
		put "style(shadestyle(intensity("
		put rect.cb_intensity
		put "))) "
	}

	if ! rect.cb_width.isdefault() {
		put "style(linestyle(width("
		put rect.cb_width
		put "))) "
	}
	if ! rect.cb_pattern.isdefault() {
		put "style(linestyle(pattern("
		put rect.cb_pattern
		put "))) "
	}
	if ! rect.ck_hide.isdefault() {
		if rect.ck_hide {
			put "set(draw_view no) "
		}
		else {
			put "set(draw_view yes) "
		}
	}
END

PROGRAM rect_key_output
BEGIN
	call program rect_get_defaults

	if ! rect.cl_fclr.isvisible() & ! rect.cb_fclr.isdefault() {
		put "style(area(shadestyle(color(" 
		put rect.cb_fclr 
		put ")))) "
	}
	if rect.cl_fclr.isvisible() & ! rect.cl_fclr.isdefault() {
		put "style(area(shadestyle(color("  `"""'
		put rect.cl_fclr
		put `"""'
		put ")))) "
	}
	if ! rect.ck_outline & rect.cb_fclr.isneq(line_color) {
		if ! rect.cl_fclr.isvisible() {
			put "style(area(linestyle(color(" 
			put rect.cb_fclr 
			put ")))) "
		}
		if rect.cl_fclr.isvisible() & rect.cl_fclr.isneq(line_rgb) {
			put "style(area(linestyle(color("  `"""'
			put rect.cl_fclr
			put `"""'
			put ")))) "
		}
	}
	if rect.cb_lclr & rect.ck_outline {
		if ! rect.cl_lclr.isvisible() & ! rect.cb_lclr.isdefault() {
			put "style(area(linestyle(color(" 
			put rect.cb_lclr 
			put ")))) "
		}
		if rect.cl_lclr.isvisible() & ! rect.cl_lclr.isdefault() {
			put "style(area(linestyle(color("  `"""'
			put rect.cl_lclr
			put `"""'
			put ")))) "
		}
	}

	if ! rect.cb_intensity.isdefault() & rect.cb_intensity {
		put "style(area(shadestyle(intensity("
		put rect.cb_intensity
		put ")))) "
	}

	if ! rect.cb_width.isdefault() {
		put "style(area(linestyle(width("
		put rect.cb_width
		put ")))) "
	}
	if ! rect.cb_pattern.isdefault() {
		put "style(area(linestyle(pattern("
		put rect.cb_pattern
		put ")))) "
	}
	if ! rect.ck_hide.isdefault() {
		if rect.ck_hide {
			put "set(draw_view no) "
		}
		else {
			put "set(draw_view yes) "
		}
	}
END
