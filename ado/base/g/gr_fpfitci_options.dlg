/*
    gr_fpfitci_options.dlg
    
*!  VERSION 1.0.0  29oct2006 

*/

VERSION 10

INCLUDE _std_large
INCLUDE header_gr_child

DEFINE _clwd 15

HELP hlp1, view("help twoway_fpfitci")
RESET res1

SCRIPT main_PREINIT
BEGIN
	create STRING lineResults
	create STRING markerResults
	create STRING rareaResults
	create STRING rbarResults
	create STRING rspikeResults
	create STRING rlineResults
END

SCRIPT main_POSTINIT
BEGIN
	program checkLineResults
	program checkMarkerResults
	program checkRangeAreaResults
	program checkRangeBarResults
	program checkRangeSpikeResults
	program checkRangeLineResults
END

DIALOG main, label("Fractional polynomial options") tabtitle("Main")
BEGIN
  TEXT     tx_command		_lft	_top	_iwd	.,	///
	label("Estimation command:")
  COMBOBOX cb_command		_lft	_ss	_vnwd	.,	///
	dropdownlist						///
	contents(est_command_list) 				///
	onselchangelist(est_command_action_list)		///
	option(estcmd) default(regress)
	
  DEFINE _x _lft
  DEFINE _y +35
  DEFINE _cx 200
  INCLUDE _sp_level

  GROUPBOX gb_plot		_lft	+35	_iwd	_ht11,	///
	label("Plots")

  TEXT     tx_plot		_ilft	_ss	120	.,	///
	label("Plot type of fit:")
DEFINE y @y
  COMBOBOX cb_plot		@	_ss	@	.,	///
	dropdownlist						///
	contents(fit_plot_list)					///
	values(fit_plot_list_values)				///
	onselchangelist(fit_plot_actions)			///
	option(fitplot) default(line)
  BUTTON   bu_line_opts		@	+35	@	.,	///
	label("Line properties  ")				///
	onpush(script line_options_show)			///
	tooltip("Line properties for fractional polynomial plot")
  BUTTON   bu_marker_opts	@	_ls	@	.,	///
	label("Marker properties  ")				///
	onpush(script marker_options_show)			///
	tooltip("Marker properties for fractional polynomial plot")

  TEXT     tx_ciplot		+160	y	140	.,	///
	label("Plot type of CI:")
  COMBOBOX cb_ciplot		@	_ss	@	.,	///
	dropdownlist						///
	contents(fit_ciplot_list)				///
	values(fit_ciplot_list_values)				///
	onselchangelist(fit_ciplot_actions)			///
	option(ciplot) default(rarea)
	
  BUTTON   bu_rarea_opts	@	+35	@	.,	///
	label("Area properties  ")				///
	onpush(script rarea_options_show)			///
	tooltip("Area properties for confidence interval plot")
  BUTTON   bu_rbar_opts		@	@	@	.,	///
	label("Bar properties  ")				///
	onpush(script rbar_options_show)			///
	tooltip("Bar properties for confidence interval plot")
  BUTTON   bu_rspike_opts	@	@	@	.,	///
	label("Spike properties  ")				///
	onpush(script rspike_options_show)			///
	tooltip("Spike properties for confidence interval plot")
  BUTTON   bu_rline_opts	@	@	@	.,	///
	label("Line properties  ")				///
	onpush(script rline_options_show)			///
	tooltip("Line properties for confidence interval plot")
  
  TEXT     tx_cap_size		+175	-20	120	.,	///
	label("Cap size:") 
  COMBOBOX cb_cap_size		@	+20	100	.,	///
	dropdown 						///
	contents(symbolsizes) option(msize)
END

LIST est_command_list
BEGIN
	regress
	clogit
	glm
	logit
	logistic
	poisson
	probit
	stcox
	streg	
END

LIST est_command_action_list
BEGIN
	script regress_on
	script clogit_on
	script glm_on
	script logit_on
	script logistic_on
	script poisson_on
	script probit_on
	script stcox_on
	script streg_on
END

SCRIPT regress_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for regress:"
	adv.bu_regress.show
	adv.bu_clogit.hide
	adv.bu_glm.hide
	adv.bu_logit.hide
	adv.bu_logistic.hide
	adv.bu_poisson.hide
	adv.bu_probit.hide
	adv.bu_stcox.hide
	adv.bu_streg.hide
	
	adv.bu_pregress.show
	adv.bu_pclogit.hide
	adv.bu_pglm.hide
	adv.bu_plogit.hide
	adv.bu_plogistic.hide
	adv.bu_ppoisson.hide
	adv.bu_pprobit.hide
	adv.bu_pstcox.hide
	adv.bu_pstreg.hide
END

SCRIPT clogit_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for clogit:"
	adv.bu_regress.hide
	adv.bu_clogit.show
	adv.bu_glm.hide
	adv.bu_logit.hide
	adv.bu_logistic.hide
	adv.bu_poisson.hide
	adv.bu_probit.hide
	adv.bu_stcox.hide
	adv.bu_streg.hide
	
	adv.bu_pregress.hide
	adv.bu_pclogit.show
	adv.bu_pglm.hide
	adv.bu_plogit.hide
	adv.bu_plogistic.hide
	adv.bu_ppoisson.hide
	adv.bu_pprobit.hide
	adv.bu_pstcox.hide
	adv.bu_pstreg.hide
END

SCRIPT glm_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for glm:"
	adv.bu_regress.hide
	adv.bu_clogit.hide
	adv.bu_glm.show
	adv.bu_logit.hide
	adv.bu_logistic.hide
	adv.bu_poisson.hide
	adv.bu_probit.hide
	adv.bu_stcox.hide
	adv.bu_streg.hide

	adv.bu_pregress.hide
	adv.bu_pclogit.hide
	adv.bu_pglm.show
	adv.bu_plogit.hide
	adv.bu_plogistic.hide
	adv.bu_ppoisson.hide
	adv.bu_pprobit.hide
	adv.bu_pstcox.hide
	adv.bu_pstreg.hide
END

SCRIPT logit_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for logit:"
	adv.bu_regress.hide
	adv.bu_clogit.hide
	adv.bu_glm.hide
	adv.bu_logit.show
	adv.bu_logistic.hide
	adv.bu_poisson.hide
	adv.bu_probit.hide
	adv.bu_stcox.hide
	adv.bu_streg.hide
	
	adv.bu_pregress.hide
	adv.bu_pclogit.hide
	adv.bu_pglm.hide
	adv.bu_plogit.show
	adv.bu_plogistic.hide
	adv.bu_ppoisson.hide
	adv.bu_pprobit.hide
	adv.bu_pstcox.hide
	adv.bu_pstreg.hide
END

SCRIPT logistic_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for logistic:"
	adv.bu_regress.hide
	adv.bu_clogit.hide
	adv.bu_glm.hide
	adv.bu_logit.hide
	adv.bu_logistic.show
	adv.bu_poisson.hide
	adv.bu_probit.hide
	adv.bu_stcox.hide
	adv.bu_streg.hide

	adv.bu_pregress.hide
	adv.bu_pclogit.hide
	adv.bu_pglm.hide
	adv.bu_plogit.hide
	adv.bu_plogistic.show
	adv.bu_ppoisson.hide
	adv.bu_pprobit.hide
	adv.bu_pstcox.hide
	adv.bu_pstreg.hide
END

SCRIPT poisson_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for poisson:"
	adv.bu_regress.hide
	adv.bu_clogit.hide
	adv.bu_glm.hide
	adv.bu_logit.hide
	adv.bu_logistic.hide
	adv.bu_poisson.show
	adv.bu_probit.hide
	adv.bu_stcox.hide
	adv.bu_streg.hide

	adv.bu_pregress.hide
	adv.bu_pclogit.hide
	adv.bu_pglm.hide
	adv.bu_plogit.hide
	adv.bu_plogistic.hide
	adv.bu_ppoisson.show
	adv.bu_pprobit.hide
	adv.bu_pstcox.hide
	adv.bu_pstreg.hide
END

SCRIPT probit_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for probit:"
	adv.bu_regress.hide
	adv.bu_clogit.hide
	adv.bu_glm.hide
	adv.bu_logit.hide
	adv.bu_logistic.hide
	adv.bu_poisson.hide
	adv.bu_probit.show
	adv.bu_stcox.hide
	adv.bu_streg.hide

	adv.bu_pregress.hide
	adv.bu_pclogit.hide
	adv.bu_pglm.hide
	adv.bu_plogit.hide
	adv.bu_plogistic.hide
	adv.bu_ppoisson.hide
	adv.bu_pprobit.show
	adv.bu_pstcox.hide
	adv.bu_pstreg.hide
END

SCRIPT stcox_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for stcox:"
	adv.bu_regress.hide
	adv.bu_clogit.hide
	adv.bu_glm.hide
	adv.bu_logit.hide
	adv.bu_logistic.hide
	adv.bu_poisson.hide
	adv.bu_probit.hide
	adv.bu_stcox.show
	adv.bu_streg.hide

	adv.bu_pregress.hide
	adv.bu_pclogit.hide
	adv.bu_pglm.hide
	adv.bu_plogit.hide
	adv.bu_plogistic.hide
	adv.bu_ppoisson.hide
	adv.bu_pprobit.hide
	adv.bu_pstcox.show
	adv.bu_pstreg.hide
END

SCRIPT streg_on
BEGIN
	adv.tx_cmdopts.setlabel "Options for streg:"
	adv.bu_regress.hide
	adv.bu_clogit.hide
	adv.bu_glm.hide
	adv.bu_logit.hide
	adv.bu_logistic.hide
	adv.bu_poisson.hide
	adv.bu_probit.hide
	adv.bu_stcox.hide
	adv.bu_streg.show

	adv.bu_pregress.hide
	adv.bu_pclogit.hide
	adv.bu_pglm.hide
	adv.bu_plogit.hide
	adv.bu_plogistic.hide
	adv.bu_ppoisson.hide
	adv.bu_pprobit.hide
	adv.bu_pstcox.hide
	adv.bu_pstreg.show
END

LIST fit_plot_list
BEGIN
	Line
	Connected
	Scatter
END
LIST fit_plot_list_values
BEGIN
	line
	connected
	scatter
END
LIST fit_plot_actions
BEGIN
	script line_on
	script connected_on
	script scatter_on
END

SCRIPT line_on
BEGIN
	main.bu_line_opts.enable
	main.bu_marker_opts.disable
END
SCRIPT connected_on
BEGIN
	main.bu_line_opts.enable
	main.bu_marker_opts.enable
END
SCRIPT scatter_on
BEGIN
	main.bu_line_opts.disable
	main.bu_marker_opts.enable
END

LIST fit_ciplot_list
BEGIN
	Range area
	Range bar
	Range spike
	Range spike w/cap
	Range line
END
LIST fit_ciplot_list_values
BEGIN
	rarea
	rbar
	rspike
	rcap
	rline
END
LIST fit_ciplot_actions
BEGIN
	script rarea_on
	script rbar_on
	script rspike_on
	script rcap_on
	script rline_on
END

SCRIPT rarea_on
BEGIN
	main.bu_rarea_opts.show
	main.bu_rbar_opts.hide
	main.bu_rspike_opts.hide
	main.bu_rline_opts.hide
	main.cb_cap_size.hide
	main.tx_cap_size.hide
END
SCRIPT rbar_on
BEGIN
	main.bu_rarea_opts.hide
	main.bu_rbar_opts.show
	main.bu_rspike_opts.hide
	main.bu_rline_opts.hide
	main.cb_cap_size.hide
	main.tx_cap_size.hide
END
SCRIPT rspike_on
BEGIN
	main.bu_rarea_opts.hide
	main.bu_rbar_opts.hide
	main.bu_rspike_opts.show
	main.bu_rline_opts.hide
	main.cb_cap_size.hide
	main.tx_cap_size.hide
END
SCRIPT rcap_on
BEGIN
	main.bu_rarea_opts.hide
	main.bu_rbar_opts.hide
	main.bu_rspike_opts.show
	main.bu_rline_opts.hide
	main.cb_cap_size.show
	main.tx_cap_size.show
END
SCRIPT rline_on
BEGIN
	main.bu_rarea_opts.hide
	main.bu_rbar_opts.hide
	main.bu_rspike_opts.hide
	main.bu_rline_opts.show
	main.cb_cap_size.hide
	main.tx_cap_size.hide
END

SCRIPT line_options_show
BEGIN
	create CHILD gr_line_options AS lineOptions, allowsubmit
	lineOptions.setExitString lineResults
	lineOptions.settitle "Line properties for fractional polynomial plot"
	lineOptions.setExitAction "program checkLineResults"
	lineOptions.setSubmitAction "script lineSubmit"
	lineOptions.callthru "script gr_line_setOptionsTo_cl"
END
PROGRAM checkLineResults
BEGIN
	if lineResults.iseq("") {
		call main.bu_line_opts.setlabel "Line properties  "
	}
	if lineResults.isneq("") {
		call main.bu_line_opts.setlabel "Line properties *"
	}
END
SCRIPT lineSubmit
BEGIN
	program checkLineResults
	Submit
END

SCRIPT marker_options_show
BEGIN
	create CHILD gr_marker_options AS markerOptions, allowsubmit
	markerOptions.setExitString markerResults
	markerOptions.settitle "Marker properties for fractional polynomial plot"
	markerOptions.setExitAction "program checkMarkerResults"
	markerOptions.setSubmitAction "script markerSubmit"
END
PROGRAM checkMarkerResults
BEGIN
	if markerResults.iseq("") {
		call main.bu_marker_opts.setlabel "Marker properties  "
	}
	if markerResults.isneq("") {
		call main.bu_marker_opts.setlabel "Marker properties *"
	}
END
SCRIPT markerSubmit
BEGIN
	program checkMarkerResults
	Submit
END

SCRIPT rarea_options_show
BEGIN
	create CHILD gr_rarea_options AS rareaOptions, allowsubmit
	rareaOptions.setExitString rareaResults
	rareaOptions.settitle "Area properties for fractional polynomial CI's"
	rareaOptions.setExitAction "program checkRangeAreaResults"
	rareaOptions.setSubmitAction "script rareaSubmit"
	rareaOptions.callthru "script gr_rarea_setOptionsTo_bl"
	rareaOptions.callthru "script gr_rarea_setOrientationOff"
END
PROGRAM checkRangeAreaResults
BEGIN
	if rareaResults.iseq("") {
		call main.bu_rarea_opts.setlabel "Area properties  "
	}
	if rareaResults.isneq("") {
		call main.bu_rarea_opts.setlabel "Area properties *"
	}
END
SCRIPT rareaSubmit
BEGIN
	program checkRangeAreaResults
	Submit
END

SCRIPT rbar_options_show
BEGIN
	create CHILD gr_rbar_options AS rbarOptions, allowsubmit
	rbarOptions.setExitString rbarResults
	rbarOptions.settitle "Bar properties for fractional polynomial CI's"
	rbarOptions.setExitAction "program checkRangeBarResults"
	rbarOptions.setSubmitAction "script rbarSubmit"
	rbarOptions.callthru "script gr_rbar_setOptionsTo_bl"
	rbarOptions.callthru "script gr_rbar_setOrientationOff"
	rbarOptions.callthru "script gr_rbar_setBarWidthOff"
END
PROGRAM checkRangeBarResults
BEGIN
	if rbarResults.iseq("") {
		call main.bu_rbar_opts.setlabel "Bar properties  "
	}
	if rbarResults.isneq("") {
		call main.bu_rbar_opts.setlabel "Bar properties *"
	}
END
SCRIPT rbarSubmit
BEGIN
	program checkRangeBarResults
	Submit
END

SCRIPT rspike_options_show
BEGIN
	create CHILD gr_rspike_options AS rspikeOptions, allowsubmit
	rspikeOptions.setExitString rspikeResults
	rspikeOptions.settitle "Spike properties for fractional polynomial CI's"
	rspikeOptions.setExitAction "program checkRangeSpikeResults"
	rspikeOptions.setSubmitAction "script rspikeSubmit"
	rspikeOptions.callthru "script gr_rspike_setOptionsTo_bl"
	rspikeOptions.callthru "script gr_rspike_setOrientationOff"
END
PROGRAM checkRangeSpikeResults
BEGIN
	if rspikeResults.iseq("") {
		call main.bu_rspike_opts.setlabel "Spike properties  "
	}
	if rspikeResults.isneq("") {
		call main.bu_rspike_opts.setlabel "Spike properties *"
	}
END
SCRIPT rspikeSubmit
BEGIN
	program checkRangeSpikeResults
	Submit
END

SCRIPT rline_options_show
BEGIN
	create CHILD gr_line_options AS rlineOptions, allowsubmit
	rlineOptions.setExitString rlineResults
	rlineOptions.settitle "Line properties for fractional polynomial CI's"
	rlineOptions.setExitAction "program checkRangeLineResults"
	rlineOptions.setSubmitAction "script rlineSubmit"
	rlineOptions.callthru "script gr_line_setOptionsTo_bl"
END
PROGRAM checkRangeLineResults
BEGIN
	if rlineResults.iseq("") {
		call main.bu_rline_opts.setlabel "Line properties  "
	}
	if rlineResults.isneq("") {
		call main.bu_rline_opts.setlabel "Line properties *"
	}
END
SCRIPT rlineSubmit
BEGIN
	program checkRangeLineResults
	Submit
END
//--
DIALOG adv, tabtitle("Advanced")
BEGIN
  TEXT     tx_cmdopts		_lft	_top	_vlwd	.,	///
	label("Options for estimation command:")
  EDIT	   ed_cmdopts		@	_ss	@	.,	///
	option(estopts)
  BUTTON   bu_regress		_vlsep	@	_clwd	.,	///
	label("?") tooltip("Help - regress") 			///
	onpush(view help regress##|_new)
  BUTTON   bu_clogit		@	@	@	.,	///
	label("?") tooltip("Help - clogit") 			///
	onpush(view help clogit##|_new)
  BUTTON   bu_glm		@	@	@	.,	///
	label("?") tooltip("Help - glm") 			///
	onpush(view help glm##|_new)	
  BUTTON   bu_logit		@	@	@	.,	///
	label("?") tooltip("Help - logit") 			///
	onpush(view help logit##|_new)
  BUTTON   bu_logistic		@	@	@	.,	///
	label("?") tooltip("Help - logistic") 			///
	onpush(view help logistic##|_new)
  BUTTON   bu_poisson		@	@	@	.,	///
	label("?") tooltip("Help - poisson") 			///
	onpush(view help poisson##|_new)
  BUTTON   bu_probit		@	@	@	.,	///
	label("?") tooltip("Help - probit") 			///
	onpush(view help probit##|_new)
  BUTTON   bu_stcox		@	@	@	.,	///
	label("?") tooltip("Help - stcox") 			///
	onpush(view help stcox##|_new)
  BUTTON   bu_streg		@	@	@	.,	///
	label("?") tooltip("Help - streg") 			///
	onpush(view help streg##|_new)

  TEXT     tx_predict		_lft	_ls	_vlwd	.,	///
	label("Predict options:")
  EDIT	   ed_predict		@	_ss	@	.,	///
	option(predopts)	
  BUTTON   bu_pregress		_vlsep	@	_clwd	.,	///
	label("?") tooltip("Help - predict after regress")	///
	onpush(view help regress postestimation##predict|_new)
  BUTTON   bu_pclogit		@	@	@	.,	///
	label("?") tooltip("Help - predict after clogit") 	///
	onpush(view help clogit postestimation##predict|_new)
  BUTTON   bu_pglm		@	@	@	.,	///
	label("?") tooltip("Help - predict after glm") 		///
	onpush(view help glm postestimation##predict|_new)	
  BUTTON   bu_plogit		@	@	@	.,	///
	label("?") tooltip("Help - predict after logit") 	///
	onpush(view help logit postestimation##predict|_new)
  BUTTON   bu_plogistic		@	@	@	.,	///
	label("?") tooltip("Help - predict after logistic") 	///
	onpush(view help logistic postestimation##predict|_new)
  BUTTON   bu_ppoisson		@	@	@	.,	///
	label("?") tooltip("Help - predict after poisson") 	///
	onpush(view help poisson postestimation##predict|_new)
  BUTTON   bu_pprobit		@	@	@	.,	///
	label("?") tooltip("Help - predict after probit") 	///
	onpush(view help probit postestimation##predict|_new)
  BUTTON   bu_pstcox		@	@	@	.,	///
	label("?") tooltip("Help - predict after stcox") 	///
	onpush(view help stcox postestimation##predict|_new)
  BUTTON   bu_pstreg		@	@	@	.,	///
	label("?") tooltip("Help - predict after streg") 	///
	onpush(view help streg postestimation##predict|_new)
END

PROGRAM command
BEGIN
	optionarg /hidedefault main.cb_command
	optionarg /hidedefault main.sp_level
	
	optionarg /hidedefault main.cb_plot
	if ! D(main.bu_line_opts) {
		put " " lineResults " "
	}
	if ! D(main.bu_marker_opts) {
		put " " markerResults " "
	}
	
	optionarg /hidedefault main.cb_ciplot
	if ! H(main.bu_rarea_opts) {
		put " " rareaResults " "
	}
	if ! H(main.bu_rbar_opts) {
		put " " rbarResults " "
	}
	if ! H(main.bu_rspike_opts) {
		put " " rspikeResults " "
	}
	if ! H(main.bu_rline_opts) {
		put " " rlineResults " "
	}
	optionarg /hidedefault main.cb_cap_size
	
	optionarg adv.ed_cmdopts
	optionarg adv.ed_predict
END
