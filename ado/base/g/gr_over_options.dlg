/*
    gr_over_options.dlg

*!  VERSION 1.0.1  08feb2007

*/

VERSION 10.0

INCLUDE _std_mlarge
DEFINE _dlght 350
INCLUDE header_gr_child

HELP hlp1, view("help graph bar##over_subopts")
RESET res1

DIALOG main, tabtitle("Main")				///
	title("Categorical axis and label properties")
BEGIN
  CHECKBOX ck_relabel	_lft	_top	_iwd	_ht4h,	///
	groupbox					///
	onclickon(program check_relabel)		///
	onclickoff(program check_relabel)		///
	label("Override labels for this group")
  TEXT     tx_relabel	_ilft	_ss	_ilw15	.,	///
	label("Label specification:")
  EDIT     ed_relabel	@       _ss	@	.,	///
	option(relabel)
  BUTTON   bu_relabel	_islw15	@	15	.,	///
	label("?") onpush(view help graph_over_relabel##|_new)

  TEXT     tx_gap	_lft	+55	75	.,	///
	label("Category gap:") right
  EDIT     ed_gap	+80	@	_vnwd	.,	///
	option(gap)
  BUTTON   bu_gap	_vnsep	@	15	.,	///
	label("?") onpush(view help graph_over_gap##|_new)
	
  TEXT     tx_sort	_lft	_mls	75	.,	///
	label("Sort order:") right
  DEFINE y @y
  VARNAME  vn_sort	+80	@	_vnwd	.,	///
	option(sort)
  BUTTON   bu_sort	_vnsep	@	15	.,	///
	label("?") onpush(view help graph_over_sort##|_new)

  CHECKBOX ck_sort	+40	@	140	.,	///
	label("Sort descending") option(descending)


  CHECKBOX ck_noaxis	_lft	+35	_iwd	.,	///
	onclickon(script main_axis_off) 		///
	onclickoff(program main_axis_on) 		///
	option(axis(off))				///
	label("Suppress display of the entire categorical axis")
  CHECKBOX ck_nolabel	@	_ms	@	.,	///
	label("Suppress categorical axis labels") 	///
	option(nolabel)					///
	onclickon(script main_lab_off) 			///
	onclickoff(program main_lab_on)
	
  GROUPBOX gb_label	_lft	_mls	_iwd	_ht5,	///
	label("Labels")
  TEXT     tx_lclr	_ilft	_ss	50	.,	///
	label("Color:") right
  COMBOBOX cb_lclr	+55	@	100	.,	///
	dropdownlist contents(colors) 			///
	onselchangelist(lclr_action_list) option(labcolor)
  COLOR    cl_lclr	+105  	@	15	.,	///
	option(labcolor)

  TEXT     tx_angle	_lft2	@	50	.,	///
	label("Angle:") right
  COMBOBOX cb_angle	+55	@	100	.,	///
	contents(angles) option(angle) dropdown

  TEXT     tx_lsize	_ilft	_mls	50	.,	///
  	label("Size:") right
  COMBOBOX cb_lsize	+55	@	100	.,	///
 	contents(textsizes) option(labsize) dropdown

DEFINE x @x

  TEXT     tx_lgap	_lft2	@	50	.,	///
	label("Gap:") right
  EDIT     ed_lgap	+55	@	100	.,	///
	option(labgap)
	
  TEXT     tx_aclr	_ilft	+45	60	.,	///
	label("Axis color:") right
  COMBOBOX cb_aclr	+65	@	100	.,	///
	dropdownlist option(lcolor) contents(colors)	///
	onselchangelist(aclr_action_list)
  COLOR    cl_aclr	+105	@	15	.,	///
	option(lcolor)

  TEXT     tx_ogap	_lft2	@	60	.,	///
	label("Outer gap:") right
  EDIT     ed_ogap	+65	@	100	.,	///
	option(outergap)
END

PROGRAM check_relabel
BEGIN
	if main.ck_relabel {
		call main.tx_relabel.enable
		call main.ed_relabel.enable
		call main.bu_relabel.enable
	}
	else {
		call main.tx_relabel.disable
		call main.ed_relabel.disable
		call main.bu_relabel.disable
	}
END

LIST lclr_action_list
BEGIN
	main.cl_lclr.hide
	main.cl_lclr.show
	main.cl_lclr.hide
END

LIST aclr_action_list
BEGIN
	main.cl_aclr.hide
	main.cl_aclr.show
	main.cl_aclr.hide
END

SCRIPT main_axis_off
BEGIN
	main.gb_label.disable
	main.ck_nolabel.disable
	script main_lab_off
	main.tx_aclr.disable
	main.cb_aclr.disable
	main.cl_aclr.disable
	main.tx_ogap.disable
	main.ed_ogap.disable
END

PROGRAM main_axis_on
BEGIN
	call main.gb_label.enable
	call main.ck_nolabel.enable
	if ! main.ck_nolabel {
		call program main_lab_on
	}
	call main.tx_aclr.enable
	call main.cb_aclr.enable
	call main.cl_aclr.enable
	call main.tx_ogap.enable
	call main.ed_ogap.enable
END

PROGRAM main_lab_on
BEGIN
	if main.ck_nolabel.isenabled() {
		call main.gb_label.enable
		call main.tx_lclr.enable
		call main.cb_lclr.enable
		call main.cl_lclr.enable
		call main.tx_angle.enable
		call main.cb_angle.enable
		call main.tx_lsize.enable
		call main.cb_lsize.enable
		call main.tx_lgap.enable
		call main.ed_lgap.enable
	}
END

SCRIPT main_lab_off
BEGIN
	main.gb_label.disable
	main.tx_lclr.disable
	main.cb_lclr.disable
	main.cl_lclr.disable
	main.tx_angle.disable
	main.cb_angle.disable
	main.tx_lsize.disable
	main.cb_lsize.disable
	main.tx_lgap.disable
	main.ed_lgap.disable
END

PROGRAM main_axis_options
BEGIN
	if main.cl_aclr.isvisible() {
		optionarg /quoted main.cl_aclr
	}
	else {
		optionarg /hidedefault main.cb_aclr
	}	
	optionarg main.ed_ogap
END

PROGRAM main_label_options
BEGIN
	option main.ck_nolabel
	if main.cl_lclr.isvisible() {
		optionarg /quoted main.cl_lclr
	}
	else {
		optionarg /hidedefault main.cb_lclr
	}
	optionarg /hidedefault main.cb_angle
	optionarg /hidedefault main.cb_lsize
	optionarg main.ed_lgap
END

PROGRAM command
BEGIN
	if main.ck_relabel {
		require main.ed_relabel
		optionarg main.ed_relabel
	}
	optionarg main.ed_gap
	optionarg main.vn_sort
	option main.ck_sort
	option main.ck_noaxis
	if ! main.ck_noaxis {	
		if main.ck_nolabel | main.cb_lclr | main.cb_angle 	///
			| main.cb_lsize | main.ed_lgap 			///
		{
			put "label(" 
			put /program main_label_options
			put ") "
		}

		if main.cb_aclr | main.ed_ogap {
			put "axis("
			put /program main_axis_options
			put ") "
		}
	}
END
