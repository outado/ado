/*
    gr_taxis.idlg

*!  VERSION 1.0.1  19feb2007

	// SCRIPTS and PROGRAMS for external use
	   * SCRIPT gr_taxis_disable
	   * SCRIPT gr_taxis_enable
	   * SCRIPT gr_taxis_setByoptsContext_on
	   * SCRIPT gr_taxis_setByoptsContext_off
	   * SCRIPT gr_taxis_setByContext_on
	   * SCRIPT gr_taxis_setByContext_off

*** 	This tab is very closely related to gr_xaxis and somewhat closely
***	related gr_yaxis. Any changes should be reflected in those files.
*/

SCRIPT gr_taxis_disable
BEGIN
	taxis.ck_noscale.disable
	taxis.tx_title.disable
	taxis.ed_title.disable
	taxis.bu_title.disable
	taxis.bu_majorTick.disable
	taxis.bu_minorTick.disable
	taxis.bu_axisLine.disable
	taxis.bu_scale.disable
	taxis.bu_refLines.disable
	taxis.bu_adv.disable
	taxis.ck_alt.disable
END

SCRIPT gr_taxis_enable
BEGIN
	program gr_taxis_enable_wrk
END
PROGRAM gr_taxis_enable_wrk
BEGIN
	call taxis.ck_noscale.enable
	call taxis.bu_majorTick.enable
	call taxis.bu_minorTick.enable
	call taxis.bu_scale.enable
	call taxis.bu_refLines.enable
	call taxis.bu_adv.enable
	if ! taxis.ck_noscale {
		call taxis.tx_title.enable
		call taxis.ed_title.enable
		call taxis.bu_title.enable
		call taxis.bu_axisLine.enable
		call taxis.ck_alt.enable
	}
END

SCRIPT gr_taxis_setByoptsContext_on
BEGIN
	taxis.bu_adv.show
	gr_taxis_advanedByopts_on.settrue
END
SCRIPT gr_taxis_setByoptsContext_off
BEGIN
	taxis.bu_adv.hide
	gr_taxis_advanedByopts_on.setfalse
END
SCRIPT gr_taxis_setByContext_on
BEGIN
	taxis.bu_adv.show
	gr_taxis_advanedBy_on.settrue
END
SCRIPT gr_taxis_setByContext_off
BEGIN
	taxis.bu_adv.hide
	gr_taxis_advanedBy_on.setfalse
END

SCRIPT taxis_PREINIT
BEGIN
	create STRING taxisTitleResults
	create STRING taxisLabelResults
	create STRING taxisMinorLabelResults
	create STRING taxisLineResults
	create STRING taxisScaleResults
	create STRING taxisReflineResults
	create STRING taxisByAdvResults

	create BOOLEAN gr_taxis_advanedBy_on
	create BOOLEAN gr_taxis_advanedByopts_on
	taxis.bu_adv.hide
END
SCRIPT taxis_POSTINIT
BEGIN
	program taxis_checkTitleResults
	program taxis_checkLabelResults
	program taxis_checkMinorLabelResults
	program taxis_checkLineResults
	program taxis_checkScaleResults
	program taxis_checkReflineResults
	program taxis_checkByAdvResults
END

DIALOG taxis, tabtitle("Time axis")
BEGIN

  TEXT     tx_title	_lft	_top	_iwd	.,		///
	label("Title:")
  EDIT	   ed_title	@	_ss	_lw80	.,
  BUTTON   bu_title	_slw80	@	80	.,		///
	label("Properties  ")					///
	onpush(program get_taxis_title_options)			///
	tooltip("Axis title properties")

  BUTTON   bu_majorTick	_lft	_xls	165	.,		///
	onpush(program get_taxis_label_options)			///
	label("Major tick/label properties")

  BUTTON   bu_minorTick	+190	@	@	.,		///
	onpush(program get_taxis_mlabel_options)		///
	label("Minor tick/label properties")

  BUTTON   bu_axisLine	_lft	_mls	@	.,		///
	label("Axis line properties")				///
	onpush(program get_taxis_line)

  BUTTON   bu_scale	+190	@	@	.,		///
	label("Axis scale properties  ")			///
	onpush(program get_taxis_scale_options)			///
	tooltip("Axis scale properties")

  BUTTON   bu_refLines	_lft	_mls	@	.,		///
	label("Reference lines")				///
	onpush(program get_taxis_refline)

  BUTTON   bu_adv	+190	@	@	.,		///
	label("Subgraph axes  ")				///
	onpush(program taxis_get_advanced_sub)			///
	tooltip("Affect how time axes relate to individual subgraphs")

  CHECKBOX ck_noscale	_lft	_ls	_iwd	.,		///
	label("Hide axis") 					///
	onclickoff(program taxis_noscale_off)			///
	onclickon(script taxis_noscale_on)

  CHECKBOX ck_alt	_lft	_ms	_iwd	.,		///
	label("Place axis on opposite side of graph")		///
	option(alt)
END

SCRIPT taxis_noscale_on
BEGIN
	taxis.tx_title.disable
	taxis.ed_title.disable
	taxis.bu_title.disable
	taxis.bu_axisLine.disable
	taxis.ck_alt.disable
END
PROGRAM taxis_noscale_off
BEGIN
	if taxis.ck_noscale.isenabled() {
		call taxis.tx_title.enable
		call taxis.ed_title.enable
		call taxis.bu_title.enable
		call taxis.bu_axisLine.enable
		call taxis.ck_alt.enable
	}
END

PROGRAM get_taxis_title_options
BEGIN
	call create CHILD gr_axis_title_options AS ttitle, allowsubmit
	call ttitle.setExitString taxisTitleResults
	call ttitle.settitle "Axis title properties (time axis)"
	call ttitle.setExitAction "program taxis_checkTitleResults"
	call ttitle.setSubmitAction "program taxis_titleSubmit"
	call ttitle.callthru `"titleOption.setstring "ttitle""'
	call ttitle.callthru `"scaleOption.setstring "tscale""'
END
PROGRAM taxis_checkTitleResults
BEGIN
	if taxisTitleResults.iseq("") {
		call taxis.bu_title.setlabel "Properties  "
	}
	else {
		call taxis.bu_title.setlabel "Properties *"
	}
END
PROGRAM taxis_titleSubmit
BEGIN
	call program taxis_checkTitleResults
	call Submit
END

PROGRAM get_taxis_label_options
BEGIN
	call create CHILD gr_axis_tick_label_options AS tlabel, allowsubmit
	call tlabel.setExitString taxisLabelResults
	call tlabel.settitle "Axis tick and label properties (time axis) (major ticks)"
	call tlabel.setExitAction "program taxis_checkLabelResults"
	call tlabel.setSubmitAction "program taxis_labelSubmit"
	call tlabel.callthru "script axis_setRuleMajor"
END
PROGRAM taxis_checkLabelResults
BEGIN
	if taxisLabelResults.iseq("") {
		call taxis.bu_majorTick.setlabel "Major tick/label properties  "
	}
	else {
		call taxis.bu_majorTick.setlabel "Major tick/label properties *"
	}
END
PROGRAM taxis_labelSubmit
BEGIN
	call program taxis_checkLabelResults
	call Submit
END

PROGRAM get_taxis_mlabel_options
BEGIN
	call create CHILD gr_axis_tick_label_options AS tmlabel, allowsubmit
	call tmlabel.setExitString taxisMinorLabelResults
	call tmlabel.settitle "Axis tick and label properties (time axis) (minor ticks)"
	call tmlabel.setExitAction "program taxis_checkMinorLabelResults"
	call tmlabel.setSubmitAction "program taxis_mlabelSubmit"
	call tmlabel.callthru "script axis_setRuleMinor"
END
PROGRAM taxis_checkMinorLabelResults
BEGIN
	if taxisMinorLabelResults.iseq("") {
		call taxis.bu_minorTick.setlabel "Minor tick/label properties  "
	}
	else {
		call taxis.bu_minorTick.setlabel "Minor tick/label properties *"
	}
END
PROGRAM taxis_mlabelSubmit
BEGIN
	call program taxis_checkMinorLabelResults
	call Submit
END

PROGRAM get_taxis_line
BEGIN
	call create CHILD gr_axis_line_options AS tlines, allowsubmit
	call tlines.setExitString taxisLineResults
	call tlines.settitle "Axis line properties (time axis)"
	call tlines.setExitAction "program taxis_checkLineResults"
	call tlines.setSubmitAction "program taxis_lineSubmit"
END
PROGRAM taxis_checkLineResults
BEGIN
	if taxisLineResults.iseq("") {
		call taxis.bu_axisLine.setlabel "Axis line properties  "
	}
	else {
		call taxis.bu_axisLine.setlabel "Axis line properties *"
	}
END
PROGRAM taxis_lineSubmit
BEGIN
	call program taxis_checkLineResults
	call Submit
END

PROGRAM get_taxis_scale_options
BEGIN
	call create CHILD gr_axis_scale_options AS tscale, allowsubmit
	call tscale.setExitString taxisScaleResults
	call tscale.settitle "Axis scale properties (time axis)"
	call tscale.setExitAction "program taxis_checkScaleResults"
	call tscale.setSubmitAction "program taxis_scaleSubmit"
END
PROGRAM taxis_checkScaleResults
BEGIN
	if taxisScaleResults.iseq("") {
		call taxis.bu_scale.setlabel "Axis scale properties  "
	}
	else {
		call taxis.bu_scale.setlabel "Axis scale properties *"
	}
END
PROGRAM taxis_scaleSubmit
BEGIN
	call program taxis_checkScaleResults
	call Submit
END

PROGRAM get_taxis_refline
BEGIN
	call create CHILD gr_taxis_reflines AS tReflines, allowsubmit
	call tReflines.setExitString taxisReflineResults
	call tReflines.settitle "Reference lines (time axis)"
	call tReflines.setExitAction "program taxis_checkReflineResults"
	call tReflines.setSubmitAction "program taxis_reflineSubmit"
END
PROGRAM taxis_checkReflineResults
BEGIN
	if taxisReflineResults.iseq("") {
		call taxis.bu_refLines.setlabel "Reference lines  "
	}
	else {
		call taxis.bu_refLines.setlabel "Reference lines *"
	}
END
PROGRAM taxis_reflineSubmit
BEGIN
	call program taxis_checkReflineResults
	call Submit
END

PROGRAM taxis_get_advanced_sub
BEGIN
	call create CHILD gr_by_axis_advanced AS taxis_by_adv, allowsubmit
	call taxis_by_adv.setExitString taxisByAdvResults
	call taxis_by_adv.settitle "Axis properties for subgraphs (time axis)"
	call taxis_by_adv.setExitAction "program taxis_checkByAdvResults"
	call taxis_by_adv.setSubmitAction "program taxis_byAdvSubmit"
	call taxis_by_adv.callthru "script setAxis_t"
END
PROGRAM taxis_checkByAdvResults
BEGIN
	if taxisByAdvResults {
		call taxis.bu_adv.setlabel "Subgraph axes *"
	}
	else {
		call taxis.bu_adv.setlabel "Subgraph axes  "
	}
END
PROGRAM taxis_byAdvSubmit
BEGIN
	call program taxis_checkByAdvResults
	call Submit
END

PROGRAM taxis_title_output
BEGIN
	put taxis.ed_title
END

PROGRAM gr_taxis_output
BEGIN
	if ! taxis.ck_noscale & taxis.ck_noscale.isenabled() {
		if taxis.ed_title {
			put " ttitle("
			put /program taxis_title_output
			put ") "
		}
		put " " taxisTitleResults " "
	}
	if taxis.ck_noscale {
		put " tscale(off) "
	}

	if taxisScaleResults.isneq("") {
		put " tscale(" taxisScaleResults ") "
	}
	
	if taxisLineResults.isneq("") {
		put " tscale(" taxisLineResults ") "
	}

	put " " taxisReflineResults " "
	if taxisLabelResults {
		put " tlabel("
		put taxisLabelResults
		put ") "
	}
	if taxisMinorLabelResults {
		put " tmtick("
		put taxisMinorLabelResults
		put ") "
	}

	if !H(taxis.bu_adv) & taxisByAdvResults {
		if gr_taxis_advanedByopts_on {
			put " byopts("
		}
		else {
			put " by(, "
		}
		put taxisByAdvResults
		put ") "
	}
	if taxis.ck_alt {
		put " tscale(alt) "
	}
END
