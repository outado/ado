*! version 1.0.0  26may2013
program gsem_estat_summ, rclass sortpreserve
	version 13

	if "`e(cmd)'" != "gsem" {
		error 301
	}

	capture assert e(sample)==0
	if !_rc {
		dis as err "e(sample) information not available"
		exit 498
	}

	syntax  [anything(name=eqlist)] [, 	/// ignored
		EQuation			/// ignored
		*				///
		]	
		
	if "`equation'"!="" {
		dis as txt "after gsem, option equation ignored"
		local equation
	}

	if `:length local eqlist' {
		local vlist `eqlist'
	}
	else {
		local vlist `"`e(depvar)' `: colna e(b)'"'
		local vlist : list uniq vlist
		local vlist : subinstr local vlist "bn." ".", all
		local Llist `"`e(REspec)' _cons"'
		local vlist : list vlist - Llist
		fvrevar `vlist', list
		confirm numeric variable `r(varlist)' 
	}
	estat_summ `vlist' , `options'
	return add
end

exit
