*! version 1.0.2  02jun2014
program gsem_p, sortpreserve
	version 13

	tempname tname
	capture noisily Predict `tname' `0'
	local rc = c(rc)
	capture drop `tname'*
	exit `rc'
end

program Predict
	if e(mecmd) == 1 {
		local MEOPTS stdp
	}
	local mip = c(max_intpoints)
	gettoken TNAME 0 : 0
	syntax  anything(id="stub* or newvarlist") 	///
		[if] [in] [,				///
		mu					/// statistics
		pr					///
		xb					///
		`MEOPTS'				///
		LATent					///
		eta					///
		LATent1(string)				///
		se(string)				///
		NOOFFset				/// options
		FIXEDonly				///
		MEANs					///
		MODEs					///
		Outcome(string)				///
		EQuation(string)			/// NOT documented
		INTPoints(numlist int max=1 >0 <=`mip')	///
		ITERate(numlist int max=1 >0)		///
		TOLerance(numlist max=1 >=0)		///
		LOG					/// NOT documented
	]

	if "`intpoints'" == "1" {
		di as err "invalid intpoints() option;"
		di as err "intpoints(1) is not allowed by predict"
		exit 198
	}

	CheckXB, `xb'

	// parse statistics

	if `:length local equation' {
		if `:length local outcome' {
			opts_exclusive "outcome() equation()"
		}
		local outcome : copy local equation
	}

	local STAT	`mu'		///
			`pr'		///
			`xb'		///
			`stdp'		///
			`latent'	///
			`eta'
	opts_exclusive "`STAT'"
	if `:length local latent1' {
		local STAT : list STAT - latent
		opts_exclusive "`STAT' latent()"
		local STAT latent
		local latent latent()
	}
	if `:length local outcome' {
		opts_exclusive "outcome() `latent'"
	}
	if "`STAT'" != "latent" & `:length local se' {
		di as err "option se() requires the latent option"
		exit 198
	}
	if "`STAT'" == "" {
		di as txt "(option mu assumed)"
		local STAT mu
	}

	// parse options

	opts_exclusive "`fixedonly' `latent'"
	opts_exclusive "`fixedonly' `means' `modes'"
	opts_exclusive "`xb' `means' `modes'"
	if "`STAT'" == "xb" {
		local STAT eta
		local fixedonly fixedonly
	}
	else if e(k_hinfo) == 0 {
		local fixedonly fixedonly
	}
	local EBTYPE `means' `modes'
	if "`EBTYPE'" == "" {
		local EBTYPE means
	}

	// postestimation sample

	tempname touse
	mark `touse' `if' `in'

	if "`STAT'" == "stdp" {
		if `:length local outcome' {
			capture _ms_unab outcome : `outcome'
		}
		if `:length local outcome' {
			local depvar : copy local outcome
		}
		else {
			if substr("`e(family1)'",1,4) == "mult" {
				local depvar : coleq e(b)
			}
			else {
				local depvar "`e(depvar)'"
			}
			gettoken depvar : depvar
		}
		if e(mecmd) == 0 {
			capture local vlabel : variable label `depvar'
			if c(rc) | !`:length local vlabel' {
				local vlabel : copy local depvar
			}
			local vlabel `" (`vlabel')"'
		}
		if strpos("`anything'", "*") {
			_stubstar2names `anything', nvars(1)
		}
		else {
			gsem_newvarlist `anything', nvars(1)
		}
		local typ "`s(typlist)'"
		local var "`s(varlist)'"
		local empty 0
		local match 0
		forval i = 1/`e(k_yinfo)' {
		    if "`e(yinfo`i'_name)'" == "`depvar'" {
			if "`e(yinfo`i'_finfo_family)'" == "ordinal" {
			    if "`e(yinfo`i'_xvars)'" == "" {
				local empty 1
			    }
			}
			local match 1
			continue, break
		    }
		}
		if `empty' {
			gen `typ' `var' = 0
		}
		else {
			_predict `typ' `var', stdp equation(`outcome')
		}
		if e(k_hinfo) {
			local fixmsg ", fixed portion only"
		}
		if e(xtcmd) == 1 {
			label var `var' ///
			"S.E. of the linear prediction of `e(depvar)'"
		}
		else {
			label var `var' ///
			"S.E. of the linear prediction`vlabel'`fixmsg'"
		}
		exit
	}

	// All the following mata functions produce a local macro named
	// VARLIST that contains the list of generated variables.

	local offset = "`nooffset'" == ""

	if "`fixedonly'" != "" {
		if "`STAT'" == "eta" {
			mata: _gsem_predict_xb(	"`TNAME'",		///
						"`anything'",		///
						"`touse'",		///
						`offset',		///
						"`outcome'")
		}
		else {
			mata: _gsem_predict_mu(	"`TNAME'",		///
						"`anything'",		///
						"`touse'",		///
						`offset',		///
						"`STAT'" == "pr",	///
						"`outcome'")
		}
		MISSMSG `VARLIST'
		exit
	}

	// The following predictions use empirical Bayes' estimates.

	local log = "`log'" != ""

	capture checkestimationsample
	if c(rc) {
		di as err "{p 0 0 2}"
		di as err "data have changed since estimation;{break}"
		di as err ///
"prediction of empirical Bayes `EBTYPE' requires the original " ///
"estimation data"
		di as err "{p_end}"
		exit 459
	}
	tempname esample
	quietly gen byte `esample' = e(sample)

	if "`STAT'" == "latent" {
		mata: _gsem_predict_latent(	"`TNAME'",		///
						"`anything'",		///
						"`se'",			///
						"`esample'",		///
						"`EBTYPE'",		///
						"`latent1'",		///
						"`intpoints'",		///
						"`iterate'",		///
						"`tolerance'",		///
						`log')
		capture assert `touse' == `esample'
		if c(rc) {
			FILL `touse' `VARLIST'
		}
		MISSMSG `VARLIST'
		exit
	}

	if "`STAT'" == "pr" {
		local prok 0
		local kdv = e(k_dv)
		forval i = 1/`kdv' {
			if inlist("`e(family`i')'",	"bernoulli",	///
							"ordinal",	///
							"multinomial") {
				local prok 1
				continue, break
			}
		}
		if (e(is_me)) {
			if inlist("`e(family)'",	"bernoulli",	///
							"ordinal",	///
							"multinomial") {
				local prok 1
			}
		}
		if `prok' == 0 {
			di as err "option pr not allowed;"
			di as err "{p 0 0 2}"
			di as err "no depvars were specified using"
			di as err "family(bernoulli),"
			di as err "family(ordinal),"
			di as err "or family(multinomial)"
			di as err "{p_end}"
			exit 198
		}
	}

	quietly d, varlist
	local sortlist `"`r(sortlist)'"'

	mata: _gsem_predict_latent(	"`TNAME'",		///
					"double `TNAME'_*",	///
					"",			///
					"`esample'",		///
					"`EBTYPE'",		///
					"",			///
					"`intpoints'",		///
					"`iterate'",		///
					"`tolerance'",		///
					`log')
	capture assert `touse' == `esample'
	if c(rc) {
		FILL `touse' `VARLIST'
	}
	local LATENT : copy local VARLIST

	if "`sortlist'" != "" {
		sort `sortlist'
	}

	if "`STAT'" == "eta" {
		mata: _gsem_predict_lxb("`TNAME'",		///
					"`anything'",		///
					"`touse'",		///
					`offset',		///
					"`outcome'",		///
					"`LATENT'")
		MISSMSG `VARLIST'
		exit
	}

	mata: _gsem_predict_lmu("`TNAME'",		///
				"`anything'",		///
				"`touse'",		///
				`offset',		///
				"`STAT'" == "pr",	///
				"`outcome'",		///
				"`LATENT'")
	MISSMSG `VARLIST'
end

program MISSMSG
	tempname touse
	quietly gen byte `touse' = 1
	markout `touse' `0'
	quietly count if !`touse'
	if r(N) {
		di as txt "(`r(N)' missing values generated)"
	}
end

program FILL
	gettoken touse 0 : 0
	foreach var of local 0 {
		local gvars : char `var'[gvars]
		if "`gvars'" != "" {
			quietly bysort `gvars' (`var') : ///
			replace `var' = `var'[1] if `touse'
		}
		quietly replace `var' = . if !`touse'
	}
end

program CheckXB
	syntax [, xb]
	if e(mecmd) == 1 {
		exit
	}
	if "`xb'" == "" {
		exit
	}
	di as err "option xb not allowed;"
	di as err "{p 0 0 2}"
	di as err "For linear predictions use the {opt eta} option."
	if e(k_hinfo) != 0 {
		di as err "{break}"
		di as err "For linear predictions that treat all latent"
		di as err "variables as fixed at zero, use options"
		di as err "{opt eta} and {opt fixedonly}."
	}
	di as err "{p_end}"
	exit 198
end

exit
