*! version 1.1.3  15mar2014
program gsem_parse, rclass
	version 13
	local vv : di "version " string(_caller()) ", missing:"

	// syntax:
	//	<mname> [, touse()] : <pathlist> [if] [in] [, options]

	_on_colon_parse `0'
	local 0 `"`s(before)'"'

	syntax name(name=GSEM) , TOUSE(name) [BYTOUSE(name) NOSTART]

	local byvar : length local bytouse
	if `byvar' {
		confirm var `bytouse'
	}

	if "`nostart'" != "" {
		local NOSTART nostart noest
	}

	confirm new var `touse'

	local ZERO `"`s(after)'"'
	mata: st_sem_bind_eqns()

	local 0 : copy local ZERO
	syntax [anything(equalok)] [if] [in] [, *]
	local ZERO `"`anything' `if' `in', `options'"'

	local DVOPTS					///
			Family(passthru)		///
			Link(passthru)			///
			EXPosure(passthru)		///
			OFFset(passthru)		///
			NOCONStant	CONStant	///
							 // blank

	local DIOPTS					///
			noHeader			///
			noLegend			///
			notable				///
							 // blank

	local LATOPTS					///
			LATent(passthru)		///
			NOCAPSlatent CAPSlatent		///
							 // blank

	local OPTS					///
			VCE(passthru)			///
			TECHnique(passthru)		///
			CONSTraints(string)		///
			FROM(string)			///
			METHod(string)			///
			FVSTANDard			///
			MECMD				/// NODOC
			XTCMD				/// NODOC
			NOANCHOR	ANCHOR		///
			NOESTimate			///
			NOLISTwise	LISTwise	///
			NOLOg		LOg		///
							 // blank

	
	local EVALTYPE					///
			EVALTYPE(passthru)		/// NODOC
			DNUMERICAL			///
							 // blank

	local COMMON					///
			COVSTRucture(passthru)		///
			COVariance(passthru)		///
			RELiability(passthru)		///
			STARTGrid(passthru)		///
			STARTValues(passthru)		///
			VARiance(passthru)		///
			FORCENOANchor			///
			NOSTART				///
			`EVALTYPE'			///
							 // blank

	_parse expand EQ GL : ZERO,			///
		common(					///
			`DIOPTS'			///
			`LATOPTS'			///
			`OPTS'				///
			`COMMON'			///
		)

	// prelim parse of the path specifications; make global the 'if'/'in'
	// specifications and all non-equation-specific options

	local MEOPTS mecmd xtcmd
	if "`:list MEOPTS & GL_op'" != "" {
		local MECMD mecmd
	}

	if `EQ_n' != 1 {
		local CHECK COLlinear noASIS
	}

	forval i = 1/`EQ_n' {
		local 0 : copy local EQ_`i'
		syntax [anything(equalok)] [if] [in] [,	///
			`DVOPTS'			///
			`CHECK'				///
			*				///
		]
		Check `collinear' `asis'
		ShortCuts, `MECMD' `family' `link' `options'
		local family	`"`s(family)'"'
		local link	`"`s(link)'"'
		local options	`"`s(options)'"'
		opts_exclusive "`noconstant' `constant'"
		opts_exclusive "`offset' `exposure'"
		local offset `offset' `exposure'
		if `:length local if' {
			if `:length local GL_if' {
				di as err ///
				"multiple 'if' specifications not allowed"
				exit 198
			}
			local GL_if : copy local if
		}
		if `:length local in' {
			if `:length local GL_in' {
				di as err ///
				"multiple 'in' specifications not allowed"
				exit 198
			}
			local GL_in : copy local in
		}
		local EQ_`i' `"`anything', `family' `link' `noconstant' `offset'"'
		if `:length local options' {
			local GL_op `"`GL_op' `options'"'
		}
	}

	if `:length local GL_in' & `byvar' {
		di as error "in may not be combined with by"
		exit 190 
	}

	local neq = `EQ_n'
	if `neq' > 1 {
		_parse canon 0 : EQ GL, drop
		syntax anything(equalok				///
				name=PATHLIST			///
				id="path specification")	///
			[if] [in] [fw pw iw] [,	*]
		local GL_if : copy local if
		local GL_in : copy local in
	}
	else {
		local PATHLIST `"(`EQ_1')"'
		local options : copy local GL_op
	}

	while `:length local PATHLIST' {
		gettoken path PATHLIST : PATHLIST, match(par)
		if "`path'" != "" {
			local pathlist `"`pathlist' (`path')"'
		}
	}

	if `:length local pathlist' == 0 {
		di as err "invalid model specification;"
		di as err "no paths specified"
		exit 322
	}

	_get_diopts diopts options, `options'
	local 0 `", `NOSTART' `options'"'
	syntax [, `DIOPTS' *]
	local diopts	`diopts'	///
			`header'	///
			`legend'	///
			`table'		///
					 // blank

	// combine the contents of the following options if they are specified
	// multiple times:
	// 	latent()
	_parse combop options : options, opsin option(LATENT)
	local 0 `", `options'"'
	syntax [, `LATOPTS' *]
	opts_exclusive `"`nocapslatent' `capslatent'"'
	opts_exclusive `"`latent' `capslatent'"'
	local latent `nocapslatent' `capslatent' `latent'

	// parse globally specified equation options
	local 0 `", `options'"'
	syntax [, `DVOPTS' `OPTS' `EVALTYPE' *]
	if !inlist("`method'", "", "ml") {
		di as err "invalid method() option;"
		di as err "option `method' not allowed"
		exit 198
	}
	if "`xtcmd'" != "" {
		local mecmd mecmd
	}
	if "`mecmd'" != "" {
		local fvstandard fvstandard
	}
	ShortCuts, `mecmd' `family' `link' `options'
	local family	`"`s(family)'"'
	local link	`"`s(link)'"'
	local options	`"`s(options)'"'
	opts_exclusive "`offset' `exposure'"
	opts_exclusive "`noconstant' `constant'"
	opts_exclusive "`noanchor' `anchor'"
	opts_exclusive "`nolistwise' `listwise'"
	opts_exclusive "`nolog' `log'"
	opts_exclusive "`evaltype' `dnumerical'"
	local offset `offset' `exposure'
	local dvopts `"`family' `link' `offset' `noconstant'"'

	if "`dnumerical'" != "" {
		local evaltype evaltype(gf0)
	}

	mlopts mlopts options, `options' `technique'
	local collinear "`s(collinear)'"
	local mlopts : list mlopts - collinear
	local options	`noanchor'	///
			`listwise'	///
			`collinear'	///
			`evaltype'	///
			`options'
	ChkFrom `from'

	mark `touse' `GL_if' `GL_in'

	if `byvar' {
		qui replace `touse' = 0 if !`bytouse'
	}

	_vce_parse `touse',		///
		opt(OIM OPG Robust)	///
		argopt(CLuster)		///
		: , `vce'
	local vce `"`r(vceopt)'"'

	qui count if `touse'
	if (c(N)>0 & r(N)==0) error 2000
	if (c(N)>0 & r(N)==1) error 2001

	// This function consumes the following local macros:
	//
	//	fvstandard
	//	latent
	//	dvopts
	//	pathlist
	//	from
	//	nolog
	//	noestimate
	//	mecmd
	//	xtcmd
	//	vce
	//	technique
	//	constraints
	//	options
	//
	// See the source _gsem_return.mata for details.

	capture mata: rmexternal("`GSEM'")
	capture drop `GSEM'*

	foreach var of varlist * {
		local ORIGBASE `ORIGBASE' (`var' `:char `var'[_fv_base]')
	}

nobreak {
capture noisily break {

	`vv' mata: st_gsem_parse("`GSEM'", "`touse'")

} // capture noisily break
	local rc = c(rc)

	SetBase `ORIGBASE'

} // nobreak
	if `rc' {
		exit `rc'
	}

	return add
	return local mlopts	`"`nolog' `mlopts'"'
	return local mlvce	`"`vce'"'
	return local nolog	`"`nolog'"'
	return local diopts	`"`diopts'"'
	return scalar estimate = `"`noestimate'"' == ""
end

program Check
	if "`1'" != "" {
		di as err "option `1' not allowed;"
		di as err "{p 0 0 2}"
		di as err "The `1' option is global and"
		di as err "is not allowed as an option in a path"
		di as err "specification."
		di as err "{p_end}"
		exit 198
	}
end

program ShortCuts, sclass
	syntax [,	Family(passthru)	///
			Link(passthru)		///
			mecmd			///
			cloglog			/// bernoulli
			logit			/// bernoulli
			probit			/// bernoulli
			ocloglog		/// ordinal
			ologit			/// ordinal
			oprobit			/// ordinal
			mlogit			/// multinomial
			REGress			/// gaussian
			poisson			/// poisson
			nbreg			/// nbreg
			gamma			/// gamma
			*			///
	]

	local cmd	`cloglog'	///
			`logit'		///
			`probit'	///
			`ocloglog'	///
			`ologit'	///
			`oprobit'	///
			`mlogit'	///
			`regress'	///
			`poisson'	///
			`nbreg'		///
			`gamma'

	if "`cmd'" != "" {
		if "`mecmd'" != "" {
			local 0 `", `cmd'"'
			syntax [, NOOPT]
			error 198	// [sic]
		}

		opts_exclusive "`cmd'"
		opts_exclusive "`cmd' `family'"
		opts_exclusive "`cmd' `link'"
		if "`cmd'" == "cloglog" {
			local family	family(bernoulli)
			local link	link(cloglog)
		}
		else if "`cmd'" == "logit" {
			local family	family(bernoulli)
			local link	link(logit)
		}
		else if "`cmd'" == "probit" {
			local family	family(bernoulli)
			local link	link(probit)
		}
		else if "`cmd'" == "ocloglog" {
			local family	family(ordinal)
			local link	link(cloglog)
		}
		else if "`cmd'" == "ologit" {
			local family	family(ordinal)
			local link	link(logit)
		}
		else if "`cmd'" == "oprobit" {
			local family	family(ordinal)
			local link	link(probit)
		}
		else if "`cmd'" == "mlogit" {
			local family	family(multinomial)
			local link	link(logit)
		}
		else if "`cmd'" == "regress" {
			local family	family(gaussian)
			local link	link(identity)
		}
		else if "`cmd'" == "poisson" {
			local family	family(poisson)
			local link	link(log)
		}
		else if "`cmd'" == "nbreg" {
			local family	family(nbinomial mean)
			local link	link(log)
		}
		else if "`cmd'" == "gamma" {
			local family	family(gamma)
			local link	link(log)
		}
	}
	sreturn local family	`"`family'"'
	sreturn local link	`"`link'"'
	sreturn local options	`"`options'"'
end

program ChkFrom
	gettoken FROM 0 : 0, parse(",") bind
	capture syntax [, skip]
	if c(rc) {
		di as err "invalid from() option;"
		syntax [, skip]
		exit 198	// [sic]
	}
end

program MakeCns, eclass
	gettoken b cnslist : 0
	ereturn post `b'
	makecns `cnslist', r
end

program BadBase
	di as err "invalid base specification;"
	di as err "'`0'' found where base level factor variable expected"
	exit 198
end

program SetBase
	gettoken set 0 : 0, match(par)
	while `:length local set' {
		gettoken var set : set
		char `var'[_fv_base] `set'
		gettoken set 0 : 0, match(par)
	}
end

exit
