{smcl}
{* *! version 1.0.1  31may2013}{...}
{vieweralsosee "[SEM] gsem postestimation" "mansection SEM gsempostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[SEM] gsem reporting options" "help gsem_reporting_options"}{...}
{viewerjumpto "Description" "gsem_postestimation##description"}{...}
{viewerjumpto "Remarks" "gsem_postestimation##remarks"}{...}
{title:Title}

{p2colset 5 34 36 2}{...}
{p2col:{manlink SEM gsem postestimation} {hline 2}}Postestimation tools for
	gsem{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following are the postestimation commands that you can use after
estimation by {cmd:gsem}:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
{synopt :{helpb gsem_command:gsem, coeflegend}}display {cmd:_b[]} notation{p_end}

{synopt :{helpb sem_estat_eform:estat eform}}display exponentiated
coefficients{p_end}
INCLUDE help post_estatic

{synopt :{helpb sem_lrtest:lrtest}}likelihood-ratio tests {p_end}
{synopt :{helpb sem_test:test}}Wald tests {p_end}
{synopt :{helpb sem_lincom:lincom}}linear combination of parameters {p_end}
{synopt :{helpb sem_nlcom:nlcom}}nonlinear combination of parameters {p_end}
{synopt :{helpb sem_testnl:testnl}}Wald tests of nonlinear hypotheses {p_end}

INCLUDE help post_estatsum
INCLUDE help post_estatvce

{synopt :{helpb gsem_predict:predict}}generalized linear predictions, etc. {p_end}

{synopt :{helpb margins}}marginal means, predictive margins, and marginal effects{p_end}
{synopt :{helpb contrast}}contrasts and linear hypothesis tests{p_end}
{synopt :{helpb pwcompare}}pairwise comparisons{p_end}

{synopt :{helpb estimates:estimates}}cataloging estimation results{p_end}
{synoptline}
{p2colreset}{...}

{pstd}
For a summary of postestimation features, see {manlink SEM intro 7}.


{marker remarks}{...}
{title:Remarks}

{pstd}
This manual entry concerns {cmd:gsem}.
For information on postestimation features available after {cmd:sem},
see {helpb sem postestimation:[SEM] sem postestimation}.
{p_end}
