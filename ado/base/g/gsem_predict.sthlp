{smcl}
{* *! version 1.0.3  30jul2013}{...}
{viewerdialog predict "dialog gsem_p"}{...}
{vieweralsosee "[SEM] predict after gsem" "mansection SEM predict after gsem"}{...}
{vieweralsosee "[SEM] intro 7" "mansection SEM intro7"}{...}
{vieweralsosee "[SEM] methods and formulas for gsem" "mansection SEM methodsandformulasforgsem"}{...}
{findalias asgsemoirt}{...}
{findalias asgsemtirt}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[SEM] gsem" "help gsem_command"}{...}
{vieweralsosee "[SEM] gsem postestimation" "help gsem_postestimation"}{...}
{viewerjumpto "Syntax" "gsem_predict##syntax"}{...}
{viewerjumpto "Menu" "gsem_predict##menu"}{...}
{viewerjumpto "Description" "gsem_predict##description"}{...}
{viewerjumpto "Options" "gsem_predict##options"}{...}
{viewerjumpto "Remarks" "gsem_predict##remarks"}{...}
{viewerjumpto "Examples" "gsem_predict##examples"}{...}
{title:Title}

{p2colset 5 33 35 2}{...}
{p2col:{manlink SEM predict after gsem} {hline 2}}Generalized linear
predictions, etc.{p_end}
{p2colreset}{...}


{marker syntax}{...}
{title:Syntax for predict}

{p 8 16 2}
{cmd:predict} {dtype}
{c -(}{it:stub}{cmd:*}{c |}{it:{help newvarlist}}{c )-}
{ifin} [{cmd:,} {it:{help gsem_predict##options_obs_endog:options_obs_endog}}]

{p 8 16 2}
{cmd:predict} {dtype}
{c -(}{it:stub}{cmd:*}{c |}{it:{help newvarlist}}{c )-}
{ifin} [{cmd:,} {it:{help gsem_predict##options_obs_endog:options_obs_endog}}
                {it:{help gsem_predict##options_latent:options_latent}}]


{phang}
The default is to predict observed endogenous variables with empirical
Bayes means predictions of the latent variables.


{marker options_obs_endog}{...}
{synoptset 22 tabbed}{...}
{synopthdr:options_obs_endog}
{synoptline}
{p2coldent:+ {cmd:outcome(}{depvar} [{it:#}]{cmd:)}}specify observed
response variable (default all){p_end}

{synopt :{opt mu}}calculate expected value of {depvar}{p_end}
{synopt :{opt pr}}calculate probability (synonym for {cmd:mu} when mu is a
probability){p_end}
{synopt :{opt eta}}calculate expected value of linear prediction of
{depvar}{p_end}

{synopt :{opt nooffset}}make calculation ignoring Poisson or nbreg offset or
exposure{p_end}
{synoptline}
{synopt :{opt fixedonly}}make calculation setting latent variables to 0{p_end}
{synopt :}{hline 3}or{hline 3}{p_end}
{synopt :{opt means}}use empirical Bayes means of latent variables; the
default{p_end}
{synopt :{opt modes}}use empirical Bayes modes of latent variables{p_end}
{synopt :{opt intpoints(#)}}{it:#} of integration points; default is
{cmd:intpoints(7)} (means only){p_end}
{synopt :{opt tolerance(#)}}tolerance; default is {cmd:tolerance(1.0e-8)}
(means and modes){p_end}
{synopt :{opt iterate(#)}}maximum {it:#} of iterations; default is
{cmd:iterate(1001)} (means and modes){p_end}
{synoptline}
{p 4 6 2}
+ {opt outcome(depvar #)} is allowed only after {cmd:mlogit}, {cmd:ologit},
    and {cmd:oprobit}.  Predicting other generalized responses requires
    specifying only {opt outcome(depvar)}.{break}
   {opt outcome(depvar #)} may also be specified as
   {cmd:outcome(}{it:#}.{it:depvar}{cmd:)}
   or {cmd:outcome(}{it:depvar} {cmd:#}{it:#}{cmd:)}.{break}
   {cmd:outcome(}{it:depvar} {cmd:#3)} means the third outcome value.
   {cmd:outcome(}{it:depvar} {cmd:#3)} would mean the same as
   {cmd:outcome(}{it:depvar} {cmd:4)} if outcomes were 1, 3, and 4.


{marker options_latent}{...}
{synopthdr:options_latent}
{synoptline}
{p2coldent :* {opt latent}}calculate empirical Bayes prediction of all latent variables{p_end}
{p2coldent :* {opth latent(varlist)}}calculate empirical Bayes prediction of specified latent variables{p_end}

{synopt :{cmd:se(}{it:stub}{cmd:*}{c |}{help varlist:{it:newvarlist}}{cmd:)}}calculate standard errors{p_end}

{synopt :{opt means}}use empirical Bayes means of latent variables; the default{p_end}
{synopt :{opt modes}}use empirical Bayes modes of latent variables{p_end}
{synopt :{opt intpoints(#)}}{it:#} of integration points; default is
        {cmd:intpoints(7)} (means only){p_end}
{synopt :{opt tolerance(#)}}tolerance; default is {cmd:tolerance(1.0e-8)} (means and modes){p_end}
{synopt :{opt iterate(#)}}maximum {it:#} of iterations; default is {cmd:iterate(1001)} (means and modes){p_end}
{synoptline}
{p2colreset}{...}
{p 4 6 2}
* Either {cmd:latent} or {cmd:latent()} must be specified to obtain
predictions of latent variables.


{marker menu}{...}
{title:Menu}

{phang}
{bf:Statistics > SEM (structural equation modeling) > Predictions}


{marker description}{...}
{title:Description}

{pstd}
{cmd:predict} is a standard postestimation command of Stata.
This entry concerns use of {cmd:predict} after {cmd:gsem}.
See {helpb sem_predict:[SEM] predict after sem} if you fit your model with
{cmd:sem}.

{pstd}
{cmd:predict} after {cmd:gsem} creates new variables containing
observation-by-observation values of estimated observed response variables,
linear predictions of observed response variables, or endogenous or
exogenous latent variables.

{pstd}
Out-of-sample prediction is allowed in three cases:

{phang}
1.  if the prediction does not involve latent variables, or

{phang}
2.  if the prediction involves latent variables, directly or indirectly,
option {cmd:fixedonly} is specified, or

{phang}
3.  if the prediction involves latent variables, directly or indirectly,
   the model is multilevel and no observational-level latent variables
   are involved.

{pstd}
{cmd:predict} has two ways of specifying the name(s) of the variable(s) to be
created:

{phang3}
{cmd:. predict} {it:stub}{cmd:*,} ...

{pstd}
or

{phang3}
{cmd:. predict} {it:firstname secondname} ...{cmd:,} ...

{pstd}
The first creates variables named {it:stub}{cmd:1}, {it:stub}{cmd:2}, ....
The second creates variables named as you specify.  We strongly recommend using
the {it:stub}{cmd:*} syntax when creating multiple variables because you have
no way of knowing the order in which to specify the individual variable names
to correspond to the order in which {cmd:predict} will make the calculations.
If you use {it:stub}{cmd:*}, the variables will be labeled and you can rename
them.

{pstd}
The second syntax is useful when creating one variable and you specify
either {cmd:outcome()} or {cmd:latent()}.


{marker options}{...}
{title:Options}

{phang}
{cmd:outcome(}{it:depvar} [{it:#}]{cmd:)} and
{cmd:latent}[({it:{help varlist}}{cmd:)}]
    determine what is to be calculated.

{synoptset 32 tabbed}{...}
{synopt :neither specified}predict all observed response variables{p_end}
{synopt :{cmd:outcome(}{it:depvar} [{it:#}]{cmd:)} specified}predict specified observed response variable{p_end}
{synopt :{cmd:latent} specified}predict all latent variables{p_end}
{synopt :{opt latent(varlist)} specified}predict specified latent variables{p_end}
{p2colreset}{...}

{pmore}
    If you are predicting latent variables, both empirical Bayes means 
    and modes are available; see options 
    {cmd:means}, {cmd:modes}, 
    {opt intpoints(#)}, 
    {opt tolerance(#)}, and 
    {opt iterate(#)} below.

{pmore} 
    If you are predicting observed response variables, you can obtain
    g^{-1}({bf:x} beta hat) or {bf:x}beta hat; see options {cmd:mu} and
    {cmd:eta} below.  Predictions can include latent variables or treat them as
    0; see option {cmd:fixedonly}.  If predictions include latent variables,
    then just as when predicting latent variables, both means and modes are
    available; see options 
    {cmd:means}, {cmd:modes}, 
    {opt intpoints(#)}, 
    {opt tolerance(#)}, and 
    {opt iterate(#)}.

{phang}
{cmd:mu} and {cmd:pr}
    specify that g^{-1}({bf:x}beta hat) be calculated,
    the inverse-link
    of the expected value of the linear predictions.
    {bf:x} by default
    contains predictions of latent variables.
    {cmd:pr} is a synonym
    for {cmd:mu} if response variables are multinomial, ordinal, or
    Bernoulli.  Otherwise, {cmd:pr} is not allowed.

{phang} 
{cmd:eta}
    specifies that {bf:x} beta hat be calculated, the expected value of the
linear prediction.  {bf:x} by default contains predictions of latent variables.

{phang}
{cmd:fixedonly} and {cmd:nooffset}
    are relevant only if observed response variables are being predicted.

{pmore}
    {cmd:fixedonly} concerns predictions of latent variables used
    in the prediction of observed response variables.  {cmd:fixedonly}
    specifies latent variables be treated as 0, and thus
    only the fixed-effects part of the model is used to produce the
    predictions.

{pmore}
    {cmd:nooffset}
    is relevant only if option {cmd:offset()} or {cmd:exposure()} were
    specified at estimation time.
    {cmd:nooffset} specifies that {cmd:offset()} or {cmd:exposure()} 
    be ignored, thus producing predictions as if all subjects had equal
    exposure.

{phang}
{cmd:means}, {cmd:modes},
{opt intpoints(#)},
{opt tolerance(#)}, and
{opt iterate(#)}
    specify what predictions of the latent variables are to be calculated.

{pmore}
    {cmd:means} and {cmd:modes} specify that empirical Bayes means or
    modes be used.  Means are the default.

{pmore}
    {opt intpoints(#)} specifies the number of numerical integration
    points and is relevant only in the calculation of empirical Bayes means.
    {opt intpoints()} defaults to the number of integration points specified
    at estimation time or to {cmd:intpoints(7)}.

{pmore}
    {opt tolerance(#)} is relevant for the calculation of empirical
    Bayes means and modes.  It specifies the convergence tolerance.
    It defaults to the value specified at estimation time with {cmd:gsem}'s
    {opt adaptopts()} or to {cmd:tolerance(1e-8)}.

{pmore}
    {opt iterate(#)} is relevant for the calculation of
    empirical Bayes means and modes. It specifies the maximum number
    of iterations to be performed in the calculation of each integral.
    It defaults to the value specified at estimation time
    with {cmd:gsem}'s {cmd:adaptopts()} or to {cmd:tolerance(1e-8)}.


{marker remarks}{...}
{title:Remarks}

{pstd}
See {manlink SEM intro 7}, {findalias gsemoirt}, and {findalias gsemtirt}.


{marker examples}{...}
{title:Examples}

{pstd}Setup{p_end}
{phang2}{cmd:. webuse gsem_cfa}{p_end}
{phang2}{cmd:. gsem (MathAb -> (q1-q8)@b), logit var(MathAb@1)}{p_end}

{pstd}Predicted probability of success for all observed response variables{p_end}
{phang2}{cmd:. predict pr*, pr}{p_end}

{pstd}Empirical Bayes mean prediction of the latent variable{p_end}
{phang2}{cmd:. predict ability, latent(MathAb)}{p_end}
