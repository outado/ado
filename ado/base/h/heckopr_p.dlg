/*
  heckoprobit_predict

*! VERSION 1.0.6  03jun2013


*/
VERSION 13.0

INCLUDE _std_large
DEFINE _dlght 380
INCLUDE header

HELP hlp1, view("help heckoprobit postestimation")
RESET res1


DIALOG main, label("predict - Prediction after estimation") tabtitle("Main")
BEGIN
  TEXT tx_vars			_lft	_top	_vlwd	.,		///
	label("New variable names or variable stub* :")
  EDIT ed_vars			@	_ss	@	.,		///
	label("New variable names or variable stub*")
  TEXT tx_var			_lft	_top	@	.,		///
	label("New variable name:")
  EDIT ed_var			@	_ss	_vnwd	.,		///
	label("New variable name") max(32)

  TEXT tx_type			_vlsep	_top	_vlr	.,		///
	label("New variable type:")
  COMBOBOX cb_type		@	_ss	@	.,		///
	dropdownlist							///
	contents(type) default(c(type))					///
	label("Type")

  GROUPBOX gb_opts		_lft	_ls	_iwd	_ht25,		///
	label("Produce:")
  RADIO rb_pmargin		_ilft	_ss	_ibwd	., first	///
	option(pmargin)							///
	clickon("script oneon_p")					///
	label("Probabilities (specify as many variables as number of outcomes)")
  RADIO rb_p1			@	+55	@	.,		///
	option(p1)							///
	clickon("script oneone")					///
	label("Bivariate probabilities of levels with selection")
  RADIO rb_p0			@	_ss	@	.,		///
	option(p0)							///
	clickon("script oneone")					///
	label("Bivariate probabilities of levels with no selection")
  RADIO rb_pcond1		@	_ss	@	.,		///
	option(pcond1)							///
	clickon("script oneone")					///
	label("Probabilities of levels conditional on selection")
  RADIO rb_pcond0		@	_ss	@	.,		///
	option(pcond0)							///
	clickon("script oneone")					///
	label("Probabilities of levels conditional on no selection")
  RADIO rb_psel			@	_ss	@	.,		///
	option(psel)							///
	clickon("script manyon")					///
	label("Probability (specify one variable)")
  RADIO rb_xb			_ilft	_ss	_ibwd	.,		///
	option(xb)							///
	clickon("script oneon")						///
	label("Linear prediction (xb)")
  RADIO rb_stdp			@	_ss	@	.,		///
	option(stdp)							///
	clickon("script oneon")						///
	label("Standard error of linear prediction")
  RADIO rb_xbsel		@	_ss	@	.,		///
	option(xbsel)							///
	clickon("script oneon")						///
	label("Linear prediction for selection equation")
  RADIO rb_stdpsel		@	_ss	@	.,		///
	option(stdpsel)							///
	clickon("script oneon")						///
	label("Standard error of the linear prediction for selection equation")
  RADIO rb_score		@	_ss	@	., last		///
	option(scores)							///
	clickon("script score_on")					///
	label("Equation-level scores")

  TEXT tx_outcome		_indent2 -220	_iwd	.,		///
	label("For outcome (value of dependent variable):")
  EDIT ed_outcome		@	_ss	_vnwd	.,		///
	option(outcome)							///
	label("For outcome (value of dependent variable)")

  CHECKBOX ck_nooffset		_lft	+240	_iwd	.,		///
	option(nooffset)						///
	label("Ignore offset variable (if any)")
END

INCLUDE _p_gentype_sc
INCLUDE ifin
INCLUDE _type_list_fd

SCRIPT score_on
BEGIN
	script manyon
	main.ck_nooffset.disable
END

SCRIPT spin_on
BEGIN
	main.sp_select.enable
END

SCRIPT spin_off
BEGIN
	main.sp_select.disable
END


SCRIPT manyon
BEGIN
	script manyvars
	script eqoff
	main.ck_nooffset.enable
END

SCRIPT oneon
BEGIN
	script onevar
	script eqoff
	main.ck_nooffset.enable
END

SCRIPT oneon_p
BEGIN
	script onevar
	script eqon
	main.ck_nooffset.enable
END

SCRIPT manyvars
BEGIN
	main.tx_vars.show
	main.ed_vars.show
	main.tx_var.hide
	main.ed_var.hide
END

SCRIPT onevar
BEGIN
	main.tx_vars.show
	main.ed_vars.show
	main.tx_var.hide
	main.ed_var.hide
END

SCRIPT eqon
BEGIN
	main.tx_outcome.enable
	main.ed_outcome.enable
END

SCRIPT eqoff
BEGIN
	main.tx_outcome.disable
	main.ed_outcome.disable
END


PROGRAM command
BEGIN
	put "predict "
	if main.cb_type.isdefault() {
		if main.rb_pmargin | main.rb_score {
			require main.ed_vars
			put main.ed_vars " "
		}
		if !main.rb_pmargin & !main.rb_score {
			require main.ed_vars
			put main.ed_vars " "
		}
	}
	if ! main.cb_type.isdefault() {
		put main.cb_type " " "("
		if main.rb_pmargin | main.rb_score {
			require main.ed_vars
			put main.ed_vars
		}
		if !main.rb_pmargin & !main.rb_score {
			require main.ed_vars
			put main.ed_vars
		}
		put ") "
	}

	put " " /program ifin_output
	beginoptions
		option radio(main rb_pmargin rb_p1 rb_p0 rb_pcond1 rb_pcond0 rb_psel rb_xb rb_stdp rb_xbsel rb_stdpsel rb_score)
		optionarg main.ed_outcome
		option main.ck_nooffset
	endoptions
END
