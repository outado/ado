*! version 1.0.9  03dec2012
program define import
	version 12

	gettoken subcmd 0 : 0, parse(" ,")

	if `"`subcmd'"' == "delim" {
		ImpDelim `0'
	}
	else if `"`subcmd'"' == "delimi" {
		ImpDelim `0'
	}
	else if `"`subcmd'"' == "delimit" {
		ImpDelim `0'
	}
	else if `"`subcmd'"' == "delimite" {
		ImpDelim `0'
	}
	else if `"`subcmd'"' == "delimited" {
		ImpDelim `0'
	}
	else if `"`subcmd'"' == "exc" {
		ImpExcel `0'
	}
	else if `"`subcmd'"' == "exce" {
		ImpExcel `0'
	}
	else if `"`subcmd'"' == "excel" {
		ImpExcel `0'
	}
	else if `"`subcmd'"' == "hav" {
		ImpHaver `0'
	}
	else if `"`subcmd'"' == "have" {
		ImpHaver `0'
	}
	else if `"`subcmd'"' == "haver" {
		ImpHaver `0'
	}
	else if `"`subcmd'"' == "sasxport" {
		ImpSasxport `0'
	}
	else {
		display as error `"import: unknown subcommand "`subcmd'""'
		exit 198
	}

end

program ImpDelim
	version 13

	import_delimited `0'
end

program ImpExcel
	version 12

	scalar ImpExcelCleanUp = -1
	capture noi import_excel `0'

	nobreak {
		local rc = _rc
		if `rc' {
			if ImpExcelCleanUp >= 0 {
				mata : import_excel_cleanup()
			}
		}
	}
	scalar drop ImpExcelCleanUp
	exit `rc'
end

program ImpHaver
	version 12

	capture noi import_haver `0'

	nobreak {
		local rc = _rc
		if `rc' {
			mata : import_haver_cleanup()
		}
	}
	exit `rc'
end

program ImpSasxport
	capture syntax [anything] , Describe [*]
	if _rc==0 {
		fdadescribe `anything', `options'
		exit
	}

	fdause `0'
end

version 12.0
mata:

void import_excel_cleanup()
{
	_xlbkrelease(st_numscalar("ImpExcelCleanUp"))
}

void import_haver_cleanup()
{
	if (st_global("c(os)") == "Windows") {
		(void) _haver_set_aggregation(0)
		(void) _haver_close_db()
	}
}


end

