*! version 1.0.12  08jul2014
program meglm, eclass byable(onecall) prop(or irr mi)
	version 13
	local vv : di "version " string(_caller()) ", missing:"

	if _by() {
		local by "by `_byvars'`_byrc0':"
	}
	if replay() {
		if "`e(cmd)'" != "meglm" & "`e(cmd2)'" != "meglm" {
			error 301
		}
		if _by() {
			error 190
		}
		_me_display `0'
		exit
	}
	
	capture noisily `vv' `by' Estimate `0'
	local rc = _rc
	exit `rc'
end

program Estimate, sortpreserve eclass byable(recall)
	version 13
	local vv : di "version " string(_caller()) ", missing:"
	
	if _by() {
		tempname bytouse
		mark `bytouse'
	}
	
	local 0_orig `0'
	_parse expand eq opt : 0
	local gopts `opt_op'
	
	forvalues i=1/`=`eq_n'-1' {
		local 0 `eq_`i''
		syntax [anything] [if] [in] [, or irr eform EFORM1(passthru) *]
		local eqs `eqs' `anything' `if' `in', `options' ||
		local dixtra `dixtra' `or' `irr' `eform' `eform1'
	}
	
	local 0 `eq_`eq_n''
	syntax [anything] [if] [in] [, *]
	local eqn `anything' `if' `in'
	local 0 , `options' `gopts'
	syntax [anything] [, noTABle noLRtest noGRoup noHEADer noLOg 	///
		or irr eform  EFORM1(passthru) noESTimate COEFLegend * ]
	local dixtra `dixtra' `or' `irr' `eform' `eform1'
	local dixtra : list uniq dixtra
	opts_exclusive "`dixtra'"
	
	_get_diopts diopts opts, `options'
	local diopts `diopts' `table' `lrtest' `group' `header' `log' 	///
		`estimate' `coeflegend' `dixtra'
	local diopts : list uniq diopts
	
	local 0 `eqs' `eqn' , `opts' `log' `estimate' `coeflegend' `dixtra'
	`vv' _me_estimate "`bytouse'" `0'
	_me_display , `diopts'
	
end
exit
