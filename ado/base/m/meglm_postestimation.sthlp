{smcl}
{* *! version 1.0.7  14apr2013}{...}
{viewerdialog predict "dialog meglm_p"}{...}
{viewerdialog estat "dialog meglm_estat"}{...}
{vieweralsosee "[ME] meglm postestimation" "mansection ME meglmpostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] meglm" "help meglm"}{...}
{viewerjumpto "Description" "meglm postestimation##description"}{...}
{viewerjumpto "Special-interest postestimation commands" "meglm postestimation##special"}{...}
{viewerjumpto "Syntax for predict" "meglm postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "meglm postestimation##menu_predict"}{...}
{viewerjumpto "Options for predict" "meglm postestimation##options_predict"}{...}
{viewerjumpto "Syntax for estat group" "meglm postestimation##syntax_estat_group"}{...}
{viewerjumpto "Menu for estat" "meglm postestimation##menu_estat"}{...}
{viewerjumpto "Examples" "meglm postestimation##examples"}{...}
{viewerjumpto "Reference" "meglm postestimation##reference"}{...}
{title:Title}

{p2colset 5 34 36 2}{...}
{p2col :{manlink ME meglm postestimation} {hline 2}}Postestimation tools for meglm{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation command is of special interest after
{cmd:meglm}:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
{synopt :{helpb meglm postestimation##estatgroup:estat group}}summarize
the composition of the nested groups{p_end}
{synoptline}
{p2colreset}{...}

{pstd}
The following standard postestimation commands are also available:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
INCLUDE help post_contrast
INCLUDE help post_estatic
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_estimates
INCLUDE help post_lincom
INCLUDE help post_lrtest
INCLUDE help post_margins
INCLUDE help post_marginsplot
INCLUDE help post_nlcom
{synopt :{helpb meglm postestimation##predict:predict}}predictions, residuals, influence statistics, and other diagnostic measures{p_end}
INCLUDE help post_predictnl
INCLUDE help post_pwcompare
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{p2colreset}{...}


{marker special}{...}
{title:Special-interest postestimation commands}

{pstd}
{cmd:estat group} reports number of groups and minimum, average, and maximum
group sizes for each level of the model.  Model levels are identified by
the corresponding group variable in the data.  Because groups are treated
as nested, the information in this summary may differ from what you would
get if you used the {cmd:tabulate} command on each group variable individually.


{marker syntax_predict}{...}
{marker predict}{...}
{title:Syntax for predict}

{p 4 4 2}
Syntax for obtaining estimated random effects and their standard errors

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} {cmd:,} {{opt remeans}|{opt remodes}}
[{cmd:reses(}{it:newvarsspec}{cmd:)}]


{p 4 4 2}
Syntax for obtaining other predictions

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} 
[{cmd:,} {it:{help meglm_postestimation##statistic:statistic}}
{it:{help meglm_postestimation##options_table:options}}]


{pstd}
{it:newvarsspec} is {it:stub}{cmd:*} or {it:{help newvarlist}}.


{marker statistic}{...}
{synoptset 18 tabbed}{...}
{synopthdr :statistic}
{synoptline}
{syntab :Main}
{synopt :{cmd:mu}}mean response; the default{p_end}
{synopt :{cmd:pr}}synonym for {cmd:mu} for ordinal and binary response models{p_end}
{synopt :{opt fit:ted}}fitted linear predictor{p_end}
{synopt :{cmd:xb}}linear predictor for the fixed portion of the model only{p_end}
{synopt :{cmd:stdp}}standard error of the fixed-portion linear prediction{p_end}
{synopt :{opt res:iduals}}raw residuals; available only with the Gaussian family{p_end}
{synopt :{opt pea:rson}}Pearson residuals{p_end}
{synopt :{opt dev:iance}}deviance residuals{p_end}
{synopt :{opt ans:combe}}Anscombe residuals{p_end}
{synoptline}
{p2colreset}{...}
INCLUDE help esample

{marker options_table}{...}
{synoptset 18 tabbed}{...}
{synopthdr :options}
{synoptline}
{syntab :Main}
{synopt :{opt means}}compute {it:statistic} using empirical Bayes means; the default{p_end}
{synopt :{opt modes}}compute {it:statistic} using empirical Bayes modes{p_end}
{synopt :{opt nooff:set}}ignore the offset or exposure variable in calculating predictions;
	relevant only if you specified {opt offset()} or {opt exposure()} when you fit the model{p_end}
{synopt :{opt fixed:only}}prediction for the fixed portion of the model only{p_end}
{synopt :{opt out:come(outcome)}}outcome category for predicted probabilities for ordinal models{p_end}

{syntab :Integration}
{synopt :{opt intp:oints(#)}}use {it:#} quadrature points to compute empirical Bayes means{p_end}
{synopt :{opt iter:ate(#)}}set maximum number of iterations in computing statistics
	involving empirical Bayes estimators{p_end}
{synopt :{opt tol:erance(#)}}set convergence tolerance for computing statistics
	involving empirical Bayes estimators{p_end}
{synoptline}
{p2colreset}{...}
{p 4 6 2}
For ordinal outcomes, you can specify one or {it:k} new variables in {it:{help newvarlist}}
with {cmd:mu} and {cmd:pr}, where {it:k} is the number of outcomes.
If you do not specify {cmd:outcome()}, these options assume {cmd:outcome(#1)}.{p_end}


INCLUDE help menu_predict


{marker options_predict}{...}
{title:Options for predict}

{dlgtab:Main}

{phang}
{cmd:remeans} calculates posterior means of the random effects, also known as
empirical Bayes means.  You must specify {it:q} new variables, where {it:q} is
the number of random-effects terms in the model.  However, it is much easier
to just specify {it:stub}{cmd:*} and let Stata name the variables
{it:stub}{cmd:1}, {it:stub}{cmd:2},...,{it:stub}{it:q} for you.

{phang}
{cmd:remodes} calculates posterior modes of the random effects, also known as
empirical Bayes modes.  You must specify {it:q} new variables, where {it:q} is
the number of random-effects terms in the model.  However, it is much easier
to just specify {it:stub}{cmd:*} and let Stata name the variables
{it:stub}{cmd:1}, {it:stub}{cmd:2},...,{it:stub}{it:q} for you.

{phang}
{cmd:reses(}{it:stub}{cmd:*}|{it:{help varlist:newvarlist}}{cmd:)} calculates
standard errors of the empirical Bayes estimators and stores the result in
{it:newvarlist}.  This option requires the {cmd:remeans} or the {cmd:remodes}
option.  You must specify {it:q} new variables, where {it:q} is the number of
random-effects terms in the model.  However, it is much easier to just specify
{it:stub}{cmd:*} and let Stata name the variables {it:stub}{cmd:1},
{it:stub}{cmd:2},...,{it:stub}{it:q} for you.

{pmore}
The {cmd:remeans}, {cmd:remodes}, and {cmd:reses()} options often generate
multiple new variables at once.  When this occurs, the random effects (and
standard errors) contained in the generated variables correspond to the order
in which the variance components are listed in the output of {cmd:meglm}.
Still, examining the variable labels of the generated variables 
(by using the {cmd:describe} command, for instance) can be useful in deciphering
which variables correspond to which terms in the model.

{phang}
{cmd:mu}, the default, calculates the predicted mean, that is, the
inverse link function applied to the linear prediction.
By default, this is based on a linear predictor that includes
both the fixed effects and the random effects, and the predicted mean is
conditional on the values of the random effects.  Use the {cmd:fixedonly}
option if you want predictions that include only the fixed portion
of the model, that is, if you want random effects set to 0.

{phang}
{cmd:pr} calculates predicted probabilities and is a synonym for {cmd:mu}.
This option is available only for ordinal and binary response models.

{phang}
{cmd:fitted} calculates the fitted linear prediction.
By default, the fitted predictor includes both the fixed effects and the
random effects.  Use the {cmd:fixedonly} option
if you want predictions that include only the fixed portion
of the model, that is, if you want random effects set to 0.

{phang}
{cmd:xb} calculates the linear prediction xb
based on the estimated fixed effects (coefficients) in the model.  This 
is equivalent to fixing all random effects in the model to their theoretical
(prior) mean value of 0.

{phang}
{cmd:stdp} calculates the standard error of the fixed-effects linear predictor
xb.

{phang}
{cmd:residuals} calculates raw residuals, that is, responses minus the fitted
values.  This option is available only for the Gaussian family.

{phang}
{cmd:pearson} calculates Pearson residuals.  Pearson residuals that are large
in absolute value may indicate a lack of fit.  By default, residuals include
both the fixed portion and the random portion of the model.  The {cmd:fixedonly}
option modifies the calculation to include the fixed portion only.

{phang}
{cmd:deviance} calculates deviance residuals.  Deviance residuals are
recommended by {help meglm_postestimation##MN1989:McCullagh and Nelder (1989)}
as having the best properties for examining the goodness of fit of a GLM.
They are approximately normally distributed if the model is correctly
specified.  They may be plotted against the fitted values or against a
covariate to inspect the model fit.  By default, residuals include both the
fixed portion and the random portion of the model.  The {cmd:fixedonly} option
modifies the calculation to include the fixed portion only.

{phang}
{cmd:anscombe} calculates Anscombe residuals, which are designed to closely
follow a normal distribution.  By default, residuals include both the fixed
portion and the random portion of the model.  The {cmd:fixedonly} option
modifies the calculation to include the fixed portion only.

{phang}
{cmd:means} specifies that posterior means be used as the estimates of the
random effects for any {it:statistic} involving random effects.  {cmd:means}
is the default.

{phang}
{cmd:modes} specifies that posterior modes be used as the estimates of the
random effects for any {it:statistic} involving random effects.

{phang}
{cmd:nooffset} is relevant only if you specified
{opth offset:(varname:varname_o)}  or {opt exposure(varname_e)} with
{cmd:meglm}.  It modifies the calculations made by {cmd:predict} so that they
ignore the offset or the exposure variable; the linear prediction is treated as
xb rather than xb + offset or xb + exposure, whichever is relevant.

{phang}
{cmd:fixedonly} modifies predictions to include only the fixed portion
of the model, equivalent to setting all random effects equal to 0. 

{phang}
{opt outcome(outcome)} specifies the outcome for which the predicted
probabilities are to be calculated.  {cmd:outcome()} should contain either one
value of the dependent variable or one of {bf:#1}, {bf:#2}, ..., with {bf:#1}
meaning the first category of the dependent variable, {bf:#2} meaning the
second category, etc.

{dlgtab:Integration}

{phang}
{cmd:intpoints(#)} specifies the number of quadrature points used to
compute the empirical Bayes means; the default is the value from estimation.

{phang}
{cmd:iterate(#)} specifies the maximum number of iterations when computing
statistics involving empirical Bayes estimators; the default is the value from
estimation.

{phang}
{cmd:tolerance(#)} specifies convergence tolerance when computing
statistics involving empirical Bayes estimators; the default is the value
from estimation.


{marker syntax_estat_group}{...}
{marker estatgroup}{...}
{title:Syntax for estat group}

{p 8 14 2}
{cmd:estat} {opt gr:oup} 


INCLUDE help menu_estat


{marker examples}{...}
{title:Examples}

    {hline}
{pstd}Setup{p_end}
{phang2}{cmd:. webuse towerlondon}{p_end}
{phang2}{cmd:. meglm dtlm difficulty i.group || family: || subject:, family(bernoulli)}{p_end}

{pstd}Obtain predicted probabilities based on the contribution of both fixed effects and random effects{p_end}
{phang2}{cmd:. predict pr}{p_end}

{pstd}Obtain predicted probabilities based on the contribution of fixed effects only{p_end}
{phang2}{cmd:. predict prfixed, fixedonly}{p_end}

{pstd}Obtain predictions of the posterior means and their standard errors{p_end}
{phang2}{cmd:. predict re_means*, reses(se_means*) remeans}{p_end}

{pstd}Obtain predictions of the posterior modes and their standard errors{p_end}
{phang2}{cmd:. predict re_modes*, reses(se_modes*) remodes}{p_end}

    {hline}
{pstd}Setup{p_end}
{phang2}{cmd:. use http://www.stata-press.com/data/mlmus3/schiz, clear}{p_end}
{phang2}{cmd:. generate impso = imps}{p_end}
{phang2}{cmd:. recode impso -9=. 1/2.4=1 2.5/4.4=2 4.5/5.4=3 5.5/7=4}{p_end}
{phang2}{cmd:. meglm impso week treatment || id:, family(ordinal)}{p_end}

{pstd}Obtain predicted probabilities for each outcome based on the contribution of both fixed effects and random effects{p_end}
{phang2}{cmd:. predict pr*}{p_end}

    {hline}
{pstd}Setup{p_end}
{phang2}{cmd:. use http://www.stata-press.com/data/mlmus3/drvisits, clear}
{p_end}
{phang2}{cmd:. meglm numvisit reform age married loginc || id: reform, family(poisson)}{p_end}

{pstd}Obtain the predicted counts based on the contribution of both fixed effects and random effects{p_end}
{phang2}{cmd:. predict n}{p_end}

    {hline}
    

{marker reference}{...}
{title:Reference}

{marker MN1989}{...}
{phang}
McCullagh, P., and J. A. Nelder. 1989.
{browse "http://www.stata.com/bookstore/glm.html":{it:Generalized Linear Models}. 2nd ed.}
London: Chapman & Hall/CRC.
{p_end}
