*! version 1.0.11  08jul2014
program melogit, eclass byable(onecall) prop(or mi)
	version 13
	local vv : di "version " string(_caller()) ", missing:"
	
	if _by() {
		local by "by `_byvars'`_byrc0':"
	}
	if replay() {
		if "`e(cmd)'" != "melogit" {
			error 301
		}
		if _by() {
			error 190
		}
		_me_display `0'
		exit
	}
	
	capture noisily `vv' `by' Estimate `0'
	local rc = _rc
	exit `rc'
end

program Estimate, sortpreserve eclass byable(recall)
	version 13
	local vv : di "version " string(_caller()) ", missing:"
	
	if _by() {
		tempname bytouse
		mark `bytouse'
	}
	
	local 0_orig `0'
	_parse expand eq opt : 0
	local gopts `opt_op'
	
	forvalues i=1/`=`eq_n'-1' {
		local 0 `eq_`i''
		syntax [anything] [if] [in] [, or irr eform BINomial(string) ///
			Family(string) Link(string) EXPosure(string) *]
		_me_chk_opts, binomial(`binomial') exposure(`exposure')  ///
			irr(`irr') family(`family') link(`link')
		
		local 0 `anything' `if' `in', family(`family') link(`link') ///
			binomial(`binomial') exposure(`exposure') `options'
		syntax [anything] [if] [in] [, family(passthru) ///
			link(passthru) binomial(passthru) exposure(passthru) *]
		local eq_`i' `anything' `if' `in', `family' `link' ///
			`binomial' `exposure' `options'
		
		local eqs `eqs' `eq_`i'' ||
		local dixtra `dixtra' `or' `eform'
	}
	
	local 0 `eq_`eq_n''
	syntax [anything] [if] [in] [, *]
	local eqn `anything' `if' `in'
	local 0 , `options' `gopts'
	syntax [anything] [, BINomial(string) noTABle noLRtest noGRoup 	///
		noHEADer noLOg or irr eform Family(string) Link(string) ///
		EXPosure(string) noESTimate COEFLegend * ]
	
	_me_chk_opts, family(`family') link(`link') exposure(`exposure') ///
		irr(`irr') binomial(`binomial')
	local dixtra `dixtra' `or' `eform'
	local dixtra : list uniq dixtra
	opts_exclusive "`dixtra'"
	
	_get_diopts diopts opts, `options'
	local diopts `diopts' `table' `lrtest' `group' `header' `log' 	///
		`estimate' `coeflegend' `dixtra'
	
	if `"`binomial'"'!="" {
		local family family(binomial `binomial')
		local fam binomial
	}
	else {
		local family family(bernoulli)
		local fam bernoulli
	}
	
	local 0 `eqs' `eqn' , link(logit) `family' `opts' `log' `estimate' ///
		`coeflegend'
	`vv' _me_estimate "`bytouse'" `0'
	
	ereturn local family `fam'
	ereturn local link logit
	
	if e(k_r) ereturn local title "Mixed-effects logistic regression"
	else ereturn local title "Logistic regression"
	ereturn local model logistic
		
	ereturn local cmd melogit
	ereturn local cmdline melogit `0_orig'

	ereturn local predict melogit_p
	ereturn local estat_cmd melogit_estat
	ereturn hidden local cmd2 meglm
	ereturn local cmdline2
	
	_me_display , `diopts'
		
end
exit
