*! version 1.0.7  08jul2014
program menbreg, eclass byable(onecall) prop(irr mi)
	
	version 13
	
	if _by() {
		local by "by `_byvars'`_byrc0':"
	}
	if replay() {
		if "`e(cmd)'" != "menbreg" {
			error 301
		}
		if _by() {
			error 190
		}
		_me_display `0'
		exit
	}
	
	capture noisily `by' Estimate `0'
	local rc = _rc
	exit `rc'
end

program Estimate, sortpreserve eclass byable(recall)
	
	local 0_orig `0'
	_parse expand eq opt : 0
	local gopts `opt_op'
	
	forvalues i=1/`=`eq_n'-1' {
		local 0 `eq_`i''
		syntax [anything] [if] [in] [, or irr eform 		///
			Dispersion(string) Family(string) Link(string) *]
		_me_chk_opts, family(`family') link(`link') or(`or')	///
			dispersion(`dispersion') sret
		
		local 0 `anything' `if' `in', family(`family') link(`link') ///
			dispersion(`dispersion') `options'
		syntax [anything] [if] [in] [, family(passthru) 	///
			link(passthru) dispersion(passthru) *]
		local eq_`i' `anything' `if' `in', `family' `link' 	///
			`dispersion' `options'
		
		local eqs `eqs' `eq_`i'' ||
		local dixtra `dixtra' `irr' `eform'
	}
	
	local 0 `eq_`eq_n''
	syntax [anything] [if] [in] [, *]
	local eqn `anything' `if' `in'
	local 0 , `options' `gopts'
	syntax [anything] [, Dispersion(string) noTABle noLRtest noGRoup ///
		noHEADer noLOg or irr eform Family(string) Link(string) ///
		noESTimate COEFLegend * ]
	
	_me_chk_opts, family(`family') link(`link') or(`or') 	///
		dispersion(`dispersion') sret
	local d `s(dispersion)'
	local fd nbinomial `d'
	local fam family(`fd') dispersion(`d')
	local dixtra `dixtra' `irr' `eform'
	local dixtra : list uniq dixtra
	opts_exclusive "`dixtra'"
	
	_get_diopts diopts opts, `options'
	local diopts `diopts' `table' `lrtest' `group' `header' `log' 	///
		`estimate' `coeflegend' `dixtra'
	local diopts : list uniq diopts
	
	local 0 `eqs' `eqn' , link(log) `fam' `opts' `log' `estimate' ///
		`coeflegend'
	`by' _me_estimate "`bytouse'" `0'
	
	ereturn local family nbinomial
	ereturn local link log
	ereturn local dispersion `d'
	
	if e(k_r) ereturn local title "Mixed-effects nbinomial regression"
	else ereturn local title "Negative binomial regression"
	ereturn local model nbinomial
	
	ereturn local cmd menbreg
	ereturn local cmdline menbreg `0_orig'

	ereturn local predict menbreg_p
	ereturn local estat_cmd menbreg_estat
	ereturn hidden local cmd2 meglm
	ereturn local cmdline2
	
	_me_display , `diopts'
	
end
exit
