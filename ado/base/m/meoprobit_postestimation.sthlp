{smcl}
{* *! version 1.0.4  16may2013}{...}
{viewerdialog predict "dialog meoprobit_p"}{...}
{viewerdialog estat "dialog meoprobit_estat"}{...}
{vieweralsosee "[ME] meoprobit postestimation" "mansection ME meoprobitpostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] meoprobit" "help meoprobit"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] meglm postestimation" "help meglm postestimation"}{...}
{viewerjumpto "Description" "meoprobit postestimation##description"}{...}
{viewerjumpto "Special-interest postestimation commands" "meoprobit postestimation##special"}{...}
{viewerjumpto "Syntax for predict" "meoprobit postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "meoprobit postestimation##menu_predict"}{...}
{viewerjumpto "Options for predict" "meoprobit postestimation##options_predict"}{...}
{viewerjumpto "Syntax for estat group" "meoprobit postestimation##syntax_estat_group"}{...}
{viewerjumpto "Menu for estat" "meoprobit postestimation##menu_estat"}{...}
{viewerjumpto "Examples" "meoprobit postestimation##examples"}{...}
{title:Title}

{p2colset 5 38 40 2}{...}
{p2col :{manlink ME meoprobit postestimation} {hline 2}}Postestimation tools for meoprobit{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation command is of special interest after
{cmd:meoprobit}:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
{synopt :{helpb meoprobit postestimation##estatgroup:estat group}}summarize
the composition of the nested groups{p_end}
{synoptline}
{p2colreset}{...}

{pstd}
The following standard postestimation commands are also available:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
INCLUDE help post_contrast
INCLUDE help post_estatic
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_estimates
INCLUDE help post_lincom
INCLUDE help post_lrtest
INCLUDE help post_margins
INCLUDE help post_marginsplot
INCLUDE help post_nlcom
{synopt :{helpb meoprobit postestimation##predict:predict}}predictions, residuals, influence statistics, and other diagnostic measures{p_end}
INCLUDE help post_predictnl
INCLUDE help post_pwcompare
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{p2colreset}{...}


{marker special}{...}
{title:Special-interest postestimation commands}

{pstd}
{cmd:estat group} reports number of groups and minimum, average, and maximum
group sizes for each level of the model.  Model levels are identified by
the corresponding group variable in the data.  Because groups are treated
as nested, the information in this summary may differ from what you would
get if you used the {cmd:tabulate} command on each group variable individually.


{marker syntax_predict}{...}
{marker predict}{...}
{title:Syntax for predict}

{p 4 4 2}
Syntax for obtaining estimated random effects and their standard errors

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} {cmd:,} {{opt remeans}|{opt remodes}}
[{cmd:reses(}{it:newvarsspec}{cmd:)}]


{p 4 4 2}
Syntax for obtaining other predictions

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} 
[{cmd:,} {it:{help meoprobit_postestimation##statistic:statistic}}
{it:{help meologit_postestimation##options_table:options}}]


{pstd}
{it:newvarsspec} is {it:stub}{cmd:*} or {it:{help newvarlist}}.


{marker statistic}{...}
{synoptset 18 tabbed}{...}
{synopthdr :statistic}
{synoptline}
{syntab :Main}
{synopt :{cmd:pr}}predicted probabilities; the default{p_end}
{synopt :{opt fit:ted}}fitted linear predictor{p_end}
{synopt :{cmd:xb}}linear predictor for the fixed portion of the model only{p_end}
{synopt :{cmd:stdp}}standard error of the fixed-portion linear prediction{p_end}
{synoptline}
{p2colreset}{...}
INCLUDE help esample

{marker options_table}{...}
{synoptset 18 tabbed}{...}
{synopthdr :options}
{synoptline}
{syntab :Main}
{synopt :{opt means}}compute {it:statistic} using empirical Bayes means; the default{p_end}
{synopt :{opt modes}}compute {it:statistic} using empirical Bayes modes{p_end}
{synopt :{opt nooff:set}}ignore the offset variable in calculating predictions;
	relevant only if you specified {opt offset()} when you fit the model{p_end}
{synopt :{opt fixed:only}}prediction for the fixed portion of the model only{p_end}
{synopt :{opt out:come(outcome)}}outcome category for predicted probabilities{p_end}

{syntab :Integration}
{synopt :{opt intp:oints(#)}}use {it:#} quadrature points to compute empirical Bayes means{p_end}
{synopt :{opt iter:ate(#)}}set maximum number of iterations in computing statistics
	involving empirical Bayes estimators{p_end}
{synopt :{opt tol:erance(#)}}set convergence tolerance for computing statistics
	involving empirical Bayes estimators{p_end}
{synoptline}
{p2colreset}{...}
{p 4 6 2}
You specify one or {it:k} new variables in {it:{help newvarlist}}
with {cmd:pr}, where {it:k} is the number of outcomes.
If you do not specify {cmd:outcome()}, these options assume {cmd:outcome(#1)}.{p_end}


INCLUDE help menu_predict


{marker options_predict}{...}
{title:Options for predict}

{dlgtab:Main}

{phang}
{cmd:remeans}, {cmd:remodes}, {cmd:reses()};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.

{phang}
{cmd:pr}, the default, calculates the predicted probabilities.
By default, the probabilities are based on a linear predictor that includes
both the fixed effects and the random effects, and the predicted probabilities
are conditional on the values of the random effects.  Use the {cmd:fixedonly}
option if you want predictions that include only the fixed portion
of the model, that is, if you want random effects set to 0.

{pmore}
You specify one or {it:k} new variables, where {it:k} is the number of
categories of the dependent variable.  If you specify the {cmd:outcome()}
option, the probabilities will be predicted for the requested outcome only, in
which case you specify only one new variable.  If you specify only one new
variable and do not specify {cmd:outcome()}, {cmd:outcome(#1)} is assumed.

{phang}
{cmd:fitted},
{cmd:xb},
{cmd:stdp},
{cmd:means},
{cmd:modes},
{cmd:nooffset},
{cmd:fixedonly};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.

{pmore}
By default or if the {cmd:means} option is specified, statistics
{cmd:pr}, {cmd:fitted}, {cmd:xb}, and {cmd:stdp} are based on the posterior
mean estimates of random effects. If the {cmd:modes} option is specified,
these statistics are based on the posterior mode estimates of random effects.

{phang}
{opt outcome(outcome)} specifies the outcome for which the predicted
probabilities are to be calculated.  {cmd:outcome()} should contain either one
value of the dependent variable or one of {bf:#1}, {bf:#2}, ..., with {bf:#1}
meaning the first category of the dependent variable, {bf:#2} meaning the
second category, etc.

{dlgtab:Integration}

{phang}
{cmd:intpoints()},
{cmd:iterate()},
{cmd:tolerance()};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.


{marker syntax_estat_group}{...}
{marker estatgroup}{...}
{title:Syntax for estat group}

{p 8 14 2}
{cmd:estat} {opt gr:oup} 


INCLUDE help menu_estat


{marker examples}{...}
{title:Examples}

{pstd}Setup{p_end}
{phang2}{cmd:. webuse tvsfpors}{p_end}
{phang2}{cmd:. meoprobit thk prethk cc##tv || school: || class:}{p_end}

{pstd}Obtain predicted probabilities for each outcome based on the contribution of both fixed effects and random effects{p_end}
{phang2}{cmd:. predict pr*}{p_end}

{pstd}Predictions of random effects{p_end}
{phang2}{cmd:. predict re*, remeans}{p_end}

{pstd}Summarize composition of nested groups{p_end}
{phang2}{cmd:. estat group}{p_end}
