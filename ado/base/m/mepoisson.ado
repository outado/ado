*! version 1.0.8  08jul2014
program mepoisson, eclass byable(onecall) prop(irr mi)
	
	version 13
	
	if _by() {
		local by "by `_byvars'`_byrc0':"
	}
	if replay() {
		if "`e(cmd)'" != "mepoisson" {
			error 301
		}
		if _by() {
			error 190
		}
		_me_display `0'
		exit
	}
	
	capture noisily `by' Estimate `0'
	local rc = _rc
	exit `rc'
end

program Estimate, sortpreserve eclass byable(recall)
	
	if _by() {
		tempname bytouse
		mark `bytouse'
	}
	
	local 0_orig `0'
	_parse expand eq opt : 0
	local gopts `opt_op'
	
	forvalues i=1/`=`eq_n'-1' {
		local 0 `eq_`i''
		syntax [anything] [if] [in] [, or irr eform Family(string) ///
			Link(string) *]
		_me_chk_opts, family(`family') link(`link') or(`or')
		
		local 0 `anything' `if' `in', family(`family') link(`link') ///
			`options'
		syntax [anything] [if] [in] [, family(passthru) 	///
			link(passthru) *]
		local eq_`i' `anything' `if' `in', `family' `link' `options'
		
		local eqs `eqs' `eq_`i'' ||
		local dixtra `dixtra' `irr' `eform'
	}
	
	local 0 `eq_`eq_n''
	syntax [anything] [if] [in] [, *]
	local eqn `anything' `if' `in'
	local 0 , `options' `gopts'
	syntax [anything] [, BINomial(string) noTABle noLRtest noGRoup 	///
		noHEADer noLOg or irr eform Family(string) Link(string) ///
		noESTimate COEFLegend * ]
	
	_me_chk_opts, family(`family') link(`link') or(`or')
	local dixtra `dixtra' `irr' `eform'
	local dixtra : list uniq dixtra
	opts_exclusive "`dixtra'"
	
	_get_diopts diopts opts, `options'
	local diopts `diopts' `table' `lrtest' `group' `header' `log' 	///
		`estimate' `coeflegend' `dixtra'
	local diopts : list uniq diopts
	
	local 0 `eqs' `eqn' , link(log) fam(poisson) `opts' `log' ///
		`estimate' `coeflegend'
	`by' _me_estimate "`bytouse'" `0'
	
	ereturn local family poisson
	ereturn local link log
	
	if e(k_r) ereturn local title "Mixed-effects Poisson regression"
	else ereturn local title "Poisson regression"
	ereturn local model Poisson
	
	ereturn local cmd mepoisson
	ereturn local cmdline mepoisson `0_orig'

	ereturn local predict mepoisson_p
	ereturn local estat_cmd mepoisson_estat
	ereturn hidden local cmd2 meglm
	ereturn local cmdline2
	
	_me_display , `diopts'
		
end
exit
