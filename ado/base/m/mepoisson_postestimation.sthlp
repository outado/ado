{smcl}
{* *! version 1.0.5  29mar2013}{...}
{viewerdialog predict "dialog mepoisson_p"}{...}
{viewerdialog estat "dialog mepoisson_estat"}{...}
{vieweralsosee "[ME] mepoisson postestimation" "mansection ME mepoissonpostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] mepoisson" "help mepoisson"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] meglm postestimation" "help mecloglog"}{...}
{viewerjumpto "Description" "mepoisson postestimation##description"}{...}
{viewerjumpto "Special-interest postestimation commands" "mepoisson postestimation##special"}{...}
{viewerjumpto "Syntax for predict" "mepoisson postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "mepoisson postestimation##menu_predict"}{...}
{viewerjumpto "Options for predict" "mepoisson postestimation##options_predict"}{...}
{viewerjumpto "Syntax for estat group" "mepoisson postestimation##syntax_estat"}{...}
{viewerjumpto "Menu for estat" "mepoisson postestimation##menu_estat"}{...}
{viewerjumpto "Examples" "mepoisson postestimation##examples"}{...}
{title:Title}

{p2colset 5 38 40 2}{...}
{p2col :{manlink ME mepoisson postestimation} {hline 2}}Postestimation tools for
mepoisson{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation command is of special interest after
{cmd:mepoisson}:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
{synopt :{helpb mepoisson postestimation##estatgroup:estat group}}summarize
the composition of the nested groups{p_end}
{synoptline}
{p2colreset}{...}

{pstd}
The following standard postestimation commands are also available:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
INCLUDE help post_contrast
INCLUDE help post_estatic
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_estimates
INCLUDE help post_lincom
INCLUDE help post_lrtest
INCLUDE help post_margins
INCLUDE help post_marginsplot
INCLUDE help post_nlcom
{synopt :{helpb mepoisson postestimation##predict:predict}}predictions, residuals, influence statistics, and other diagnostic measures{p_end}
INCLUDE help post_predictnl
INCLUDE help post_pwcompare
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{p2colreset}{...}


{marker special}{...}
{title:Special-interest postestimation commands}

{pstd}
{cmd:estat group} reports number of groups and minimum, average, and maximum
group sizes for each level of the model.  Model levels are identified by
the corresponding group variable in the data.  Because groups are treated
as nested, the information in this summary may differ from what you would
get if you used the {cmd:tabulate} command on each group variable individually.


{marker syntax_predict}{...}
{marker predict}{...}
{title:Syntax for predict}

{p 4 4 2}
Syntax for obtaining estimated random effects and their standard errors

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} {cmd:,} {{opt remeans}|{opt remodes}}
[{cmd:reses(}{it:newvarsspec}{cmd:)}]


{p 4 4 2}
Syntax for obtaining other predictions

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} 
[{cmd:,} {it:{help mepoisson_postestimation##statistic:statistic}}
{it:{help mepoisson_postestimation##options_table:options}}]


{pstd}
{it:newvarsspec} is {it:stub}{cmd:*} or {it:{help newvarlist}}.


{marker statistic}{...}
{synoptset 18 tabbed}{...}
{synopthdr :statistic}
{synoptline}
{syntab :Main}
{synopt :{cmd:mu}}number of events; the default{p_end}
{synopt :{opt fit:ted}}fitted linear predictor{p_end}
{synopt :{cmd:xb}}linear predictor for the fixed portion of the model only{p_end}
{synopt :{cmd:stdp}}standard error of the fixed-portion linear prediction{p_end}
{synopt :{opt pea:rson}}Pearson residuals{p_end}
{synopt :{opt dev:iance}}deviance residuals{p_end}
{synopt :{opt ans:combe}}Anscombe residuals{p_end}
{synoptline}
{p2colreset}{...}
INCLUDE help esample

{marker options_table}{...}
{synoptset 18 tabbed}{...}
{synopthdr :options}
{synoptline}
{syntab :Main}
{synopt :{opt means}}compute {it:statistic} using empirical Bayes means; the default{p_end}
{synopt :{opt modes}}compute {it:statistic} using empirical Bayes modes{p_end}
{synopt :{opt nooff:set}}ignore the offset or exposure variable in calculating predictions;
	relevant only if you specified {opt offset()} or {opt exposure()} when you fit the model{p_end}
{synopt :{opt fixed:only}}prediction for the fixed portion of the model only{p_end}

{syntab :Integration}
{synopt :{opt intp:oints(#)}}use {it:#} quadrature points to compute empirical Bayes means{p_end}
{synopt :{opt iter:ate(#)}}set maximum number of iterations in computing statistics
	involving empirical Bayes estimators{p_end}
{synopt :{opt tol:erance(#)}}set convergence tolerance for computing statistics
	involving empirical Bayes estimators{p_end}
{synoptline}
{p2colreset}{...}


INCLUDE help menu_predict


{marker options_predict}{...}
{title:Options for predict}

{dlgtab:Main}

{phang}
{cmd:remeans}, {cmd:remodes}, {cmd:reses()};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.

{phang}
{cmd:mu}, the default, calculates the predicted mean (the
predicted number of events),
that is, the inverse link function applied to the linear prediction.  By
default, this is based on a linear predictor that includes both the fixed
effects and the random effects, and the predicted mean is conditional on the
values of the random effects.  Use the {cmd:fixedonly} option if you want
predictions that include only the fixed portion of the model, that is, if you
want random effects set to 0.

{phang}
{cmd:fitted},
{cmd:xb},
{cmd:stdp},
{cmd:pearson},
{cmd:deviance},
{cmd:anscombe},
{cmd:means},
{cmd:modes},
{cmd:nooffset},
{cmd:fixedonly};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.

{pmore}
By default or if the {cmd:means} option is specified, statistics {cmd:mu},
{cmd:pr}, {cmd:fitted}, {cmd:xb}, {cmd:stdp}, {cmd:pearson}, {cmd:deviance},
and {cmd:anscombe} are based on the posterior mean estimates of random
effects. If the {cmd:modes} option is specified, these statistics are based on
the posterior mode estimates of random effects.

{dlgtab:Integration}

{phang}
{cmd:intpoints()},
{cmd:iterate()},
{cmd:tolerance()};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.


{marker syntax_estat}{...}
{marker estatgroup}{...}
{title:Syntax for estat group}

{p 8 14 2}
{cmd:estat} {opt gr:oup} 


INCLUDE help menu_estat


{marker examples}{...}
{title:Examples}

{pstd}Setup{p_end}
{phang2}{cmd:. webuse melanoma}{p_end}
{phang2}{cmd:. mepoisson deaths c.uv##c.uv, exposure(expected) || nation: || region:}{p_end}

{pstd}Summarize composition of nested groups{p_end}
{phang2}{cmd:. estat group}{p_end}

{pstd}Predicted counts, incorporating random effects{p_end}
{phang2}{cmd:. predict n}{p_end}

{pstd}Predicted counts, setting all random effects to zero{p_end}
{phang2}{cmd:. predict n_fixed, fixedonly}{p_end}

{pstd}Predictions of random effects{p_end}
{phang2}{cmd:. predict re*, remeans}{p_end}
