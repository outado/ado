{smcl}
{* *! version 1.0.4  16may2013}{...}
{viewerdialog predict "dialog meprobit_p"}{...}
{viewerdialog estat "dialog meprobit_estat"}{...}
{vieweralsosee "[ME] meprobit postestimation" "mansection ME meprobitpostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] meprobit" "help meprobit"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] meglm postestimation" "help mecloglog"}{...}
{viewerjumpto "Description" "meprobit postestimation##description"}{...}
{viewerjumpto "Special-interest postestimation commands" "meprobit postestimation##special"}{...}
{viewerjumpto "" "--"}{...}
{viewerjumpto "Syntax for predict" "meprobit postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "meprobit postestimation##menu_predict"}{...}
{viewerjumpto "Options for predict" "meprobit postestimation##options_predict"}{...}
{viewerjumpto "" "--"}{...}
{viewerjumpto "Syntax for estat" "meprobit postestimation##syntax_estat"}{...}
{viewerjumpto "Menu for estat" "meprobit postestimation##menu_estat"}{...}
{viewerjumpto "Option for estat icc" "meprobit postestimation##option_estat_icc"}{...}
{viewerjumpto "" "--"}{...}
{viewerjumpto "Examples" "meprobit postestimation##examples"}{...}
{viewerjumpto "Stored results" "meprobit postestimation##results"}{...}
{title:Title}

{p2colset 5 37 39 2}{...}
{p2col :{manlink ME meprobit postestimation} {hline 2}}Postestimation tools for
meprobit{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation commands are of special interest after
{cmd:meprobit}:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
{synopt :{helpb meprobit postestimation##estatgroup:estat group}}summarize
the composition of the nested groups{p_end}
{synopt :{helpb meprobit postestimation##estaticc:estat icc}}estimate
intraclass correlations{p_end}
{synoptline}
{p2colreset}{...}

{pstd}
The following standard postestimation commands are also available:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
INCLUDE help post_contrast
INCLUDE help post_estatic
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_estimates
INCLUDE help post_lincom
INCLUDE help post_lrtest
INCLUDE help post_margins
INCLUDE help post_marginsplot
INCLUDE help post_nlcom
{synopt :{helpb meprobit postestimation##predict:predict}}predictions, residuals, influence statistics, and other diagnostic measures{p_end}
INCLUDE help post_predictnl
INCLUDE help post_pwcompare
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{p2colreset}{...}


{marker special}{...}
{title:Special-interest postestimation commands}

{pstd}
{cmd:estat group} reports number of groups and minimum, average, and maximum
group sizes for each level of the model.  Model levels are identified by
the corresponding group variable in the data.  Because groups are treated
as nested, the information in this summary may differ from what you would
get if you used the {cmd:tabulate} command on each group variable individually.

{pstd}
{cmd:estat icc} displays the intraclass correlation for pairs of latent linear
responses at each nested level of the model.  Intraclass correlations are
available for random-intercept models or for random-coefficient models
conditional on random-effects covariates being equal to 0.  They are not
available for crossed-effects models.


{marker syntax_predict}{...}
{marker predict}{...}
{title:Syntax for predict}

{p 4 4 2}
Syntax for obtaining estimated random effects and their standard errors

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} {cmd:,} {{opt remeans}|{opt remodes}}
[{cmd:reses(}{it:newvarsspec}{cmd:)}]


{p 4 4 2}
Syntax for obtaining other predictions

{p 8 16 2}
{cmd:predict} {dtype} {it:newvarsspec} {ifin} 
[{cmd:,} {it:{help meprobit_postestimation##statistic:statistic}}
{it:{help meprobit_postestimation##options_table:options}}]


{pstd}
{it:newvarsspec} is {it:stub}{cmd:*} or {it:{help newvarlist}}.


{marker statistic}{...}
{synoptset 18 tabbed}{...}
{synopthdr :statistic}
{synoptline}
{syntab :Main}
{synopt :{cmd:mu}}predicted mean; the default{p_end}
{synopt :{opt fit:ted}}fitted linear predictor{p_end}
{synopt :{cmd:xb}}linear predictor for the fixed portion of the model only{p_end}
{synopt :{cmd:stdp}}standard error of the fixed-portion linear prediction{p_end}
{synopt :{opt pea:rson}}Pearson residuals{p_end}
{synopt :{opt dev:iance}}deviance residuals{p_end}
{synopt :{opt ans:combe}}Anscombe residuals{p_end}
{synoptline}
{p2colreset}{...}
INCLUDE help esample

{marker options_table}{...}
{synoptset 18 tabbed}{...}
{synopthdr :options}
{synoptline}
{syntab :Main}
{synopt :{opt means}}compute {it:statistic} using empirical Bayes means; the default{p_end}
{synopt :{opt modes}}compute {it:statistic} using empirical Bayes modes{p_end}
{synopt :{opt nooff:set}}ignore the offset variable in calculating predictions;
	relevant only if you specified {opt offset()} when you fit the model{p_end}
{synopt :{opt fixed:only}}prediction for the fixed portion of the model only{p_end}

{syntab :Integration}
{synopt :{opt intp:oints(#)}}use {it:#} quadrature points to compute empirical Bayes means{p_end}
{synopt :{opt iter:ate(#)}}set maximum number of iterations in computing statistics
	involving empirical Bayes estimators{p_end}
{synopt :{opt tol:erance(#)}}set convergence tolerance for computing statistics
	involving empirical Bayes estimators{p_end}
{synoptline}
{p2colreset}{...}


INCLUDE help menu_predict


{marker options_predict}{...}
{title:Options for predict}

{dlgtab:Main}

{phang}
{cmd:remeans}, {cmd:remodes}, {cmd:reses()};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.

{phang}
{cmd:mu}, the default, calculates the predicted mean (the predicted
probability of a positive outcome), that is, the inverse link function applied
to the linear prediction.  By default, this is based on a linear predictor
that includes both the fixed effects and the random effects, and the predicted
mean is conditional on the values of the random effects.  Use the
{cmd:fixedonly} option if you want predictions that include only the fixed
portion of the model, that is, if you want random effects set to 0.

{phang}
{cmd:fitted},
{cmd:xb},
{cmd:stdp},
{cmd:pearson},
{cmd:deviance},
{cmd:anscombe},
{cmd:means},
{cmd:modes},
{cmd:nooffset},
{cmd:fixedonly};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.

{pmore}
By default or if the {cmd:means} option is specified, statistics {cmd:mu},
{cmd:fitted}, {cmd:xb}, {cmd:stdp}, {cmd:pearson}, {cmd:deviance},
and {cmd:anscombe} are based on the posterior mean estimates of random
effects. If the {cmd:modes} option is specified, these statistics are based on
the posterior mode estimates of random effects.

{dlgtab:Integration}

{phang}
{cmd:intpoints()},
{cmd:iterate()},
{cmd:tolerance()};
see {helpb meglm postestimation##options_predict:[ME] meglm postestimation}.


{marker syntax_estat}{...}
{marker estatgroup}{...}
{marker estaticc}{...}
{title:Syntax for estat}

{pstd}
Summarize the composition of the nested groups

{p 8 14 2}
{cmd:estat} {opt gr:oup} 


{pstd}
Estimate intraclass correlations

{p 8 14 2}
{cmd:estat} {opt icc} [{cmd:,} {opt l:evel(#)}]


INCLUDE help menu_estat


{marker option_estat_icc}{...}
{title:Option for estat icc}

{phang}
{opt level(#)}
specifies the confidence level, as a percentage, for confidence intervals.
The default is {cmd:level(95)} or as set by {helpb set level}.


{marker examples}{...}
{title:Examples}

{pstd}Setup{p_end}
{phang2}{cmd:. webuse towerlondon}{p_end}
{phang2}{cmd:. meprobit dtlm difficulty i.group || family: || subject:}
{p_end}

{pstd}Summarize composition of nested groups{p_end}
{phang2}{cmd:. estat group}{p_end}

{pstd}Predictions of random effects{p_end}
{phang2}{cmd:. predict re_urban re_cons, remeans}{p_end}

{pstd}Predicted probabilities, incorporating random effects{p_end}
{phang2}{cmd:. predict p}{p_end}

{pstd}Predicted probabilities, ignoring subject and family effects{p_end}
{phang2}{cmd:. predict p_fixed, fixedonly}{p_end}

{pstd}Compute residual intraclass correlations{p_end}
{phang2}{cmd:. estat icc}{p_end}


{marker results}{...}
{title:Stored results}

{pstd}
{cmd:estat icc} stores the following in {cmd:r()}:

{synoptset 13 tabbed}{...}
{p2col 5 20 24 2: Scalars}{p_end}
{synopt:{cmd:r(icc}{it:#}{cmd:)}}level-{it:#} intraclass correlation{p_end}
{synopt:{cmd:r(se}{it:#}{cmd:)}}standard errors of level-{it:#} intraclass
        correlation{p_end}
{synopt:{cmd:r(level)}}confidence level of confidence intervals{p_end}

{p2col 5 20 24 2: Macros}{p_end}
{synopt:{cmd:r(label}{it:#}{cmd:)}}label for level {it:#}{p_end}

{p2col 5 20 24 2: Matrices}{p_end}
{synopt:{cmd:r(ci}{it:#}{cmd:)}}vector of confidence intervals (lower and upper)
        for level-{it:#} intraclass correlation{p_end}
{p2colreset}{...}

{pstd}
For a {it:G}-level nested model, {it:#} can be any integer between 2 and {it:G}.
{p_end}
