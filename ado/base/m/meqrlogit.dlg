/*
  meqrlogit

*!  VERSION 1.0.3  31may2013

*/

VERSION 13.0

INCLUDE _std_large
DEFINE _dlght 475
INCLUDE header

HELP hlp1, view("help meqrlogit")
RESET res1

SCRIPT PREINIT
BEGIN
	create STRING main_equ_tmp
	create STRING main_equ_tmp2
	create STRING main_equ_childdlg
	create STRING main_equ_dlgname
	create STRING main_equ_child_result
	create STRING main_equ_cmd_output
	create STRING main_equ_cmd_last

	create ARRAY main_equ_list
	create ARRAY main_equ_toklist

	create SVECTOR mainEQUCommandDisplayList
	create SVECTOR mainEQUCommandList
	create SVECTOR mainEQUCommandValues
	create SVECTOR mainEQUStatusList
	create SVECTOR mainEQUChildDialogList

	create DOUBLE main_equ_count
	create DOUBLE main_equ_position
	create DOUBLE main_equ_arraysize
	create DOUBLE main_equ_tmpcount

	main.bu_equ_edit.disable
	main.bu_equ_disable.disable
	main.bu_equ_enable.disable

	create BOOLEAN allow_fweights
	create BOOLEAN allow_pweights

	allow_fweights.setfalse
	allow_pweights.setfalse
END

PROGRAM POSTINIT_PROGRAM
BEGIN
	call program check_bytab
	call script max_setListNoBHHH
	call script max_setOptionXTME
END

SCRIPT POSTINIT
BEGIN
	mainEQUCommandDisplayList.copyToArray main_equ_list
	main.lb_equBuild.repopulate
END

PROGRAM check_bytab
BEGIN
	if __MESSAGE.contains("__MI__") {
		call script byifin_set_by_off
	}
END

DIALOG main, tabtitle("Model")	///
	label("meqrlogit - Multilevel mixed-effects logistic regression")
BEGIN
	GROUPBOX gb_fe		_lft	_top	_iwd	_ht10h,		///
		label("Fixed-effects model")
	DEFINE _x _indent
	DEFINE _y _ss
	INCLUDE me_dviv

	CHECKBOX ck_nocons	@	_ms	@	.,		///
		option(noconstant)					///
		label("Suppress constant term")
		
	TEXT     tx_offset	_ilft	_ss	_cwd2	.,		///
		label("Offset variable:")

	VARNAME  vn_offset	_ilft	_ss	_vnwd	.,		///
		option(offset)						///
		label("Offset variable")

	DEFINE _x _lft
	DEFINE _y +35
	INCLUDE me_equ

	GROUPBOX gb_opts	_lft	+40	_iwd	_ht8,		///
		label("Options")

	CHECKBOX ck_binomial	20	_ms	_ibwd	_ht4,		///
		groupbox						///
		onclickon(program binomial_on)				///
		onclickoff(script binomial_off)				///
		label("Binomial trials per observation")
	RADIO    rb_variable	30	_ss	_cwd1	.,		///
		first							///
		option(binomial)					///
		onclickon(program binomial_on)				///
		label("Variable:")
	RADIO    rb_fixed	_lft2	@	@	.,		///
		last							///
		option(binomial)					///
		onclickon(program binomial_on)				///
		label("Fixed number:")
	VARNAME  vn_binomial	40	_ss	_vnwd	.,		///
		option(binomial)					///
		numeric							///
		label("Variable:")
	SPINNER  sp_binomial	_ilft2	@	_spwd	.,		///
		option(binomial) min(1) max(10000)			///
		label("Fixed number:")
END

PROGRAM show_fixed_binomial
BEGIN
	if main.rb_fixed {
		call main.sp_binomial.show
		call main.vn_binomial.hide
	}
END

PROGRAM show_variable_binomial
BEGIN
	if main.rb_variable {
		call main.sp_binomial.hide
		call main.vn_binomial.show
	}
END

PROGRAM binomial_on
BEGIN
	if main.ck_binomial {
		call main.rb_fixed.enable
		if main.rb_fixed {
			call main.sp_binomial.enable
		}
		else {
			call main.sp_binomial.disable
		}
		call main.rb_variable.enable
		if main.rb_variable {
			call main.vn_binomial.enable
		}
		else {
			call main.vn_binomial.disable
		}
	}
	else {
		call script binomial_off
	}
END

SCRIPT binomial_off
BEGIN
	main.rb_fixed.disable
	main.rb_variable.disable
	main.sp_binomial.disable
	main.vn_binomial.disable
END

INCLUDE me_qrequ_pr
INCLUDE _constraints_sc

INCLUDE byifin

SCRIPT rpt_POSTINIT
BEGIN
	create STRING rpt_bu_fmtcoefResults
	program rpt_bu_fmtcoef_ckResults
	create STRING rpt_bu_facvarsResults
	program rpt_bu_facvars_ckResults
END

DIALOG rpt, tabtitle("Reporting")
BEGIN
	DEFINE _x _lft
	DEFINE _cx _spr2
	DEFINE _y _top
	INCLUDE _sp_level

	CHECKBOX ck_or			_lft	_ls	_iwd	.,	///
		option(or)						///
		label("Report fixed-effects coefficients as odds ratios")
	RADIO rb_variance		@	_ls	@	.,first	///
		onclickon(program rpt_check_options)			///
		onclickoff(program rpt_check_options)			///
		label("Show random-effects parameter estimates as variances and covariances")

	RADIO rb_stddev			@	_ms	@	.,last 	///
		option(stddeviations)					///
		onclickon(program rpt_check_options)			///
		onclickoff(program rpt_check_options)			///
		label("Show random-effects parameter estimates as standard deviations and correlations")

	CHECKBOX ck_noretable		@	_ls	@	.,	///
		option(noretable)					///
		onclickon(program rpt_check_options)			///
		onclickoff(program rpt_check_options)			///
		label("Suppress random-effects table")
	CHECKBOX ck_nofetable		@	_ms	@	.,	///
		option(nofetable)					///
		onclickon(program rpt_check_options)			///
		onclickoff(program rpt_check_options)			///
		label("Suppress fixed-effects table")
	CHECKBOX ck_estmetric		@	_ms	@	.,	///
		option(estmetric)					///
		onclickon(script rpt_estmetric_on)			///
		onclickoff(script rpt_estmetric_off)			///
		label("Show parameter estimates in the estimation metric")
	CHECKBOX ck_noheader		@	_ms	@	.,	///
		option(noheader)					///
		label("Suppress output header")
	CHECKBOX ck_nogroup		@	_ms	@	.,	///
		option(nogroup)						///
		label("Suppress table summarizing groups")
	CHECKBOX ck_nolrtest		@	_ms	@	.,	///
		option(nolrtest)					///
		label("Do not perform LR test comparing with logistic regression")

	DEFINE _x _lft
	DEFINE _y _ls
	INCLUDE _bu_factor_vars_reporting

	DEFINE _x _lft2
	DEFINE _y  @
	INCLUDE _bu_coef_table_reporting
END

PROGRAM rpt_check_options
BEGIN
	call rpt.ck_estmetric.enable
	if rpt.rb_stddev | rpt.ck_noretable | rpt.ck_nofetable {
		call rpt.ck_estmetric.disable
	}
	if !rpt.rb_stddev & !rpt.ck_noretable. & !rpt.ck_nofetable  {
		call rpt.ck_estmetric.enable
	}
END

SCRIPT rpt_estmetric_on
BEGIN
	rpt.rb_variance.disable
	rpt.rb_stddev.disable
	rpt.ck_noretable.disable
	rpt.ck_nofetable.disable
END

SCRIPT rpt_estmetric_off
BEGIN
	rpt.rb_variance.enable
	rpt.rb_stddev.enable
	rpt.ck_noretable.enable
	rpt.ck_nofetable.enable
END

INCLUDE fmt_coef_table_reporting_pr
INCLUDE factor_vars_reporting_pr

DIALOG int,  tabtitle("Integration")
BEGIN
	RADIO	rb_intp		_lft	_top	_iwd	., first	///
		onclickon(int.ed_intp.enable)				///
		onclickoff(int.ed_intp.disable)				///
		label("Number of integration points for adaptive Gaussian quadrature")
	EDIT	ed_intp		_iilft	_ms	_vnwd	.,		///
		default(7)						///
		option(intpoints)					///
		label("Number of integration points for adaptive Gaussian quadrature")
	RADIO	rb_intm		_lft	_ms	_iwd	., last		///
		option(laplace)						///
		label("Use Laplacian approximation")
END

INCLUDE max_ml

PROGRAM	fe_opt_output
BEGIN
	option main.ck_nocons
	optionarg main.vn_offset
END

PROGRAM rpt_output
BEGIN
	optionarg /hidedefault rpt.sp_level
	option rpt.ck_or
	option rpt.rb_stddev
	option rpt.ck_noretable
	option rpt.ck_nofetable
	option rpt.ck_estmetric
	option rpt.ck_noheader
	option rpt.ck_nogroup
	option rpt.ck_nolrtest
	put " " rpt_bu_facvarsResults
	put " " rpt_bu_fmtcoefResults
END

PROGRAM	command
BEGIN
	call mainEQUCommandValues.copyToString main_equ_cmd_output
	call main_equ_cmd_output.tokenizeOnStr main_equ_toklist "||"
	call main_equ_tmpcount.storeClsArraySize main_equ_toklist
	call main_equ_tmpcount.withvalue main_equ_cmd_last.setvalue class main_equ_toklist[@]

	INCLUDE _by_pr
	put "meqrlogit "
	varlist main.vn_dv [main.vl_iv]
	put " " /program ifin_output

	if main_equ_cmd_last.contains(",") {
		beginoptions
			put " " /program fe_opt_output
			put " " main_equ_cmd_output
		endoptions
	}
	else {
		if main_equ_cmd_output {
			if main.ck_nocons | main.vn_offset {
				put ","
			}
			if main.ck_nocons {
				put " noconstant"
			}
			if main.vn_offset {
				put " offset("
				put main.vn_offset
				put ")"
			}
			put " " main_equ_cmd_output
		}
		else {
			beginoptions
				put " " /program fe_opt_output
			endoptions
		}
	}

	beginoptions
		if main.rb_variable {
			optionarg main.vn_binomial
		}
		if main.rb_fixed {
			optionarg main.sp_binomial
		}
		if int.rb_intp {
			require int.ed_intp
			optionarg /hidedefault int.ed_intp
		}
		option int.rb_intm
		put " " /program rpt_output
		put " " /program max_output
	endoptions
END
