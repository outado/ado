/*
    merge_multiple.dlg

*!  VERSION 1.0.0  04apr2005

*/

VERSION 9.0

INCLUDE _std_large
DEFINE _dlght 350
INCLUDE header

HELP hlp1, view("help merge")
RESET res1

DIALOG main, tabtitle("Main") 	///
	label("merge - Merge dataset in memory with multiple datasets on disk")
BEGIN
  GROUPBOX gb_type	_lft	_top	_iwd	_ht6,			///
  	label("Type of merge")
  RADIO    rb_1_to_1	_ilft	_ss	_inwd	.,			///
	first								///
	label("One-to-one on key variables")				///
	onclickon(script main_1to1_on) option(unique)
  RADIO    rb_m_to_1	@	_ss	@	.,			///
	onclickon(script main_mto1_on) option(uniqusing)		///
	label("Many-to-one on key variables (unique key for data on disk)")
  RADIO    rb_1_to_1_ob	@	_ss	@	.,			///
	last								///
	label("One-to-one by observation")				///
	onclickon(script main_1_to_1_ob_on)
  GROUPBOX gb_merge	_lft	_xls	_iwd	_ht19h,			///
	label("Merge dataset in memory with datasets on disk")
  TEXT     tx_browse	_ilft	_ss	_ibwd	.,			///
	label("Select dataset on disk:")
  FILE     fi_filename  @       _ss	@	.,			///
	error("Select dataset on disk")					///
	filter("Stata Dataset (*.dta)|*.dta|All Files (*.*)|*.*")	///
	defext(dta) label("Browse...")
  BUTTON   bu_dtaadd    @	_ms	_cwd3_2	.,			///
	label("Select an additional file") onpush(program main_dta_add)

  TEXT     tx_include	@	_ls	_cwd1	.,			///
	label("List of files on disks:")
  EDIT     ed_multiple	@	_ss    _ibwd	.,			///
	label("List of files on disks")


  TEXT     tx_var	@	_ls	@	.,			///
	label("Key variables common to all datasets: (match variables)")
  VARLIST  vl_varlist	@	_ss	@	.,			///
	label("Key variables")
  CHECKBOX  ck_sort	@	_ls	@	.,			///
	label("Sort key variables when necessary") option(sort)		///
	onclickon(program main_sort_on)					///
	onclickoff(program main_sort_off)
END

SCRIPT main_1to1_on
BEGIN
	main.tx_var.show
	main.ck_sort.show
	main.vl_varlist.show
END

SCRIPT main_1tom_on
BEGIN
	main.tx_var.show
	main.ck_sort.hide
	main.vl_varlist.show
END

SCRIPT main_mto1_on
BEGIN
	main.tx_var.show
	main.ck_sort.hide
	main.vl_varlist.show
END

SCRIPT main_1_to_1_ob_on
BEGIN
	main.tx_var.hide
	main.ck_sort.hide
	main.vl_varlist.hide
END

DIALOG opt, tabtitle("Options")
BEGIN
  TEXT      tx_keep	_lft	_top	_iwd	.,			///
	label("Keep the following variables: (If not specified, all variables are kept)")
  VARLIST   vl_keep	_lft	_ss	_iwd	.,			///
	option(keep) label("Keep the following variables")
  TEXT      tx_merge	_lft	_ls	_iwd	.,			///
	label("Specify name of new variable to mark result of merge:")
  EDIT      ed_merge	_lft	_ss	_vnwd	.,			///
	max(32)	option(_merge) default("_merge")			///
	label("Specify name of new variable to mark result of merge")
  CHECKBOX  ck_nolabel	_lft	_ls	_iwd	.,			///
	option(nolabel)	label("Do not copy value label definitions from file")
  DEFINE    _y @y
  CHECKBOX ck_nonotes	@	_ms	@	.,			///
	label("Do not copy notes from file") option(nonotes)
  CHECKBOX  ck_nokeep	@	_ms	@	.,			///
	option(nokeep) label("Drop observations in dataset on disk that do not match")
  CHECKBOX ck_nosummary	@	_ms	@	.,			///
	option(nosummary) label("Drop summary variables")
END

PROGRAM main_dta_add
BEGIN
	if main.fi_filename {
		call script main_add
	}
END

SCRIPT main_add
BEGIN
	  main.fi_filename.withvalue main.ed_multiple.smartinsert `""@""'
	  main.fi_filename.setvalue ""
END

PROGRAM command
BEGIN
	if ! main.ed_multiple & ! main.fi_filename {
		stopbox stop `"On the "Main" tab,"'	///
			`"A dataset on disk must be selected""'
	}
	put "merge "
	if main.rb_1_to_1 | main.rb_m_to_1 {
		require main.vl_varlist
		put " " main.vl_varlist
	}
	put " " "using "
	put main.ed_multiple " "
	if main.fi_filename {
		put `"""'
		put main.fi_filename
		put `"""' " "
	}
	beginoptions
		option radio(main rb_1_to_1 rb_m_to_1)
		option main.ck_sort
		optionarg opt.vl_keep
		option opt.ck_nolabel
		option opt.ck_nonotes
		option opt.ck_nokeep
		option opt.ck_nosummary
		optionarg /hidedefault opt.ed_merge
	endoptions
END
