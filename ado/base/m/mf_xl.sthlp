{smcl}
{* *! version 1.0.9  21oct2013}{...}
{vieweralsosee "[M-5] xl()" "mansection M-5 xl()"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[M-2] class" "help m2_class"}{...}
{vieweralsosee "[M-5] _docx*()" "help mf__docx"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[M-4] io" "help m4_io"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[D] import excel" "help import excel"}{...}
{vieweralsosee "[P] putexcel" "help putexcel"}{...}
{viewerjumpto "Syntax" "mf_xl##syntax"}{...}
{viewerjumpto "Description" "mf_xl##description"}{...}
{viewerjumpto "Remarks" "mf_xl##remarks"}{...}
{title:Title}

{p 4 8 2}
{manlink M-5 xl()} {hline 2} Excel file I/O class


{marker syntax}{...}
{title:Syntax}

{pstd}
If you are reading this entry for the first time, skip to 
{bf:{help mf_xl##description:Description}}.  If you are trying to import or 
export an Excel file from or to Stata, see
{manhelp import_excel D:import excel}. 
If you are trying to export a table created by Stata to Excel, see 
{manhelp putexcel P}.

{pstd}
The syntax diagrams below describe a Mata class.  For
help with class programming in Mata, see {manhelp m2_class M-2:class}.

{p 4 4 2}
Syntax is presented under the following headings:

	{help mf_xl##syn_step1:Step 1:  Initialization}
	{help mf_xl##syn_step2:Step 2:  Creating and opening an Excel workbook}
	{help mf_xl##syn_step3:Step 3:  Setting the Excel worksheet}
	{help mf_xl##syn_step4:Step 4:  Reading and writing data from and to an Excel worksheet}
	{help mf_xl##syn_utility:Utility functions for use in all steps}
	
{p 4 4 2}
Click on the [] at the left of each function to find out more about the
function.

{marker syn_step1}{...}
    {title:Step 1: Initialization}

{p 8 25 1}
{help mf_xl##init:[]}{...}
{bind:               }
{it:{help mf_xl##def_B:B}}
{cmd:=}
{cmd:xl()}


{marker syn_step2}{...}
    {title:Step 2: Creating and opening an Excel workbook}

{p 8 45 2}
{help mf_xl##create_book:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.create_book(}{cmd:"}{it:filename}{cmd:",} {cmd:"}{it:sheetname}{cmd:"} {break}
[{cmd:,} {c -(}{cmd:"xls"} | {cmd:"xlsx"}{c )-}]{cmd:)}

{p 8 25 1}
{help mf_xl##load_book:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.load_book(}{cmd:"}{it:filename}{cmd:"}{cmd:)}

{p 8 25 1}
{help mf_xl##clear_book:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.clear_book(}{cmd:"}{it:filename}{cmd:"}{cmd:)}

{p 8 25 1}
{help mf_xl##set_mode:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.set_mode(}{cmd:"open"}|{cmd:"closed"}{cmd:)}

{p 8 25 1}
{help mf_xl##close_book:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.close_book()}


{marker syn_step3}{...}
    {title:Step 3: Setting the Excel worksheet}

{p 8 25 1}
{help mf_xl##add_sheet:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.add_sheet(}{cmd:"}{it:sheetname}{cmd:"}{cmd:)}

{p 8 25 1}
{help mf_xl##set_sheet:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.set_sheet(}{cmd:"}{it:sheetname}{cmd:"}{cmd:)}

{p 8 25 1}
{help mf_xl##clear_sheet:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.clear_sheet(}{cmd:"}{it:sheetname}{cmd:"}{cmd:)}

{p 8 25 1}
{help mf_xl##get_sheets:[]}{...}
{it:string colvector}{bind:   }
{it:{help mf_xl##def_B:B}}{cmd:.get_sheets(}{cmd:)}


{marker syn_step4}{...}
    {title:Step 4: Reading and writing data from and to an Excel worksheet}

{p 8 45 1}
{help mf_xl##set_missing:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.set_missing(}[{it:real scalar num}|{break}
{it:string scalar val}]{cmd:)}

{p 8 45 2}
{help mf_xl##get_string:[]}{...}
{it:string matrix}{bind:      }
{it:{help mf_xl##def_B:B}}{cmd:.get_string(}{it:real vector row}{cmd:,}
{break}{it:real vector col}{cmd:)}

{p 8 45 2}
{help mf_xl##get_number:[]}{...}
{it:real matrix}{bind:        }
{it:{help mf_xl##def_B:B}}{cmd:.get_number(}{it:real vector row}{cmd:,}
{break}{it:real vector col}{break}
[{cmd:,} {c -(}{cmd:"asdate"} | {cmd:"asdatetime"}{c )-}]{cmd:)}

{p 8 45 2}
{help mf_xl##get_cell_type:[]}{...}
{it:string matrix}{bind:      }
{it:{help mf_xl##def_B:B}}{cmd:.get_cell_type(}{it:real vector row}{cmd:,}
{break}{it:real vector col}{cmd:)}

{p 8 45 2}
{help mf_xl##put_string:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.put_string(}{it:real scalar row}{cmd:,}{break}
{it:real scalar col}{cmd:,}{break}{it:string matrix s}{cmd:)}

{p 8 45 2}
{help mf_xl##put_number:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.put_number(}{it:real scalar row}{cmd:,}{break}
{it:real scalar col}{cmd:,}{break}
{it:real matrix r}{break}
[{cmd:,} {c -(}{cmd:"asdate"} | {cmd:"asdatetime"}{c )-}]{cmd:)}


{marker syn_utility}{...}
    {title:Utility functions for use in all steps}

{p 8 25 1}
{help mf_xl##query:[]}{...}
{it:(varies)}{bind:           }
{it:{help mf_xl##def_B:B}}{cmd:.query(}[{cmd:"}{it:item}{cmd:"}]{cmd:)}

{p 8 25 1}
{help mf_xl##get_colnum:[]}{...}
{it:real vector}{bind:        }
{it:{help mf_xl##def_B:B}}{cmd:.get_colnum(}{it:string vector}{cmd:)}

{p 8 25 1}
{help mf_xl##set_keep_cell_format:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.set_keep_cell_format(}{cmd:"on"}|{cmd:"off"}{cmd:)}

{p 8 25 1}
{help mf_xl##set_error_mode:[]}{...}
{it:(void)}{bind:             }
{it:{help mf_xl##def_B:B}}{cmd:.set_error_mode(}{cmd:"on"}|{cmd:"off"}{cmd:)}

{p 8 25 1}
{help mf_xl##get_last_error:[]}{...}
{it:real scalar}{bind:        }
{it:{help mf_xl##def_B:B}}{cmd:.get_last_error()}

{p 8 25 1}
{help mf_xl##get_last_error_message:[]}{...}
{it:string scalar}{bind:      }
{it:{help mf_xl##def_B:B}}{cmd:.get_last_error_message()}

{p 8 8 2}
where {it:item} can be

		{cmd:filename}
		{cmd:mode}
		{cmd:filetype}
		{cmd:sheetname}
		{cmd:missing}


{marker description}{...}
{title:Description}

{p 4 4 2}
The {cmd:xl()} class allows you to create Excel 1997/2003 ({cmd:.xls})
files and Excel 2007/2013 ({cmd:.xlsx}) files and load them from and to Mata
matrices.  The two Excel file types have different data size limits that you
can read about in
{it:{help import_excel##technote1:Technical note: Excel data size limits}} of
{cmd:import excel}.  The {cmd:xl()} class is supported on Windows, Mac, and
Linux.


{marker remarks}{...}
{title:Remarks}

{p 4 4 2}
Remarks are presented under the following headings:

	{help mf_xl##syn_B:Definition of B}
	{help mf_xl##syn_book_funcs:Specifying the Excel workbook}
	{help mf_xl##syn_sheet_funcs:Specifying the Excel worksheet}
	{help mf_xl##syn_get_funcs:Reading data from Excel}
	{help mf_xl##syn_put_funcs:Writing data to Excel}
	{help mf_xl##syn_miss_func:Dealing with missing values}
	{help mf_xl##syn_dates:Dealing with dates}
	{help mf_xl##syn_util_funcs:Utility functions}
	{help mf_xl##syn_util_error:Handling errors}
	{help mf_xl##syn_error_codes:Errors codes}


{marker syn_B}{...}
{marker def_B}{...}
{marker init}{...}
    {title:Definition of B}

{p 4 4 2}
A variable of type {cmd:xl} is called an
{help m6_glossary##instance:instance} of the {cmd:xl()}
class.  {it:B} is an instance of {cmd:xl()}.  You can use the class
interactively:

		{cmd}b = xl()
		b.create_book("results", "Sheet1")
		...{txt}

{p 4 4 2}
In a function, you would declare one instance of the {cmd:xl()} class {it:B}
as a {cmd:scalar}.

		{cmd}void myfunc()
		{
			class xl scalar   b

			b = xl()
			b.create_book("results", "Sheet1")
			...
		}{txt}

{p 4 4 2}
When using the class inside other functions, you do not need to create the
instance explicitly as long as you declare the member-instance variable to be a
{cmd:scalar}:


		{cmd}void myfunc()
		{
			class xl scalar   b

			b.create_book("results", "Sheet1")
			...
		}{txt}


{marker syn_book_funcs}{...}
    {title:Specifying the Excel workbook}

{p 4 4 2}
To read from or write to an existing Excel workbook, you need to tell
{cmd:xl()} class about that workbook.  To create a new workbook to write to,
you need to tell {cmd:xl()} class what to name that workbook and what type of
Excel file that workbook should be.   Excel 1997/2003 ({cmd:.xls}) files and
Excel 2007/2010 ({cmd:.xlsx}) files can be created.  You must either load or
create a workbook before you can use any sheet or read or write
{it:{help m2_class##def_member:member functions}} of {cmd:xl()} class.

{marker create_book}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.create_book(}{cmd:"}{it:filename}{cmd:"},
{cmd:"}{it:sheetname}{cmd:"},
[{cmd:,} {c -(}{cmd:"xls"} | {cmd:"xlsx"}{c )-}]{cmd:)}
     creates an Excel workbook named {it:filename} with the sheet
     {it:sheetname}.  By default, an {cmd:.xls}
     file is created.  If you use the optional {cmd:.xlsx} argument, then an
     {cmd:.xlsx} file is created.

{marker load_book}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.load_book(}{cmd:"}{it:filename}{cmd:"}{cmd:)}
     loads an existing Excel workbook.  Once it is loaded, you can read from
     or write to the workbook.

{marker clear_book}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.clear_book(}{cmd:"}{it:filename}{cmd:"}{cmd:)}
     removes all worksheets from an existing Excel workbook.

{pstd}
To create an {cmd:.xlsx} workbook, code

		{cmd}b = xl()
		b.create_book("results", "Sheet1", "xlsx"){txt}

{pstd}
To load an {cmd:.xls} workbook, code

		{cmd}b = xl()
		b.load_book("Budgets.xls"){txt}

{pstd}
The {cmd:xl()} class will open and close the workbook for each member
function you use that reads from or writes to the workbook.  This is done by
default, so you do not have to worry about opening and closing a file handle.
This can be slow if you are reading or writing data at the cell level.  In
these cases, you should leave the workbook open, deal with your data, and then
close the workbook.  The following member functions allow you to control how
the class handles file I/O.

{marker set_mode}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.set_mode(}{cmd:"open"}|{cmd:"closed"}{cmd:)}
     sets whether the workbook file is left open for reading or writing data.
     {cmd:set_mode("closed")}, the default, means that the workbook is
     opened and closed after every sheet or read or write member function.

{marker close_book}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.close_book()}
     closes a workbook file if the file has been left open using {cmd:set_mode("open")}.

{pstd}
Below is an example of how to speed up file I/O when writing data.

		{cmd}b = xl()
		b.create_book("results", "year1")

		b.set_mode("open")
		for(i=1;i<10000;i++) {
			b.put_number(i,1,i)
			...
		}
		b.close_book(){txt}


{marker syn_sheet_funcs}{...}
    {title:Specifying the Excel worksheet}

{p 4 4 2}
The following member functions are used to set the active worksheet the
{cmd:xl()} class will use to read data from or write data to.  By default, if
you do not specify a worksheet, {cmd:xl()} class will use the first worksheet
in the workbook.

{marker add_sheet}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.add_sheet(}{cmd:"}{it:sheetname}{cmd:"}{cmd:)}
      adds a new worksheet named {it:sheetname} to the workbook and sets
      the active worksheet to that sheet.

{p 8 12 2}
{marker set_sheet}{...}
{it:{help mf_xl##def_B:B}}{cmd:.set_sheet(}{cmd:"}{it:sheetname}{cmd:"}{cmd:)}
      sets the active worksheet to {it:sheetname} in the {cmd:xl()} class.

{p 4 4 2}
The following member functions are sheet utilities:

{marker clear_sheet}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.clear_sheet(}{cmd:"}{it:sheetname}{cmd:"}{cmd:)}
      clears all cell values for {it:sheetname}.

{marker get_sheets}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.get_sheets()} returns a {cmd:string colvector}
of all the sheetnames in the current workbook.

{p 4 4 2}
You may need to make a change to all the sheets in a workbook.
{cmd:get_sheets()} can help you do this.

		{cmd}void myfunc()
		{
			class xl scalar   b
			string colvector  sheets
			real scalar	  i

			b.load_book("results")
			sheets = b.get_sheets()

			for(i=1;i<rows(sheets);i++) {
				b.set_sheet(sheets[i])
				b.clear_sheet(sheets[i])
				...
			}
		}{txt}

{p 4 4 2}
To create a new workbook with multiple new sheets, code

		{cmd}b.create_book("Budgets", "Budget 2009")

		for(i=10;i<13;i++) {
			sheet = "Budget 20" + strofreal(i)
			b.add_sheet(sheet)
		}{txt}


{marker syn_get_funcs}{...}
    {title:Reading data from Excel}

{p 4 4 2}
The following member functions of
{cmd:xl()} class are used to read data.  Both {it:row} and {it:col} can be a
{cmd:real} {cmd:scalar} or a 1x2 {cmd:real} {cmd:vector}.

{marker get_string}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.get_string(}{it:row}{cmd:,} {it:col}{cmd:)}
     returns a string matrix containing values in a cell range depending
     on the range specified in {it:row} and {it:col}.

{marker get_number}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.get_number(}{it:row}{cmd:,} {it:col}
[{cmd:,} {c -(}{cmd:"asdate"} | {cmd:"asdatetime"}{c )-}]{cmd:)}
     returns a {cmd:real matrix} containing values in an Excel cell range
     depending on the range specified in {it:row} and {it:col}.

{marker get_cell_type}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.get_cell_type(}{it:row}{cmd:,} {it:col}{cmd:)}
     returns a {cmd:string matrix} containing the string values
     {cmd:numeric}, {cmd:string}, {cmd:date}, {cmd:datetime}, or {cmd:blank}
     for each Excel cell in the Excel cell range specified in {it:row} and
     {it:col}.

{p 4 4 2}
To get the value in cell {cmd:A1} from Excel into a {cmd:string scalar}, code

		{cmd}string scalar val

		val = b.get_string(1,1){txt}

{p 4 4 2}
If {cmd:A1} contained the value {cmd:"Yes"}, then {cmd:val} would contain
{cmd:"Yes"}.  If {cmd:A1} contained the numeric value {cmd:1}, then {cmd:val}
would contain {cmd:1}.  {cmd:get_string()} will convert numeric values
to strings.

{p 4 4 2}
To get the value in cell {cmd:A1} from Excel into a {cmd:real scalar}, code

		{cmd}real scalar val

		val = b.get_number(1,1){txt}

{p 4 4 2}
If {cmd:A1} contained the value {cmd:"Yes"}, then {cmd:val} would contain a
missing value.  {cmd:get_number} will return a missing value for a string value.
If {cmd:A1} contained the numeric value {cmd:1}, then {cmd:val} would contain
the value {cmd:1}.

{p 4 4 2}
To read a range of data into Mata, you must specify the cell range by using a
1x2 {cmd:rowvector}. To read row {cmd:1}, columns {cmd:B} through {cmd:F} of a
worksheet, code

		{cmd}string rowvector cells
		real rowvector cols

		cols = (2,6)
		cells = b.get_string(1,cols){txt}

{p 4 4 2}
To read rows {cmd:1} through {cmd:3} and columns {cmd:B} through {cmd:D} of a
worksheet, code

		{cmd}real matrix cells
		real rowvector rows, cols

		rows = (1,3)
		cols = (2,4)
		cells = b.get_number(rows,cols){txt}


{marker syn_put_funcs}{...}
    {title:Writing data to Excel}

{p 4 4 2}
The following member functions of {cmd:xl()} class are used to write data.
{it:row} and {it:col} are {cmd:real} {cmd:scalars}.  When you write a matrix or
vector, {it:row} and {it:col} are the starting (upper-left) cell in the Excel
worksheet to which you want to begin saving.

{marker put_string}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.put_string(}{it:row}{cmd:,} {it:col}{cmd:,}
{it:s}{cmd:)}
    writes a {cmd:string} {cmd:scalar}, {cmd:vector}, or {cmd:matrix} to an
    Excel worksheet.

{marker put_number}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.put_number(}{it:row}{cmd:,} {it:col}{cmd:,}
{it:r}[{cmd:,} {c -(}{cmd:"asdate"} | {cmd:"asdatetime"}{c )-}]{cmd:)}
    writes a {cmd:real} {cmd:scalar}, {cmd:vector}, or {cmd:matrix} to an
    Excel worksheet.

{p 4 4 2}
To write the string {cmd:"Auto Dataset"} in cell {cmd:A1} of a worksheet, code

		{cmd:b.put_string(1, 1, "Auto Dataset")}

{p 4 4 2}
To write {cmd:mpg}, {cmd:rep78}, and {cmd:headroom} to cells {cmd:B1} through
{cmd:D1} in a worksheet, code

		{cmd}names = ("mpg", "rep78", "headroom")
		b.put_string(1, 2, names){txt}

{p 4 4 2}
To write values {cmd:22}, {cmd:17}, {cmd:22}, {cmd:20}, and {cmd:15} to cells
{cmd:B2} through {cmd:B6} in a worksheet, code

		{cmd}mpg_vals = (22\17\22\20\15)
		b.put_number(2, 2, mpg_vals){txt}


{marker syn_miss_func}{...}
    {title:Dealing with missing values}

{marker set_missing}{...}
{p 4 4 2}
{cmd:set_missing(}{cmd:)} sets how Mata missing values are to be treated when
writing data to a worksheet.  Here are the three syntaxes:

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.set_missing(}{cmd:)} specifies that missing
values be written as blank cells.  This is the default.

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.set_missing(}{it:num}{cmd:)} specifies that
missing values be written as the {cmd:real scalar} {it:num}.

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.set_missing(}{it:val}{cmd:)} specifies that
missing values be written as the {cmd:string scalar} {it:val}.

{p 4 4 2}
Let's look at an example.

		{cmd}my_mat = J(1,3,.)

		b.load_book("results")
		b.set_sheet("Budget 2012")

		b.set_missing(-99)
		b.put_number(1, 1, my_mat)
		b.set_missing("no data")
		b.put_number(2, 1, my_mat)
		b.set_missing()
		b.put_number(3, 1, my_mat){txt}

{p 4 4 2}
This code would write the numeric value {cmd:-99} in cells {cmd:A1} through
{cmd:C1} and {cmd:"no data"} in cells  {cmd:A2} through {cmd:C2}; cells
{cmd:A3} through {cmd:C3} would be blank.


{marker syn_dates}{...}
    {title:Dealing with dates}

{p 4 4 2}
Say that cell {cmd:A1} contained the date value {cmd:1/1/1960}. If
you coded

		{cmd}mydate = b.get_number(1,1)
		mydate
		21916{txt}

{p 4 4 2}
the value displayed, {cmd:21916}, is the number of days since 31dec1899. If we
used the optional {cmd:get_number()} argument {cmd:"asdate"} or
{cmd:"asdatetime"}, {cmd:mydate} would contain {cmd:0} because the date
{cmd:1/1/1960} is {cmd:0} for both {it:{help mf_date##td:td}} and
{it:{help mf_date##tc:tc}} dates.  To store {cmd:1/1/1960} in Mata, code

		{cmd}mysdate = b.get_string(1,1)
		mysdate
		1/1/1960{txt}

{p 4 4 2}
To write dates to Excel, you must tell {cmd:xl()} class how to convert the date
to Excel's date or datetime format.  To write the date {cmd:1/1/1960 00:00:00}
to Excel, code

		{cmd}b.put_number(1,1,0, "asdatetime"){txt}

{p 4 4 2}
To write the dates {cmd:1/1/1960}, {cmd:1/2/1960}, and {cmd:1/3/1960} to Excel
column {cmd:A}, rows {cmd:1} through {cmd:3}, code

		{cmd}date_vals = (0\1\2)
		b.put_number(1, 1, date_vals, "asdate"){txt}

{pstd}
Note: Excel has two different date systems; see 
{it:{help import_excel##technote2:Technical note: Dates and times}} in
{cmd:import excel}.


{marker syn_util_funcs}{...}
    {title:Utility functions}

{p 4 4 2}
The following functions can be used whenever you have an instance of 
{cmd:xl()} class.

{marker query}{...}
{p 4 4 2}
{cmd:query()} returns information about an {cmd:xl()} class.
Here are the syntaxes for {cmd:query()}:

	{it:void} 			{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:)}
	{it:string scalar}		{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"filename"}{cmd:)}
	{it:real scalar}		{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"mode"}{cmd:)}
	{it:real scalar}		{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"filetype"}{cmd:)}
	{it:string scalar}		{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"sheetname"}{cmd:)}
	{it:transmorphic scalar}	{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"missing"}{cmd:)}


{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:)}
	lists the current values and setting of the class.

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"filename"}{cmd:)}
	returns the filename of the current workbook.

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"mode"}{cmd:)}
	returns {cmd:0} if the workbook is always closed by member functions
        or returns {cmd:1} if the current workbook is open.

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"filetype"}{cmd:)}
	returns {cmd:0} if the workbook is of type {cmd:.xls} or returns
	{cmd:1} if the workbook is of type {cmd:.xlsx}.

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"sheetname"}{cmd:)}
	returns the active sheetname in a string scalar.

{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.query(}{cmd:"missing"}{cmd:)}
	returns {cmd:J(1,0,.)} (if set to {cmd:blanks}), a {cmd:string scalar},
        or a {cmd:real scalar} depending on what was set with
        {helpb mf_xl##set_missing:set_missing()}.

{p 4 4 2}
When working with different Excel file types, you need to know the type of
Excel file you are using because the two file types have different column and
row limits.  You can use {cmd:xl.query("filetype")} to obtain that information.

	{cmd}...
	if (xl.query("filetype")) {
		...
	}
	else {
		...
	}{txt}

{marker get_colnum}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.get_colnum()} returns a vector of
    column numbers based on the Excel column labels in the string vector
    argument.

{p 4 4 2}
To get the column number for Excel columns {cmd:AA} and {cmd:AD}, code

	: {cmd:col = b.get_colnum("AA","AD")}
	: {cmd:col}
	{txt}       {txt} 1    2
	    {c TLC}{hline 11}{c TRC}
	  1 {c |}  {res}27   30{txt}  {c |}
	    {c BLC}{hline 11}{c BRC}

{p 4 4 2}
The following function is used for cell formats and styles.

{marker set_keep_cell_format}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.set_keep_cell_format(}{cmd:"on"}|{cmd:"off"}{cmd:)}
     sets whether the {cmd:put_number()} class member function preserve a
     cell's style and format when writing a value.  By default, preserving a 
     cell's style and format is {cmd:off}.

{p 4 4 2}
The following functions are used for error handling with an instance of class
{cmd:xl}.

{marker set_error_mode}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.set_error_mode(}{cmd:"on"}|{cmd:"off"}{cmd:)}
     sets whether {cmd:xl()} class member functions issue errors.
     By default, errors are turned {cmd:on}.

{marker get_last_error}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.get_last_error(}{cmd:)}
     returns the last error code issued by the {cmd:xl()} class if
     {cmd:set_error_mode()} is set {cmd:off}.

{marker get_last_error_message}{...}
{p 8 12 2}
{it:{help mf_xl##def_B:B}}{cmd:.get_last_error_message(}{cmd:)}
     returns the last error message issued by the {cmd:xl()} class if
     {cmd:set_error_mode()} is set {cmd:off}.


{marker syn_util_error}{...}
    {title:Handling errors}

{p 4 4 2}
Turning errors off for an instance of {cmd:xl()} class is useful when using the
class in an {help m1_ado:ado-file}.  You should issue a Stata error code in
the ado-file instead of a Mata error code.  For example, in Mata, when trying
to load a file that does not exist within an instance, you will receive the
error code {cmd:r(16103)}:

	{cmd:: b = xl()}
	{cmd:: b.load_book("zzz")}
	file zzz.xls could not be loaded
	r(16103);

{p 4 4 2}
The correct Stata error code for this type of error is {cmd:603}, not
{cmd:16103}.  To issue the correct error, code

	{cmd}b = xl()
	b.set_error_mode("off")
	b.load_book("zzz")
	if (b.get_last_error()==16103) {
		error(603)
	}{txt}

{p 4 4 2}
You should also turn off errors if you
{helpb mf_xl##set_mode:set_mode("open")} because you need to close your Excel
file before exiting your ado-file.  You should code

	{cmd}b = xl()
	b.set_mode("open")
	b.set_error_mode("off")
	b.load_book("zzz")
	...
	b.put_string(1,300, "zzz.xls")
	if (b.get_last_error()==16103) {
		b.close_book()
		error(603)
	}{txt}

{p 4 4 2}
If {cmd:set_mode("closed")} is used, you do not have to worry about closing the
Excel file because it is done automatically.


{marker syn_error_codes}{...}
    {title:Error codes}

{p 4 4 2}
The error codes specific to the {cmd:xl()} class are the following:

	 Code    Meaning
	{hline 67}
	 16101    file not found
	 16102    file already exists
	 16103    file could not be opened
	 16104    file could not be closed
	 16105    file is too big
	 16106    file could not be saved
	 16111    worksheet not found
	 16112    worksheet already exists
	 16113    could not clear worksheet
	 16114    could not add worksheet
	 16115    could not read from worksheet 
	 16116    could not write to worksheet
	 16121    invalid syntax
	 16122    invalid range
	{hline 67}
