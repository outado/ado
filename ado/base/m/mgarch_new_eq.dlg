/*
  mgarch_new_eq.dlg

*!  VERSION 1.0.1  12oct2012

  keyword:  eclass

*/

VERSION 12.0

INCLUDE _std_xlarge
DEFINE _dlght 310
INCLUDE header

RESET res1

DIALOG main, tabtitle("Main")
BEGIN
  TEXT tx_dep		_lft	_top	_iwd	.,			///
	label("Dependent variables:")
  VARLIST vl_dep	@	_ss	@	.,			///
	ts								///
	numeric								///
	label("Dependent variables")

  TEXT tx_indep		@	_ls	@	.,			///
	label("Independent variables: (optional)")
  VARLIST vl_indep	@	_ss	_iwd	.,			///
	fv ts								///
	numeric								///
	label("Independent variables") 
  CHECKBOX ck_nocons	@	_ls	@	.,			///
	label("Suppress the constant term")				///
	option(noconstant)

  GROUPBOX gb_opts	_lft	+30	_iwd	_ht13,			///
	label("Equation-specific options (not allowed with diagonal-vech model)")

  RADIO rb_lag_max	_indent	_ss	_cwd3	., first		///
 	onclickon("program main_lag_on")				///
	label("Specify maximum lags:")
  RADIO rb_lag_list	_lft2	@	_cwd3	., last			///
 	onclickon("script main_lags_on")				///
	label(`"Specify list of lags: (e.g., "1 3")"')
  DEFINE y1 @y
  CHECKBOX ck_arch	40	_ms	_ckwd	.,			///
 	onclickon("script main_arch_on")				///
 	onclickoff("script main_arch_off")
  DEFINE holdy @y
  DEFINE holdx @x
  SPINNER sp_arch	_cksep	@	_spwd	.,			///
	option(arch)							///
	min(1)                  					/// 
	max(c(N))                      					/// 
	default(1)							///
	label("ARCH maximum lag")
  TEXT tx_arch		_spsep	@	_rbspr2b .,			///
	label("ARCH maximum lag")

  CHECKBOX ck_garch	holdx	_ls	_ckwd	.,			///
	onclickon("script main_garch_on")				///
	onclickoff("script main_garch_off")
  SPINNER sp_garch	_cksep	@	_spwd	.,			///
	option(garch)							///
	min(1)                  					/// 
	max(c(N))                      					/// 
	default(1)							///
	label("GARCH maximum lag")
  TEXT tx_garch		_spsep	@	_rbspr2b .,			///
	label("GARCH maximum lag")

  EDIT ed_archs		_iilft2	holdy	_en14wd	.,			///
	option(arch)							///
	label("ARCH lags")
  DEFINE holdx @x
  TEXT tx_archs		_en14sep @	100	.,			///
	label("ARCH lags")
  EDIT ed_garchs	holdx	_ls	_en14wd	.,			///
	option(garch)							///
	label("GARCH lags")
  TEXT tx_garchs	_en14sep @	100	.,			///
	label("GARCH lags")

  CHECKBOX ck_het	_ilft	_ls	_ibwd	.,			///
	onclickon(main.vl_het.enable)					///
	onclickoff(main.vl_het.disable)					///
	label("Include variables in the specification of the conditional variance:")
  VARLIST  vl_het	+20	_ss	_inwd	.,			///
	option(het)							///
	numeric								///
	label("Include variables in the specification of the conditional variance") 
END

PROGRAM main_lag_on
BEGIN
	call main.ck_arch.enable
	if main.ck_arch {
		call script main_arch_on
	}
	call main.ck_garch.enable
	if main.ck_garch {
		call script main_garch_on
	}

	call main.ed_archs.disable
	call main.tx_archs.disable
	call main.ed_garchs.disable
	call main.tx_garchs.disable
END

SCRIPT main_lags_on
BEGIN
	main.ed_archs.enable
	main.tx_archs.enable
	main.ed_garchs.enable
	main.tx_garchs.enable

	main.ck_arch.disable	
	main.sp_arch.disable
	main.tx_arch.disable
	main.ck_garch.disable	
	main.sp_garch.disable
	main.tx_garch.disable
END

SCRIPT main_arch_on
BEGIN
	main.sp_arch.enable
	main.tx_arch.enable
END

SCRIPT main_arch_off
BEGIN
	main.sp_arch.disable
	main.tx_arch.disable
END

SCRIPT main_garch_on
BEGIN
	main.sp_garch.enable
	main.tx_garch.enable
END

SCRIPT main_garch_off
BEGIN
	main.sp_garch.disable
	main.tx_garch.disable
END

PROGRAM main_output
BEGIN
	put main.vl_dep " = " main.vl_indep
	beginoptions
		if main.rb_lag_max {
			if main.ck_arch {
				put "arch(1/" main.sp_arch ") "
			}
			if main.ck_garch {
				put "garch(1/" main.sp_garch ") "
			}
		}
		if main.rb_lag_list {
			if (main.ed_archs & !main.ed_archs.isnumlist()) {
				stopbox stop `"There is an invalid numlist in "ARCH lags""'
			}
			if (main.ed_garchs & !main.ed_garchs.isnumlist()) {
				stopbox stop `"There is an invalid numlist in "GARCH lags""'
			}
			optionarg main.ed_archs
			optionarg main.ed_garchs
		}
		if main.ck_het {
			require main.vl_het
			optionarg main.vl_het
		}
		option main.ck_nocons
	endoptions
END

PROGRAM command
BEGIN
	require main.vl_dep
	put "(" /program main_output ")"
END
