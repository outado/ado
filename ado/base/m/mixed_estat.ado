*! version 1.0.1  25jun2013
program mixed_estat
	version 13
	
	if "`e(cmd)'" != "mixed" {
		error 301
	}
	
	gettoken sub rest: 0, parse(" ,")
	if `"`sub'"' == substr("wcorrelation",1,max(4,length("`sub'"))) {
		_mixed_wcorr `rest'
		exit
	}
	
	_xtme_estat `0'
	
end

exit
