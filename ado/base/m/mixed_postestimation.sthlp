{smcl}
{* *! version 1.0.6  22may2014}{...}
{viewerdialog predict "dialog mixed_p"}{...}
{viewerdialog estat "dialog mixed_estat"}{...}
{vieweralsosee "[ME] mixed postestimation" "mansection ME mixedpostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[ME] mixed" "help mixed"}{...}
{viewerjumpto "Description" "mixed postestimation##description"}{...}
{viewerjumpto "Special-interest postestimation commands" "mixed postestimation##special"}{...}
{viewerjumpto "" "--"}{...}
{viewerjumpto "Syntax for predict" "mixed postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "mixed postestimation##menu_predict"}{...}
{viewerjumpto "Options for predict" "mixed postestimation##options_predict"}{...}
{viewerjumpto "" "--"}{...}
{viewerjumpto "Syntax for estat" "mixed postestimation##syntax_estat"}{...}
{viewerjumpto "Menu for estat" "mixed postestimation##menu_estat"}{...}
{viewerjumpto "Option for estat icc" "mixed postestimation##option_estat_icc"}{...}
{viewerjumpto "Options for estat recovariance" "mixed postestimation##options_estat_recov"}{...}
{viewerjumpto "Options for estat wcorrelation" "mixed postestimation##options_estat_wcorrelation"}{...}
{viewerjumpto "" "--"}{...}
{viewerjumpto "Examples" "mixed postestimation##examples"}{...}
{viewerjumpto "Stored results" "mixed postestimation##results"}{...}
{title:Title}

{p2colset 5 34 36 2}{...}
{p2col :{manlink ME mixed postestimation} {hline 2}}Postestimation tools for
mixed{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation commands are of special interest after
{cmd:mixed}:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
{synopt :{helpb mixed postestimation##estatgroup:estat group}}summarize
the composition of the nested groups{p_end}
{synopt :{helpb mixed postestimation##estaticc:estat icc}}estimate
intraclass correlations{p_end}
{synopt :{helpb mixed postestimation##estatcov:estat recovariance}}display
the estimated random-effects covariance matrix (or matrices){p_end}
{synopt :{helpb mixed postestimation##estatwcor:estat wcorrelation}}display
model-implied within-cluster correlations and standard deviations{p_end}
{synoptline}
{p2colreset}{...}

{pstd}
The following standard postestimation commands are also available:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
INCLUDE help post_contrast
INCLUDE help post_estatic
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_estimates
INCLUDE help post_hausman
INCLUDE help post_lincom
INCLUDE help post_lrtest
INCLUDE help post_margins
INCLUDE help post_marginsplot
INCLUDE help post_nlcom
{synopt :{helpb mixed postestimation##predict:predict}}predictions, residuals, influence statistics, and other diagnostic measures{p_end}
INCLUDE help post_predictnl
INCLUDE help post_pwcompare
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{p2colreset}{...}


{marker special}{...}
{title:Special-interest postestimation commands}

{pstd}
{cmd:estat group} reports the number of groups and minimum, average, and
maximum group sizes for each level of the model.  Model levels are identified
by the corresponding group variable in the data.  Because groups are treated
as nested, the information in this summary may differ from what you would get
if you used the {cmd:tabulate} command on each group variable individually.

{pstd}
{cmd:estat icc} displays the intraclass correlation for pairs of responses at
each nested level of the model.  Intraclass correlations are available for
random-intercept models or for random-coefficient models conditional on
random-effects covariates being equal to 0.  They are not available for
crossed-effects models or with residual error structures other than
independent structures.

{pstd}
{cmd:estat recovariance} displays the estimated variance-covariance matrix 
of the random effects for each level in the model.  Random effects can be
either random intercepts, in which case the corresponding rows and columns of
the matrix are labeled as {cmd:_cons}, or random coefficients, in which case
the label is the name of the associated variable in the data.

{pstd}
{cmd:estat wcorrelation} displays the overall correlation matrix for a given
cluster calculated on the basis of the design of the random effects and their
assumed covariance and the correlation structure of the residuals.  This allows
for a comparison of different multilevel models in terms of the ultimate
within-cluster correlation matrix that each model implies.


{marker syntax_predict}{...}
{marker predict}{...}
{title:Syntax for predict}

{p 4 4 2}
Syntax for obtaining BLUPs of random effects, or the BLUPs' standard errors

{p 8 16 2}
{cmd:predict} {dtype} {{it:stub}{cmd:*}{c |}{it:{help newvarlist}}} {ifin}
{cmd:,} {{opt ref:fects} | {opt reses}} [{opt relev:el(levelvar)}]


{p 4 4 2}
Syntax for obtaining scores after ML estimation

{p 8 16 2}
{cmd:predict} {dtype} {c -(}{it:stub}{cmd:*}|{it:{help newvarlist}}{c )-}
{ifin} {cmd:,} {cmdab:sc:ores}


{p 4 4 2}
Syntax for obtaining other predictions

{p 8 16 2}
{cmd:predict} {dtype} {newvar} {ifin} 
[{cmd:,} {it:statistic} {opt relev:el(levelvar)}]


{synoptset 18 tabbed}{...}
{synopthdr :statistic}
{synoptline}
{syntab :Main}
{synopt :{cmd:xb}}linear prediction for the fixed portion of the 
model only; the default{p_end}
{synopt :{cmd:stdp}}standard error of the fixed-portion linear 
prediction{p_end}
{synopt :{opt fit:ted}}fitted values, fixed-portion linear prediction plus
contributions based on predicted random effects{p_end}
{synopt :{opt res:iduals}}residuals, response minus fitted values{p_end}
{p2coldent :* {opt rsta:ndard}}standardized residuals{p_end}
{synoptline}
{p2colreset}{...}
INCLUDE help unstarred


INCLUDE help menu_predict


{marker options_predict}{...}
{title:Options for predict}

{dlgtab:Main}

{phang}
{opt xb}, the default, calculates the linear prediction xb
based on the estimated fixed effects (coefficients) in the model.  This
is equivalent to fixing all random effects in the model to their theoretical
mean value of 0.

{phang}
{opt stdp} calculates the standard error of the linear predictor xb.

{marker reffects}{...}
{phang}
{opt reffects} calculates best linear unbiased predictions (BLUPs) of the 
random effects.  By default, BLUPs for all random effects in the model 
are calculated.  However, if the {opt relevel(levelvar)} option is specified,
then BLUPs for only level {it:levelvar} in the model are calculated.  For
example, if {cmd:class}es are nested within {cmd:school}s, then typing

{p 12 16 2}{cmd:. predict b*, reffects relevel(school)}{p_end}

{pmore}
would produce BLUPs at the school level.  You must specify
q new variables, where q is the number of random-effects terms
in the model (or level).   However, it is much easier to just specify 
{it:stub}{cmd:*} and let Stata name the variables {it:stub}{cmd:1},
{it:stub}{cmd:2}, ..., {it:stub}q for you.

{phang}
{opt reses} calculates the standard errors of the best linear unbiased
predictions (BLUPs) of the random effects.  By default, standard 
errors for all BLUPs in the model are calculated.  However, if the
{opt relevel(levelvar)} option is specified, then standard errors for 
only level {it:levelvar} in the model are calculated; see
the {helpb mixed postestimation##reffects:reffects} option.
You must specify q new variables, where q is the number of
random-effects terms in the model (or level).  However, it is much easier to
just specify {it:stub}{cmd:*} and let Stata name the variables
{it:stub}{cmd:1}, {it:stub}{cmd:2}, ..., {it:stub}q for you.

{pmore}
The {cmd:reffects} and {cmd:reses} options often generate multiple new 
variables at once.  When this occurs, the random effects (or standard 
errors) contained in the generated variables correspond to the order in which
the variance components are listed in the output of {cmd:mixed}.  Still,
examining the variable labels of the generated variables (with the
{cmd:describe} command, for instance) can be useful in deciphering which
variables correspond to which terms in the model.

{phang}
{opt fitted} calculates fitted values, which are equal to the fixed-portion
linear predictor plus contributions based on predicted
random effects, or in mixed-model notation, xb + Zu.   By default, the fitted
values take into account random effects from all levels in the model, however,
if the {opt relevel(levelvar)} option is specified, then the fitted values are
fit beginning at the topmost level down to and including level
{it:levelvar}.  For example, if {cmd:class}es are nested within {cmd:school}s,
then typing

{p 12 16 2}{cmd:. predict yhat_school, fitted relevel(school)}{p_end}

{pmore}
would produce school-level predictions.  That is, the predictions 
would incorporate school-specific random effects but not those for 
each class nested within each school.

{phang}
{opt residuals} calculates residuals, equal to the responses minus fitted 
values.  By default, the fitted values take into account random effects
from all levels in the model; however, if the {opt relevel(levelvar)} option is
specified, then the fitted values are fit beginning at the topmost 
level down to and including level {it:levelvar}.

{phang}
{opt rstandard} calculates standardized residuals, equal to the residuals
multiplied by the inverse square root of the estimated error covariance matrix.

{phang}
{cmd:scores} calculates the parameter-level scores, one for each parameter 
in the model including regression coefficients and variance
components.  The score for a parameter is the first derivative of the
log likelihood (or log pseudolikelihood) with respect to that parameter.
One score per highest-level group is calculated, and it is placed on the
last record within that group.  Scores are calculated in the estimation
metric as stored in {cmd:e(b)}.

{pmore}
{cmd:scores} is not available after restricted maximum-likelihood
(REML) estimation.

{phang}
{opt relevel(levelvar)} specifies the level in the model at which
predictions involving random effects are to be obtained; see the options
above for the specifics.  {it:levelvar} is the name of the model level and is
either the name of the variable describing the grouping at that level or is
{cmd:_all}, a special designation for a group comprising all the estimation
data.


{marker syntax_estat}{...}
{marker estatgroup}{marker estatcov}{marker estaticc}{marker estatwcor}{...}
{title:Syntax for estat}

{pstd}Summarize the composition of the nested groups

{p 8 14 2}
{cmd:estat} {opt gr:oup} 


{pstd}Estimate intraclass correlations

{p 8 14 2}
{cmd:estat} {opt icc} [{cmd:,} {opt l:evel(#)}] 


{pstd}Display the estimated random-effects covariance matrix (or matrices)

{p 8 14 2}
{cmd:estat} {opt recov:ariance} [{cmd:,} {opt relev:el(levelvar)}
          {opt corr:elation} {help matlist:{it:matlist_options}}]


{pstd}Display model-implied within-cluster correlations and standard deviations

{p 8 14 2}
{cmd:estat} {opt wcor:relation} [{cmd:,} {it:wcor_options}]


{marker options_estat_wcorrelation}{...}
{synoptset 18}{...}
{synopthdr :wcor_options}
{synoptline}
{synopt :{opt at(at_spec)}}specify the cluster for which you want the
	correlation matrix; default is the first two-level cluster
	encountered in the data{p_end}
{synopt:{opt all}}display correlation matrix for all the data{p_end}
{synopt:{opt cov:ariance}}display the covariance matrix instead of the
	correlation matrix{p_end}
{synopt:{opt list}}list the data corresponding to the correlation matrix{p_end}
{synopt:{opt nosort}}list the rows and columns of the correlation matrix in
	the order they were originally present in the data{p_end}
{synopt :{opth format(%fmt)}}set the display format; default is {cmd:format(%6.3f)}{p_end}
{synopt :{help matlist:{it:matlist_options}}}style and formatting options that control how
	matrices are displayed{p_end}
{synoptline}
{p2colreset}{...}


INCLUDE help menu_estat


{marker option_estat_icc}{...}
{title:Option for estat icc}

{phang}
{opt level(#)}
specifies the confidence level, as a percentage, for confidence intervals.
The default is {cmd:level(95)} or as set by {helpb set level}.


{marker options_estat_recov}{...}
{title:Options for estat recovariance}

{phang}
{opt relevel(levelvar)} specifies the level in the model for which the
random-effects covariance matrix is to be displayed.
By default, the covariance matrices for all levels in the model
are displayed.  {it:levelvar} is the name of the model level and is either the
name of the variable describing the grouping at that level or is {cmd:_all}, a
special designation for a group comprising all the estimation data.

{phang}
{opt correlation} displays the covariance matrix as a correlation matrix.

{phang}
{it:matlist_options} are style and formatting options that control how the
matrix (or matrices) is displayed; see {helpb matlist:[P] matlist} for
a list of what is available.


{title:Options for estat wcorrelation}

{marker atspec}{...}
{phang}
{opt at(at_spec)} specifies the cluster of observations for which you want the
within-cluster correlation matrix.  {it:at_spec} is

{phang3}
{it:relevel_var} {cmd:=} {it:value}
   [{it:relevel_var} {cmd:=} {it:value} ...]

{pmore}
For example, if you specify

{phang3}
{cmd:. estat wcorrelation, at(school = 33)}

{pmore}
you get the within-cluster correlation matrix for those observations 
in school 33.  If  you specify

{phang3}
{cmd:. estat wcorrelation, at(school = 33 classroom = 4)}

{pmore}
you get the correlation matrix for classroom 4 in school 33.

{pmore}
If {cmd:at()} is not specified, then you get the correlations
for the first level-two cluster encountered in the data.  This is
usually what you want.

{phang}
{opt all} specifies that you want the correlation matrix for all 
the data.  This is not recommended unless you have a relatively small dataset
or you enjoy seeing large N x N matrices.  However, this can prove
useful in some cases.

{phang}
{opt covariance} specifies that the within-cluster covariance matrix be
displayed instead of the default correlations and standard deviations.

{phang}
{opt list} lists the model data for those observations depicted in the
displayed correlation matrix.  This option is useful if you have many
random-effects design variables and you wish to see the represented values of
these design variables.

{phang}
{opt nosort} lists the rows and columns of the correlation matrix in the order
that they were originally present in the data.  Normally,
{cmd:estat wcorrelation} will first sort the data according to level
variables, by-group variables, and time variables to produce correlation
matrices whose rows and columns follow a natural ordering.  {opt nosort}
suppresses this.

{phang}
{opth format(%fmt)} sets the display format for the standard-deviation vector
and correlation matrix.  The default is {cmd:format(%6.3f)}.

{phang}
{it:matlist_options} control how matrices are displayed.  See 
{helpb matlist:[P] matlist} for details.


{marker examples}{...}
{title:Examples}

    {hline}
{pstd}Setup{p_end}
{phang2}{cmd:. webuse pig}{p_end}
{phang2}{cmd:. mixed weight week || id: week, covariance(unstructured)}{p_end}

{pstd}Random-effects correlation matrix for level ID{p_end}
{phang2}{cmd:. estat recovariance, correlation}{p_end}

{pstd}Display within-cluster marginal standard deviations and correlations
for a cluster{p_end}
{phang2}{cmd:. estat wcorrelation, format(%4.2g)}{p_end}

{pstd}BLUPS of random effects{p_end}
{phang2}{cmd:. predict u1 u0, reffects}{p_end}

{pstd}Standard errors of BLUPs{p_end}
{phang2}{cmd:. predict s1 s0, reses}{p_end}

    {hline}
{pstd}Setup{p_end}
{phang2}{cmd:. webuse productivity, clear}{p_end}
{phang2}{cmd:. mixed gsp private emp hwy water other unemp || region: ||}
             {cmd:state:}{p_end}

{pstd}Summarize composition of nested groups{p_end}
{phang2}{cmd:. estat group}{p_end}

{pstd}Fitted values at region level{p_end}
{phang2}{cmd:. predict gsp_region, fitted relevel(region)}{p_end}

{pstd}Log likelihood scores{p_end}
{phang2}{cmd:. predict double sc*, scores}{p_end}

{pstd}Compute residual intraclass correlations{p_end}
{phang2}{cmd:. estat icc}{p_end}

    {hline}
{pstd}Setup{p_end}
{phang2}{cmd:. use http://www.stata-press.com/data/r13/childweight, clear}
{p_end}
{phang2}{cmd:. mixed weight age || id: age, covariance(unstructured)}{p_end}

{pstd}Display within-cluster correlations for the first cluster{p_end}
{phang2}{cmd:. estat wcorrelation, list}{p_end}

{pstd}Display within-cluster correlations for ID 258{p_end}
{phang2}{cmd:. estat wcorrelation, at(id=258) list}{p_end}

    {hline}


{marker results}{...}
{title:Stored results}

{pstd}
{cmd:estat icc} stores the following in {cmd:r()}:

{synoptset 13 tabbed}{...}
{p2col 5 20 24 2: Scalars}{p_end}
{synopt:{cmd:r(icc}{it:#}{cmd:)}}level-{it:#} intraclass correlation{p_end}
{synopt:{cmd:r(se}{it:#}{cmd:)}}standard errors of level-{it:#} intraclass
         correlation{p_end}
{synopt:{cmd:r(level)}}confidence level of confidence intervals{p_end}
{p2colreset}{...}

{synoptset 13 tabbed}{...}
{p2col 5 20 24 2: Macros}{p_end}
{synopt:{cmd:r(label}{it:#}{cmd:)}}label for level {it:#}{p_end}
{p2colreset}{...}

{synoptset 13 tabbed}{...}
{p2col 5 20 24 2: Matrices}{p_end}
{synopt:{cmd:r(ci}{it:#}{cmd:)}}vector of confidence intervals (lower and upper)
        for level-{it:#} intraclass correlation{p_end}
{p2colreset}{...}

{pstd}
For a {it:G}-level nested model, {it:#} can be any integer between 2 and {it:G}.
{p_end}

{pstd}
{cmd:estat recovariance} stores the following in {cmd:r()}:

{synoptset 13 tabbed}{...}
{p2col 5 20 24 2: Scalars}{p_end}
{synopt:{cmd:r(relevels)}}number of levels{p_end}
{p2colreset}{...}

{synoptset 13 tabbed}{...}
{p2col 5 20 24 2: Matrices}{p_end}
{synopt:{cmd:r(Cov}{it:#}{cmd:)}}level-{it:#} random-effects covariance matrix{p_end}
{synopt:{cmd:r(Corr}{it:#}{cmd:)}}level-{it:#} random-effects correlation matrix
	(if option {cmd:correlation} was specified){p_end}
{p2colreset}{...}

{pstd}
For a {it:G}-level nested model, {it:#} can be any integer between 2 and {it:G}.
{p_end}

{pstd}
{cmd:estat wcorrelation} stores the following in {cmd:r()}:

{synoptset 13 tabbed}{...}
{p2col 5 15 19 2: Matrices}{p_end}
{synopt:{cmd:r(sd)}}standard deviations{p_end}
{synopt:{cmd:r(Corr)}}within-cluster correlation matrix{p_end}
{synopt:{cmd:r(Cov)}}within-cluster variance-covariance matrix{p_end}
{synopt:{cmd:r(G)}}variance-covariance matrix of random effects{p_end}
{synopt:{cmd:r(Z)}}model-based design matrix{p_end}
{synopt:{cmd:r(R)}}variance-covariance matrix of level-one errors{p_end}
