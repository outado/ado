{smcl}
{* *! version 1.0.3  08feb2013}{...}
{viewerdialog predict "dialog mlexp_p"}{...}
{vieweralsosee "[R] mlexp postestimation" "mansection R mlexppostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[R] mlexp" "help mlexp"}{...}
{viewerjumpto "Description" "mlexp postestimation##description"}{...}
{viewerjumpto "Syntax for predict" "mlexp postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "mlexp postestimation##menu_predict"}{...}
{viewerjumpto "Option for predict" "mlexp postestimation##option_predict"}{...}
{viewerjumpto "Example" "mlexp postestimation##example"}{...}
{title:Title}

{p2colset 5 33 35 2}{...}
{p2col :{manlink R mlexp postestimation} {hline 2}}Postestimation tools for mlexp{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation commands are available after {cmd:mlexp}:

{synoptset 17 notes}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
INCLUDE help post_estatic
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_estimates
INCLUDE help post_lincom
INCLUDE help post_lrtest_star
INCLUDE help post_nlcom
{synopt :{helpb nl postestimation##predict:predict}}scores{p_end}
INCLUDE help post_predictnl
INCLUDE help post_suest
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{p2colreset}{...}
INCLUDE help post_lrtest_star_msg


{marker syntax_predict}{...}
{title:Syntax for predict}

{p 8 16 2}
{cmd:predict}
{dtype}
{c -(}{it:stub}{cmd:*}{c |}{it:{help newvar:newvar_1}} ... {it:{help newvar:newvar_k}}{c )-}
{ifin}
[{cmd:,}
{opt sc:ores}]

{phang}
This statistic is only available for observations within the estimation 
sample.  k represents the number of parameters in the model.


INCLUDE help menu_predict


{marker option_predict}{...}
{title:Option for predict}

{phang}
{opt scores}, the default, calculates the equation-level score variables.  The
{it:j}th new variable will contain the scores for the {it:j}th parameter of
the model.  Linear combinations are expanded prior to computing scores, so
each variable's parameter will have its own score variable.


{marker example}{...}
{title:Example}

{pstd}Setup{p_end}
{phang2}{cmd:. sysuse auto}{p_end}
{phang2}{cmd:. mlexp (ln(normalden(mpg, {b0}+{b1}*gear, {sigma})))}{p_end}

{pstd}Calculate scores{p_end}
{phang2}{cmd:. predict s*, scores}{p_end}
