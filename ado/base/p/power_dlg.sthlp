{smcl}
{* *! version 1.0.0  03jun2013}{...}
{vieweralsosee "[PSS] power" "mansection PSS"}{...}

{pstd}
Power and sample-size analysis:

{phang2}{dialog power_onecorr:One-sample correlation test}{p_end}

{phang2}{dialog power_onemean:One-sample onemean test}{p_end}

{phang2}{help power_oneprop_dlg:One-sample proportion test}{p_end}

{phang2}{dialog stpower_cox:One-sample regression slope, Cox model}{p_end}

{phang2}{dialog power_onevar_sd:One-sample standard-deviation test}{p_end}

{phang2}{dialog power_onevar_var:One-sample variance test}{p_end}

{phang2}{dialog power_twocorr:Two-sample correlations test}{p_end}

{phang2}{dialog stpower_exponential:Two-sample exponential hazard-rates test}{p_end}

{phang2}{dialog power_twomeans:Two-sample means test}{p_end}

{phang2}{help power_twoprop_dlg:Two-sample proportions test}{p_end}

{phang2}{dialog power_twovar_sd:Two-sample standard-deviations test}{p_end}

{phang2}{dialog power_twovar_var:Two-sample variances test}{p_end}

{phang2}{help power_pairedm_dlg:Two-sample paired-means test}{p_end}

{phang2}{help power_pairedpr_dlg:Two-sample paired-proportions test}{p_end}

{phang2}{dialog stpower_logrank:Two-sample survival-rates, Log-rank test}{p_end}
