{smcl}
{* *! version 1.0.0  06jun2013}{...}
{vieweralsosee "[PSS] power oneproportion" "mansection PSS poweroneproportion"}{...}

{pstd}
Power analysis for a one-sample proportion:

{phang2}{dialog power_oneprop_binom:Binomial test}{p_end}

{phang2}{dialog power_oneprop_score:Score test}{p_end}

{phang2}{dialog power_oneprop_wald:Wald test}{p_end}
