{smcl}
{* *! version 1.0.1  06jun2013}{...}
{vieweralsosee "[PSS] power pairedmeans" "mansection PSS powerpairedmeans"}{...}

{pstd}
Power analysis for a two-sample paired-means test:

{phang2}{dialog power_pairedmeans_sddiff:Specify standard deviation of the differences}{p_end}

{phang2}{dialog power_pairedmeans_corr:Specify correlation between paired observations}{p_end}
