{smcl}
{* *! version 1.0.1  06jun2013}{...}
{vieweralsosee "[PSS] power pairedproportions" "mansection PSS powerpairedproportions"}{...}

{pstd}
Power analysis for a two-sample paired-proportions test:

{phang2}{dialog power_pairedprop_discord:Specify discordant proportions}{p_end}

{phang2}{dialog power_pairedprop_marginal:Specify marginal proportions}{p_end}
