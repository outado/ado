{smcl}
{* *! version 1.0.0  06jun2013}{...}
{vieweralsosee "[PSS] power twoproportions" "mansection PSS powertwoproportions"}{...}

{pstd}
Power analysis for a two-sample proportions:

{phang2}{dialog power_twoprop_chi2:Pearson's chi-squared test}{p_end}

{phang2}{dialog power_twoprop_lrchi2:Likelihood-ratio test}{p_end}

{phang2}{dialog power_twoprop_fisher:Fisher's exact test}{p_end}
