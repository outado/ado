{smcl}
{* *! version 1.0.5  05jun2013}{...}
{viewerdialog projmanager "stata projmanager"}{...}
{vieweralsosee "[P] Project Manager" "mansection P ProjectManager"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[R] doedit" "help doedit"}{...}
{viewerjumpto "Description" "project manager##description"}{...}
{viewerjumpto "Remarks" "project manager##remarks"}{...}
{title:Title}

{p2colset 5 28 30 2}{...}
{p2col :{manlink P Project Manager} {hline 2}}Organize Stata files{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The Project Manager is a tool for organizing and navigating Stata files.  It
allows you to collect all the files associated with a given project into a
single interface where you can have quick access to them without navigating
through file dialogs.  You can open do-files in the Do-file Editor, use Stata
data, and draw saved Stata graphs by double-clicking on the files in the
Project Manager.  There are no limitations to the kinds of files you can add
to a project. If you are using Stata for Mac, you can also open non-Stata
documents in their default applications just by double-clicking on the files.

{pstd}
To open the Project Manager, from within the Do-file Editor on Windows and
Unix or from within the main menu on Mac, select 
{bf:File > New > Project...}.


{marker remarks}{...}
{title:Remarks}

{pstd}
See {mansection P ProjectManagerRemarksandexamples:{it:Remarks and examples}}
in {bf:[P] Project Manager} for more information.
{p_end}
