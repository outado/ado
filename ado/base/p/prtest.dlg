/*

*! VERSION 1.0.1  15nov2012

*/

VERSION 13.0

INCLUDE _std_medium
DEFINE _dlght 265
INCLUDE header

HELP hlp1, view("help prtest")
RESET res1

PROGRAM POSTINIT_PROGRAM
BEGIN
	if __MESSAGE.iseq("-one-") {
		call main.rb_prtest1.seton
	}
	if __MESSAGE.iseq("-two-") {
		call main.rb_prtest2.seton
	}
	if __MESSAGE.iseq("-by-") {
		call main.rb_prtestby.seton
	}
END

DIALOG main, label("prtest - Tests of proportions") tabtitle("Main")
BEGIN
	GROUPBOX	gb_prtest	_lft	_top	_iwd	_ht6,	///
		label("Tests of proportions")
	RADIO		rb_prtest1	_indent	_ss	_ibwd	.,	///
		first							///
		onclickon("program main_prtest1_on")			///
		label("One-sample")
	RADIO		rb_prtestby	@	_ss	@	.,	///
		onclickon("program main_prtestby_on")			///
		label("Two-group using groups")
	RADIO		rb_prtest2	@	_ss	@	.,	///
		last							///
		onclickon("program main_prtest2_on")			///
		label("Two-sample using variables")
	
	GROUPBOX	gb_group	_lft	_xls	_iwd	_ht9,	///
		label("One-sample test of proportion")
	
	/* One-sample test of proportion */
	TEXT     	tx_var    	_indent _ss     _cwd1	.,	///
		label("Variable:")				
	DEFINE	holdx @x
	DEFINE	holdy @y
	VARNAME  	vn_var    	@       _ss     _vnwd   .,	///
		numeric							///
		label("Variable")			
	
	TEXT     	tx_val    	_lft2   holdy	_cwd2   .,	///
		label("Hypothesized proportion:")			
	EDIT     	en_val    	@       _ss     _en14wd	.,	///
		numonly							///
		label("Hypothesized proportion")								
	
	/* Two-sample test of proportions */
	TEXT     	tx_var1   	holdx	holdy	_cwd1	.,	///
		label("First variable:")				
	VARNAME  	vn_var1   	@       _ss     _vnwd   .,	///
		numeric							///
		label("First variable")					///
	
	TEXT     	tx_var2   	_lft2	holdy	_cwd2	.,	///
		label("Second variable:")				
	VARNAME  	vn_var2   	@	_ss	_vnwd	.,	///
		numeric							///
		label("Second variable")	
	
	/* Two-group proportion test */ 
	TEXT     	tx_gvar    	holdx	holdy	_cwd1	.,	///
		label("Variable name:")			
	VARNAME  	vn_gvar    	@       _ss     _vnwd   .,	///
		numeric							///
		label("Variable name")				
	
	TEXT     	tx_byopt	_lft2	holdy 	_cwd2	.,	///
		label("Group variable name:")				
	VARNAME  	vn_byopt	@	_ss	_vnwd	.,	///
		numeric							///
		label("Group variable name")				
	
	DEFINE _x holdx
	DEFINE _cx _cwd2
	DEFINE _y _xls
  	INCLUDE _sp_level
  	
END

INCLUDE byifin

PROGRAM main_prtest1_on	
BEGIN
	call script main_prtest1_show
	call script main_prtest2_hide
	call script main_prtestby_hide
	call main.gb_group.setlabel "One-sample test of proportion"
END

PROGRAM main_prtest2_on
BEGIN
	call script main_prtest1_hide
	call script main_prtest2_show
	call script main_prtestby_hide	
	call main.gb_group.setlabel "Two-sample test of proportions using variables"	
END

PROGRAM main_prtestby_on
BEGIN
	call script main_prtest1_hide
	call script main_prtest2_hide
	call script main_prtestby_show
	call main.gb_group.setlabel "Two-group proportion test using groups"
END

SCRIPT main_prtest1_show
BEGIN
	main.tx_var.show
	main.vn_var.show
	main.tx_val.show
	main.en_val.show
	main.sp_level.setvalue "95"
END

SCRIPT main_prtest1_hide
BEGIN
	main.tx_var.hide
	main.vn_var.hide
	main.tx_val.hide
	main.en_val.hide	
END

SCRIPT main_prtest2_show
BEGIN
	main.tx_var1.show
	main.vn_var1.show
	main.tx_var2.show
	main.vn_var2.show
	main.sp_level.setvalue "95"
END

SCRIPT main_prtest2_hide
BEGIN
	main.tx_var1.hide
	main.vn_var1.hide
	main.tx_var2.hide
	main.vn_var2.hide
END

SCRIPT main_prtestby_show
BEGIN
	main.tx_gvar.show
	main.vn_gvar.show
	main.tx_byopt.show
	main.vn_byopt.show
	main.sp_level.setvalue "95"
END

SCRIPT main_prtestby_hide
BEGIN
	main.tx_gvar.hide
	main.vn_gvar.hide
	main.tx_byopt.hide
	main.vn_byopt.hide	
END

PROGRAM main_by_group_opt
BEGIN
	varlist main.vn_byopt
END

PROGRAM command
BEGIN
	INCLUDE _by_pr
	put "prtest "
	if main.rb_prtest1 {
		varlist main.vn_var
		put "== "
		require main.en_val
		put main.en_val
	}
	if main.rb_prtest2 {
		varlist main.vn_var1
		put "== "
		varlist main.vn_var2
	}
	if main.rb_prtestby {
		varlist main.vn_gvar
	}
	INCLUDE _ifin_pr
	
	beginoptions
		if main.rb_prtestby {
			put "by("
			put /program main_by_group_opt
			put ")"
		}
		INCLUDE _level_main_pr
	endoptions
END

