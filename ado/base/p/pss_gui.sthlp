{smcl}
{* *! version 1.0.0  31may2013}{...}
{viewerdialog "power" "dialog power_dlg"}{...}
{vieweralsosee "[PSS] GUI" "mansection PSS GUI"}{...}
{vieweralsosee "[PSS] intro" "mansection PSS intro"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[PSS] power" "help power"}{...}
{vieweralsosee "[PSS] Glossary" "help pss_glossary"}{...}
{viewerjumpto "Description" "pss_gui##description"}{...}
{viewerjumpto "Menu" "pss_gui##menu"}{...}
{viewerjumpto "Remarks" "pss_gui##remarks"}{...}
{title:Title}

{p2colset 5 18 20 2}{...}
{p2col:{manlink PSS GUI} {hline 2}}Graphical user interface for power and
sample-size analysis{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
This entry describes the graphical user interface (GUI) for the {cmd:power}.
See {helpb power:[PSS] power} for a general introduction to the {cmd:power}
command.


{marker menu}{...}
{title:Menu}

{phang}
{bf:Statistics > Power and sample size}


{marker remarks}{...}
{title:Remarks}

{pstd}
See {manlink PSS GUI}.
{p_end}
