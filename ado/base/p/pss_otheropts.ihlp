{* *! version 1.0.0  28may2013}{...}
{synopt:{cmdab:sav:ing(}{it:{help filename}} [{cmd:,} {cmd:replace}]{cmd:)}}save the
table data to {it:filename}; use {cmd:replace} to overwrite existing 
{it:filename}{p_end}

{syntab:Graph}
{synopt:{cmdab:gr:aph}[{cmd:(}{helpb power_optgraph##graphopts:{it:graphopts}}{cmd:)}}graph
results; see {helpb power_optgraph:[PSS] power, graph}{p_end}

