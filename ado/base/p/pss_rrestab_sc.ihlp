{* *! version 1.0.0  06mar2013}{...}
{synopt:{cmd:r(separator)}}number of lines between separator lines in the table{p_end}
{synopt:{cmd:r(divider)}}{cmd:1} if {cmd:divider} is requested in the table; {cmd:0} otherwise{p_end}
