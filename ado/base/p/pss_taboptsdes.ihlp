{* *! version 1.0.1  30may2013}{...}
{dlgtab:Table}

{phang}
{cmd:table}, {opt table(tablespec)},
{cmd:notable}; see 
{manhelp power_opttable PSS: power, table}.

{phang}
{cmd:saving()}; see {helpb power##saving():[PSS] power}.
