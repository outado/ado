{* *! version 1.0.0  28nov2012}{...}
{p2coldent:* {opth n1(numlist)}}sample size of the control group{p_end}
{p2coldent:* {opth n2(numlist)}}sample size of the experimental group{p_end}
{p2coldent:* {opth nrat:io(numlist)}}ratio of sample sizes, N2/N1; default is
{cmd:nratio(1)}, meaning equal group sizes{p_end}
{p2coldent:* {opt comp:ute}({cmd:n1}|{cmd:n2})}solve for N1 or N2{p_end}
