{* *! version 1.0.2  30may2013}{...}
{synopt: {opt dir:ection}({opt u:pper}|{opt l:ower})}direction 
   of the effect for effect-size determination; default is
   {cmd:direction(upper)}, which means that the postulated value of the
   parameter is larger than the hypothesized value{p_end}
{synopt :{opt onesid:ed}}one-sided test; default is two sided{p_end}
{synopt: {opt par:allel}}treat number lists in starred options as parallel
when multiple values per option are specified (do not enumerate all possible
combinations of values)
{p_end}
