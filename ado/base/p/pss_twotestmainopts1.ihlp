{* *! version 1.0.2  31may2013}{...}
INCLUDE help pss_testmainopts1.ihlp
{p2coldent:* {opth n1(numlist)}}sample size of the control group{p_end}
{p2coldent:* {opth n2(numlist)}}sample size of the experimental group{p_end}
{p2coldent:* {opth nrat:io(numlist)}}ratio of sample sizes, N2/N1; default is
{cmd:nratio(1)}, meaning equal group sizes{p_end}
{synopt: {cmd:compute(n1}|{cmd:n2)}}solve for N1 given N2 or for N2 given N1{p_end}
