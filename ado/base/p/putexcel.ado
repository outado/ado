*! version 1.0.7  11oct2013
program define putexcel
	version 13

	if ("`c(excelsupport)'" != "1") {
		dis as err "putexcel is not supported on this platform."
		exit 198
	}

	cap syntax anything(name=cellexplist id="cellexplist" equalok)	///
		using/							///
		[, SHeet(string)					///
		COLWise							///
		MODify							///
		KEEPCELLFormat						///
		REPLACE]
	if _rc {
		if ("`1'" == "set") {
			gettoken subcmd 0 : 0, parse(" ,")
			local 0 `"using `0'"'
			syntax using / [,MODify REPLACE SHeet(string)	///
				KEEPCELLFormat ]
			mata : putexcel_adv_set()
		}
		else if ("`1'" == "describe") {
			if ("`2'" != "") {
				di as err "invalid syntax"
				exit 198
			}
			mata : putexcel_adv_describe()
		}
		else if ("`1'" == "clear") {
			if ("`2'" != "") {
				di as err "invalid syntax"
				exit 198
			}
			mata : putexcel_adv_clear()
		}
		else {
		syntax anything(name=cellexplist id="cellexplist" equalok) ///
			[, SHeet(string)				///
			COLWise]
			mata : putexcel_adv_cellexplist()
		}
	}
	else {
		mata : putexcel()
	}
end

local True	1
local False	0

local CEI struct cellexp_info scalar
local EI struct exp_info scalar
local RI struct return_info scalar

version 12.0
mata:
mata set matastrict on

// ============================================================================
// Structures
// ============================================================================

struct parse_info {
	class xl 		xl_class
	string scalar		filename
	string scalar		op_sheet
	real scalar		op_sheetreplace
	real scalar		op_colwise
	real scalar		op_modify
	real scalar		op_replace
	real scalar		op_keepcellformat
	real scalar		is_xlsx
	pointer(`CEI') rowvector cei
}

struct cellexp_info {
	struct cell_info scalar ci
	struct exp_info scalar ei
}

struct cell_info {
	string scalar		cell
	real scalar		cell_col
	real scalar		cell_row
}

struct exp_info {
	string scalar		expression
	string scalar		eval_str_scalar
	real scalar		eval_num_scalar
	real matrix		eval_mat
	string matrix		eval_mat_row_stripes
	string matrix		eval_mat_col_stripes
	string scalar		shortcut_cat
	string scalar		shortcut_subcat
	real scalar		is_shortcut
	real scalar		is_shortcut_names
	real scalar		is_matrix
	real scalar		names
	real scalar		rownames
	real scalar		colnames
	real scalar		is_str_scalar
	real scalar		asdate
	real scalar		asdatetime
	pointer(`RI') rowvector ri
}

struct return_info {
	string scalar		type
	string scalar		name
	real scalar		ret_scalar
	string scalar		ret_macro
	real matrix		ret_matrix
	string colvector	return_names
	real scalar		cell_ul_col
	real scalar		cell_ul_row
	real scalar		cell_lr_col
	real scalar		cell_lr_row
}

// ============================================================================
// Main
// ============================================================================

void putexcel()
{
	struct parse_info scalar pr

	putexcel_parse_info_init(pr)
	putexcel_parse_syntax(pr)
//	putexcel_dump_parse_info(pr)
	putexcel_write_data(pr)
}

// ============================================================================
// Initializations
// ============================================================================

void putexcel_parse_info_init(struct parse_info scalar pr)
{
	pr.xl_class = xl()
	pr.filename = ""
	pr.op_sheet = "Sheet1"
	pr.op_sheetreplace = 0
	pr.op_colwise = 0
	pr.op_modify = 0
	pr.op_replace = 0
	pr.op_keepcellformat = 0
	pr.is_xlsx = 1
}

void putexcel_cellexp_info_init(`CEI' cei)
{
	cei.ci.cell = ""
	cei.ci.cell_col = -1
	cei.ci.cell_row = -1

	cei.ei.expression = ""
	cei.ei.eval_str_scalar = ""
	cei.ei.eval_num_scalar = .
	cei.ei.eval_mat = J(0,0,.)
	cei.ei.eval_mat_row_stripes = J(0,0,"")
	cei.ei.eval_mat_col_stripes = J(0,0,"")
	cei.ei.shortcut_cat = ""
	cei.ei.shortcut_subcat = ""
	cei.ei.is_shortcut = 0
	cei.ei.is_shortcut_names = 0
	cei.ei.is_matrix = 0
	cei.ei.names = 0
	cei.ei.rownames = 0
	cei.ei.colnames = 0
	cei.ei.is_str_scalar = 0
	cei.ei.asdate = 0
	cei.ei.asdatetime = 0
}

void putexcel_return_info_init(`RI' ri)
{
	ri.type = ""
	ri.name = ""
	ri.ret_scalar = .
	ri.ret_macro = ""
	ri.ret_matrix = J(0,0,.)
	ri.return_names = J(0,1,"")
	ri.cell_ul_col = .
	ri.cell_ul_row = .
	ri.cell_lr_col = .
	ri.cell_lr_row = .
}

// ============================================================================
// Parsing options
// ============================================================================

void putexcel_parse_syntax(struct parse_info scalar pr)
{
	putexcel_build_filename(pr)

	pr.op_colwise = (st_local("colwise") != "")
	pr.op_modify = (st_local("modify") != "")
	pr.op_keepcellformat = (st_local("keepcellformat") != "")
	pr.op_replace = (st_local("replace") != "")

	if (pr.op_modify & pr.op_replace) {
		errprintf(`"options {bf:modify} and {bf:replace} "')
		errprintf(`"are mutually exclusive\n"')
		exit(198)
	}

	if (st_local("sheet") != "") {
		putexcel_parse_sheet(pr)
	}

	putexcel_parse_cellexplist(pr)
}

void putexcel_build_filename(struct parse_info scalar pr)
{
	string scalar		default_filetype
	string scalar		using_file, file, path, ext
	string scalar		HoldPWD, basename
	real scalar		rc

	using_file = st_local("using")
	basename = pathbasename(using_file)

	if (basename=="") {
		errprintf("%s not a valid filename\n", using_file)
		exit(198)
	}

	if (substr(using_file, 1, 1)=="~") {
		if (st_global("c(os)") != "Windows") {
			path = ""
			file = ""
			(void) pathsplit(using_file, path, file)
			HoldPWD = pwd()
			rc = _chdir(path)
			if (rc == 0) {
				path = pwd()
			}
			else {
				errprintf("invalid path :%s", path)
				exit(170)
			}
			chdir(HoldPWD)
			using_file = path + file
		}
	}

	default_filetype = ".xlsx"
	if (pathsuffix(using_file) == "") {
		using_file = using_file + default_filetype
	}
	pr.filename = using_file
	ext =  strlower(strreverse(pr.filename))
	if (substr(ext, 1, 4)=="slx.") {
		pr.is_xlsx = 0
	}
	else {
		pr.is_xlsx = 1
	}
}

void putexcel_parse_sheet(struct parse_info scalar pr)
{
	transmorphic		t
	string scalar		tmpstr, token

	tmpstr = st_local("sheet")

	t = tokeninit("", `","', `""""', 0, 0)
	tokenset(t, tmpstr)
	pr.op_sheet = strtrim(tokenget(t))
	(void) tokenget(t) // comma

	tokenwchars(t, `" "')
	tokenpchars(t, `" "')

	for (token=tokenget(t); token != "";token = tokenget(t)) {
		if (token == "replace") {
			pr.op_sheetreplace = 1
		}
		else {
errprintf("{bf:%s}: is invalid {bf:sheet()} argument; use {bf:replace}", token)
			exit(198)
		}
	}
}

// ============================================================================
// Parsing cellexplist --- (cell)=(exp)|matrix(exp)|shortcut
// ============================================================================

void putexcel_parse_cellexplist(struct parse_info scalar pr)
{
	transmorphic		t
	string scalar		cellexplist, token, cell

	cellexplist = st_local("cellexplist")

	if (cellexplist == "") {
		errprintf("{bf:cellexplist} is required\n")
		exit(198)
	}

	t = tokeninit(" ", `"="', `"()"')
	tokenset(t, cellexplist)

	for (token=tokenget(t); token!=""; token=tokenget(t)) {
		if (tokenpeek(t)=="=") {
			(void) tokenget(t)
			cell = token
			token = tokenget(t)
			if (token=="") {
				errprintf(
				"nothing found where expression expected\n")
				exit(198)
			}
			if (token == "mat" | token == "matr" |
				token == "matri" | token == "matrix") {
				token = tokenget(t)
				if (!putexcel_check_paren(token)) {
					errprintf(
"%s: matrix expression must be enclosed in ()\n", token)
					exit(198)
				}
				putexcel_add_cellexplist(pr, cell, token,
					`True')
				continue
			}
			putexcel_add_cellexplist(pr, cell, token, `False')
		}
		else {
			errprintf("{bf:%s}: invalid cell name\n", token)
			exit(198)
		}
	}
}

void putexcel_add_cellexplist(struct parse_info scalar pr,
	string scalar cell, string scalar expression, real scalar is_matrix)
{
	real scalar		i

	i = length(pr.cei)
	i++
	pr.cei = pr.cei, J(1, 1, NULL)
	pr.cei[i] = &(cellexp_info())

	putexcel_cellexp_info_init(*(pr.cei[i]))

	pr.cei[i]->ci.cell = putexcel_remove_paren(strtrim(cell))
	pr.cei[i]->ei.expression = strtrim(expression)
	pr.cei[i]->ei.is_matrix = is_matrix

	putexcel_get_cell_info(pr, i)
	putexcel_get_exp_info(pr, i)
}

// ============================================================================
// Parse cell info
// ============================================================================

void putexcel_get_cell_info(struct parse_info scalar pr, real scalar idx)
{
	real rowvector		col_row
	string scalar		cell

	cell = pr.cei[idx]->ci.cell

	if (strpos(cell, ":")>0) {
		cell = substr(cell, 1, strpos(cell, ":")-1)
	}

	col_row = putexcel_get_col_row_from_cell(pr, cell)

	if (missing(col_row[1,1]) | missing(col_row[1,2])) {
		errprintf("{bf:%s}: invalid cell name\n", cell)
		exit(198)
	}

	if (!(pr.xl_class.is_valid_col(pr.is_xlsx, col_row[1,1]))) {
		errprintf("{bf:%s}: Excel column out of range\n", cell)
		exit(198)
	}

	if (!(pr.xl_class.is_valid_row(pr.is_xlsx, col_row[1,2]))) {
		errprintf("{bf:%s}: Excel row out of range\n", cell)
		exit(198)
	}

	pr.cei[idx]->ci.cell_col = col_row[1,1]
	pr.cei[idx]->ci.cell_row = col_row[1,2]
}

// ============================================================================
// Parse exp info
// ============================================================================

void putexcel_get_exp_info(struct parse_info scalar pr, real scalar idx)
{
	if (pr.cei[idx]->ei.is_matrix) {
		putexcel_is_matrix(pr.cei[idx]->ei, pr.xl_class,
			pr.cei[idx]->ci.cell_row, pr.cei[idx]->ci.cell_col,
			pr.is_xlsx)
	}
	else if (putexcel_is_shortcut(pr.cei[idx]->ei)) {
		putexcel_get_return_info(*(pr.cei[idx]), pr.xl_class,
			pr.is_xlsx, pr.op_colwise)
	}
	else {
		putexcel_is_exp(pr.cei[idx]->ei)
	}
}

void putexcel_is_matrix(`EI' ei, class xl scalar xl_class, real scalar ul_row,
	real scalar ul_col, real scalar is_xlsx)
{
	real scalar		rc
	string scalar		expression, rev, new_exp, tmpname
	real scalar		row_stripe_space, col_stripe_space, rows, cols

	expression = ei.expression
	rev = strreverse(expression)
	rev = stritrim(strtrim(substr(rev, 1, strpos(rev, ",")-1)))

	if (rev == ")seman") {
		ei.names = 1
	}
	else if (rev == ")semanwor") {
		ei.rownames = 1
	}
	else if (rev == ")semanloc") {
		ei.colnames = 1
	}

	if (ei.names | ei.colnames | ei.rownames) {
		rev = strreverse(expression)
		new_exp = strreverse(substr(rev, strpos(rev, ",")+1, .))
		new_exp = substr(new_exp, strpos(new_exp, "(") +1, .)
		expression = new_exp
	}

	tmpname = st_tempname()

	rc = _stata(sprintf("matrix define %s = %s", tmpname, expression))
	if (rc) {
		exit(rc)
	}

	ei.eval_mat = st_matrix(tmpname)

	row_stripe_space = 0
	col_stripe_space = 0
	if (ei.names) {
		ei.eval_mat_row_stripes=putexcel_get_matrix_stripes(tmpname,0)
		ei.eval_mat_col_stripes=putexcel_get_matrix_stripes(tmpname,1)
		row_stripe_space = cols(ei.eval_mat_row_stripes)
		col_stripe_space = rows(ei.eval_mat_col_stripes)
	}
	else if (ei.rownames) {
		ei.eval_mat_row_stripes=putexcel_get_matrix_stripes(tmpname,0)
		row_stripe_space = cols(ei.eval_mat_row_stripes)
	}
	else if (ei.colnames) {
		ei.eval_mat_col_stripes=putexcel_get_matrix_stripes(tmpname,1)
		col_stripe_space = rows(ei.eval_mat_col_stripes)
	}

	rows = ul_row + rows(ei.eval_mat) + col_stripe_space - 1

	if (!(xl_class.is_valid_row(is_xlsx, rows))) {
		errprintf("matrix rows out of Excel file range\n")
		exit(198)
	}

	cols = ul_col + cols(ei.eval_mat) + row_stripe_space - 1
	if (!(xl_class.is_valid_col(is_xlsx, cols))) {
		errprintf("matrix columns out of Excel file range\n")
		exit(198)
	}
}

string matrix putexcel_get_matrix_stripes(string scalar matname,
	real scalar col_stripes)
{
	real scalar		i
	real rowvector		sums
	string matrix		stripes, ret_mat

	if (col_stripes) {
		stripes = st_matrixcolstripe(matname)
	}
	else {
		stripes = st_matrixrowstripe(matname)
	}

	sums = colsum(strlen(stripes))
	ret_mat = J(rows(stripes),0,"")

	for(i=1; i<=cols(sums); i++) {
		if (sums[i] != 0) {
			ret_mat = ret_mat, stripes[.,i]
		}
	}
	if(col_stripes) {
		ret_mat = ret_mat'
	}

	return(ret_mat)
}


real scalar putexcel_is_shortcut(`EI' ei)
{
	string scalar		shortcut

	shortcut = ei.expression
	if (shortcut == "escal" | shortcut == "escala" |
		shortcut == "escalar" | shortcut == "escalars") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "numscalar"
		return(`True')
	}
	if (shortcut == "rscal" | shortcut == "rscala" |
		shortcut == "rscalar" | shortcut == "rscalars") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "numscalar"
		return(`True')
	}
	if (shortcut == "emac" | shortcut == "emacr" |
		shortcut == "emacro" | shortcut == "emacros") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "macro"
		return(`True')
	}
	if (shortcut == "rmac" | shortcut == "rmacr" |
		shortcut == "rmacro" | shortcut == "rmacros") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "macro"
		return(`True')
	}
	if (shortcut == "emat" | shortcut == "ematr" |
		shortcut == "ematri" | shortcut == "ematric" |
		shortcut == "ematrice" | shortcut == "ematrices") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "matrix"
		return(`True')
	}
	if (shortcut == "rmat" | shortcut == "rmatr" |
		shortcut == "rmatri" | shortcut == "rmatric" |
		shortcut == "rmatrice" | shortcut == "rmatrices") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "matrix"
		return(`True')
	}
	if (shortcut == "e*") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "all"
		return(`True')
	}
	if (shortcut == "r*") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `False'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "all"
		return(`True')
	}

	if (shortcut == "escalarn" | shortcut == "escalarna" |
		shortcut == "escalarnam" | shortcut  == "escalarname" |
		shortcut  == "escalarnames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "numscalar"
		return(`True')
	}
	if (shortcut == "rscalarn" | shortcut == "rscalarna" |
		shortcut == "rscalarnam" | shortcut  == "rscalarname" |
		shortcut  == "rscalarnames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "numscalar"
		return(`True')
	}
	if (shortcut == "emacron" | shortcut == "emacrona" |
		shortcut == "emacronam" | shortcut  == "emacroname" |
		shortcut  == "emacronames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "macro"
		return(`True')
	}
	if (shortcut == "rmacron" | shortcut == "rmacrona" |
		shortcut == "rmacronam" | shortcut  == "rmacroname" |
		shortcut  == "rmacronames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "macro"
		return(`True')
	}
	if (shortcut == "ematrixn" | shortcut == "ematrixna" |
		shortcut == "ematrixnam" | shortcut  == "ematrixname" |
		shortcut  == "ematrixnames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "matrix"
		return(`True')
	}
	if (shortcut == "rmatrixn" | shortcut == "rmatrixna" |
		shortcut == "rmatrixnam" | shortcut  == "rmatrixname" |
		shortcut  == "rmatrixnames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "matrix"
		return(`True')
	}
	if (shortcut == "ena" | shortcut == "enam" |
		shortcut == "ename" | shortcut  == "enames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "e()"
		ei.shortcut_subcat = "all"
		return(`True')
	}
	if (shortcut == "rna" | shortcut == "rnam" |
		shortcut == "rname" | shortcut  == "rnames") {
		ei.is_shortcut = `True'
		ei.is_shortcut_names = `True'
		ei.shortcut_cat = "r()"
		ei.shortcut_subcat = "all"
		return(`True')
	}

	return(`False')
}

void putexcel_get_return_info(`CEI' cei, class xl scalar xl_class,
	real scalar is_xlsx, real scalar colwise)
{
	real scalar		i, start_col, start_row
	pointer scalar		pstart_col, pstart_row
	string scalar		cat, subcat

	i = length(cei.ei.ri)

	if (i == 0) {
		start_col = cei.ci.cell_col
		start_row = cei.ci.cell_row
	}
	else {
		start_col = cei.ei.ri[i]->cell_lr_col
		start_row = cei.ei.ri[i]->cell_lr_row
		if (colwise) {
			start_col++
		}
		else {
			start_row++
		}
	}

	pstart_col = &start_col
	pstart_row = &start_row

	cat = cei.ei.shortcut_cat
	subcat = cei.ei.shortcut_subcat

	if (cei.ei.is_shortcut_names) {
		putexcel_add_returnnames_info(cei.ei.ri, cat, subcat,
			start_row, start_col, colwise, xl_class, is_xlsx)
	}
	else if (subcat == "all") {
		putexcel_add_return_info(cei.ei.ri, cat, "numscalar",
			*pstart_row, *pstart_col, colwise, xl_class, is_xlsx)
		putexcel_add_return_info(cei.ei.ri, cat, "macro",
			*pstart_row, *pstart_col, colwise, xl_class, is_xlsx)
		putexcel_add_return_info(cei.ei.ri, cat, "matrix",
			*pstart_row, *pstart_col, colwise, xl_class, is_xlsx)
	}
	else {
		putexcel_add_return_info(cei.ei.ri, cat, subcat,
			*pstart_row, *pstart_col, colwise, xl_class, is_xlsx)
	}
}

void putexcel_add_returnnames_info(pointer(`RI') rowvector ri,string scalar cat,
	string scalar subcat, real scalar start_row, real scalar start_col,
	real scalar colwise, class xl scalar xl_class, real scalar is_xlsx)
{
	real scalar		i, rows, cols
	string colvector	return_names, tmp_return_names

	if (subcat == "all" ) {
		return_names = st_dir(cat, "numscalar", "*")
		tmp_return_names =  st_dir(cat, "macro", "*")
		return_names = return_names \ tmp_return_names
		tmp_return_names =  st_dir(cat, "matrix", "*")
		return_names = return_names \ tmp_return_names
	}
	else {
		return_names = st_dir(cat, subcat, "*")
	}

	if (!rows(return_names)) {
		return
	}

	i = length(ri)
	i++

	ri = ri, J(1, 1, NULL)
	ri[i] = &(return_info())
	putexcel_return_info_init(*(ri[i]))

	ri[i]->return_names = return_names
	ri[i]->cell_ul_col = start_col
	ri[i]->cell_ul_row = start_row

	rows = start_row + rows(return_names)
	cols = start_col + rows(return_names)

	if (colwise) {
		if (!(xl_class.is_valid_col(is_xlsx, cols))) {
errprintf("return values columns out of Excel file range\n")
			exit(198)
		}
		ri[i]->cell_lr_col = cols
	}
	else {
		if (!(xl_class.is_valid_row(is_xlsx, rows))) {
errprintf("return values rows out of Excel file range\n")
			exit(198)
		}
		ri[i]->cell_lr_row = rows
	}
}

void putexcel_add_return_info(pointer(`RI') rowvector ri, string scalar cat,
	string scalar subcat, real scalar start_row, real scalar start_col,
	real scalar colwise, class xl scalar xl_class, real scalar is_xlsx)
{
	real scalar		i, j, col, row, mat_cols, mat_rows, rc
	real scalar		is_eclass, vals
	string scalar		return_name, tmpname
	string colvector	return_names

	return_names = st_dir(cat, subcat, "*")

	if (!rows(return_names)) {
		return
	}

	is_eclass = 0
	if (cat=="e()") {
		is_eclass = 1
	}

	i = length(ri)
	i++
	col = start_col
	row = start_row
	j = 1

	vals = i + length(return_names)

	for (i; i<vals; i++) {
		ri = ri, J(1, 1, NULL)
		ri[i] = &(return_info())
		putexcel_return_info_init(*(ri[i]))
		if (is_eclass) {
			return_name = "e(" + return_names[j] + ")"
		}
		else {
			return_name = "r(" + return_names[j] + ")"
		}
		ri[i]->type = subcat
		ri[i]->name = return_name

		tmpname = st_tempname()

		if (subcat == "matrix") {
			rc = _stata(sprintf("matrix define %s = %s",
				tmpname, return_name))
			if (rc) {
				exit(rc)
			}

			ri[i]->ret_matrix = st_matrix(tmpname)
			mat_cols = cols(ri[i]->ret_matrix)
			mat_rows = rows(ri[i]->ret_matrix)

			ri[i]->cell_ul_col = col
			ri[i]->cell_ul_row = row
			ri[i]->cell_lr_col = col + mat_cols
			ri[i]->cell_lr_row = row + mat_rows

			if (colwise) {
				col = col + mat_cols + 1
			}
			else {
				row = row + mat_rows + 1
			}
		}
		else if (subcat == "numscalar") {
			ri[i]->ret_scalar = st_numscalar(return_name)

			ri[i]->cell_ul_col = col
			ri[i]->cell_ul_row = row
			ri[i]->cell_lr_col = col
			ri[i]->cell_lr_row = row

			if (colwise) {
				col++
			}
			else {
				row++
			}
		}
		else {
			ri[i]->ret_macro = st_global(return_name)

			ri[i]->cell_ul_col = col
			ri[i]->cell_ul_row = row
			ri[i]->cell_lr_col = col
			ri[i]->cell_lr_row = row

			if (colwise) {
				col++
			}
			else {
				row++
			}
		}

		if (!(xl_class.is_valid_col(is_xlsx, col))) {
errprintf("return values columns out of Excel file range\n")
				exit(198)
		}
		if (!(xl_class.is_valid_row(is_xlsx, row))) {
errprintf("return values rows out of Excel file range\n")
				exit(198)
		}
		j++
	}
	if (colwise) {
		start_col = col + 1
	}
	else {
		start_row = row + 1
	}
}

void putexcel_is_exp(`EI' ei)
{
	string scalar		expression, rev, new_exp, tmpname
	real scalar		rc

	expression = ei.expression

	if (!putexcel_check_paren(expression)) {
		errprintf("%s: expression must be enclosed in ()\n", expression)
		exit(198)
	}

	rev = strreverse(expression)
	rev = subinstr(substr(rev, 1, strpos(rev, ",")-1), " ", "", .)

	if (rev == ")etadsa") {
		ei.asdate = 1
	}
	else if (rev == ")emitetadsa") {
		ei.asdatetime = 1
	}

	if (ei.asdate | ei.asdatetime) {
		rev =  strreverse(expression)
		new_exp = strreverse(substr(rev, strpos(rev, ",")+1, .))
		expression = new_exp
	}

	expression = putexcel_remove_paren(expression)

	tmpname = st_tempname()
	rc = _stata(sprintf("scalar %s = %s", tmpname, expression))
	if (rc) {
		exit(rc)
	}

	if (st_numscalar(tmpname)==J(0,0,.)) {
		if (ei.asdate | ei.asdatetime ) {
			errprintf("%s: invalid date\n", expression)
			exit(198)

		}
		ei.eval_str_scalar = st_strscalar(tmpname)
		ei.is_str_scalar = 1
	}
	else {
		ei.eval_num_scalar = st_numscalar(tmpname)
	}
}

// ============================================================================
// Write data
// ============================================================================

void putexcel_write_data(struct parse_info scalar pr)
{
	putexcel_open_file(pr)
	putexcel_write_values(pr)
	putexcel_close_file(pr)
}

void putexcel_open_file(struct parse_info scalar pr)
{
	real scalar		rc, newsheet, newfile
	string scalar		quote

	newfile = newsheet = 0

	quote = `"""'
	rc = _stata(sprintf("quietly confirm new file %s%s%s",
			quote, pr.filename, quote), 1)
	if (rc==0) {
		newfile = 1
	}

	if (pr.op_replace) {
		newfile = 1
	}

	pr.xl_class.set_mode("open")
	pr.xl_class.set_error_mode("off")

	if (pr.op_keepcellformat) {
		pr.xl_class.set_keep_cell_format("on")
	}
	else {
		pr.xl_class.set_keep_cell_format("off")
	}

	if (newfile) {
		if (pr.is_xlsx) {
			pr.xl_class.create_book(pr.filename, pr.op_sheet,
				"xlsx")
			if (pr.xl_class.get_last_error()) {
				errprintf("file {bf:%s} could not be created\n",
					pr.filename)
				exit(603)
			}
		}
		else {
			pr.xl_class.create_book(pr.filename, pr.op_sheet,
				"xls")
			if (pr.xl_class.get_last_error()) {
				errprintf("file {bf:%s} could not be created\n",
					pr.filename)
				exit(603)
			}
		}
	}
	else {
		if (!pr.op_modify & !pr.op_replace ) {
			errprintf("file already exists\n")
			errprintf("you must specify either the {bf:modify} ")
			errprintf("or {bf:replace} option\n")
			exit(198)
		}

		pr.xl_class.load_book(pr.filename)
		if (pr.xl_class.get_last_error()) {
			errprintf("workbook {bf:%s} could not be loaded\n",
				pr.filename)
			exit(603)
		}

		if (pr.op_sheet != "") {
			pr.xl_class.set_sheet(pr.op_sheet)
			if (pr.xl_class.get_last_error_message()!="") {
				pr.xl_class.add_sheet(pr.op_sheet)
				if (pr.xl_class.get_last_error_message()!="") {
					errprintf("worksheet {bf:%s} could not be created\n", pr.op_sheet)
					pr.xl_class.close_book()
					exit(603)
				}
				newsheet = 1
			}
		}

		if (newsheet == 0 & !pr.op_modify) {
			errprintf("worksheet {bf:%s} already exists, must specify modify\n", pr.op_sheet)
			pr.xl_class.close_book()
			exit(602)
		}

		if (pr.op_sheetreplace & newsheet == 0) {
			pr.xl_class.clear_sheet(pr.op_sheet)
			if (pr.xl_class.get_last_error_message()!="") {
			errprintf("worksheet {bf:%s} could not be opened\n",
					pr.op_sheet)
				pr.xl_class.close_book()
				exit(603)
			}
		}
	}
}

void putexcel_write_values(struct parse_info scalar pr)
{
	real scalar		i

	for (i=1; i<=length(pr.cei); i++) {
		if (pr.cei[i]->ei.is_shortcut) {
			putexcel_write_return_vals(pr.cei[i]->ei.ri,
				pr.xl_class,  pr.cei[i]->ei.is_shortcut_names,
				pr.op_colwise)
		}
		else if (pr.cei[i]->ei.is_matrix) {
			putexcel_write_matrix(*(pr.cei[i]), pr.xl_class)
		}
		else {
			putexcel_write_exp(*(pr.cei[i]), pr.xl_class)
		}
	}
}

void putexcel_write_return_vals(pointer(`RI') rowvector ri,
	class xl scalar xl_class, real scalar is_shortcut_names,
	real scalar colwise)
{
	real scalar		i, ul_col, ul_row

	for (i=1; i<=length(ri); i++) {
		ul_col = ri[i]->cell_ul_col
		ul_row = ri[i]->cell_ul_row

		if (is_shortcut_names) {
			if (colwise) {
				xl_class.put_string(ul_row, ul_col,
					ri[i]->return_names')
				if (xl_class.get_last_error()) {
					xl_class.close_book()
					errprintf("could not write to file\n")
					exit(691)
				}
			}
			else {
				xl_class.put_string(ul_row, ul_col,
					ri[i]->return_names)
				if (xl_class.get_last_error()) {
					xl_class.close_book()
					errprintf("could not write to file\n")
					exit(691)
				}
			}
		}
		else if (ri[i]->type == "numscalar") {
			xl_class.put_number(ul_row, ul_col,
				ri[i]->ret_scalar)
			if (xl_class.get_last_error()) {
				xl_class.close_book()
				errprintf("could not write to file\n")
				exit(691)
			}
		}
		else if (ri[i]->type == "macro") {
			xl_class.put_string(ul_row, ul_col,
				ri[i]->ret_macro)
			if (xl_class.get_last_error()) {
				xl_class.close_book()
				errprintf("could not write to file\n")
				exit(691)
			}
		}
		else if (ri[i]->type == "matrix") {
			xl_class.put_number(ul_row, ul_col,
				ri[i]->ret_matrix)
			if (xl_class.get_last_error()) {
				xl_class.close_book()
				errprintf("could not write to file\n")
				exit(691)
			}
		}
		else {
			xl_class.close_book()
			errprintf("invalid return type\n")
			exit(198)
		}
	}
}

void putexcel_write_matrix(`CEI' cei, class xl scalar xl_class)
{
	real scalar		ul_col, ul_row
	real scalar		row_stripe_space, col_stripe_space

	ul_col = cei.ci.cell_col
	ul_row = cei.ci.cell_row

	row_stripe_space = 0
	col_stripe_space = 0

	if (cei.ei.rownames | cei.ei.names) {
		row_stripe_space = cols(cei.ei.eval_mat_row_stripes)
	}

	if (cei.ei.colnames | cei.ei.names) {
		col_stripe_space = rows(cei.ei.eval_mat_col_stripes)
	}

	if (cei.ei.names) {
		xl_class.put_string(ul_row+col_stripe_space, ul_col,
			cei.ei.eval_mat_row_stripes)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
		xl_class.put_string(ul_row, ul_col+row_stripe_space,
			cei.ei.eval_mat_col_stripes)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
		xl_class.put_number(ul_row+col_stripe_space,
			ul_col+row_stripe_space,
			cei.ei.eval_mat)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
	else if (cei.ei.rownames) {
		xl_class.put_string(ul_row, ul_col,
			cei.ei.eval_mat_row_stripes)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
		xl_class.put_number(ul_row, ul_col+row_stripe_space,
			cei.ei.eval_mat)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
	else if (cei.ei.colnames) {
		xl_class.put_string(ul_row, ul_col,
			cei.ei.eval_mat_col_stripes)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
		xl_class.put_number(ul_row+col_stripe_space, ul_col,
			cei.ei.eval_mat)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
	else {
		xl_class.put_number(ul_row, ul_col, cei.ei.eval_mat)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
}

void putexcel_write_exp(`CEI' cei, class xl scalar xl_class)
{
	real scalar		ul_col, ul_row


	ul_col = cei.ci.cell_col
	ul_row = cei.ci.cell_row

	if (cei.ei.asdate) {
		xl_class.put_number(ul_row, ul_col, cei.ei.eval_num_scalar,
			"asdate")
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
	else if (cei.ei.asdatetime) {
		xl_class.put_number(ul_row, ul_col, cei.ei.eval_num_scalar,
			"asdatetime")
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
	else if (cei.ei.is_str_scalar) {
		xl_class.put_string(ul_row, ul_col,
			cei.ei.eval_str_scalar)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
	else {
		xl_class.put_number(ul_row, ul_col, cei.ei.eval_num_scalar)
		if (xl_class.get_last_error()) {
			xl_class.close_book()
			errprintf("could not write to file\n")
			exit(691)
		}
	}
}

void putexcel_close_file(struct parse_info scalar pr)
{
	real scalar		rc

	if (pr.op_replace) {
		rc = _stata(sprintf("quietly erase %s", pr.filename), 1)
	}

	pr.xl_class.close_book()
	if (pr.xl_class.get_last_error()) {
		errprintf("file {bf:%s} could not be saved\n", pr.filename) ;
		exit(603)
	}

	printf("{txt}file %s saved\n", pr.filename)
}

// ============================================================================
// Advanced syntax
// ============================================================================

void putexcel_adv_set()
{
	struct parse_info scalar pr
	string scalar		ftype, fmode, fmtmode

	putexcel_parse_info_init(pr)
	putexcel_build_filename(pr)

	if (pr.is_xlsx) {
		ftype = "xlsx"
	}
	else {
		ftype = "xls"
	}

	if (st_local("modify") != "" & st_local("replace")!="") {
		errprintf(`"options {bf:modify} and {bf:replace} "')
		errprintf(`"are mutually exclusive\n"')
		exit(198)
	}

	if (st_local("modify") != "") {
		fmode = "modify"
	}
	else if (st_local("replace") != "") {
		fmode = "replace"
	}
	else {
		fmode = "create"
	}
	
	if (st_local("keepcellformat") != "") {
		fmtmode = "on"
	}
	else {
		fmtmode = "off"
	}
	
	if (st_local("sheet") != "") {
		putexcel_parse_sheet(pr)
	}

	putexcel_adv_set_file_info(pr.filename, ftype, fmode, pr.op_sheet,
		fmtmode)
}

void putexcel_adv_set_file_info(string scalar filename, string scalar filetype,
	string scalar filemode, string scalar sheetname, string scalar fmtmode)
{
	st_global("PUTEXCEL_FILE_NAME", filename)
	st_global("PUTEXCEL_FILE_TYPE", filetype)
	st_global("PUTEXCEL_FILE_MODE", filemode)
	st_global("PUTEXCEL_SHEET_NAME", sheetname)
	st_global("PUTEXCEL_KEEP_CELL_FORMAT", fmtmode)
}

void putexcel_adv_describe()
{
	real scalar		maxlen
	string colvector	file_info

	file_info = putexcel_adv_get_file_info()

	displayas("txt")
	printf("\n")
	maxlen = 11
	maxlen = strlen(file_info[1])
	if (maxlen < 11) maxlen = 11
	if (maxlen > 60) maxlen = 60
	printf("  {hline %g}{c +}{hline %g}\n", 11, maxlen+3)
	printf("  {cmd:Filename}   {c |}  %s\n", file_info[1])
	printf("  {cmd:Filetype}   {c |}  %s\n", file_info[2])
	printf("  {cmd:Write mode} {c |}  %s\n", file_info[3])
	printf("  {cmd:Sheetname}  {c |}  %s\n", file_info[4])
}

void putexcel_adv_clear()
{
	putexcel_adv_set_file_info("", "", "", "", "")
}

void putexcel_adv_cellexplist()
{
	struct parse_info scalar pr
	string scalar		file_info

	file_info = putexcel_adv_get_file_info()
	putexcel_parse_info_init(pr)

	pr.filename = file_info[1]

	if (file_info[3] == "replace") {
		pr.op_modify = 0
		pr.op_replace = 1
		putexcel_adv_set_file_info(pr.filename, file_info[2],
			"modify", file_info[4], file_info[5])
	}
	else if (file_info[3] == "create") {
		pr.op_modify = 0
		pr.op_replace = 0
		putexcel_adv_set_file_info(pr.filename, file_info[2],
			"modify", file_info[4], file_info[5])
	}
	else {
		pr.op_modify = 1
		pr.op_replace = 0
	}

	if (file_info[2] == "xlsx") {
		pr.is_xlsx = 1
	}
	else {
		pr.is_xlsx = 0
	}

	if (file_info[4]!="") {
		pr.op_sheet = file_info[4]
	}

	if (st_local("sheet") != "") {
		putexcel_parse_sheet(pr)
		file_info = putexcel_adv_get_file_info()
		putexcel_adv_set_file_info(file_info[1], file_info[2],
			file_info[3], pr.op_sheet, file_info[5])
	}

	if (file_info[5]=="on") {
		pr.op_keepcellformat = 1
	}


	pr.op_colwise = (st_local("colwise") != "")
	putexcel_parse_cellexplist(pr)
	putexcel_write_data(pr)
}

string colvector putexcel_adv_get_file_info()
{
	string scalar		tmp
	string colvector	file_info

	file_info = J(5,1,"")

	tmp = st_global("PUTEXCEL_FILE_NAME")

	if (tmp == "") {
		errprintf("no Excel file specified\n")
		exit(198)
	}
	file_info[1,1] = tmp
	tmp = st_global("PUTEXCEL_FILE_TYPE")
	file_info[2,1] = tmp
	tmp = st_global("PUTEXCEL_FILE_MODE")
	file_info[3,1] = tmp
	tmp = st_global("PUTEXCEL_SHEET_NAME")
	file_info[4,1] = tmp
	tmp = st_global("PUTEXCEL_KEEP_CELL_FORMAT")
	file_info[5,1] = tmp

	return(file_info)
}

// ============================================================================
// Utilities
// ============================================================================

real rowvector putexcel_get_col_row_from_cell(struct parse_info scalar pr,
	string scalar cell)
{
	real rowvector		col_row
	string scalar		column
	string scalar		str_row
	string scalar		c1
	real scalar		i
	real scalar		len
	real scalar		num_c

	col_row = J(1, 2, .)

	cell = strupper(cell)
	len = strlen(cell)
	if (len < 2) {
		return(col_row)
	}

	column = ""
	for (i=1;i<=len;i++) {
		c1 = substr(cell, i, 1)
		num_c = ascii(c1)
		if (num_c < 65 | num_c > 90) {
			if (i==1) {
				return(col_row)
			}
			break
		}
		else {
			column = column + c1
		}
	}

	col_row[1,1] = pr.xl_class.get_colnum(column)

	str_row = substr(cell, i, .)
	col_row[1,2] = strtoreal(str_row)

	return(col_row)
}

real scalar putexcel_check_paren(string scalar expression)
{
	if (substr(expression, 1, 1) != "(") {
		return(`False')
	}
	if (substr(expression, strlen(expression), 1) != ")") {
		errprintf("%s: expression must be enclosed in ()\n")
		return(`False')
	}
	return(`True')
}

string scalar putexcel_remove_paren(string scalar value)
{
	string scalar		rev

	if (substr(value, 1, 1) == "(") {
		value = subinstr(value, "(", "", 1)
	}

	rev = strreverse(value)
	if (substr(rev, 1, 1) == ")") {
		value = strreverse(subinstr(rev, ")", "", 1))
	}
	return(value)
}

string scalar putexcel_remove_quote(string scalar value)
{
	if (strpos(value, `"""') > 0) {
		value = subinstr(value, `"""', "")
	}
	return(value)
}

void putexcel_dump_parse_info(struct parse_info scalar pr)
{
	real scalar		i

	printf("Parsing info\n")
	printf("-------------------------------------------------\n")
	printf("           filename = %s\n", pr.filename)
	printf("           op_sheet = %s\n", pr.op_sheet)
	printf("    op_sheetreplace = %g\n", pr.op_sheetreplace)
	printf("         op_colwise = %g\n", pr.op_colwise)
	printf("          op_modify = %g\n", pr.op_modify)
	printf("         op_replace = %g\n", pr.op_replace)
	printf("            is_xlsx = %g\n", pr.is_xlsx)

	for (i=1; i<=length(pr.cei); i++) {
		putexcel_dump_cellexp(i, *(pr.cei[i]))
	}
}

void putexcel_dump_cellexp(real scalar i, `CEI' cei)
{
	real scalar		j


	printf("\n")
	printf("%g Cell=Expression\n", i)
	printf("-------------------------------------------------\n")
	printf("              cell = %s\n", cei.ci.cell)
	printf("          cell_col = %g\n", cei.ci.cell_col)
	printf("          cell_row = %g\n", cei.ci.cell_row)
	printf("\n")

	printf("        expression = %s\n", cei.ei.expression)
	printf("   eval_str_scalar = %s\n", cei.ei.eval_str_scalar)
	printf("   eval_num_scalar = %g\n", cei.ei.eval_num_scalar)
	cei.ei.eval_mat
	cei.ei.eval_mat_row_stripes
	cei.ei.eval_mat_col_stripes
	printf("       is_shortcut = %g\n", cei.ei.is_shortcut)
	printf("      shortcut_cat = %s\n", cei.ei.shortcut_cat)
	printf("   shortcut_subcat = %s\n", cei.ei.shortcut_subcat)
	printf("         is_matrix = %g\n", cei.ei.is_matrix)

	printf("\n")

	if (cei.ei.is_shortcut) {
		for (j=1; j<length(cei.ei.ri); j++) {
			putexcel_dump_return_info(j, *(cei.ei.ri[j]))
		}
	}
}

void putexcel_dump_return_info(real scalar j, `RI' ri)
{
	printf("%g Return value\n", j)
	printf("-------------------------------------------------\n")
	printf("              type = %s\n", ri.type)
	printf("              name = %s\n", ri.name)
	printf("        ret_scalar = %g\n", ri.ret_scalar)
	printf("         ret_macro = %s\n", ri.ret_macro)
//	printf("        ret_matrix = %s\n", ri.tmpname)
	printf("\n")
	printf("        cell_ul_col = %g\n", ri.cell_ul_col)
	printf("        cell_lr_col = %g\n", ri.cell_lr_col)
	printf("        cell_ul_row = %g\n", ri.cell_ul_row)
	printf("        cell_lr_row = %g\n", ri.cell_lr_row)
	printf("\n")
}
end
