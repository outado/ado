{smcl}
{* *! version 1.0.8  21oct2013}{...}
{viewerdialog "putexcel" "dialog putexcel"}{...}
{vieweralsosee "[P] putexcel" "mansection P putexcel"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[D] export" "help export"}{...}
{vieweralsosee "[D] import" "help import"}{...}
{vieweralsosee "[P] postfile" "help postfile"}{...}
{vieweralsosee "[P] return" "help return"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[M-5] _docx*()" "help mf__docx"}{...}
{vieweralsosee "[M-5] xl()" "help mf_xl"}{...}
{viewerjumpto "Syntax" "putexcel##syntax"}{...}
{viewerjumpto "Menu" "putexcel##menu"}{...}
{viewerjumpto "Description" "putexcel##description"}{...}
{viewerjumpto "Options" "putexcel##options"}{...}
{viewerjumpto "Remarks and examples" "putexcel##remarks"}{...}
{viewerjumpto "Technical note" "putexcel##technote1"}{...}
{title:Title}

{p2colset 5 21 23 2}{...}
{p2col :{manlink P putexcel} {hline 2}}Export results to an Excel file{p_end}
{p2colreset}{...}


{marker syntax}{...}
{title:Syntax}

{p 4 8 2}
{title:Basic syntax}

{p 8 32 2}
{cmd:putexcel} {it:{help putexcel##cellexplist:cellexplist}}
{cmd:using} {it:{help filename}}
[{cmd:,} {it:{help putexcel##options_tbl:options}}]


{p 4 8 2}
{title:Advanced syntax}

{p 8 32 2}
{cmd:putexcel} {cmd:set} {it:{help filename}} 
[{cmd:,} {it:{help putexcel##set_options_tbl:putexcel_set_options}}]

{p 8 32 2}
{cmd:putexcel describe} 

{p 8 32 2}
{cmd:putexcel clear} 

{p 8 32 2}
{cmd:putexcel} {it:{help putexcel##cellexplist:cellexplist}}
[{cmd:,} {cmdab:sh:eet("}{it:sheetname}{cmd:",} ...{cmd:)} {cmdab:colw:ise}]


{marker options_tbl}{...}
{synoptset 30}{...}
{synopthdr}
{synoptline}
{synopt :{cmdab:mod:ify}}modify Excel file{p_end}
{synopt :{opt replace}}overwrite Excel file{p_end}
{synopt :{cmdab:sh:eet("}{it:sheetname}{cmd:"} [{cmd:, replace}]{cmd:)}}write to Excel worksheet {it:sheetname}{p_end}
{synopt :{cmdab:colw:ise}}write {it:{help putexcel##resultset:resultset}} values in consecutive columns instead of consecutive rows{p_end}
{synopt :{cmdab:keepcellf:ormat}}when writing {it:{help putexcel##resultset:resultset}}, preserve the cell style and format of an existing worksheet{p_end}
{synoptline}
{p2colreset}{...}


{marker set_options_tbl}{...}
{synoptset 30}{...}
{synopthdr:putexcel_set_options}
{synoptline}
{synopt :{cmdab:mod:ify}}modify Excel file{p_end}
{synopt :{opt replace}}overwrite Excel file{p_end}
{synopt :{cmdab:sh:eet("}{it:sheetname}{cmd:"} [{cmd:, replace}]{cmd:)}}write to Excel worksheet {it:sheetname}{p_end}
{synopt :{cmdab:keepcellf:ormat}}when writing {it:{help putexcel##resultset:resultset}}, preserve the cell style and format of an existing worksheet{p_end}
{synoptline}
{p2colreset}{...}


{marker cellexplist}{...}
{p 4 4 2}
{it:cellexplist} is one or more of any of the following:

		{it:cell}{cmd:=}{cmd:(}{it:{help exp}}[{cmd:,} {cmd:asdate}|{cmd:asdatetime}]{cmd:)}
		{it:cell}{cmd:=}{cmdab:ma:trix(}{it:{help matrix:name}}[{cmd:,} {cmd:names}|{cmd:rownames}|{cmd:colnames}]{cmd:)}
		{it:cell}{cmd:=}{it:resultset}
            
{p 4 4 2}
{it:cell} is a valid Excel cell specified using standard Excel notation.
For {cmd:matrix(}{it:{help matrix:name}}{cmd:)}, {it:cell} is where the first
value of the matrix will be written.  If you specify a cell multiple times in
a {cmd:putexcel} command, the rightmost {it:cell}{cmd:=}{it:value} is the one
that is written to the Excel file.

{p 4 4 2}
If your expression evaluates to a Stata date and you want that date to be
written as an Excel date, use {cmd:(}{it:{help exp}}{cmd:, asdate)}.  If your
expression evaluates to a Stata datetime and you want that datetime to be
written as an Excel datetime, use {cmd:(}{it:exp}{cmd:, asdatetime)}.

{p 4 4 2}
Use {cmd:matrix(}{it:{help matrix:name}}{cmd:)} when working with any
Stata matrix.  {cmd:matrix(}{it:name}{cmd:, names)} specifies that matrix
row and column names, row and column equation names, and the matrix values be
written to the Excel worksheet.  By default, matrix row and column names are
not written.  {cmd:matrix(}{it:name}{cmd:, rownames)} writes the
matrix row names and values to the Excel worksheet, and
{cmd:matrix(}{it:name}{cmd:, colnames)} writes the column names
and values to the Excel worksheet.

{p 4 4 2}
{marker resultset}{...}
{it:resultset} is a shortcut name used to identify a group of {help return}
values that are stored by a Stata command.  {it:resultset} can be

{p2colset 10 30 30 2}{...}
{p2col:{cmdab:rscal:ars}}{cmdab:rscalarn:ames}{p_end}
{p2col:{cmdab:escal:ars}}{cmdab:escalarn:ames}{p_end}
{p2col:{cmdab:emac:ros}}{cmdab:emacron:ames}{p_end}
{p2col:{cmdab:rmac:ros}}{cmdab:rmacron:ames}{p_end}
{p2col:{cmdab:emat:rices}}{cmdab:ematrixn:ames}{p_end}
{p2col:{cmdab:rmat:rices}}{cmdab:rmatrixn:ames}{p_end}
{p2col:{cmd:e*}}{cmdab:ena:mes}{p_end}
{p2col:{cmd:r*}}{cmdab:rna:mes}{p_end}

{p2colreset}{...}
{marker examples}{...}
{p 4 4 2}
Example:  {cmd:putexcel A1=(2+2) using file}
{p_end}
{p 8 8 2}
Write the result of the expression {cmd:2+2} into Excel column A row 1 in the
file {cmd:file.xlsx}.

{p 4 4 2}
Example:  {cmd:putexcel A1=("Mean of mpg") A2=(r(mean)) using file}
{p_end}
{p 8 8 2}
Write {cmd:"Mean of mpg"} in Excel column A row 1, and write the r-class
result {cmd:r(mean)} in Excel column A row 2.

{p 4 4 2}
Example:  {cmd:putexcel D14=matrix(A) using file}
{p_end}
{p 8 8 2}
Take the Stata matrix {cmd:A}, and write it into Excel using
column D row 14 as the upper-left cell for matrix {cmd:A}.

{p 4 4 2}
Example:  {cmd:putexcel D4=("Coefficients") B5=matrix(e(b)) using file}
{p_end}
{p 8 8 2}
Write {cmd:"Coefficients"} in Excel column D row 4, and  write the values
of e-class matrix {cmd:e(b)} into Excel using column B row 5 as the upper-left
cell.

{p 4 4 2}
Example:  {cmd:putexcel A1=rscalars using file, sheet("Results")}
{p_end}
{p 8 8 2}
Write all r-class scalars in memory to sheet {cmd:Results} in {cmd:file.xlsx}.
The first scalar value in memory will be written in Excel column A row 1, the
next value in column A row 2, etc.  

{p 4 4 2}
Example:  {cmd:putexcel A1=rscalarnames B1=rscalars using file, sheet("Results")}
{p_end}
{p 8 8 2}
Write all r-class scalar names and values in memory to sheet {cmd:Results}
in {cmd:file.xlsx}.  The first scalar name will be written in Excel column A
row 1, the next in column A row 2, and so on, and the first scalar value will
be written in column B row 1, the next in column B row 2, and so on. 

{p 4 4 2}
Example:  {cmd:putexcel A1=r* B1=e* using file}
{p_end}
{p 8 8 2} Write all r-class scalars, macros, and matrices and all e-class
scalars, macros, and matrices in memory to file {cmd:file.xlsx}.  The
first r-class  value in memory will be written in Excel column A row 1, the
next value in column A row 2, and so on.  The first e-class value in memory
will be written in Excel column B row 1, the next value in column B row 2, and
so on.


{marker menu}{...}
{title:Menu}

{phang}
{bf:File > Export > Results to Excel spreadsheet (*.xls;*.xlsx)}


{marker description}{...}
{title:Description}

{pstd}
{cmd:putexcel} writes Stata {help expressions:expressions},
{help matrix:matrices}, and {help return:stored results} to an Excel file.
{cmd:putexcel} is supported on Windows, Mac, and Linux. Excel 1997/2003
({cmd:.xls}) files and Excel 2007/2010 ({cmd:.xlsx}) files are supported.
{cmd:putexcel} looks at the file extension {cmd:.xls} or {cmd:.xlsx} to
determine which Excel format to write.

{pstd}
{cmd:putexcel} {cmd:set} {it:{help filename}} sets the Excel file to create,
modify, or replace subsequent {cmd:putexcel} {it:cellexplist} commands.
If {it:filename} does not exist, {cmd:putexcel set} will
create the file.  If {it:filename} exists, it will not be modified
unless you specify the {cmd:modify} or {cmd:replace} options.

{pstd}
{cmd:putexcel describe} displays the file information set by
{cmd:putexcel set}.

{pstd}
{cmd:putexcel clear} clears the file information set by
{cmd:putexcel set}.

{pstd}
{cmd:putexcel} {it:{help putexcel##cellexplist:cellexplist}} writes Stata 
{help expressions}, {help matrix:matrices}, and 
{help return:stored results} to an Excel file.  

{pstd}
The default file extension for {cmd:putexcel} is {cmd:.xlsx}.


{marker options}{...}
{title:Options}

{phang}
{opt modify} in the basic syntax modifies an existing Excel file.

{pmore}
In the advanced syntax, {opt modify} allows you to modify the file specified
with {cmd:putexcel set} using subsequent {cmd:putexcel} {it:cellexplist}
commands.

{phang}
{opt replace} in the basic syntax overwrites an existing Excel file.

{pmore}
In the advanced syntax, {opt replace} replaces the file specified with
{cmd:putexcel set} and then allows you to modify the file using
subsequent {cmd:putexcel} {it:cellexplist} commands.

{phang}
{cmd:sheet("}{it:sheetname}{cmd:")} writes to the worksheet named
{it:sheetname}.  If there is no worksheet named {it:sheetname} in the workbook,
a new sheet named {it:sheetname} is created.  If this option is not specified,
the first worksheet of the workbook is used.

{pmore}
{cmd:sheet("}{it:sheetname}{cmd:", replace)} clears the worksheet before values 
are written to it.  

{phang}
{opt colwise} specifies that if a {it:{help putexcel##resultset:resultset}} is
used, the values written to the Excel worksheet are written in consecutive
columns.  By default, the values are written in consecutive rows.

{phang}
{opt keepcellformat} specifies that when writing the 
{it:{help putexcel##resultset:resultset}}, {cmd:putexcel} should preserve the
existing worksheet's cell style and format. 
By default, {cmd:putexcel} does not preserve a cell's style or format.  


{marker remarks}{...}
{title:Remarks and examples}

{pstd}
To demonstrate the use of {cmd:putexcel}, we will first load
{cmd:auto.dta} and export the results of the {helpb summarize} command
to an Excel file named {cmd:results.xlsx}:

        {cmd:. sysuse auto}
        (1978 Automobile Data)
        
	{cmd:. summarize mpg}

	{cmd:. return list}

	 scalars:
			   r(N) =  74
		       r(sum_w) =  74
			r(mean) =  21.2972972972973
			 r(Var) =  33.47204738985561
			  r(sd) =  5.785503209735141
			 r(min) =  12
			 r(max) =  41
			 r(sum) =  1576

        {cmd:. putexcel A30=rscalars using results, sheet("June 3") modify}

{pstd}
The above command modifies Excel workbook {cmd:results.xlsx} sheet {cmd:June 3} 
with the following cell values:

	A30 =  74
	A31 =  74
        A32 =  21.2972972972973
        A33 =  33.47204738985561
        A34 =  5.785503209735141
        A35 =  12
        A36 =  41
        A37 =  1576

{pstd}
You can write out specific results by using the following command:
        
	{cmd:. putexcel A30=(r(min)) A31=(r(N)) using results,}		 ///
		{cmd:sheet("June 3", replace) modify}

{pstd}
The above command would write over sheet {cmd:June 3} in {cmd:results.xls} so
that just cell {cmd:A30} and {cmd:A31} contained values {cmd:12} and {cmd:74}.

{pstd}
You can use {cmd:putexcel} to create tables in Excel using Stata 
{help return} results.  To create a 
{helpb tabulate oneway} table of the variable {cmd:foreign} in
Excel format, type

	{cmd:. tabulate foreign, matcell(cell) matrow(rows)}

	   Car type |      Freq.     Percent        Cum.
	------------+-----------------------------------
	   Domestic |         52       70.27       70.27
	    Foreign |         22       29.73      100.00
	------------+-----------------------------------
	      Total |         74      100.00

	{cmd:. putexcel A1=("Car type") B1=("Freq.") using results,}	 ///
		{cmd:sheet("tabulate oneway") replace}

	{cmd:. putexcel A2=matrix(rows) B2=matrix(cell) using results,} ///
		{cmd:sheet("tabulate oneway") modify}
	
	{cmd:. putexcel A4=("Total") B4=(r(N)) using results,}           ///
		{cmd:sheet("tabulate oneway") modify}

{pstd}
If you are going to export complex tables or export numerous objects, you
should use the advanced syntax of {cmd:putexcel}.  For example, to create a
regression table in Excel format using returned results from {cmd:regress},
type

	{cmd:. sysuse auto, clear}
	{cmd:. regress price turn gear}

	{cmd:. putexcel set "results.xls", sheet("regress results")}

	{cmd:. putexcel F1=("Number of obs") G1=(e(N))}
	{cmd:. putexcel F2=("F")             G2=(e(F))}
	{cmd:. putexcel F3=("Prob > F")      G3=(Ftail(e(df_m), e(df_r), e(F)))}
	{cmd:. putexcel F4=("R-squared")     G4=(e(r2))}
	{cmd:. putexcel F5=("Adj R-squared") G5=(e(r2_a))}
	{cmd:. putexcel F6=("Root MSE")      G6=(e(rmse))}
	{cmd:. matrix a = r(table)'}
	{cmd:. matrix a = a[.,1..6]}
	{cmd:. putexcel A8=matrix(a, names)}


{marker technote1}{...}
{title:Technical note:  Excel data size limits and date and times}

{pstd}
You can read about Excel data size limits and the two different Excel date
systems in {helpb import_excel##technote1:help import excel}.
{p_end}
