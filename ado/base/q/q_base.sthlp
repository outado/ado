{smcl}
{* *! version 1.2.3  04jun2013}{...}
{bf:Datasets for Stata Base Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:{help anova}}
	apple.dta{col 30}{stata "use http://www.stata-press.com/data/r13/apple.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/apple.dta":describe}
	systolic.dta{col 30}{stata "use http://www.stata-press.com/data/r13/systolic.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/systolic.dta":describe}
	manuf.dta{col 30}{stata "use http://www.stata-press.com/data/r13/manuf.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/manuf.dta":describe}
	byssin.dta{col 30}{stata "use http://www.stata-press.com/data/r13/byssin.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/byssin.dta":describe}
	census2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census2.dta":describe}
	machine.dta{col 30}{stata "use http://www.stata-press.com/data/r13/machine.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/machine.dta":describe}
	sewage.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sewage.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sewage.dta":describe}
	reading.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reading.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reading.dta":describe}
	latinsq.dta{col 30}{stata "use http://www.stata-press.com/data/r13/latinsq.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/latinsq.dta":describe}
	t43.dta{col 30}{stata "use http://www.stata-press.com/data/r13/t43.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/t43.dta":describe}
	t77.dta{col 30}{stata "use http://www.stata-press.com/data/r13/t77.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/t77.dta":describe}
	t713.dta{col 30}{stata "use http://www.stata-press.com/data/r13/t713.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/t713.dta":describe}

    {title:{help anova postestimation}}
	machine.dta{col 30}{stata "use http://www.stata-press.com/data/r13/machine.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/machine.dta":describe}
	sewage.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sewage.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sewage.dta":describe}
	systolic.dta{col 30}{stata "use http://www.stata-press.com/data/r13/systolic.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/systolic.dta":describe}
	canfill.dta{col 30}{stata "use http://www.stata-press.com/data/r13/canfill.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/canfill.dta":describe}
	twowaytrend.dta{col 30}{stata "use http://www.stata-press.com/data/r13/twowaytrend.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/twowaytrend.dta":describe}

    {title:{help areg}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}

    {title:{help areg postestimation}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}

    {title:{help asclogit}}
	choice.dta{col 30}{stata "use http://www.stata-press.com/data/r13/choice.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/choice.dta":describe}

    {title:{help asclogit postestimation}}
	choice.dta{col 30}{stata "use http://www.stata-press.com/data/r13/choice.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/choice.dta":describe}

    {title:{help asmprobit}}
	travel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/travel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/travel.dta":describe}

    {title:{help asmprobit postestimation}}
	travel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/travel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/travel.dta":describe}

    {title:{help asroprobit}}
	wlsrank.dta{col 30}{stata "use http://www.stata-press.com/data/r13/wlsrank.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/wlsrank.dta":describe}

    {title:{help asroprobit postestimation}}
	wlsrank.dta{col 30}{stata "use http://www.stata-press.com/data/r13/wlsrank.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/wlsrank.dta":describe}

{hline}

    {title:{help binreg}}
	binreg.dta{col 30}{stata "use http://www.stata-press.com/data/r13/binreg.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/binreg.dta":describe}

    {title:{help biprobit}}
	school.dta{col 30}{stata "use http://www.stata-press.com/data/r13/school.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/school.dta":describe}

    {title:{help bitest}}
	quick.dta{col 30}{stata "use http://www.stata-press.com/data/r13/quick.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/quick.dta":describe}

    {title:{help bootstrap}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	lowbirth2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lowbirth2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lowbirth2.dta":describe}

    {title:{help bootstrap postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help boxcox}}
	nhanes2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nhanes2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nhanes2.dta":describe}

    {title:{help boxcox postestimation}}
	nhanes2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nhanes2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nhanes2.dta":describe}

    {title:{help brier}}
	bball.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bball.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bball.dta":describe}

    {title:{help bsample}}
	bsample1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bsample1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bsample1.dta":describe}
	bsample2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bsample2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bsample2.dta":describe}

    {title:{help bstat}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help centile}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help ci}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	promo.dta{col 30}{stata "use http://www.stata-press.com/data/r13/promo.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/promo.dta":describe}
	promonone.dta{col 30}{stata "use http://www.stata-press.com/data/r13/promonone.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/promonone.dta":describe}
	petri.dta{col 30}{stata "use http://www.stata-press.com/data/r13/petri.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/petri.dta":describe}
	petrinone.dta{col 30}{stata "use http://www.stata-press.com/data/r13/petrinone.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/petrinone.dta":describe}

    {title:{help clogit}}
	clogitid.dta{col 30}{stata "use http://www.stata-press.com/data/r13/clogitid.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/clogitid.dta":describe}
	lowbirth2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lowbirth2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lowbirth2.dta":describe}
	union.dta{col 30}{stata "use http://www.stata-press.com/data/r13/union.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/union.dta":describe}

    {title:{help clogit postestimation}}
	clogitid.dta{col 30}{stata "use http://www.stata-press.com/data/r13/clogitid.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/clogitid.dta":describe}

    {title:{help cloglog}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	union.dta{col 30}{stata "use http://www.stata-press.com/data/r13/union.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/union.dta":describe}

    {title:{help cloglog postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help cnsreg}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help constraint}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help contrast}}
	cholesterol.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cholesterol.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cholesterol.dta":describe}
	bpchange.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bpchange.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bpchange.dta":describe}
	cont3way.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cont3way.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cont3way.dta":describe}
	cholesterol2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cholesterol2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cholesterol2.dta":describe}
	sat.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sat.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sat.dta":describe}
	cholesterol3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cholesterol3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cholesterol3.dta":describe}
	census3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census3.dta":describe}
	hospital.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hospital.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hospital.dta":describe}
	jaw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/jaw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/jaw.dta":describe}

    {title:{help contrast postestimation}}
	cholesterol.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cholesterol.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cholesterol.dta":describe}

    {title:{help correlate}}
	census13.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census13.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census13.dta":describe}
	auto1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto1.dta":describe}

    {title:{help cumul}}
	hsng.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hsng.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hsng.dta":describe}
	citytemp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/citytemp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/citytemp.dta":describe}

    {title:{help cusum}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help diagnostic plots}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help dotplot}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	dotgr.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dotgr.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dotgr.dta":describe}
	dotdose.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dotdose.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dotdose.dta":describe}

    {title:{help dstdize}}
	mortality.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mortality.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mortality.dta":describe}
	1962.dta{col 30}{stata "use http://www.stata-press.com/data/r13/1962.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/1962.dta":describe}
	hbp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hbp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hbp.dta":describe}
	popkahn.dta{col 30}{stata "use http://www.stata-press.com/data/r13/popkahn.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/popkahn.dta":describe}
	kahn.dta{col 30}{stata "use http://www.stata-press.com/data/r13/kahn.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/kahn.dta":describe}

{hline}

    {title:{help eform_option}}
	nhanes2d.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nhanes2d.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nhanes2d.dta":describe}

    {title:{help eivreg}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help esize}}
	depression.dta{col 30}{stata "use http://www.stata-press.com/data/r13/depression.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/depression.dta":describe}
	fuel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel.dta":describe}

    {title:{help estat classification}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}

    {title:{help estat gof}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}

    {title:{help estat ic}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help estat summarize}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}
	supDem.dta{col 30}{stata "use http://www.stata-press.com/data/r13/supDem.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/supDem.dta":describe}

    {title:{help estat vce}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help estimates}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates describe}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates for}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates notes}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates replay}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates save}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates stats}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates store}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates table}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help estimates title}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help exlogistic}}
	hiv1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiv1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiv1.dta":describe}
	hiv_n.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiv_n.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiv_n.dta":describe}
	schizophrenia.dta{col 30}{stata "use http://www.stata-press.com/data/r13/schizophrenia.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/schizophrenia.dta":describe}

    {title:{help exlogistic postestimation}}
	hiv_n.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiv_n.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiv_n.dta":describe}

    {title:{help expoisson}}
	cerebacc.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cerebacc.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cerebacc.dta":describe}
	heartvalve.dta{col 30}{stata "use http://www.stata-press.com/data/r13/heartvalve.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/heartvalve.dta":describe}

    {title:{help expoisson postestimation}}
	smokes.dta{col 30}{stata "use http://www.stata-press.com/data/r13/smokes.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/smokes.dta":describe}

{hline}

    {title:{help fp}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	igg.dta{col 30}{stata "use http://www.stata-press.com/data/r13/igg.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/igg.dta":describe}
	legulcer1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/legulcer1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/legulcer1.dta":describe}
	smoking.dta{col 30}{stata "use http://www.stata-press.com/data/r13/smoking.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/smoking.dta":describe}

    {title:{help fp postestimation}}
	igg.dta{col 30}{stata "use http://www.stata-press.com/data/r13/igg.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/igg.dta":describe}
	legulcer1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/legulcer1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/legulcer1.dta":describe}
	smoking.dta{col 30}{stata "use http://www.stata-press.com/data/r13/smoking.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/smoking.dta":describe}

    {title:{help frontier}}
	greene9.dta{col 30}{stata "use http://www.stata-press.com/data/r13/greene9.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/greene9.dta":describe}
	frontier1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/frontier1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/frontier1.dta":describe}
	frontier2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/frontier2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/frontier2.dta":describe}

    {title:{help frontier postestimation}}
	frontier1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/frontier1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/frontier1.dta":describe}

    {title:{help fvrevar}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}

    {title:{help fvset}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}

{hline}

    {title:{help glm}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}
	ldose.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ldose.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ldose.dta":describe}
	beetle.dta{col 30}{stata "use http://www.stata-press.com/data/r13/beetle.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/beetle.dta":describe}
	idle2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/idle2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/idle2.dta":describe}
	airline.dta{col 30}{stata "use http://www.stata-press.com/data/r13/airline.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/airline.dta":describe}

    {title:{help glm postestimation}}
	ldose.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ldose.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ldose.dta":describe}
	beetle.dta{col 30}{stata "use http://www.stata-press.com/data/r13/beetle.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/beetle.dta":describe}

    {title:{help glogit}}
	xmpl1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/xmpl1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/xmpl1.dta":describe}
	xmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/xmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/xmpl2.dta":describe}
	census7.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census7.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census7.dta":describe}

    {title:{help gmm}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	hsng2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hsng2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hsng2.dta":describe}
	docvisits.dta{col 30}{stata "use http://www.stata-press.com/data/r13/docvisits.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/docvisits.dta":describe}
	poisson1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/poisson1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/poisson1.dta":describe}
	poisson2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/poisson2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/poisson2.dta":describe}
	cr.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cr.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cr.dta":describe}
	klein.dta{col 30}{stata "use http://www.stata-press.com/data/r13/klein.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/klein.dta":describe}
	abdata.dta{col 30}{stata "use http://www.stata-press.com/data/r13/abdata.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/abdata.dta":describe}
	poissonwts.dta{col 30}{stata "use http://www.stata-press.com/data/r13/poissonwts.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/poissonwts.dta":describe}

    {title:{help gmm postestimation}}
	docvisits.dta{col 30}{stata "use http://www.stata-press.com/data/r13/docvisits.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/docvisits.dta":describe}

    {title:{help grmeanby}}
	auto1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto1.dta":describe}

{hline}

    {title:{help hausman}}
	nlswork4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlswork4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlswork4.dta":describe}
	sysdsn3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn3.dta":describe}

    {title:{help heckman}}
	womenwk.dta{col 30}{stata "use http://www.stata-press.com/data/r13/womenwk.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/womenwk.dta":describe}
	twopart.dta{col 30}{stata "use http://www.stata-press.com/data/r13/twopart.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/twopart.dta":describe}

    {title:{help heckman postestimation}}
	womenwk.dta{col 30}{stata "use http://www.stata-press.com/data/r13/womenwk.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/womenwk.dta":describe}

    {title:{help heckprobit}}
	school.dta{col 30}{stata "use http://www.stata-press.com/data/r13/school.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/school.dta":describe}

    {title:{help heckoprobit}}
	womensat.dta{col 30}{stata "use http://www.stata-press.com/data/r13/womensat.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/womensat.dta":describe}

    {title:{help heckoprobit postestimation}}
	womensat.dta{col 30}{stata "use http://www.stata-press.com/data/r13/womensat.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/womensat.dta":describe}

    {title:{help histogram}}
	sp500.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sp500.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sp500.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	voter.dta{col 30}{stata "use http://www.stata-press.com/data/r13/voter.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/voter.dta":describe}

{hline}

    {title:{help icc}}
	judges.dta{col 30}{stata "use http://www.stata-press.com/data/r13/judges.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/judges.dta":describe}
	adoption.dta{col 30}{stata "use http://www.stata-press.com/data/r13/adoption.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/adoption.dta":describe}

    {title:{help intreg}}
	womenwage.dta{col 30}{stata "use http://www.stata-press.com/data/r13/womenwage.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/womenwage.dta":describe}

    {title:{help intreg postestimation}}
	intregxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/intregxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/intregxmpl.dta":describe}

    {title:{help ivpoisson}}
	website.dta{col 30}{stata "use http://www.stata-press.com/data/r13/website.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/website.dta":describe}
	trip.dta{col 30}{stata "use http://www.stata-press.com/data/r13/trip.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/trip.dta":describe}

    {title:{help ivpoisson postestimation}}
	website.dta{col 30}{stata "use http://www.stata-press.com/data/r13/website.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/website.dta":describe}

    {title:{help ivprobit}}
	laborsup.dta{col 30}{stata "use http://www.stata-press.com/data/r13/laborsup.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/laborsup.dta":describe}

    {title:{help ivprobit postestimation}}
	laborsup.dta{col 30}{stata "use http://www.stata-press.com/data/r13/laborsup.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/laborsup.dta":describe}

    {title:{help ivregress}}
	hsng.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hsng.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hsng.dta":describe}
	nlswork.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlswork.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlswork.dta":describe}

    {title:{help ivregress postestimation}}
	hsng.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hsng.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hsng.dta":describe}

    {title:{help ivtobit}}
	laborsup.dta{col 30}{stata "use http://www.stata-press.com/data/r13/laborsup.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/laborsup.dta":describe}

    {title:{help ivtobit postestimation}}
	laborsup.dta{col 30}{stata "use http://www.stata-press.com/data/r13/laborsup.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/laborsup.dta":describe}

{hline}

    {title:{help jackknife}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help kappa}}
	rate2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rate2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rate2.dta":describe}
	rate2no3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rate2no3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rate2no3.dta":describe}
	p612.dta{col 30}{stata "use http://www.stata-press.com/data/r13/p612.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/p612.dta":describe}
	p615.dta{col 30}{stata "use http://www.stata-press.com/data/r13/p615.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/p615.dta":describe}
	p615b.dta{col 30}{stata "use http://www.stata-press.com/data/r13/p615b.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/p615b.dta":describe}
	rvary.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rvary.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rvary.dta":describe}
	rvary2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rvary2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rvary2.dta":describe}

    {title:{help kdensity}}
	trocolen.dta{col 30}{stata "use http://www.stata-press.com/data/r13/trocolen.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/trocolen.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help ksmirnov}}
	ksxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ksxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ksxmpl.dta":describe}

    {title:{help kwallis}}
	census.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census.dta":describe}

{hline}

    {title:{help ladder}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	citytemp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/citytemp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/citytemp.dta":describe}

    {title:{help level}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help lincom}}
	regress.dta{col 30}{stata "use http://www.stata-press.com/data/r13/regress.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/regress.dta":describe}
	lbw3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw3.dta":describe}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help linktest}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help lnskew0}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help logistic}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}
	hospid1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hospid1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hospid1.dta":describe}
	hospid2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hospid2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hospid2.dta":describe}

    {title:{help logistic postestimation}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}

    {title:{help logit}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	repair.dta{col 30}{stata "use http://www.stata-press.com/data/r13/repair.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/repair.dta":describe}
	logitxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/logitxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/logitxmpl.dta":describe}

    {title:{help logit postestimation}}
	repair.dta{col 30}{stata "use http://www.stata-press.com/data/r13/repair.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/repair.dta":describe}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}

    {title:{help loneway}}
	auto7.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto7.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto7.dta":describe}

    {title:{help lowess}}
	lowess1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lowess1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lowess1.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help lpoly}}
	motorcycle.dta{col 30}{stata "use http://www.stata-press.com/data/r13/motorcycle.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/motorcycle.dta":describe}

    {title:{help lroc}}
	heart.dta{col 30}{stata "use http://www.stata-press.com/data/r13/heart.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/heart.dta":describe}
	lbw3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw3.dta":describe}

    {title:{help lrtest}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}

    {title:{help lsens}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}
	lbw3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw3.dta":describe}

    {title:{help lv}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help margins}}
	margex.dta{col 30}{stata "use http://www.stata-press.com/data/r13/margex.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/margex.dta":describe}
	peach.dta{col 30}{stata "use http://www.stata-press.com/data/r13/peach.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/peach.dta":describe}
	hstandard.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hstandard.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hstandard.dta":describe}
	acmemanuf.dta{col 30}{stata "use http://www.stata-press.com/data/r13/acmemanuf.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/acmemanuf.dta":describe}
	estimability.dta{col 30}{stata "use http://www.stata-press.com/data/r13/estimability.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/estimability.dta":describe}

    {title:{help margins postestimation}}
	margex.dta{col 30}{stata "use http://www.stata-press.com/data/r13/margex.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/margex.dta":describe}

    {title:{help margins, contrast}}
	margex.dta{col 30}{stata "use http://www.stata-press.com/data/r13/margex.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/margex.dta":describe}
	nlsw88.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlsw88.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlsw88.dta":describe}

    {title:{help margins, pwcompare}}
	nhanes2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nhanes2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nhanes2.dta":describe}

    {title:{help marginsplot}}
	nhanes2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nhanes2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nhanes2.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help mean}}
	fuel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel.dta":describe}
	hbp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hbp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hbp.dta":describe}

    {title:{help mean postestimation}}
	rates.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rates.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rates.dta":describe}

    {title:{help mfp}}
	brcancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/brcancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/brcancer.dta":describe}

    {title:{help mfp postestimation}}
	brcancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/brcancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/brcancer.dta":describe}

    {title:{help misstable}}
	studentsurvey.dta{col 30}{stata "use http://www.stata-press.com/data/r13/studentsurvey.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/studentsurvey.dta":describe}

    {title:{help mkspline}}
	mksp1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mksp1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mksp1.dta":describe}
	mksp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mksp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mksp2.dta":describe}

    {title:{help ml}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	cancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cancer.dta":describe}

    {title:{help mlexp}}
	greengamma.dta{col 30}{stata "use http://www.stata-press.com/data/r13/greengamma.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/greengamma.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help mlogit}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help mlogit postestimation}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help mprobit}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help mprobit postestimation}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

{hline}

    {title:{help nbreg}}
	rod93.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rod93.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rod93.dta":describe}

    {title:{help nbreg postestimation}}
	rod93.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rod93.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rod93.dta":describe}

    {title:{help nestreg}}
	census4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census4.dta":describe}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}

    {title:{help nl}}
	production.dta{col 30}{stata "use http://www.stata-press.com/data/r13/production.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/production.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help nl postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help nlcom}}
	regress.dta{col 30}{stata "use http://www.stata-press.com/data/r13/regress.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/regress.dta":describe}
	trafint.dta{col 30}{stata "use http://www.stata-press.com/data/r13/trafint.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/trafint.dta":describe}
	airline.dta{col 30}{stata "use http://www.stata-press.com/data/r13/airline.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/airline.dta":describe}

    {title:{help nlogit}}
	restaurant.dta{col 30}{stata "use http://www.stata-press.com/data/r13/restaurant.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/restaurant.dta":describe}

    {title:{help nlogit postestimation}}
	restaurant.dta{col 30}{stata "use http://www.stata-press.com/data/r13/restaurant.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/restaurant.dta":describe}

    {title:{help nlsur}}
	petridish.dta{col 30}{stata "use http://www.stata-press.com/data/r13/petridish.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/petridish.dta":describe}
	mfgcost.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mfgcost.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mfgcost.dta":describe}
	food.dta{col 30}{stata "use http://www.stata-press.com/data/r13/food.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/food.dta":describe}

    {title:{help nptrend}}
	sg.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sg.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sg.dta":describe}

{hline}

    {title:{help ologit}}
	fullauto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fullauto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fullauto.dta":describe}

    {title:{help ologit postestimation}}
	fullauto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fullauto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fullauto.dta":describe}

    {title:{help oneway}}
	apple.dta{col 30}{stata "use http://www.stata-press.com/data/r13/apple.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/apple.dta":describe}
	census8.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census8.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census8.dta":describe}

    {title:{help oprobit}}
	fullauto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fullauto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fullauto.dta":describe}

    {title:{help oprobit postestimation}}
	fullauto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fullauto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fullauto.dta":describe}

    {title:{help orthog}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help pcorr}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help permute}}
	permute1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/permute1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/permute1.dta":describe}
	permute2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/permute2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/permute2.dta":describe}

    {title:{help pk}}
	auc.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auc.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auc.dta":describe}
	pkdata.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pkdata.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pkdata.dta":describe}

    {title:{help pkcollapse}}
	pkdata.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pkdata.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pkdata.dta":describe}

    {title:{help pkcross}}
	chowliu.dta{col 30}{stata "use http://www.stata-press.com/data/r13/chowliu.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/chowliu.dta":describe}
	balaam.dta{col 30}{stata "use http://www.stata-press.com/data/r13/balaam.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/balaam.dta":describe}
	pkdata3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pkdata3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pkdata3.dta":describe}
	nobalance.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nobalance.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nobalance.dta":describe}

    {title:{help pkequiv}}
	pkdata3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pkdata3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pkdata3.dta":describe}
	chowliu2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/chowliu2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/chowliu2.dta":describe}

    {title:{help pkexamine}}
	auc.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auc.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auc.dta":describe}

    {title:{help pkshape}}
	chowliu.dta{col 30}{stata "use http://www.stata-press.com/data/r13/chowliu.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/chowliu.dta":describe}
	music.dta{col 30}{stata "use http://www.stata-press.com/data/r13/music.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/music.dta":describe}
	applesales.dta{col 30}{stata "use http://www.stata-press.com/data/r13/applesales.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/applesales.dta":describe}
	pkdata2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pkdata2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pkdata2.dta":describe}

    {title:{help pksumm}}
	pksumm.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pksumm.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pksumm.dta":describe}

    {title:{help poisson}}
	airline.dta{col 30}{stata "use http://www.stata-press.com/data/r13/airline.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/airline.dta":describe}
	dollhill3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dollhill3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dollhill3.dta":describe}

    {title:{help poisson postestimation}}
	dollhill3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dollhill3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dollhill3.dta":describe}

    {title:{help predict}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	newautos.dta{col 30}{stata "use http://www.stata-press.com/data/r13/newautos.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/newautos.dta":describe}
	airline.dta{col 30}{stata "use http://www.stata-press.com/data/r13/airline.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/airline.dta":describe}

    {title:{help predictnl}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help probit}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	union.dta{col 30}{stata "use http://www.stata-press.com/data/r13/union.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/union.dta":describe}
	repair.dta{col 30}{stata "use http://www.stata-press.com/data/r13/repair.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/repair.dta":describe}

    {title:{help proportion}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}

    {title:{help proportion postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help prtest}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	cure.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cure.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cure.dta":describe}

    {title:{help pwcompare}}
	yield.dta{col 30}{stata "use http://www.stata-press.com/data/r13/yield.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/yield.dta":describe}
	hospital.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hospital.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hospital.dta":describe}
	jaw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/jaw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/jaw.dta":describe}
	cholesterol3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cholesterol3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cholesterol3.dta":describe}
	cholesterol2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cholesterol2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cholesterol2.dta":describe}

    {title:{help pwcompare postestimation}}
	yield.dta{col 30}{stata "use http://www.stata-press.com/data/r13/yield.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/yield.dta":describe}

    {title:{help pwmean}}
	yield.dta{col 30}{stata "use http://www.stata-press.com/data/r13/yield.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/yield.dta":describe}

    {title:{help pwmean postestimation}}
	yield.dta{col 30}{stata "use http://www.stata-press.com/data/r13/yield.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/yield.dta":describe}

{hline}

    {title:{help qc}}
	ncu.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ncu.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ncu.dta":describe}
	ncu2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ncu2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ncu2.dta":describe}
	ncu3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ncu3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ncu3.dta":describe}

    {title:{help qreg}}
	twogrp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/twogrp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/twogrp.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help qreg postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help ranksum}}
	fuel2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel2.dta":describe}

    {title:{help ratio}}
	fuel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel.dta":describe}
	census2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census2.dta":describe}

    {title:{help reg3}}
	klein.dta{col 30}{stata "use http://www.stata-press.com/data/r13/klein.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/klein.dta":describe}
	supDem.dta{col 30}{stata "use http://www.stata-press.com/data/r13/supDem.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/supDem.dta":describe}
	klein2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/klein2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/klein2.dta":describe}
	kleinfull.dta{col 30}{stata "use http://www.stata-press.com/data/r13/kleinfull.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/kleinfull.dta":describe}

    {title:{help reg3 postestimation}}
	supDem.dta{col 30}{stata "use http://www.stata-press.com/data/r13/supDem.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/supDem.dta":describe}
	klein2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/klein2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/klein2.dta":describe}

    {title:{help regress}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	regsmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/regsmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/regsmpl.dta":describe}
	census9.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census9.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census9.dta":describe}

    {title:{help regress postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	newautos.dta{col 30}{stata "use http://www.stata-press.com/data/r13/newautos.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/newautos.dta":describe}
	bodyfat.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bodyfat.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bodyfat.dta":describe}
	extreme_collin.dta{col 30}{stata "use http://www.stata-press.com/data/r13/extreme_collin.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/extreme_collin.dta":describe}
	depression.dta{col 30}{stata "use http://www.stata-press.com/data/r13/depression.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/depression.dta":describe}

    {title:{help regress postestimation diagnostic plots}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	auto1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto1.dta":describe}

    {title:{help regress postestimation time series}}
	klein.dta{col 30}{stata "use http://www.stata-press.com/data/r13/klein.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/klein.dta":describe}

    {title:{help roccomp}}
	ct.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ct.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ct.dta":describe}
	ct2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ct2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ct2.dta":describe}
	ct3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ct3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ct3.dta":describe}

    {title:{help rocfit}}
	hanley.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hanley.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hanley.dta":describe}

    {title:{help rocreg}}
	hanley.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hanley.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hanley.dta":describe}
	ct.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ct.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ct.dta":describe}
	ct2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ct2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ct2.dta":describe}
	nnhs.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nnhs.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nnhs.dta":describe}
	dp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dp.dta":describe}

    {title:{help rocreg postestimation}}
	nnhs.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nnhs.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nnhs.dta":describe}
	dp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dp.dta":describe}
	ct2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ct2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ct2.dta":describe}

    {title:{help rocregplot}}
	nnhs.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nnhs.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nnhs.dta":describe}
	dp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dp.dta":describe}
	hanley.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hanley.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hanley.dta":describe}
	ct2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ct2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ct2.dta":describe}

    {title:{help roctab}}
	hanley.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hanley.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hanley.dta":describe}
	lorenz.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lorenz.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lorenz.dta":describe}

    {title:{help rologit}}
	evignet.dta{col 30}{stata "use http://www.stata-press.com/data/r13/evignet.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/evignet.dta":describe}

    {title:{help rreg}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help runtest}}
	run1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/run1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/run1.dta":describe}
	additive.dta{col 30}{stata "use http://www.stata-press.com/data/r13/additive.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/additive.dta":describe}

{hline}

    {title:{help scobit}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	union.dta{col 30}{stata "use http://www.stata-press.com/data/r13/union.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/union.dta":describe}

    {title:{help scobit postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help sdtest}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	fuel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel.dta":describe}
	stay.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stay.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stay.dta":describe}

    {title:{help serrbar}}
	assembly.dta{col 30}{stata "use http://www.stata-press.com/data/r13/assembly.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/assembly.dta":describe}

    {title:{help set cformat}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help set emptycells}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help set showbaselevels}}
	cholesterol2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cholesterol2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cholesterol2.dta":describe}
	jaw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/jaw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/jaw.dta":describe}

    {title:{help signrank}}
	fuel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel.dta":describe}

    {title:{help sktest}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help slogit}}
	auto2yr.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2yr.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2yr.dta":describe}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help slogit postestimation}}
	sysdsn1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn1.dta":describe}

    {title:{help smooth}}
	fishdata.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fishdata.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fishdata.dta":describe}

    {title:{help spearman}}
	states2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/states2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/states2.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help spikeplot}}
	ghanaage.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ghanaage.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ghanaage.dta":describe}

    {title:{help stem}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help stepwise}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help stored results}}
	auto4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto4.dta":describe}

    {title:{help suest}}
	sysdsn4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sysdsn4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sysdsn4.dta":describe}
	income.dta{col 30}{stata "use http://www.stata-press.com/data/r13/income.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/income.dta":describe}

    {title:{help summarize}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}
	census.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census.dta":describe}

    {title:{help sunflower}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help sureg}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help sureg postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help swilk}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	cancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cancer.dta":describe}

    {title:{help symmetry}}
	iran.dta{col 30}{stata "use http://www.stata-press.com/data/r13/iran.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/iran.dta":describe}
	bd163.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bd163.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bd163.dta":describe}

{hline}

    {title:{help table}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}
	byssin.dta{col 30}{stata "use http://www.stata-press.com/data/r13/byssin.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/byssin.dta":describe}
	byssin1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/byssin1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/byssin1.dta":describe}

    {title:{help tabstat}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help tabulate oneway}}
	hiway.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiway.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiway.dta":describe}

    {title:{help tabulate twoway}}
	hiway2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiway2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiway2.dta":describe}
	citytemp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/citytemp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/citytemp2.dta":describe}
	dose.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dose.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dose.dta":describe}
	birthcat.dta{col 30}{stata "use http://www.stata-press.com/data/r13/birthcat.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/birthcat.dta":describe}

    {title:{help tabulate, summarize()}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help test}}
	census3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census3.dta":describe}
	census4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census4.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help testnl}}
	earnings.dta{col 30}{stata "use http://www.stata-press.com/data/r13/earnings.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/earnings.dta":describe}
	promotion.dta{col 30}{stata "use http://www.stata-press.com/data/r13/promotion.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/promotion.dta":describe}

    {title:{help tetrachoric}}
	familyvalues.dta{col 30}{stata "use http://www.stata-press.com/data/r13/familyvalues.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/familyvalues.dta":describe}

    {title:{help tnbreg}}
	medpar.dta{col 30}{stata "use http://www.stata-press.com/data/r13/medpar.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/medpar.dta":describe}
	medproviders.dta{col 30}{stata "use http://www.stata-press.com/data/r13/medproviders.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/medproviders.dta":describe}

    {title:{help tobit}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help tobit postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help total}}
	total.dta{col 30}{stata "use http://www.stata-press.com/data/r13/total.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/total.dta":describe}

    {title:{help total postestimation}}
	total.dta{col 30}{stata "use http://www.stata-press.com/data/r13/total.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/total.dta":describe}

    {title:{help tpoisson}}
	runshoes.dta{col 30}{stata "use http://www.stata-press.com/data/r13/runshoes.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/runshoes.dta":describe}
	probe.dta{col 30}{stata "use http://www.stata-press.com/data/r13/probe.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/probe.dta":describe}

    {title:{help truncreg}}
	laborsub.dta{col 30}{stata "use http://www.stata-press.com/data/r13/laborsub.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/laborsub.dta":describe}

    {title:{help ttest}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	fuel3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel3.dta":describe}
	fuel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel.dta":describe}
	census.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census.dta":describe}

{hline}

    {title:{help vce_option}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	clogitid.dta{col 30}{stata "use http://www.stata-press.com/data/r13/clogitid.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/clogitid.dta":describe}

    {title:{help vwls}}
	vwlsxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/vwlsxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/vwlsxmpl.dta":describe}
	bp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bp.dta":describe}

{hline}

    {title:{help zinb}}
	fish.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fish.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fish.dta":describe}

    {title:{help zip}}
	fish.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fish.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fish.dta":describe}

    {title:{help zip postestimation}}
	fish.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fish.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fish.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
