{smcl}
{* *! version 1.2.1  04jun2013}{...}
{bf:Datasets for Stata Data-Management Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:{help append}}
	odd1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/odd1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/odd1.dta":describe}
	even.dta{col 30}{stata "use http://www.stata-press.com/data/r13/even.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/even.dta":describe}
	odd.dta{col 30}{stata "use http://www.stata-press.com/data/r13/odd.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/odd.dta":describe}
	oddeven.dta{col 30}{stata "use http://www.stata-press.com/data/r13/oddeven.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/oddeven.dta":describe}
	capop.dta{col 30}{stata "use http://www.stata-press.com/data/r13/capop.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/capop.dta":describe}
	ilpop.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ilpop.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ilpop.dta":describe}
	txpop.dta{col 30}{stata "use http://www.stata-press.com/data/r13/txpop.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/txpop.dta":describe}

{hline}

    {title:{help bcal}}
	sp500.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sp500.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sp500.dta":describe}

    {title:{help by}}
	autornd.dta{col 30}{stata "use http://www.stata-press.com/data/r13/autornd.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/autornd.dta":describe}

{hline}

    {title:{help checksum}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help clear}}
	bpwide.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bpwide.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bpwide.dta":describe}

    {title:{help clonevar}}
	travel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/travel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/travel.dta":describe}

    {title:{help codebook}}
	educ3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/educ3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/educ3.dta":describe}
	citytemp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/citytemp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/citytemp.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	funnyvar.dta{col 30}{stata "use http://www.stata-press.com/data/r13/funnyvar.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/funnyvar.dta":describe}

    {title:{help collapse}}
	college.dta{col 30}{stata "use http://www.stata-press.com/data/r13/college.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/college.dta":describe}
	census5.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census5.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census5.dta":describe}

    {title:{help compare}}
	fullauto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fullauto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fullauto.dta":describe}

    {title:{help compress}}
	compxmp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/compxmp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/compxmp2.dta":describe}

    {title:{help contract}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}

    {title:{help corr2data}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help count}}
	countxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/countxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/countxmpl.dta":describe}

{hline}

    {title:{help describe}}
	states.dta{col 30}{stata "use http://www.stata-press.com/data/r13/states.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/states.dta":describe}
	census.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help destring}}
	destring1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/destring1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/destring1.dta":describe}
	destring2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/destring2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/destring2.dta":describe}
	tostring.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tostring.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tostring.dta":describe}

    {title:{help drop}}
	census11.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census11.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census11.dta":describe}

    {title:{help ds}}
	educ3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/educ3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/educ3.dta":describe}

    {title:{help duplicates}}
	dupxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dupxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dupxmpl.dta":describe}

{hline}

    {title:{help egen}}
	egenxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/egenxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/egenxmpl.dta":describe}
	egenxmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/egenxmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/egenxmpl2.dta":describe}
	egenxmpl3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/egenxmpl3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/egenxmpl3.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	states1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/states1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/states1.dta":describe}
	egenxmpl4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/egenxmpl4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/egenxmpl4.dta":describe}
	auto3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto3.dta":describe}
	egenxmpl5.dta{col 30}{stata "use http://www.stata-press.com/data/r13/egenxmpl5.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/egenxmpl5.dta":describe}
	egenxmpl6.dta{col 30}{stata "use http://www.stata-press.com/data/r13/egenxmpl6.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/egenxmpl6.dta":describe}

    {title:{help encode}}
	hbp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hbp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hbp2.dta":describe}
	hbp3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hbp3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hbp3.dta":describe}

    {title:{help expand}}
	expandxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/expandxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/expandxmpl.dta":describe}

    {title:{help expandcl}}
	expclxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/expclxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/expclxmpl.dta":describe}

{hline}

    {title:{help fillin}}
	fillin1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fillin1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fillin1.dta":describe}

    {title:{help format}}
	census10.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census10.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census10.dta":describe}
	fmtxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fmtxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fmtxmpl.dta":describe}
	fmtxmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fmtxmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fmtxmpl2.dta":describe}

{hline}

    {title:{help generate}}
	genxmpl1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/genxmpl1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/genxmpl1.dta":describe}
	genxmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/genxmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/genxmpl2.dta":describe}
	genxmpl3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/genxmpl3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/genxmpl3.dta":describe}
	genxmpl4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/genxmpl4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/genxmpl4.dta":describe}

    {title:{help gsort}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	bp3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bp3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bp3.dta":describe}

{hline}

    {title:{help icd9}}
	patients.dta{col 30}{stata "use http://www.stata-press.com/data/r13/patients.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/patients.dta":describe}

    {title:{help import delimited}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help import excel}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help inspect}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	bobsdata.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bobsdata.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bobsdata.dta":describe}
	census.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census.dta":describe}
	citytemp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/citytemp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/citytemp.dta":describe}

    {title:{help ipolate}}
	ipolxmpl1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ipolxmpl1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ipolxmpl1.dta":describe}
	ipolxmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ipolxmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ipolxmpl2.dta":describe}

    {title:{help isid}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	grunfeld.dta{col 30}{stata "use http://www.stata-press.com/data/r13/grunfeld.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/grunfeld.dta":describe}

{hline}

    {title:{help joinby}}
	child.dta{col 30}{stata "use http://www.stata-press.com/data/r13/child.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/child.dta":describe}
	parent.dta{col 30}{stata "use http://www.stata-press.com/data/r13/parent.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/parent.dta":describe}

{hline}

    {title:{help label}}
	hbp4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hbp4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hbp4.dta":describe}

    {title:{help labelbook}}
	labelbook1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/labelbook1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/labelbook1.dta":describe}
	labelbook2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/labelbook2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/labelbook2.dta":describe}
	data_in_trouble.dta{col 30}{stata "use http://www.stata-press.com/data/r13/data_in_trouble.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/data_in_trouble.dta":describe}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help list}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help lookfor}}
	nlswork.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlswork.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlswork.dta":describe}

{hline}

    {title:{help memory}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help merge}}
	autosize.dta{col 30}{stata "use http://www.stata-press.com/data/r13/autosize.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/autosize.dta":describe}
	autoexpense.dta{col 30}{stata "use http://www.stata-press.com/data/r13/autoexpense.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/autoexpense.dta":describe}
	sforce.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sforce.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sforce.dta":describe}
	dollars.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dollars.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dollars.dta":describe}
	overlap1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/overlap1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/overlap1.dta":describe}
	overlap2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/overlap2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/overlap2.dta":describe}

    {title:{help mvencode}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help notes}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help order}}
	auto4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto4.dta":describe}

    {title:{help outfile}}
	outfilexmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/outfilexmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/outfilexmpl.dta":describe}

{hline}

    {title:{help pctile}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	bp1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bp1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bp1.dta":describe}
	bp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bp2.dta":describe}

    {title:{help putmata}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

{hline}

    {title:{help recast}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help recode}}
	recodexmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/recodexmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/recodexmpl.dta":describe}

    {title:{help rename}}
	renamexmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/renamexmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/renamexmpl.dta":describe}

    {title:{help reshape}}
	reshape1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reshape1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reshape1.dta":describe}
	reshape2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reshape2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reshape2.dta":describe}
	reshapexp1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reshapexp1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reshapexp1.dta":describe}
	reshape6.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reshape6.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reshape6.dta":describe}
	reshape3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reshape3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reshape3.dta":describe}
	reshape4.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reshape4.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reshape4.dta":describe}
	reshapexp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reshapexp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reshapexp2.dta":describe}

{hline}

    {title:{help sample}}
	nlswork.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlswork.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlswork.dta":describe}

    {title:{help separate}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help snapshot}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help sort}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help stack}}
	stackxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stackxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stackxmpl.dta":describe}
	citytemp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/citytemp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/citytemp.dta":describe}

    {title:{help statsby}}
	auto2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto2.dta":describe}
	statsby.dta{col 30}{stata "use http://www.stata-press.com/data/r13/statsby.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/statsby.dta":describe}

    {title:{help sysuse}}
	lifeexp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lifeexp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lifeexp.dta":describe}

{hline}

    {title:{help use}}
	hiway.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiway.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiway.dta":describe}
	nlswork.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlswork.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlswork.dta":describe}
	union.dta{col 30}{stata "use http://www.stata-press.com/data/r13/union.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/union.dta":describe}

{hline}

    {title:{help webuse}}
	lifeexp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lifeexp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lifeexp.dta":describe}

{hline}

    {title:{help xpose}}
	xposexmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/xposexmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/xposexmpl.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
