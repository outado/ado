{smcl}
{* *! version 1.2.0  27mar2013}{...}
{bf:Datasets for Stata Graphics Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:Datasets}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	autornd.dta{col 30}{stata "use http://www.stata-press.com/data/r13/autornd.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/autornd.dta":describe}
	bplong.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bplong.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bplong.dta":describe}
	bpwide.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bpwide.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bpwide.dta":describe}
	cancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cancer.dta":describe}
	census.dta{col 30}{stata "use http://www.stata-press.com/data/r13/census.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/census.dta":describe}
	citytemp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/citytemp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/citytemp.dta":describe}
	educ99gdp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/educ99gdp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/educ99gdp.dta":describe}
	gnp96.dta{col 30}{stata "use http://www.stata-press.com/data/r13/gnp96.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/gnp96.dta":describe}
	lifeexp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lifeexp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lifeexp.dta":describe}
	network1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/network1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/network1.dta":describe}
	network1a.dta{col 30}{stata "use http://www.stata-press.com/data/r13/network1a.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/network1a.dta":describe}
	nlsw88.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlsw88.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlsw88.dta":describe}
	nlswide1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nlswide1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nlswide1.dta":describe}
	pop2000.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pop2000.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pop2000.dta":describe}
	sandstone.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sandstone.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sandstone.dta":describe}
	sp500.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sp500.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sp500.dta":describe}
	surface.dta{col 30}{stata "use http://www.stata-press.com/data/r13/surface.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/surface.dta":describe}
	uslifeexp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/uslifeexp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/uslifeexp.dta":describe}
	uslifeexp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/uslifeexp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/uslifeexp2.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
