{smcl}
{* *! version 1.0.2  27mar2013}{...}
{bf:Datasets for Stata Multilevel Mixed-Effects Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:{help me}}
	pig.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pig.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pig.dta":describe}
	clippings.dta{col 30}{stata "use http://www.stata-press.com/data/r13/clippings.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/clippings.dta":describe}
	admissions.dta{col 30}{stata "use http://www.stata-press.com/data/r13/admissions.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/admissions.dta":describe}
	bangladesh.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bangladesh.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bangladesh.dta":describe}
	diuretics.dta{col 30}{stata "use http://www.stata-press.com/data/r13/diuretics.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/diuretics.dta":describe}
	tvsfpors.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tvsfpors.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tvsfpors.dta":describe}

    {title:{help mecloglog}}
	union.dta{col 30}{stata "use http://www.stata-press.com/data/r13/union.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/union.dta":describe}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}

    {title:{help mecloglog postestimation}}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}

    {title:{help meglm}}
	pig.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pig.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pig.dta":describe}
	bangladesh.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bangladesh.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bangladesh.dta":describe}
	tvsfpors.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tvsfpors.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tvsfpors.dta":describe}
	salamander.dta{col 30}{stata "use http://www.stata-press.com/data/r13/salamander.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/salamander.dta":describe}

    {title:{help meglm postestimation}}
	bangladesh.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bangladesh.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bangladesh.dta":describe}
	tvsfpors.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tvsfpors.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tvsfpors.dta":describe}

    {title:{help melogit}}
	bangladesh.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bangladesh.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bangladesh.dta":describe}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}

    {title:{help melogit postestimation}}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}

    {title:{help menbreg}}
	drvisits.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drvisits.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drvisits.dta":describe}
	melanoma.dta{col 30}{stata "use http://www.stata-press.com/data/r13/melanoma.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/melanoma.dta":describe}

    {title:{help menbreg postestimation}}
	melanoma.dta{col 30}{stata "use http://www.stata-press.com/data/r13/melanoma.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/melanoma.dta":describe}

    {title:{help meologit}}
	tvsfpors.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tvsfpors.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tvsfpors.dta":describe}

    {title:{help meologit postestimation}}
	tvsfpors.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tvsfpors.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tvsfpors.dta":describe}

    {title:{help meoprobit}}
	tvsfpors.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tvsfpors.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tvsfpors.dta":describe}

    {title:{help meoprobit postestimation}}
	tvsfpors.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tvsfpors.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tvsfpors.dta":describe}

    {title:{help mepoisson}}
	epilepsy.dta{col 30}{stata "use http://www.stata-press.com/data/r13/epilepsy.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/epilepsy.dta":describe}
	melanoma.dta{col 30}{stata "use http://www.stata-press.com/data/r13/melanoma.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/melanoma.dta":describe}

    {title:{help mepoisson postestimation}}
	melanoma.dta{col 30}{stata "use http://www.stata-press.com/data/r13/melanoma.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/melanoma.dta":describe}

    {title:{help meprobit}}
	bangladesh.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bangladesh.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bangladesh.dta":describe}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}

    {title:{help meprobit postestimation}}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}

    {title:{help meqrlogit}}
	bangladesh.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bangladesh.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bangladesh.dta":describe}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}
	fifeschool.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fifeschool.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fifeschool.dta":describe}

    {title:{help meqrlogit postestimation}}
	toenail.dta{col 30}{stata "use http://www.stata-press.com/data/r13/toenail.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/toenail.dta":describe}
	bangladesh.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bangladesh.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bangladesh.dta":describe}
	towerlondon.dta{col 30}{stata "use http://www.stata-press.com/data/r13/towerlondon.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/towerlondon.dta":describe}

    {title:{help meqrpoisson}}
	epilepsy.dta{col 30}{stata "use http://www.stata-press.com/data/r13/epilepsy.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/epilepsy.dta":describe}
	melanoma.dta{col 30}{stata "use http://www.stata-press.com/data/r13/melanoma.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/melanoma.dta":describe}

    {title:{help meqrpoisson postestimation}}
	epilepsy.dta{col 30}{stata "use http://www.stata-press.com/data/r13/epilepsy.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/epilepsy.dta":describe}

    {title:{help mixed}}
	pig.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pig.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pig.dta":describe}
	productivity.dta{col 30}{stata "use http://www.stata-press.com/data/r13/productivity.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/productivity.dta":describe}
	childweight.dta{col 30}{stata "use http://www.stata-press.com/data/r13/childweight.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/childweight.dta":describe}
	veneer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/veneer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/veneer.dta":describe}
	ovary.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ovary.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ovary.dta":describe}
	exercise.dta{col 30}{stata "use http://www.stata-press.com/data/r13/exercise.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/exercise.dta":describe}
	pisa2000.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pisa2000.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pisa2000.dta":describe}

    {title:{help mixed postestimation}}
	pig.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pig.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pig.dta":describe}
	pefrate.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pefrate.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pefrate.dta":describe}
	productivity.dta{col 30}{stata "use http://www.stata-press.com/data/r13/productivity.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/productivity.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
