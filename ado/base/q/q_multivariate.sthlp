{smcl}
{* *! version 1.2.1  01apr2013}{...}
{bf:Datasets for Stata Multivariate Statistics Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:{help alpha}}
	automiss.dta{col 30}{stata "use http://www.stata-press.com/data/r13/automiss.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/automiss.dta":describe}

{hline}

    {title:{help biplot}}
	renpainters.dta{col 30}{stata "use http://www.stata-press.com/data/r13/renpainters.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/renpainters.dta":describe}

{hline}

    {title:{help ca}}
	ca_smoking.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ca_smoking.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ca_smoking.dta":describe}
	issp93.dta{col 30}{stata "use http://www.stata-press.com/data/r13/issp93.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/issp93.dta":describe}

    {title:{help ca postestimation}}
	ca_smoking.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ca_smoking.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ca_smoking.dta":describe}

    {title:{help ca postestimation plots}}
	ca_smoking.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ca_smoking.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ca_smoking.dta":describe}

    {title:{help candisc}}
	head.dta{col 30}{stata "use http://www.stata-press.com/data/r13/head.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/head.dta":describe}

    {title:{help canon}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help canon postestimation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help cluster dendrogram}}
	labtech.dta{col 30}{stata "use http://www.stata-press.com/data/r13/labtech.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/labtech.dta":describe}

    {title:{help cluster generate}}
	labtech.dta{col 30}{stata "use http://www.stata-press.com/data/r13/labtech.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/labtech.dta":describe}
	homework.dta{col 30}{stata "use http://www.stata-press.com/data/r13/homework.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/homework.dta":describe}

    {title:{help cluster kmeans and kmedians}}
	physed.dta{col 30}{stata "use http://www.stata-press.com/data/r13/physed.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/physed.dta":describe}
	wclub.dta{col 30}{stata "use http://www.stata-press.com/data/r13/wclub.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/wclub.dta":describe}

    {title:{help cluster linkage}}
	labtech.dta{col 30}{stata "use http://www.stata-press.com/data/r13/labtech.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/labtech.dta":describe}
	homework.dta{col 30}{stata "use http://www.stata-press.com/data/r13/homework.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/homework.dta":describe}
	wclub.dta{col 30}{stata "use http://www.stata-press.com/data/r13/wclub.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/wclub.dta":describe}

    {title:{help cluster programming subroutines}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	homework.dta{col 30}{stata "use http://www.stata-press.com/data/r13/homework.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/homework.dta":describe}

    {title:{help cluster programming utilities}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}
	clprogxmpl1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/clprogxmpl1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/clprogxmpl1.dta":describe}
	clprogxmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/clprogxmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/clprogxmpl2.dta":describe}

    {title:{help cluster stop}}
	physed.dta{col 30}{stata "use http://www.stata-press.com/data/r13/physed.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/physed.dta":describe}

    {title:{help clustermat}}
	iris.dta{col 30}{stata "use http://www.stata-press.com/data/r13/iris.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/iris.dta":describe}
	wclub.dta{col 30}{stata "use http://www.stata-press.com/data/r13/wclub.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/wclub.dta":describe}

{hline}

    {title:{help discrim}}
	lawnmower2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lawnmower2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lawnmower2.dta":describe}

    {title:{help discrim estat}}
	iris.dta{col 30}{stata "use http://www.stata-press.com/data/r13/iris.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/iris.dta":describe}
	lawnmower2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lawnmower2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lawnmower2.dta":describe}
	head.dta{col 30}{stata "use http://www.stata-press.com/data/r13/head.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/head.dta":describe}

    {title:{help discrim knn}}
	head.dta{col 30}{stata "use http://www.stata-press.com/data/r13/head.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/head.dta":describe}
	mushroom.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mushroom.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mushroom.dta":describe}

    {title:{help discrim knn postestimation}}
	circlegrid.dta{col 30}{stata "use http://www.stata-press.com/data/r13/circlegrid.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/circlegrid.dta":describe}

    {title:{help discrim lda}}
	twogroup.dta{col 30}{stata "use http://www.stata-press.com/data/r13/twogroup.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/twogroup.dta":describe}
	threegroup.dta{col 30}{stata "use http://www.stata-press.com/data/r13/threegroup.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/threegroup.dta":describe}
	iris.dta{col 30}{stata "use http://www.stata-press.com/data/r13/iris.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/iris.dta":describe}

    {title:{help discrim lda postestimation}}
	rootstock.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rootstock.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rootstock.dta":describe}
	iris.dta{col 30}{stata "use http://www.stata-press.com/data/r13/iris.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/iris.dta":describe}
	lawnmower2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lawnmower2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lawnmower2.dta":describe}

    {title:{help discrim logistic}}
	senile.dta{col 30}{stata "use http://www.stata-press.com/data/r13/senile.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/senile.dta":describe}
	head.dta{col 30}{stata "use http://www.stata-press.com/data/r13/head.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/head.dta":describe}

    {title:{help discrim logistic postestimation}}
	senile.dta{col 30}{stata "use http://www.stata-press.com/data/r13/senile.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/senile.dta":describe}

    {title:{help discrim qda}}
	rootstock.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rootstock.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rootstock.dta":describe}

    {title:{help discrim qda postestimation}}
	skulls.dta{col 30}{stata "use http://www.stata-press.com/data/r13/skulls.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/skulls.dta":describe}
	rootstock.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rootstock.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rootstock.dta":describe}

{hline}

    {title:{help factor}}
	bg2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bg2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bg2.dta":describe}

    {title:{help factor postestimation}}
	bg2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bg2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bg2.dta":describe}
	sp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sp2.dta":describe}
	autofull.dta{col 30}{stata "use http://www.stata-press.com/data/r13/autofull.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/autofull.dta":describe}

{hline}

    {title:{help hotelling}}
	gasexp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/gasexp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/gasexp.dta":describe}
	gasexp2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/gasexp2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/gasexp2.dta":describe}

{hline}

    {title:{help manova}}
	rootstock.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rootstock.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rootstock.dta":describe}
	metabolic.dta{col 30}{stata "use http://www.stata-press.com/data/r13/metabolic.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/metabolic.dta":describe}
	jaw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/jaw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/jaw.dta":describe}
	fabric.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fabric.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fabric.dta":describe}
	biochemical.dta{col 30}{stata "use http://www.stata-press.com/data/r13/biochemical.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/biochemical.dta":describe}
	solardistance.dta{col 30}{stata "use http://www.stata-press.com/data/r13/solardistance.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/solardistance.dta":describe}
	videotrainer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/videotrainer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/videotrainer.dta":describe}
	reading2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/reading2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/reading2.dta":describe}
	nobetween.dta{col 30}{stata "use http://www.stata-press.com/data/r13/nobetween.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/nobetween.dta":describe}
	sorghum.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sorghum.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sorghum.dta":describe}
	table614.dta{col 30}{stata "use http://www.stata-press.com/data/r13/table614.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/table614.dta":describe}

    {title:{help manova postestimation}}
	rootstock.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rootstock.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rootstock.dta":describe}
	metabolic.dta{col 30}{stata "use http://www.stata-press.com/data/r13/metabolic.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/metabolic.dta":describe}
	jaw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/jaw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/jaw.dta":describe}
	videotrainer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/videotrainer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/videotrainer.dta":describe}

    {title:{help matrix dissimilarity}}
	labtech.dta{col 30}{stata "use http://www.stata-press.com/data/r13/labtech.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/labtech.dta":describe}
	homework.dta{col 30}{stata "use http://www.stata-press.com/data/r13/homework.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/homework.dta":describe}
	gower.dta{col 30}{stata "use http://www.stata-press.com/data/r13/gower.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/gower.dta":describe}

    {title:{help mca}}
	issp93.dta{col 30}{stata "use http://www.stata-press.com/data/r13/issp93.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/issp93.dta":describe}

    {title:{help mca postestimation}}
	issp93.dta{col 30}{stata "use http://www.stata-press.com/data/r13/issp93.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/issp93.dta":describe}

    {title:{help mca postestimation plots}}
	issp93.dta{col 30}{stata "use http://www.stata-press.com/data/r13/issp93.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/issp93.dta":describe}

    {title:{help mds}}
	cerealnut.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cerealnut.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cerealnut.dta":describe}
	mvstatsbooks.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mvstatsbooks.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mvstatsbooks.dta":describe}

    {title:{help mds postestimation}}
	morse_long.dta{col 30}{stata "use http://www.stata-press.com/data/r13/morse_long.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/morse_long.dta":describe}

    {title:{help mds postestimation plots}}
	morse_long.dta{col 30}{stata "use http://www.stata-press.com/data/r13/morse_long.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/morse_long.dta":describe}

    {title:{help mdslong}}
	morse_long.dta{col 30}{stata "use http://www.stata-press.com/data/r13/morse_long.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/morse_long.dta":describe}
	morse_wide.dta{col 30}{stata "use http://www.stata-press.com/data/r13/morse_wide.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/morse_wide.dta":describe}

    {title:{help mvreg}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help mvtest correlations}}
	milktruck.dta{col 30}{stata "use http://www.stata-press.com/data/r13/milktruck.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/milktruck.dta":describe}
	genderpsych.dta{col 30}{stata "use http://www.stata-press.com/data/r13/genderpsych.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/genderpsych.dta":describe}

    {title:{help mvtest covariances}}
	milktruck.dta{col 30}{stata "use http://www.stata-press.com/data/r13/milktruck.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/milktruck.dta":describe}
	genderpsych.dta{col 30}{stata "use http://www.stata-press.com/data/r13/genderpsych.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/genderpsych.dta":describe}

    {title:{help mvtest means}}
	milktruck.dta{col 30}{stata "use http://www.stata-press.com/data/r13/milktruck.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/milktruck.dta":describe}
	turnip.dta{col 30}{stata "use http://www.stata-press.com/data/r13/turnip.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/turnip.dta":describe}
	metabolic.dta{col 30}{stata "use http://www.stata-press.com/data/r13/metabolic.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/metabolic.dta":describe}

    {title:{help mvtest normality}}
	iris.dta{col 30}{stata "use http://www.stata-press.com/data/r13/iris.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/iris.dta":describe}

{hline}

    {title:{help pca}}
	audiometric.dta{col 30}{stata "use http://www.stata-press.com/data/r13/audiometric.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/audiometric.dta":describe}

    {title:{help pca postestimation}}
	audiometric.dta{col 30}{stata "use http://www.stata-press.com/data/r13/audiometric.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/audiometric.dta":describe}

    {title:{help procrustes}}
	speed_survey.dta{col 30}{stata "use http://www.stata-press.com/data/r13/speed_survey.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/speed_survey.dta":describe}

    {title:{help procrustes postestimation}}
	speed_survey.dta{col 30}{stata "use http://www.stata-press.com/data/r13/speed_survey.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/speed_survey.dta":describe}

{hline}

    {title:{help scoreplot}}
	renpainters.dta{col 30}{stata "use http://www.stata-press.com/data/r13/renpainters.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/renpainters.dta":describe}

    {title:{help screeplot}}
	emd.dta{col 30}{stata "use http://www.stata-press.com/data/r13/emd.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/emd.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
