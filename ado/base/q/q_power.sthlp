{smcl}
{* *! version 1.0.0  25may2013}{...}
{bf:Datasets for Stata Power and Sample-Size Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:{help power onemean}}
	sat.dta{col 30}{stata "use http://www.stata-press.com/data/r13/sat.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/sat.dta":describe}

    {title:{help power pairedmeans}}
	bpwide.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bpwide.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bpwide.dta":describe}

    {title:{help power oneproportion}}
	lbw.dta{col 30}{stata "use http://www.stata-press.com/data/r13/lbw.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/lbw.dta":describe}

    {title:{help power onevariance}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help power twovariances}}
	fuel.dta{col 30}{stata "use http://www.stata-press.com/data/r13/fuel.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/fuel.dta":describe}

    {title:{help power onecorrelation}}
	auto.dta{col 30}{stata "use http://www.stata-press.com/data/r13/auto.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/auto.dta":describe}

    {title:{help power twocorrelations}}
	genderpsych.dta{col 30}{stata "use http://www.stata-press.com/data/r13/genderpsych.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/genderpsych.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
