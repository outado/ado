{smcl}
{* *! version 1.2.2  04jun2013}{...}
{bf:Datasets for Stata Survival Analysis and Epidemiological Tables Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:{help ctset}}
	ctset1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ctset1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ctset1.dta":describe}
	ctset2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ctset2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ctset2.dta":describe}
	ctset3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ctset3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ctset3.dta":describe}

    {title:{help cttost}}
	cttost.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cttost.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cttost.dta":describe}

{hline}

    {title:{help epitab}}
	irxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/irxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/irxmpl.dta":describe}
	irxmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/irxmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/irxmpl2.dta":describe}
	irxmpl3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/irxmpl3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/irxmpl3.dta":describe}
	rm.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rm.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rm.dta":describe}
	dollhill3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/dollhill3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/dollhill3.dta":describe}
	csxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/csxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/csxmpl.dta":describe}
	ugdp.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ugdp.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ugdp.dta":describe}
	ccxmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ccxmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ccxmpl.dta":describe}
	downs.dta{col 30}{stata "use http://www.stata-press.com/data/r13/downs.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/downs.dta":describe}
	bdesop.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bdesop.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bdesop.dta":describe}
	bdendo11.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bdendo11.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bdendo11.dta":describe}
	bdendo.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bdendo.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bdendo.dta":describe}

{hline}

    {title:{help ltable}}
	rat.dta{col 30}{stata "use http://www.stata-press.com/data/r13/rat.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/rat.dta":describe}
	selvin.dta{col 30}{stata "use http://www.stata-press.com/data/r13/selvin.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/selvin.dta":describe}
	tumor.dta{col 30}{stata "use http://www.stata-press.com/data/r13/tumor.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/tumor.dta":describe}

{hline}

    {title:{help stbase}}
	mfail.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mfail.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mfail.dta":describe}
	stbasexmpl.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stbasexmpl.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stbasexmpl.dta":describe}
	stbasexmpl2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stbasexmpl2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stbasexmpl2.dta":describe}

    {title:{help stci}}
	page2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/page2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/page2.dta":describe}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	mfail2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mfail2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mfail2.dta":describe}

    {title:{help stcox}}
	kva.dta{col 30}{stata "use http://www.stata-press.com/data/r13/kva.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/kva.dta":describe}
	drugtr.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drugtr.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drugtr.dta":describe}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	drugtr2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drugtr2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drugtr2.dta":describe}
	mfail.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mfail.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mfail.dta":describe}
	catheter.dta{col 30}{stata "use http://www.stata-press.com/data/r13/catheter.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/catheter.dta":describe}

    {title:{help stcox diagnostics}}
	leukemia.dta{col 30}{stata "use http://www.stata-press.com/data/r13/leukemia.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/leukemia.dta":describe}

    {title:{help stcox postestimation}}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	drugtr.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drugtr.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drugtr.dta":describe}
	catheter.dta{col 30}{stata "use http://www.stata-press.com/data/r13/catheter.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/catheter.dta":describe}

    {title:{help stcrreg}}
	hypoxia.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hypoxia.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hypoxia.dta":describe}
	udca.dta{col 30}{stata "use http://www.stata-press.com/data/r13/udca.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/udca.dta":describe}
	hiv_si.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiv_si.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiv_si.dta":describe}
	pneumonia.dta{col 30}{stata "use http://www.stata-press.com/data/r13/pneumonia.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/pneumonia.dta":describe}

    {title:{help stcrreg postestimation}}
	hypoxia.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hypoxia.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hypoxia.dta":describe}
	hiv_si.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hiv_si.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hiv_si.dta":describe}

    {title:{help stcurve}}
	drugtr.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drugtr.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drugtr.dta":describe}
	cancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cancer.dta":describe}
	catheter.dta{col 30}{stata "use http://www.stata-press.com/data/r13/catheter.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/catheter.dta":describe}
	hypoxia.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hypoxia.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hypoxia.dta":describe}

    {title:{help stdescribe}}
	page2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/page2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/page2.dta":describe}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	mfail2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mfail2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mfail2.dta":describe}

    {title:{help stfill}}
	mrecord.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mrecord.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mrecord.dta":describe}

    {title:{help stgen}}
	mrmf.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mrmf.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mrmf.dta":describe}

    {title:{help stir}}
	page2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/page2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/page2.dta":describe}

    {title:{help stpower cox}}
	myeloma.dta{col 30}{stata "use http://www.stata-press.com/data/r13/myeloma.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/myeloma.dta":describe}

    {title:{help stpower logrank}}
	drug.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drug.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drug.dta":describe}

    {title:{help stptime}}
	stptime.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stptime.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stptime.dta":describe}
	diet.dta{col 30}{stata "use http://www.stata-press.com/data/r13/diet.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/diet.dta":describe}
	smrchd.dta{col 30}{stata "use http://www.stata-press.com/data/r13/smrchd.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/smrchd.dta":describe}

    {title:{help strate}}
	diet.dta{col 30}{stata "use http://www.stata-press.com/data/r13/diet.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/diet.dta":describe}
	smrchd.dta{col 30}{stata "use http://www.stata-press.com/data/r13/smrchd.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/smrchd.dta":describe}

    {title:{help streg}}
	kva.dta{col 30}{stata "use http://www.stata-press.com/data/r13/kva.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/kva.dta":describe}
	mfail3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mfail3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mfail3.dta":describe}
	cancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cancer.dta":describe}
	hip3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/hip3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/hip3.dta":describe}
	bc.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bc.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bc.dta":describe}
	catheter.dta{col 30}{stata "use http://www.stata-press.com/data/r13/catheter.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/catheter.dta":describe}

    {title:{help streg postestimation}}
	kva.dta{col 30}{stata "use http://www.stata-press.com/data/r13/kva.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/kva.dta":describe}
	cancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cancer.dta":describe}

    {title:{help sts}}
	drugtr.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drugtr.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drugtr.dta":describe}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	drug2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drug2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drug2.dta":describe}

    {title:{help sts graph}}
	drug2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drug2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drug2.dta":describe}
	drug2b.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drug2b.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drug2b.dta":describe}
	drug2c.dta{col 30}{stata "use http://www.stata-press.com/data/r13/drug2c.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/drug2c.dta":describe}

    {title:{help sts list}}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}

    {title:{help sts test}}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	marubini.dta{col 30}{stata "use http://www.stata-press.com/data/r13/marubini.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/marubini.dta":describe}

    {title:{help stset}}
	kva.dta{col 30}{stata "use http://www.stata-press.com/data/r13/kva.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/kva.dta":describe}
	kva2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/kva2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/kva2.dta":describe}
	stan2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan2.dta":describe}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}

    {title:{help stsplit}}
	diet.dta{col 30}{stata "use http://www.stata-press.com/data/r13/diet.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/diet.dta":describe}
	stanford.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stanford.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stanford.dta":describe}
	ocancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/ocancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/ocancer.dta":describe}
	cancer.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cancer.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cancer.dta":describe}

    {title:{help stsum}}
	page2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/page2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/page2.dta":describe}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	mfail2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/mfail2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/mfail2.dta":describe}

    {title:{help sttocc}}
	diet.dta{col 30}{stata "use http://www.stata-press.com/data/r13/diet.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/diet.dta":describe}

    {title:{help stvary}}
	stan3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stan3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stan3.dta":describe}
	stvaryex.dta{col 30}{stata "use http://www.stata-press.com/data/r13/stvaryex.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/stvaryex.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
