{smcl}
{* *! version 1.0.2  06jun2013}{...}
{bf:Datasets for Stata Treatment-Effects Reference Manual, Release 13}
{hline}
{p 4 4 2}
Datasets used in the Stata Documentation were selected to demonstrate
 the use of Stata.  Datasets were sometimes altered so that a particular
 feature could be explained.  Do not use these datasets for
 analysis purposes.
{p_end}
{hline}

    {title:{help etpoisson}}
	trip1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/trip1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/trip1.dta":describe}

    {title:{help etpoisson postestimation}}
	trip1.dta{col 30}{stata "use http://www.stata-press.com/data/r13/trip1.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/trip1.dta":describe}

    {title:{help etregress}}
	union3.dta{col 30}{stata "use http://www.stata-press.com/data/r13/union3.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/union3.dta":describe}

{hline}

    {title:{help teffects intro}}
	bweightex.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bweightex.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bweightex.dta":describe}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

    {title:{help teffects aipw}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

    {title:{help teffects ipw}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

    {title:{help teffects ipwra}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

    {cmd:teffects multivalue}
	bdsianesi5.dta{col 30}{stata "use http://www.stata-press.com/data/r13/bdsianesi5.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/bdsianesi5.dta":describe}

    {title:{help teffects nnmatch}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

    {title:{help teffects overlap}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}
	systolic2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/systolic2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/systolic2.dta":describe}

    {title:{help teffects postestimation}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

    {title:{help teffects psmatch}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

    {title:{help teffects ra}}
	cattaneo2.dta{col 30}{stata "use http://www.stata-press.com/data/r13/cattaneo2.dta":use} | {stata "describe using  http://www.stata-press.com/data/r13/cattaneo2.dta":describe}

{hline}

{p 4 4 2}
StataCorp gratefully acknowledges that some datasets in the Reference
 Manuals are proprietary and have been used in our printed documentation
  with the express permission of the copyright holders. If any copyright
 holder believes that by making these datasets available to the public,
 StataCorp is in violation of the letter or spirit of any such agreement,
 please contact {browse "mailto:tech-support@stata.com":tech-support@stata.com}
 and any such materials will be removed from this webpage.
{p_end}
