{smcl}
{* *! version 3.0.0  10aug2012}{...}
{vieweralsosee "[D] functions" "mansection D functions"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[D] drawnorm" "help drawnorm"}{...}
{vieweralsosee "[D] egen" "help egen"}{...}
{viewerjumpto "Description" "random_number_functions##description"}{...}
{viewerjumpto "Random-number functions" "random_number_functions##functions"}{...}
{viewerjumpto "References" "random_number_functions##references"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col :{manlink D functions} {hline 2}}Functions{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}This is a quick reference on functions for generating pseudorandom 
variates.  For help on all functions, see {manhelp functions D}.  See
{manhelp set_seed R:set seed} for setting the random-number seed.


{marker functions}{...}
{title:Random-number functions}

INCLUDE help f_runiform

INCLUDE help f_rbeta

INCLUDE help f_rbinomial

INCLUDE help f_rchi2

INCLUDE help f_rgamma

INCLUDE help f_rhypergeometric

INCLUDE help f_rnbinomial

INCLUDE help f_rnormal

INCLUDE help f_rpoisson

INCLUDE help f_rt


{marker references}{...}
{title:References}

{marker AD1974}{...}
{phang}
Ahrens, J. H., and U. Dieter.  1974.  Computer methods for sampling from gamma,
beta, Poisson, and binomial distributions.  {it:Computing} 12: 223-246.

{marker AW1976}{...}
{phang}
Atkinson, A. C., and J. Whittaker.  1976.  A switching algorithm for the 
generation of beta random variables with at least one parameter less
than 1.  {it:Journal of the Royal Statistical Society, Series A}
139: 462-467.

{marker AW1970}{...}
{phang}
------. 1970. Algorithm AS 134: The generation of beta random
variables with one parameter greater than and one parameter less 
than 1.  {it:Applied Statistics} 28: 90-93.

{marker B1983}{...}
{phang}
Best, D. J.  1983.  A note on gamma variate generators with shape parameters
less than unity.  {it:Computing} 30: 185-188.

{marker D1986}{...}
{phang}
Devroye, L.  1986.  {it:Non-uniform Random Variate Generation}.
New York: Springer.

{marker G2003}{...}
{phang}
Gentle, J. E.  2003.  {it:Random Number Generation and Monte Carlo Methods}. 
2nd ed.  New York: Springer.

{marker K1982}{...}
{phang}
Kachitvichyanukul, V.  1982.
Computer Generation of Poisson, Binomial, and Hypergeometric Random Variables.
PhD thesis, Purdue University.

{marker KS1985}{...}
{phang}
Kachitvichyanukul, V., and B. Schmeiser.  1985.  Computer generation
of hypergeometric random variates.
{it:Journal of Statistical Computation and Simulation} 22: 127-145.

{marker KS1988}{...}
{phang}
------.  1988.  Binomial random variate generation.
{it:Communications of the Association for Computing Machinery} 31: 216-222.

{marker K1986}{...}
{phang} 
Kemp, C. D.  1986.  A modal method for generating binomial variates.  
{it:Communications in Statistics: Theory and Methods} 15: 805-813.

{marker KK1990}{...}
{phang} 
Kemp, A. W., and C. D. Kemp.  1990.  A composition-search algorithm for 
low-parameter Poisson generation.
{it:Journal of Statistical Computation and Simulation} 35: 239-244.

{marker KK1991}{...}
{phang} 
Kemp, C. D., and A. W. Kemp.  1991.  Poisson random variate generation. 
{it:Applied Statistics} 40: 143-158.

{marker KM1977}{...}
{phang}
Kinderman, A. J., and J. F. Monahan.  1977.  Computer generation of random
variables using the ratio of uniform deviates.
{it:Association for Computing Machinery Transactions on Mathematical Software}
3: 257-260.

{marker KM1980}{...}
{phang}
------.  1980.  New methods for generating
Student's t and gamma variables.  {it:Computing} 25: 369-377.

{marker K1998}{...}
{phang} 
Knuth, D.  1998.
{it:The Art of Computer Programming, Volume 2: Seminumerical Algorithms}.
3rd ed.  Reading, MA: Addison Wesley.

{marker MMB1964}{...}
{phang} 
Marsaglia, G., M. D. MacLaren, and T. A. Bray.  1964.  A fast procedure for
generating normal random variables.  {it:Communications of the ACM} 7: 4-10.

{marker SB1980}{...}
{phang}
Schmeiser, B. W., and A. J. G. Babu.  1980.  Beta variate generation via 
exponential majorizing functions.  {it:Operations Research} 28: 917-926.

{marker SL1980}{...}
{phang}
Schmeiser, B. W., and R. Lal. 1980.
Squeeze methods for generating gamma variates.
{it:Journal of the American Statistical Association} 75: 679-682.

{marker W1977}{...}
{phang}
Walker, A. J.  1977.  An efficient method for generating discrete random
variables with general distributions.
{it:ACM Transactions on Mathematical Software} 3: 253-256.
{p_end}
