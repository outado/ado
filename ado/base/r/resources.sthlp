{smcl}
{* *! version 1.1.0  22apr2014}{...}
{vieweralsosee "[R] help" "help help"}{...}
{vieweralsosee "[R] net" "help net"}{...}
{vieweralsosee "net_mnu" "net_mnu"}{...}
{vieweralsosee "[R] news" "help news"}{...}
{vieweralsosee "[R] search" "help search"}{...}
{vieweralsosee "searchadvice" "searchadvice"}{...}
{vieweralsosee "[R] update" "help update"}{...}
{vieweralsosee "[R] view" "help view"}{...}
{title:Resources for learning more about Stata}

{phang}Search{break}
       Select {bf:Help} from the Stata menu, click on {bf:Search...}, and
       enter a topic for which you would like information.

{phang}Help files{break}
       Use {sdialog:search} to search for help files.  Or select {bf:Help}
       from the Stata menu, click on {bf:Stata Command...}, and type the name
       of a command.  Or just type {cmd:help} {it:command}.  

{pmore}
       To see an outline of Stata's help files, select {bf:Help} from the
       Stata menu and click on {bf:Contents}.

{pmore}
       Click on the blue title at the top of any help file to access the full
       PDF documentation for that entry, including an overview of the subject
       and worked examples with discussion.

{phang}PDF documentation{break}
       Select {bf:Help} from the Stata menu and click on {bf:PDF}
       {bf:Documentation}.  A complete subject table of contents to the
       documentation will appear, as well as bookmarks to all the manuals.

{phang}{browse "http://www.youtube.com/user/statacorp":Video tutorials on using Stata}
{break}
       Watch short demonstrations on using Stata on our official YouTube
       channel.

{phang}{browse "http://www.stata.com/support/faqs":Stata FAQs}{break}
       Our website is filled with commonly asked questions and their answers.

{phang}{browse "http://www.statalist.org/":Statalist}{break}
       Join a forum dedicated to Stata where thousands
       of Stata users talk about Stata and statistics.  Register and
       participate, or simply lurk and read the discussions.

{phang}{browse "http://blog.stata.com":Not Elsewhere Classified}{break}
       Visit the official Stata blog for articles on Stata written by
       the people that write the software.

{phang}{browse "http://www.stata-press.com":Stata Press books}{break}
       Written by Stata users for Stata users. Stata Press publishes a
       variety of books about Stata statistical software and statistics
       in general.

{phang}{browse "http://www.stata.com/bookstore":Books about statistics and Stata}{break} 
       StataCorp offers non-Stata Press books at our online bookstore. 

{phang}{browse "http://www.stata.com/training":Training}{break}
       StataCorp offers public training sessions, on-site training, and
       over-the-web NetCourses.

{phang}{browse "http://www.stata.com/meeting":Conferences and meetings}{break}
       See in-depth presentations from experienced Stata users and
       StataCorp experts.  Network with other Stata users and developers
       from StataCorp.

{phang}User-written programs{break}
       Stata users create thousands of freely downloadable additions
       to Stata.  Simply use {sdialog:search} to look for additions.

{phang}{browse "http://www.stata-journal.com":Stata Journal}{break}
       Stata Press publishes a peer-reviewed quarterly journal packed
       with articles about statistics and Stata.  Stata programs
       discussed in the Stata Journal may be downloaded by selecting
       {bf:Help} from the Stata menu and clicking on
       {bf:SJ and User-written Programs}.

{phang}{browse "http://www.stata.com/stata-news":Stata News}{break}
       Keep up with the latest Stata happenings.

{phang}Social media{break}
       Connect with us on
          {browse "http://www.facebook.com/StataCorp":Facebook},
          {browse "http://twitter.com/stata":Twitter},
          {browse "http://www.google.com/+stata":Google+}, and
          {browse "http://www.linkedin.com/company/statacorp":LinkedIn}.

{phang}Other resources{break}
       There are many other resources, both official and unofficial,
       to help you as you use Stata, including tutorials, training,
       books, online discussion areas, tutorial videos, blogs, and more.
       Visit Stata's resources webpage:
       {browse "http://www.stata.com/links":http://www.stata.com/links}.
{p_end}
