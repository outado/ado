*! version 3.0.3  25apr2013
program define saveold
	version 13
	local 0 `"using `0'"'
	syntax using/ [, noLabel REPLACE ALL INTERCOOLED ]
	if "`replace'"=="" {
		if (strpos(`"`using'"',".") == 0 & `"`using'"' != "") {
			confirm new file `"`using'.dta"'
		}
		else {
			confirm new file `"`using'"'
		}
	}
	if ("`intercooled'"!="") {
		di as txt // 
		"(option {bf:intercooled} irrelevant in modern Statas; ignored)"
	}
	* preserve 
	foreach var of varlist _all {
		local type : type `var'
		local bad 0
		if `"`type'"' == "strL" {
			local bad 1
		}
		else if substr(`"`type'"',1,3) == "str" {
		    if real(substr(`"`type'"',4,.)) > 244 {
			local bad 1
		    }
		}
		if `bad' {
			di as err					///
			"data cannot be saved in old format"
			di as err					///
			"{p 4 4 2}"					///
			"Data contain {bf:strL} or {bf:str}{it:#}, "	///
			"{it:#}>244, and prior releases of Stata would " ///
			"not know how to process these variables.  "	///
			"Either {bf:drop} the variables or use "	///
			"{bf:recast} with the {bf:force} option "	///
			"to change them to {bf:str244}. "		///
			"{p_end}"
			exit 459
		}
	}
	save `"`using'"', oldformat `label' `replace' `all' `intercooled'
end
