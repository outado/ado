{smcl}
{* *! version 1.0.1  20may2013}{...}
{vieweralsosee "[SEM]" "mansection SEM"}{...}
{title:Title}

{p2colset 5 25 27 2}{...}
{p2col:{bf:sem introduction }{hline 2}}Introduction to sem{p_end}
{p2colreset}{...}


{title:Manual entries}

{synoptset 15}{...}
{synopt :{manlink SEM intro 1}}Introduction{p_end}
{synopt :{manlink SEM intro 2}}Learning the language: Path diagrams and command language{p_end}
{synopt :{manlink SEM intro 3}}Learning the language: Factor-variable notation (gsem only){p_end}
{synopt :{manlink SEM intro 4}}Substantive concepts{p_end}
{synopt :{manlink SEM intro 5}}Tour of models{p_end}
{synopt :{manlink SEM intro 6}}Comparing groups (sem only){p_end}
{synopt :{manlink SEM intro 7}}Postestimation tests and predictions{p_end}
{synopt :{manlink SEM intro 8}}Robust and clustered standard errors{p_end}
{synopt :{manlink SEM intro 9}}Standard errors, the full story{p_end}
{synopt :{manlink SEM intro 10}}Fitting models using survey data (sem only){p_end}
{synopt :{manlink SEM intro 11}}Fitting models using summary statistics data (sem only){p_end}
{synopt :{manlink SEM intro 12}}Convergence problems and how to solve them{p_end}
