{smcl}
{* *! version 1.0.8  31may2013}{...}
{vieweralsosee "[SEM] sem postestimation" "mansection SEM sempostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[SEM] sem reporting options" "help sem_reporting_options"}{...}
{viewerjumpto "Description" "sem_postestimation##description"}{...}
{viewerjumpto "Remarks" "sem_postestimation##remarks"}{...}
{title:Title}

{p2colset 5 33 35 2}{...}
{p2col:{manlink SEM sem postestimation} {hline 2}}Postestimation tools for
	sem{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following are the postestimation commands that you can use after
estimation by {cmd:sem}:

{synoptset 18}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
{synopt :{helpb sem_command:sem, coeflegend}}display {cmd:_b[]} notation{p_end}
{synopt :{helpb sem_estat_framework:estat framework}}display results in
modeling framework (matrix form){p_end}

{synopt :{helpb sem_estat_gof:estat gof}}overall goodness of fit{p_end}
{synopt :{helpb sem_estat_ggof:estat ggof}}group-level goodness of fit{p_end}
{synopt :{helpb sem_estat_eqgof:estat eqgof}}equation-level goodness of fit{p_end}
{synopt :{helpb sem_estat_residuals:estat residuals}}matrices of residuals{p_end}
INCLUDE help post_estatic

{synopt :{helpb sem_estat_mindices:estat mindices}}modification indices (score tests){p_end}
{synopt :{helpb sem_estat_scoretests:estat scoretests}}score tests{p_end}
{synopt :{helpb sem_estat_ginvariant:estat ginvariant}}test of invariance of
parameters across groups{p_end}

{synopt :{helpb sem_estat_eqtest:estat eqtest}}equation-level Wald tests{p_end}
{synopt :{helpb sem_lrtest:lrtest}}likelihood-ratio tests {p_end}
{synopt :{helpb sem_test:test}}Wald tests {p_end}
{synopt :{helpb sem_lincom:lincom}}linear combination of parameters {p_end}
{synopt :{helpb sem_nlcom:nlcom}}nonlinear combination of parameters {p_end}
{synopt :{helpb sem_testnl:testnl}}Wald tests of nonlinear hypotheses {p_end}
{synopt :{helpb sem_estat_stdize:estat stdize:}}test standardized parameters{p_end}

{synopt :{helpb sem_estat_teffects:estat teffects}}decomposition of effects{p_end}
{synopt :{helpb sem_estat_stable:estat stable}}assess stability of nonrecursive systems{p_end}
INCLUDE help post_estatsum
INCLUDE help post_estatvce

{synopt :{helpb sem_predict:predict}}factor scores, predicted values, etc. {p_end}

{synopt :{helpb margins}}marginal means, predictive margins, and marginal effects{p_end}

{synopt :{helpb estimates:estimates}}cataloging estimation results{p_end}
{synoptline}
{p2colreset}{...}

{pstd}
For a summary of postestimation features, see {manlink SEM intro 7}.


{marker remarks}{...}
{title:Remarks}

{pstd}
This manual entry concerns {cmd:sem}.
For information on postestimation features available after {cmd:gsem},
see {helpb gsem postestimation:[SEM] gsem postestimation}.
{p_end}
