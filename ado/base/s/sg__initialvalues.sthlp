{smcl}
{* *!  version 1.0.2  23may2013}{...}
{* this sthlp file is called by sg__connections.dlg, sg__errvar.dlg, sg__variables.dlg}{...}
{vieweralsosee "[SEM] intro 12" "mansection SEM intro12"}{...}
{vieweralsosee "[SEM] sem and gsem path notation" "help sem_and_gsem_path_notation##item16"}{...}
{title:Initial values}

{pstd}
An initial value (starting value) is specified by entering the value in the
dialog field.

{pstd}
SEM models are fit by an iterative process.  That process is started by the
software choosing a set of values for the parameters that is good enough
for the process to iterate to a better value and so, by repeatedly iterating,
find the solution.  Sometimes those starting values are not good enough.  You
may specify starting values to overcome this problem.  See
{manlink SEM intro 12} for advice on selecting initial values.
{p_end}
