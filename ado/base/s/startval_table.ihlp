{* *! version 1.0.0  22apr2013}{...}
{synopt :{opth startv:alues(meglm##startvalues():svmethod)}}specify how starting values are to
  be computed{p_end}
{synopt :{cmdab:startg:rid}[{cmd:(}{it:{help meglm##startgrid():gridspec}}{cmd:)}]}perform a grid search
  to improve starting values{p_end}
{synopt :{opt noest:imate}}do not fit the model; show starting values
  instead{p_end}
