{smcl}
{* *! version 1.1.4  22apr2014}{...}
{findalias asfrres}{...}
{vieweralsosee "contents" "help contents"}{...}
{vieweralsosee "[R] help" "help help"}{...}
{vieweralsosee "[R] search" "help search"}{...}
{vieweralsosee "[R] update" "help update"}{...}
{vieweralsosee "whatsnew" "help whatsnew"}{...}
{vieweralsosee "[U] 3.4 The Stata forum" "help statalist"}{...}
{title:Title}

{pstd}
{findalias frres}


{title:Remarks}

    Stata is produced by

	    StataCorp
	    4905 Lakeway Drive
	    College Station, Texas 77845
	    USA

	    1-800-782-8272  (USA)
            1-800-248-8272  (Canada)
            1-979-696-4600  (Worldwide)

	    1-979-696-4601  (Fax)

	    {browse "http://www.stata.com"}
	    {browse "http://www.stata-press.com"}
	    {browse "http://www.stata-journal.com"}

	    {browse "http://blog.stata.com"}
	    {browse "http://twitter.com/stata"}
	    {browse "http://facebook.com/statacorp"}

{pstd}For information on

{p 12 28 2}Technical support {space 1}{browse "http://www.stata.com/support/tech-support/"}{p_end}

{p 12 28 2}Statalist {space 8} {browse "http://www.statalist.org/"}{p_end}

{p 12 28 2}More resources {space 3} {help resources:Resources for leaning more about Stata}{p_end}
