/*
    stptime.dlg

*!  VERSION 1.1.2  23apr2007

*/

VERSION 9.0

INCLUDE _std_large
INCLUDE _ht320
INCLUDE header

HELP hlp1, view("help stptime")
RESET res1

DIALOG main,label("stptime - Calculate person-time, incidence rates, and SMR")/*
                */ tabtitle("Main")
BEGIN
  DEFINE _x _xsetbu
  DEFINE _y _top
  INCLUDE _bu_stset
  GROUPBOX gb_person     _lft       _ss       _iwd        _ht15h,       /*
                */ label("Compute")
  RADIO    rb_incidence  _ilft      _ss       _ibwd       .,            /*
                */ label("Overall person-time and incidence rates")     /*
                */ first                                                /*
                */ onclickon(script main_rb_incidence_on)
  RADIO    rb_person     @          _ss       @           .,            /*
                */ label("Person-time at specified time intervals:")    /*
                */ last                                                 /*
                */ onclickon(script main_rb_person_on)
  EDIT     ed_person     _indent2   _ss       _inwd       .,            /*
                */ label("Person-time at specified time intervals")     /*
                */ option(at)
  TEXT     tx_ex1        @          _ms       _inwd       .,            /*
                */ label("Examples:")
  TEXT     tx_ex2        _indent2   _vss      _cwd1       .,            /*
                */ label("1 2 to 4")
  DEFINE tx @x
  TEXT     tx_ex3        _lft4_2    @         @           .,            /*
                */ label("Four numbers, 1, 2, 3, 4")

  TEXT     tx_ex4         tx        _vss      @           .,            /*
                */ label("1(2)9")
  TEXT     tx_ex5        _lft4_2    @         @           .,            /*
                */ label("Five numbers, 1, 3, 5, 7, 9")

  TEXT     tx_ex6         tx        _vss      @           .,            /*
                */ label("1 2:4")
  TEXT     tx_ex7        _lft4_2    @         @           .,            /*
                */ label("Same as 1 2 to 4")
  CHECKBOX ck_trim        _ilft     _ms       _ibwd       .,            /*
   */ label("Exclude obs. <= minimum or > maximum value of specified time intervals")  /*
                */ option(trim)
  TEXT     tx_by          _lft      _xls      _iwd        .,            /*
		*/ label("Categorical variable by which IRs or SMRs are to be computed:")
  VARNAME  vn_byvar       @         _ss       _vnwd       .,            /*
                */ option(by)						/*
		*/ label("Categorical variable by which IRs or SMRs are to be computed")
END

SCRIPT main_rb_incidence_on
BEGIN
	main.ed_person.disable
	main.ck_trim.disable
END

SCRIPT main_rb_person_on
BEGIN
	main.ed_person.enable
	main.ck_trim.enable
END

INCLUDE byif

DIALOG opt, label("")  tabtitle("Options")
BEGIN
  CHECKBOX ck_per         _lft      _top       _ckwd       .,           /*
                */ label("")                                            /*
                */ clickon("gaction opt.sp_per.enable")                 /*
                */ clickoff("gaction opt.sp_per.disable")
  SPINNER  sp_per         _cksep    @          _spwd       .,           /*
                */ label("Units used to report the rates")              /*
                */ min(1)                                               /*
                */ max(1000000)                                         /*
                */ default(0)                                           /*
                */ option(per)
  TEXT     tx_per         _spsep    @         _ckspr      .,            /*
                */ label("Units used to report the rates")
  CHECKBOX ck_dd          _lft      _ms       _ckwd       .,            /*
                */ label("")						/*
                */ clickon("gaction opt.sp_dd.enable")                  /*
                */ clickoff("gaction opt.sp_dd.disable")
  SPINNER  sp_dd          _cksep    @         _spwd       .,            /*
                */ label("Decimal digits to be displayed")              /*
                */ min(0)                                               /*
                */ max(1000)                                            /*
                */ default(0)                                           /*
                */ option(dd)
  TEXT     tx_dd          _spsep    @         _ckspr      .,            /*
                */ label("Decimal digits to be displayed")

  CHECKBOX ck_match       _lft       _ls      _iwd        _ht13h,       /*
      */ label("Compute standardized mortality/morbidity ratios (SMR)") /*
                */ groupbox                                             /*
                */ clickon("script match_on")                           /*
                */ clickoff("script match_off")
  TEXT     tx_groupvar    _ilft      _ms      _ibwd       .,            /*
   */ label("Age-group/calendar-period variable in disk dataset to match current data:")
  EDIT     ed_groupvar    @          _ss      _vnwd       .,            /*
  */ label("Age-group/calendar-period variable in disk dataset to match current data")/*
                */ option(smr) max(32)
  TEXT     tx_ratevar     @          _ms      _ibwd       .,            /*
                */ label("Variable containing reference rates in disk dataset:")
  EDIT     ed_ratevar     @          _ss      _vnwd       .,            /*
           */ label("Variable containing reference rates in disk dataset")/*
                */ option(smr) max(32)
  TEXT     tx_using       @          _ms      _ibwd       .,            /*
                */ label("Dataset filename:")
  FILE     fi_using       @          _ss      @           .,            /*
                */ label("Browse...")                                   /*
                */ error("Dataset filename")				/*
		*/ filter("Stata Dataset (*.dta)|*.dta|All Files (*.*)|*.*") /*
		*/ defext(dta)						/*
                */ option(using)
  DEFINE _x _lft
  DEFINE _cx _sprb
  DEFINE _y +45
  INCLUDE _sp_level
  CHECKBOX ck_noshow      _lft     _ls        _iwd        .,            /*
	        */ label("Do not show st setting information")          /*
                */ option(noshow)
END

DIALOG adv, label("")  tabtitle("Advanced")
BEGIN
  CHECKBOX ck_jackknife   _lft       _top     _iwd        .,            /*
	        */ label("Jackknife confidence intervals")              /*
                */ option(jackknife)
  TEXT     tx_jackknife   _cksep     _vss     _inwd       .,		/*
  		*/ label("Note: this is the default if weights were specified when the dataset was stset.")
  GROUPBOX gb_title       _lft       _ls      _iwd        _ht6,         /*
                */ label("Show label as")
  RADIO    rb_person      _ilft      _ss      _ibwd       .,            /*
                */ label("Person-time")                                 /*
                */ first                                                /*
                */ clickon("gaction adv.ed_title.disable")
  RADIO    rb_specified   @          _ss      @           .,            /*
                */ label("User-specified:")                             /*
                */ last                                                 /*
                */ clickon("gaction adv.ed_title.enable")
  EDIT     ed_title       _indent2   _ss      _vnwd       .,            /*
                */ label("User-specified")                              /*
                */ max(132)                                             /*
		*/ option(title)
  CHECKBOX ck_output      _lft       _xls     _iwd        _ht4h,        /*
                */ label("Save summary dataset to a file")             /*
                */ groupbox                                             /*
                */ clickon("script output_on")                          /*
                */ clickoff("script output_off")
  TEXT     tx_output      _ilft      _ms      _ibwd       .,            /*
                */ label("Filename:")
  FILE     fi_output      @          _ss      @           .,            /*
                */ error("Filename")					/*
                */ option(output)                                       /*
                */ label("Save As...")					/*
		*/ filter("Stata Dataset (*.dta)|*.dta|All Files (*.*)|*.*")	/*
		*/ defext(dta)						/*
                */ save
END

SCRIPT match_on
BEGIN
	opt.ed_groupvar.enable
	opt.ed_ratevar.enable
	opt.fi_using.enable
END

SCRIPT match_off
BEGIN
        opt.ed_groupvar.disable
        opt.ed_ratevar.disable
        opt.fi_using.disable
END

SCRIPT output_on
BEGIN
	adv.fi_output.enable
END

SCRIPT output_off
BEGIN
        adv.fi_output.disable
END

PROGRAM command
BEGIN
	if adv.ck_output & main.vn_byvar {
		stopbox stop /*
*/ `""Categorical variable by which incidence rates/SMR are to be computed""' /*
			*/ "may not be filled in when" /*
                	*/ `""Save summary dataset to a file""' "is checked"
	}
	if main.rb_person {
		require main.ed_person
	}
	if opt.ck_match {
		if !opt.ed_ratevar & !opt.ed_groupvar & !opt.fi_using {
			require opt.ed_groupvar
		}
		if opt.ed_ratevar {
			require opt.ed_groupvar
		}
		if opt.ed_groupvar {
			require opt.ed_ratevar
		}
		if opt.ed_ratevar & opt.ed_groupvar {
			require opt.fi_using
		}
	}
	if adv.rb_specified {
		require adv.ed_title
	}
	INCLUDE _by_pr
	put "stptime "
	INCLUDE _if_pr
	beginoptions
		optionarg main.ed_person
		optionarg main.vn_byvar
		option main.ck_trim
		optionarg opt.sp_per
		optionarg opt.sp_dd
		INCLUDE _level_opt_pr
		option opt.ck_noshow
		option adv.ck_jackknife
		if opt.ck_match {
	 		put "smr("  opt.ed_groupvar
			put " " opt.ed_ratevar ") "
			put "using("
			put `"""'
			put opt.fi_using
			put `"""'
			put ") "
		}
		optionarg adv.ed_title
		if adv.ck_output {
			require adv.fi_output
			repfile adv.fi_output
			put "output("
			put `"""'
			put adv.fi_output
			put `"""'
			put ", replace"
		        put ")"
		}
	endoptions
END
