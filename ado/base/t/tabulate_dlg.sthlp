{smcl}
{* *! version 1.1.2  29jan2013}{...}
{vieweralsosee "[R] tabulate oneway" "mansection R tabulateoneway"}{...}
{vieweralsosee "[R] tabulate twoway" "mansection R tabulatetwoway"}{...}

    {dialog tabulate1:One-way table}

    {dialog tabulate2:Two-way table with measures of association}

    {dialog tabulategen:Create indicator variables}
