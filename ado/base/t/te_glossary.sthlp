{smcl}
{* *! version 1.0.2  08may2013}{...}
{vieweralsosee "[TE] Glossary" "mansection TE Glossary"}{...}
{viewerjumpto "Description" "te_glossary##description"}{...}
{title:Title}

{p2colset 5 22 24 2}{...}
{p2col:{manlink TE Glossary} {hline 2}}Glossary of terms{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{phang}
{bf:AIPW estimator}.  See
{it:{help te_glossary##AIPW:augmented inverse-probability-weighted estimator}}.

{phang}
{bf:ATE}.  See
{it:{help te_glossary##ATE:average treatment effect}}.

{phang}
{bf:ATET}.  See
{it:{help te_glossary##ATET:average treatment effect on the treated}}.

{marker AIPW}{...}
{phang}
{bf:augmented inverse-probability-weighted estimator}.
        An augmented inverse-probability-weighted (AIPW) estimator is an
	inverse-probability-weighted estimator that includes an augmentation
        term that corrects the estimator when the treatment model is
        misspecified.  When the treatment is correctly specified, the
        augmentation term vanishes as the sample size becomes large.
	An AIPW estimator uses both an outcome model and a treatment
	model and is a doubly robust estimator.

{marker ATE}{...}
{phang}
{bf:average treatment effect}.
        The average treatment effect is the average effect of the treatment
	among all individuals in a population.

{marker ATET}{...}
{phang}
{bf:average treatment effect on the treated}.
        The average treatment effect on the treated is the
	average effect of the treatment among those individuals who actually
	get the treatment.

{phang}
{bf:CI assumption}.  See
{it:{help te_glossary##CI_assumption:conditional-independence assumption}}.

{phang}
{bf:conditional mean}.
        The conditional mean expresses the average of one variable as a
        function of some other variables.  More formally, the mean of y
        conditional on {bf:x} is the mean of y for given values of
        {bf:x}; in other words, it is E(y| {bf:x}).

{pmore}
        A conditional mean is also known as a regression or as a conditional
        expectation.

{marker CI_assumption}{...}
{phang}
{bf:conditional-independence assumption}.
        The conditional-independence assumption requires that the common
        variables that affect treatment assignment and treatment-specific
        outcomes be observable.  The dependence between treatment
        assignment and treatment-specific outcomes can be removed by
        conditioning on these observable variables.

{pmore}
        This assumption is also known as a selection-on-observables
        assumption because its central tenet is the observability of the
        common variables that generate the dependence.

{phang}
{bf:counterfactual}.  A counterfactual is an outcome a subject would have
obtained had that subject received a different level of treatment.  In the
binary-treatment case, the counterfactual outcome for a person who received
treatment is the outcome that person would have obtained had the person instead
not received treatment; similarly, the counterfactual outcome for a person who
did not receive treatment is the outcome that person would have obtained had
the person received treatment. 

{pmore}
Also see
{it:{help te_glossary##potential_outcome:potential outcome}}.

{phang}
{bf:doubly robust estimator}.
        A doubly robust estimator only needs one of two auxiliary models to
        be correctly specified to estimate a parameter of interest.

{pmore}
        Doubly robust estimators for treatment effects are consistent when
        either the outcome model or the treatment model is correctly
        specified.

{phang}
{bf:EE estimator}. See
{it:{help te_glossary##EE:estimating-equation estimator}}.

{marker EE}{...}
{phang}
{bf:estimating-equation estimator}.
        An estimating-equation (EE) estimator calculates parameters
        estimates by solving a system of equations.  Each equation in this
        system is the sample average of a function that has mean zero.

{pmore}
       These estimators are also known as M estimators or Z estimators in
       the statistics literature and as generalized method of moments
       (GMM) estimators in the econometrics literature.

{phang}
{bf:i.i.d. sampling assumption}. See
{it:{help te_glossary##iid:independent and identically distributed sampling assumption}}.

{marker iid}{...}
{phang}
{bf:independent and identically distributed sampling assumption}.
	The independent and identically distributed (i.i.d.) sampling
	assumption specifies that each observation is unrelated to
	(independent of) all the other observations and that each observation
	is a draw from the same (identical) distribution.

{phang}
{bf:individual-level treatment effect}.
        An individual-level treatment effect is the difference in an
        individual's outcome that would occur because this individual is
        given one treatment instead of another.  In other words, an
        individual-level treatment effect is the difference between two
        potential outcomes for an individual.

{pmore}
	For example, the blood pressure an individual would obtain after
	taking a pill minus the blood pressure an individual would obtain had
	that person not taken the pill is the individual-level treatment
	effect of the pill on blood pressure.

{marker IPW}{...}
{phang}
{bf:inverse-probability-weighted estimators}.
	Inverse-probability-weighted (IPW) estimators use weighted
	averages of the observed outcome variable to estimate the
	potential-outcome means. The weights are the reciprocals of the
	treatment probabilities estimated by a treatment model.

{marker IPWRA}{...}
{phang}
{bf:inverse-probability-weighted regression-adjustment estimators}.
	Inverse-probability-weighted regression-adjustment (IPWRA)
	estimators use the reciprocals of the estimated treatment probability
	as weights to estimate missing-data-corrected regression coefficients
	that are subsequently used to compute the potential-outcome means.

{phang}
{bf:IPW estimators}. See
{it:{help te_glossary##IPW:inverse-probability-weighted estimators}}.

{phang}
{bf:IPWRA estimators}. See
{it:{help te_glossary##IPWRA:inverse-probability-weighted regression-adjustment estimators}}.

{phang}
{bf:matching estimator}.
        An estimator that compares differences between the outcomes of
	similar -- that is, matched -- individuals.  Each individual
	that receives a treatment is matched to a similar individual that does
	not get the treatment, and the difference in their outcomes is used to
	estimate the individual-level treatment effect.  Likewise, each
	individual that does not receive a treatment is matched to a similar
	individual that does get the treatment, and the difference in their
	outcomes is used to estimate the individual-level treatment effect.

{phang}
{bf:multivalued treatment effect}.
	A multivalued treatment refers to a treatment that has more than two
	values.  For example, a person could have taken a 20 mg dose of a
	drug, a 40 mg dose of the drug, or not taken the drug at all.

{phang}
{bf:nearest-neighbor matching}.
        Nearest-neighbor matching uses the distance between observed
        variables to find similar individuals.

{phang}
{bf:observational data}.
        In observational data, treatment assignment is not controlled by
        those who collected the data; thus some common variables affect
        treatment assignment and treatment-specific outcomes.

{phang}
{bf:outcome model}.
        An outcome model is a model used to predict the outcome as a
        function of covariates and parameters.

{phang}
{bf:overlap assumption}.
        The overlap assumption requires that each individual have a positive
        probability of each possible treatment level.

{phang}
{bf:POMs}.  See
{it:{help te_glossary##POM:potential-outcome means}}.

{marker potential_outcome}{...}
{phang}
{bf:potential outcome}.
	The potential outcome is the outcome an individual would obtain if
	given a specific treatment.

{pmore}
        For example, an individual has one potential blood pressure after
        taking a pill and another potential blood pressure had that person not
        taken the pill.

{marker POM}{...}
{phang}
{bf:potential-outcome means}.
       The potential-outcome means refers to the means of the potential
       outcomes for a specific treatment level.

{pmore}
       The mean blood pressure if everyone takes a pill and the mean blood
       pressure if no one takes a pill are two examples.

{pmore}
       The average treatment effect is the difference between
       potential-outcome mean for the
       treated and the potential-outcome mean for the not treated.

{phang}
{bf:propensity score}.
        The propensity score is the probability that an individual receives
        a treatment.

{phang}
{bf:propensity-score matching}.
        Propensity-score matching uses the distance between estimated
        propensity scores to find similar individuals.

{phang}
{bf:regression-adjustment estimators}.
        Regression-adjustment estimators use means of predicted outcomes for
        each treatment level to estimate each potential-outcome mean.

{phang}
{bf:selection-on-observables}.  See
{it:{help te_glossary##CI_assumption:conditional-independence assumption}}.

{phang}
{bf:smooth treatment-effects estimator}.
	A smooth treatment-effects estimator is a smooth function of the data
	so that standard methods approximate the distribution of the
        estimator.  The RA, IPW, AIPW, and
	IPWRA estimators are all smooth
        treatment-effects estimators while the nearest-neighbor matching
        estimator and the propensity-score matching estimator are not.


{phang}
{bf:treatment model}.
        A treatment model is a model used to predict treatment-assignment
        probabilities as a function of covariates and parameters.

{phang}
{bf:unconfoundedness}.  See
{it:{help te_glossary##CI_assumption:conditional-independence assumption}}.
{p_end}
