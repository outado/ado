*! version 1.0.0  31dec2012

program define teffects_estat
	version 13

	if "`e(cmd)'" != "teffects" {
		di as err "{help teffects##_new:teffects} estimation " ///
		 "results not found"
		exit 301
	}
	gettoken sub rest: 0, parse(" ,")
	local lsub = length("`sub'")

	if "`sub'" == "vce" {
		estat_default `0'
	}
	else if "`sub'" == substr("summarize",1,max(2,`lsub')) {
		if ("`e(stat)'"=="pomeans") estat_default `0'
		else EstatSummarize `"`0'"'
	}
	else {
		di as err "{bf:estat `sub'} is not allowed"
		exit 321
	}
end

/* special handling of the ATE & ATT contrast style equation		*/
program define EstatSummarize, eclass
	args options

	tempname b b0 V V0

	mat `b' = e(b)
	mat `V' = e(V)
	local names : colnames `b'

	local tlevels `e(tlevels)'
	local control = e(control)
	local tlevnoc : list tlevels - control

	forvalues i=1/`=e(k_levels)-1' {
		local nm : word `i' of `names'
		local lev : word `i' of `tlevnoc'
		gettoken r var: nm, parse(".")

		local names : subinstr local names "`r'" "`lev'" 
	}
	local names : subinstr local names "r`control'" "`control'" 
	mat `b0' = `b'
	mat `V0' = `V'

	mat colnames `b' = `names'
	mat colnames `V' = `names'
	mat rownames `V' = `names'
	nobreak {
		ereturn repost b=`b' V=`V', rename

		estat_default `options'

		ereturn repost b=`b0' V=`V0', rename
	}
end

exit
