{smcl}
{* *! version 1.0.0 29mar2013}{...}
{findalias asfrconftrain}{...}
{title:Title}

{title:{findalias frconftrain}}


{title:{findalias frconference}}

{pstd}
StataCorp organizes the annual Stata Conference in the United States.  Other
conferences and users group meetings are held in several countries around the
world each year.

{pstd}
These meetings provide in-depth presentations from experienced Stata users and
experts from StataCorp.  They also provide you with the opportunity to interact
directly with the people who develop Stata and to share your thoughts and ideas
with them.

{pstd}
Visit {browse "http://www.stata.com/meeting/"} for a list of upcoming
conferences and meetings.


{title:{findalias frnetcourse}}

{pstd}
We offer courses on Stata at both introductory and advanced levels.  Courses
on software are typically expensive and time consuming.  They are expensive
because, in addition to the direct costs of the course, participants must
travel to the course site.  Courses over the Internet save everyone time and
money.

{pstd}
We offer courses over the Internet and call them Stata NetCourses.

{phang2}o {bf:What is a NetCourse?}{break}
    A NetCourse is a course offered through the Stata website
    that varies in length from 7 to 8 weeks. Everyone with an
    email address and a web browser can participate.

{phang2}o {bf:How does it work?}{break}
    Every Friday a lecture is posted on a password-protected website.  After
    reading the lecture over the weekend or perhaps on Monday, participants
    then post questions and comments on a message board. Course leaders
    typically respond to the questions and comments on the same day they are
    posted. Other participants are encouraged to amplify or otherwise respond
    to the questions or comments as well.  The next lecture is then posted on
    Friday, and the process repeats.

{phang2}o {bf:How much of my time does it take?}{break}
    It depends on the course, but the introductory courses are designed to
    take roughly 3 hours per week.

{phang2}o {bf:There are three of us here -- can just one of us enroll}
    {bf:and then redistribute the NetCourse materials ourselves?}{break}
    We ask that you not.  NetCourses are priced to cover the substantial time
    input of the course leaders.  Moreover, enrollment is typically limited to
    prevent the discussion from becoming unmanageable.  The value of a
    NetCourse, just like a real course, is the interaction of the
    participants, both with each other and with the course leaders.

{phang2}o {bf:I've never taken a course by Internet before.  I can see that}
    {bf:it might work, but then again, it might not.  How do I know I will}
    {bf:benefit?}{break}
    All Stata NetCourses come with a 30-day satisfaction guarantee. The 30
    days begins after the conclusion of the final lecture.

{pstd}
You can learn more about the current NetCourse offerings by visiting
{browse "http://www.stata.com/netcourse/"}.


    {bf:NetCourseNow}

{pstd}
A NetCourseNow offers the same material as NetCourses but it allows
you to choose the time and pace of the course, and you have a personal
NetCourse instructor.

{phang2}o {bf:What is a NetCourseNow?}{break}
A NetCourseNow offers the same material as a NetCourse, but allows you
to move at your own pace and to specify a starting date.  With a
NetCourseNow, you also have the added benefit of a personal NetCourse
instructor whom you can email directly with questions about lectures and
exercises.  You must have an email address and a web browser to participate.

{phang2}o {bf:How does it work?}{break}
All course lectures and exercises are posted at once, and you are free to
study at your own pace.  You will be provided with the email address of your
personal NetCourse instructor to contact when you have questions.

{phang2}o {bf:How much of my time does it take?}{break}
A NetCourseNow allows you to set your own pace.  How long the course
takes and how much time you spend per week is up to you.


{title:{findalias frpublictrain}}

{pstd}
Public training courses are intensive, in-depth courses taught by
StataCorp at third-party sites around the United States.


{phang2}o {bf:How is a public training course taught?}{break}
These are interactive, hands-on sessions. Participants work along with the
instructor so that they can see firsthand how to use Stata. Questions are
encouraged.

{phang2}o {bf:Do I need my own computer?}{break}
Because the sessions are in computer labs running the latest version of
Stata, there is no need to bring your own computer. Of course, you may
bring your own computer if you have a registered copy of Stata you can
use.

{phang2}o {bf:Do I get any notes?}{break}
You get a complete set of notes for each class, which includes not only
the materials from the lessons but also all the output from the example
commands. 

{pstd}
See {browse "http://www.stata.com/training/public.html"} for all course
offerings.


{title:{findalias fronsitetrain}}

{pstd}
On-site training courses are courses that are tailored to the needs of
an institution. StataCorp personnel can come to your site to teach what
you need, whether it be to teach new users or to show how to use a
specialized tool in Stata.

{phang2}o {bf:How is an on-site training course taught?}{break}
These are interactive, hands-on sessions, just like our public-training
courses.  You will need a computer for each participant.

{phang2}o {bf:What topics are available?}{break}
We offer training in anything and everything related to Stata. You work
with us to put together a curriculum that matches your needs.

{phang2}o {bf:How does licensing work?}{break}
We will supply you with the licenses you need for the training session,
whether the training is in a lab or for individuals working on laptops.
We will ship the licensing and installation instructions so that you can
have everything up and running before the session starts.

{pstd}
See {browse "http://www.stata.com/training/onsite.html"} for all the details.
{p_end}
