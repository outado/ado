{smcl}
{* *! version 1.0.9  21mar2014}{...}
{title:Title}

{pstd}
Stata YouTube channel


{title:Description}

{pstd}
The {browse "http://www.youtube.com/user/statacorp":Stata YouTube channel}
includes videos that range from providing a quick tour of Stata's
interface to performing various statistical analyses in Stata.
There is also a 
{browse "http://www.youtube.com/watch?v=y3cXcXaA0hY&list=PLN5IskQdgXWmqkY0dPy8IOlms3v3NhlHN":playlist}
of all the videos that demonstrate the new features added in Stata 13.

{pstd}
StataCorp is continually adding new videos.
A list of these videos is available at
{browse "http://www.stata.com/links/video-tutorials/"}.
{p_end}
