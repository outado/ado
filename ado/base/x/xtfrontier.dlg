/*
  xtfrontier

*!  VERSION 1.3.7  02may2014

  keyword:  eclass

*/

VERSION 12.0

INCLUDE _std_large
INCLUDE _ht330
INCLUDE header

HELP hlp1, view("help xtfrontier")
RESET res1

SCRIPT PREINIT
BEGIN
	script se_createAsOIMJknifeBstrap
END

SCRIPT POSTINIT
BEGIN
	script max_setDefaultDifficult
	script se_setCluster_off
	script se_setTitleSE
END

SCRIPT show_xtset
BEGIN
	create STRING note
	note.setvalue `"For a time-varying decay model,"'
	note.append   `" a time variable must be specified."'
	note.withvalue create CHILD xtset, message(`"" " `"@"'"')
END


DIALOG main, tabtitle("Model")	///
	label("xtfrontier - Stochastic frontier models for panel data")
BEGIN
  BUTTON   bu_xtset	_xsetbu	_top	_setbuwd .,		///
	onpush(script show_xtset)				///
	label("Panel settings...")

  TEXT tx_dv 		_lft	_topph	_vnwd	.,			///
	label("Dependent variable:")
  VARNAME vn_dv		@ 	_ss	@	.,			///
	ts								///
	numeric								///
	label("Dependent variable")

  TEXT tx_iv		_vlx	_topph	160	.,			///
	label("Independent variables:")
  VARLIST vl_iv		@	_ss	_vlwd	.,			///
	fv ts								///
	allowcat							///
	numeric								///
	label("Independent variables")

  CHECKBOX ck_nocons	@	_ms	@	.,		///
	label("Suppress the constant term")			///
	option(noconstant)

  GROUPBOX gb_dist	_lft	_ls	_iwd	_ht4,		///
	label("Distribution of the inefficiency term")
  RADIO    rb_ti	_ilft	_ss	_cwd2	.,		///
	label("Time-invariant model") 				///
	option(ti) first
  RADIO    rb_tvd	@	_ss	@	.,		///
	label("Time-varying decay model")			///
	option(tvd) last

  GROUPBOX gb_options	_lft	_xls	_iwd	_ht11h, 	///
	label("Options")

  RADIO    rb_default	_ilft	_ss	_ibwd	.,		///
	first							///
	label("Fit model in terms of a production function (default)")
  RADIO    rb_cost	@	_ss	@	.,		///
	last							///
	label("Fit model in terms of a cost function")		///
	option(cost)
  
  DEFINE _x _ilft
  DEFINE _y _ms
  DEFINE _cx _ilw80
  DEFINE _bux _islw80
  INCLUDE _constraints
  
  DEFINE _x _ilft
  DEFINE _xw _ibwd
  INCLUDE _ck_collinear
END

INCLUDE _constraints_sc 
INCLUDE byifin
INCLUDE weights_fi
INCLUDE se

SCRIPT rpt_POSTINIT
BEGIN
	create STRING rpt_bu_fmtcoefResults
	program rpt_bu_fmtcoef_ckResults
	create STRING rpt_bu_facvarsResults
        program rpt_bu_facvars_ckResults
END

DIALOG rpt, tabtitle("Reporting")
BEGIN
  DEFINE _x _lft
  DEFINE _y _top
  DEFINE _cx _spr2
  INCLUDE _sp_level

  DEFINE _x _lft
  DEFINE _y _ls
  DEFINE _cx _iwd
  INCLUDE _nocnsreport

  DEFINE _x _lft
  DEFINE _y _ls
  DEFINE _cx _iwd
  INCLUDE _report_columns

  DEFINE _x _lft
  DEFINE _y _ls
  INCLUDE _bu_factor_vars_reporting

  DEFINE _x _lft2
  DEFINE _y @
  INCLUDE _bu_coef_table_reporting
END

INCLUDE fmt_coef_table_reporting_pr
INCLUDE factor_vars_reporting_pr

INCLUDE max_ml

PROGRAM command
BEGIN
	put /program by_output " "
	put "xtfrontier "
	varlist main.vn_dv [main.vl_iv]
	if !main.vl_iv & main.ck_nocons {
		stopbox stop `""Suppress constant term" is selected without independent variables."'
	}
	put " " /program ifin_output
	put " " /program weights_output
	beginoptions
		option main.ck_nocons
		option radio(main rb_ti rb_tvd)
		INCLUDE _constraints_main_pr
		option main.ck_collinear
		option main.rb_cost
		put " " /program se_output
		optionarg /hidedefault rpt.sp_level
		INCLUDE _nocnsreport_pr
		INCLUDE _report_columns_pr
		put " " rpt_bu_facvarsResults
		put " " rpt_bu_fmtcoefResults
		put " " /program max_output
	endoptions
END
