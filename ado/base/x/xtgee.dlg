/*
  xtgee.dlg

*!  VERSION 1.2.11  25sep2013

*/

VERSION 12.0

INCLUDE glm_header
DEFINE _dlght 360
INCLUDE header

HELP hlp1, view("help xtgee")
RESET res1

SCRIPT PREINIT
BEGIN
	program parseMessage
	script se_createAsGEE
END

PROGRAM parseMessage
BEGIN
	if __MESSAGE.contains("__MI__") {
		call script se_setMI_on
		call main.bu_xtset.hide
	}
	else {
		call main.bu_mi_xtset.hide
	}
END

SCRIPT POSTINIT
BEGIN
	program check_bytab
	script max_pa_on
	script se_setGEE_on
END

PROGRAM check_bytab
BEGIN
	if __MESSAGE.contains("__MI__") {
		call script byifin_set_by_off
	}
END

SCRIPT show_xtset
BEGIN
	create STRING note
	note.append   `"Correlation structures other than"'
	note.append   `" "exchangeable" and "independent""'
	note.append   `" require that a time variable be specified."'
	note.withvalue create CHILD xtset, message(`"" " `"@"'"')
END

DIALOG main, ///
	label("xtgee - Fit population-averaged panel-data models by using GEE") ///
	tabtitle("Model")
BEGIN
  BUTTON   bu_xtset	_xsetbu	_top	_setbuwd .,		///
	onpush(script show_xtset)				///
	label("Panel settings...")

  BUTTON   bu_mi_xtset  _xsetbu	_top	_setbuwd	.,		///
	onpush("view dialog mi_xtset")					///
	label("Panel settings...")

  TEXT tx_dv 		_lft	_topph	_vnwd	.,			///
	label("Dependent variable:")
  VARNAME vn_dv		@ 	_ss	@	.,			///
	ts								///
	numeric								///
	label("Dependent variable")

  TEXT tx_iv		_vlx	_topph	160	.,			///
	label("Independent variables:")
  VARLIST vl_iv		@	_ss	_vlwd	.,			///
	fv ts								///
	allowcat							///
	numeric								///
	label("Independent variables")

  TEXT     tx_title1 _lft      _ls       irhwd     .,			/*
		*/ label("Family and")					/*
  		*/
  DEFINE y @y
  TEXT     tx_title2 _lft      _ss       irhwd     .,			/*
		*/ label("link choices:")				/*
		*/
  /* Display the column lines and headings */
  FRAME    fr_gaus   col1      y         cwd       thtgee
  DEFINE y @y
  TEXT     tx_gaus   chind     _ss       icwd      .,			/*
		*/ center						/*
		*/ label("Gaussian")					/*
		*/
  DEFINE y2 @y
  FRAME    fr_invg   icolskip  y         cwd       thtgee
  DEFINE nbinc1  @x
  TEXT     tx_invg1  chind     for       icwd      .,			/*
		*/ center						/*
		*/ label("Inverse")					/*
		*/
  DEFINE xinv @x
  FRAME    fr_bin    icolskip  y         cwd       thtgee
  FRAME    fr_pois   colskip   y         @         thtgee
  FRAME    fr_nbin   colskip   y         @         thtgee
  DEFINE nbinc2  @x
  TEXT     tx_nbin1  chind     for       icwd      .,			/*
		*/ center						/*
		*/ label("Negative")					/*
		*/
  FRAME    fr_gamm   icolskip  y         cwd       thtgee
  TEXT     tx_invg2  xinv      y2        icwd      .,			/*
		*/ center						/*
		*/ label("Gaussian")					/*
		*/
  TEXT     tx_bin    colskip   @         @         .,			/*
		*/ center						/*
		*/ label("Binomial")					/*
		*/
  TEXT     tx_pois   colskip   @         @         .,			/*
		*/ center						/*
		*/ label("Poisson")					/*
		*/
  TEXT     tx_nbin2  colskip   @         @         .,			/*
		*/ center						/*
		*/ label("binomial")					/*
		*/
  TEXT     tx_gamm   colskip   @         @         .,			/*
		*/ center						/*
		*/ label("Gamma")					/*
		*/

  /* Display the row lines and headings */
  FRAME    fr_id     _lft      skip      twd       rht
  TEXT     tx_id     ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Identity")					/*
		*/
  DEFINE row1 @y

  FRAME    fr_log    _lft      skip      twd       rht
  TEXT     tx_log    ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Log")						/*
		*/
  DEFINE row2 @y

  FRAME    fr_logit  _lft      skip      twd       rht
  TEXT     tx_logit  ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Logit")					/*
		*/
  DEFINE row3 @y

  FRAME    fr_probit _lft      skip      twd       rht
  TEXT     tx_probit ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Probit")					/*
		*/
  DEFINE row4 @y

  FRAME    fr_clog   _lft      skip      twd       rht
  TEXT     tx_clog   ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("C. log-log")					/*
		*/
  DEFINE row5 @y

  FRAME    fr_power  _lft      skip      twd       rht
  TEXT     tx_power  ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Power")					/*
		*/
  DEFINE row6 @y

  FRAME    fr_odds   _lft      skip      twd       rht
  TEXT     tx_odds   ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Odds power")					/*
		*/
  DEFINE row7 @y

  FRAME    fr_nbinr  _lft      skip      twd       rht
  TEXT     tx_nbinr  ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Neg. binom.")					/*
		*/
  DEFINE row8 @y

  FRAME    fr_recipr _lft      skip      twd       rht
  TEXT     tx_recipr ilft      for       irhwd     .,			/*
		*/ right	      					/*
		*/ label("Reciprocal")					/*
		*/
  DEFINE row9 @y

  /* Binomial options */
  TEXT     tx_bin2   _lft      _ls       comb2     .,			/*
		*/ label("Bernoulli trials (n):")			/*
		*/ right						/*
		*/
  DEFINE xbin @x
  DEFINE ybin @y
  RADIO    rb_sp_n   colskip2  @         _rbwd     .,			/*
		*/ clickon("script sp_n_on")				/*
		*/ first						/*
		*/
  DEFINE x @x
  SPINNER  sp_n      _rbsep    @         _spwd     .,			/*
		*/ min(1)						/*
		*/ onchange("program sp_n_changed")			/*
		*/ max(1000000)						/*
		*/ default(1)						/*
		*/
  TEXT     tx_sp_n   _spsep    @         cwd       .,			/*
		*/ label("Constant")					/*
		*/
  RADIO    rb_vn_n   colskip   @         _rbwd     .,			/*
		*/ clickon("script vn_n_on")				/*
		*/ last							/*
		*/
  VARNAME  vn_n      _rbsep    @         _vnwd     .,			/*
  		*/ numeric						/*
		*/ label("Variable")					/*
		*/
  TEXT     tx_vn_n   _vnsep    @         cwd      .,			/*
		*/ label("Variable")					/*
		*/

  /* Negative binomial option */
  TEXT     tx_nbin3  nbinc1    @         comb3     .,			/*
		*/ label("Successes: (k) [optional]")			/*
		*/ right						/*
		*/
  EDIT     en_k      nbinc2    @         _en7wd    .,			/*
		*/ label("Successes")					/*
		*/ numonly						/*
		*/

  /* numeric edit fields for [odds] power */
  EDIT     en_power  rmx       row7      _spwd     .,			/*
		*/ label("Power")					/*
		*/ numonly						/*
		*/ default(1)						/*
		*/
  TEXT     tx_power2 @         nss       rmwd      .,			/*
		*/ label("Power")					/*
		*/
  TEXT     tx_odds2  @         nvss      rmwd      .,			/*
		*/ label("Odds")					/*
		*/

  /* Now the radio buttons */
  RADIO    rb_id_ga  rbcol1    row1      rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ first						/*
		*/ option("family(gaussian) link(identity)")		/*
		*/
  RADIO    rb_id_ig  colskip   @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(igaussian) link(identity)")		/*
		*/
  DEFINE rbcol2 @x
  RADIO    rb_id_bi  colskip   @         rbwd      .,			/*
		*/ clickon("script binonly")				/*
		*/ option("link(identity)")				/*
		*/
  DEFINE rbcol3 @x
  RADIO    rb_id_po  colskip   @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(poisson) link(identity)")		/*
		*/
  DEFINE rbcol4 @x
  RADIO    rb_id_nb  colskip   @         rbwd      .,			/*
		*/ clickon("script nbinonly")				/*
		*/ option("link(identity)")				/*
		*/
  DEFINE rbcol5 @x
  RADIO    rb_id_gm  colskip   @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(gamma) link(identity)")		/*
		*/
  DEFINE rbcol6 @x

  RADIO    rb_lg_ga  rbcol1    row2      rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(gaussian) link(log)")			/*
		*/
  RADIO    rb_lg_ig  rbcol2    @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(igaussian) link(log)")		/*
		*/
  RADIO    rb_lg_bi  rbcol3    @         rbwd      .,			/*
		*/ clickon("script binonly")				/*
		*/ option("link(log)")					/*
		*/
  RADIO    rb_lg_po  rbcol4    @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(poisson) link(log)")			/*
		*/
  RADIO    rb_lg_nb  rbcol5    @         rbwd      .,			/*
		*/ clickon("script nbinonly")				/*
		*/ option("link(log)")				/*
		*/
  RADIO    rb_lg_gm  rbcol6    @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(gamma) link(log)")			/*
		*/

  RADIO    rb_lt_bi  rbcol3    row3      rbwd      .,			/*
		*/ clickon("script binonly")				/*
		*/ option("link(logit)")				/*
		*/

  RADIO    rb_pr_bi  rbcol3    row4      rbwd      .,			/*
		*/ clickon("script binonly")				/*
		*/ option("link(probit)")				/*
		*/

  RADIO    rb_cl_bi  rbcol3    row5      rbwd      .,			/*
		*/ clickon("script binonly")				/*
		*/ option("link(cloglog)")				/*
		*/

  RADIO    rb_pw_ga  rbcol1    row6      rbwd      .,			/*
		*/ clickon("script ponly")				/*
		*/ option("family(gaussian)")				/*
		*/
  RADIO    rb_pw_ig  rbcol2    @         rbwd      .,			/*
		*/ clickon("script ponly")				/*
		*/ option("family(igaussian)")				/*
		*/
  RADIO    rb_pw_bi  rbcol3    @         rbwd      .,			/*
		*/ clickon("script pbin")				/*
		*/ option(NONE)						/*
		*/
  RADIO    rb_pw_po  rbcol4    @         rbwd      .,			/*
		*/ clickon("script ponly")				/*
		*/ option("family(poisson)")				/*
		*/
  RADIO    rb_pw_nb  rbcol5    @         rbwd      .,			/*
		*/ clickon("script pnbin")				/*
		*/ option(NONE)						/*
		*/
  RADIO    rb_pw_gm  rbcol6    @         rbwd      .,			/*
		*/ clickon("script ponly")				/*
		*/ option("family(gamma)")				/*
		*/

  RADIO    rb_op_bi  rbcol3    row7      rbwd      .,			/*
		*/ clickon("script obin")				/*
		*/ option(NONE)						/*
		*/

  RADIO    rb_nb_nb  rbcol5    row8      rbwd      .,			/*
		*/ clickon("script nbinonly")				/*
		*/ option("link(nbinomial)")				/*
		*/

  RADIO    rb_rr_ga  rbcol1    row9      rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(gaussian)")				/*
		*/
  RADIO    rb_rr_bi  rbcol3    @         rbwd      .,			/*
		*/ clickon("script binonly")				/*
		*/ option("link(reciprocal)")				/*
		*/
  RADIO    rb_rr_po  rbcol4    @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ option("family(poisson) link(reciprocal)")		/*
		*/
  RADIO    rb_rr_gm  rbcol6    @         rbwd      .,			/*
		*/ clickon("script none")				/*
		*/ last							/*
		*/ option("family(gamma) link(reciprocal)")		/*
		*/

END

SCRIPT none
BEGIN
	script poweroff
	script binoff
	script nbinoff
	program gaussian_check
END

SCRIPT binonly
BEGIN
	script poweroff
	script binon
	script nbinoff
	program gaussian_check
END

SCRIPT nbinonly
BEGIN
	script poweroff
	script binoff
	script nbinon
	program gaussian_check
END

SCRIPT ponly
BEGIN
	script poweron
	script binoff
	script nbinoff
	program gaussian_check
END

SCRIPT oonly
BEGIN
	script oddson
	script binoff
	script nbinoff
	program gaussian_check
END

SCRIPT pbin
BEGIN
	script poweron
	script binon
	script nbinoff
	program gaussian_check
END

SCRIPT pnbin
BEGIN
	script poweron
	script binoff
	script nbinon
	program gaussian_check
END

SCRIPT obin
BEGIN
	script oddson
	script binon
	script nbinoff
	program gaussian_check
END

PROGRAM gaussian_check
BEGIN
	if main.rb_id_ga | main.rb_lg_ga | main.rb_pw_ga | main.rb_rr_ga {
		call script se_setRGF_on
	}
	if ! (main.rb_id_ga | main.rb_lg_ga | main.rb_pw_ga | main.rb_rr_ga) {
		call script se_setRGF_off
	}
END

SCRIPT binon
BEGIN
	main.tx_bin2.show
	main.rb_sp_n.show
	main.sp_n.show
	main.tx_sp_n.show
	main.rb_vn_n.show
	main.vn_n.show
	main.tx_vn_n.show
	model2.ck_asis.enable
END

SCRIPT sp_n_on
BEGIN
	main.sp_n.enable
	main.vn_n.disable
END

PROGRAM sp_n_changed
BEGIN
	if main.sp_n.iseq(1) {
		call model2.ck_asis.enable
	}
	else {
		call model2.ck_asis.disable
	}
END

SCRIPT vn_n_on
BEGIN
	main.sp_n.disable
	main.vn_n.enable
END

SCRIPT binoff
BEGIN
	main.tx_bin2.hide
	main.rb_sp_n.hide
	main.sp_n.hide
	main.tx_sp_n.hide
	main.rb_vn_n.hide
	main.vn_n.hide
	main.tx_vn_n.hide
	model2.ck_asis.disable
END

SCRIPT nbinon
BEGIN
	main.tx_nbin3.show
	main.en_k.show
END

SCRIPT nbinoff
BEGIN
	main.tx_nbin3.hide
	main.en_k.hide
END

SCRIPT poweron
BEGIN
	main.tx_odds2.hide
	main.tx_power2.show
	main.en_power.show
	main.en_power.setlabel "Power"
END

SCRIPT oddson
BEGIN
	main.tx_odds2.show
	main.tx_power2.show
	main.en_power.show
	main.en_power.setlabel "Odds power"
END

SCRIPT poweroff
BEGIN
	main.tx_power2.hide
	main.tx_odds2.hide
	main.en_power.hide
END

DIALOG model2, tabtitle("Model 2")
BEGIN
  GROUPBOX gb_opt    _lft      _top      _iwd      _ht12,		/*
		*/ label("Options")					/*
		*/
  RADIO    rb_expose _ilft     _ms       _cwd4_1   .,			/*
		*/ label("Exposure variable:")				/*
		*/ first						/*
		*/ clickon("script expose")				/*
		*/
  RADIO    rb_offset _ilft4_2  @         @         .,			/*
		*/ label("Offset variable:")				/*
		*/ last							/*
		*/ clickon("script offset")				/*
		*/

  VARNAME  vn_expose _ilft     _ss       @         .,			/*
		*/ label("Exposure variable")				/*
		*/ option("exposure")					/*
		*/
  VARNAME  vn_offset _ilft4_2  @         @         .,			/*
		*/ label("Offset variable")				/*
		*/ option("offset")					/*
		*/

  CHECKBOX ck_nocons _ilft     _ls       _cwd2     .,			/*
		*/ label("Suppress constant term")			/*
		*/ option("noconstant")					/*
		*/
  CHECKBOX ck_force  _ilft     _ms       _ibwd     .,			/*
*/ label("Force estimation if unequally spaced in time [force] (advanced)")	/*
		*/ option("force")					/*
		*/

  CHECKBOX ck_asis		_ilft	_ms	@	.,		///
	option("asis")							///
	label("Retain perfect predictor variables")
END

SCRIPT expose
BEGIN
  model2.vn_expose.enable
  model2.vn_offset.disable
END

SCRIPT offset
BEGIN
  model2.vn_expose.disable
  model2.vn_offset.enable
END

INCLUDE xtgee_common
INCLUDE byifin
INCLUDE weights_fpi
INCLUDE se

SCRIPT rpt_POSTINIT
BEGIN
	create STRING rpt_bu_fmtcoefResults
	program rpt_bu_fmtcoef_ckResults
	create STRING rpt_bu_facvarsResults
        program rpt_bu_facvars_ckResults
END

DIALOG rpt, tabtitle("Reporting")
BEGIN
  DEFINE _x _lft
  DEFINE _cx _spr2b
  DEFINE _y _top
  INCLUDE _sp_level

  CHECKBOX ck_eform  _lft      _ls       _cwd1     .,			/*
		*/ label("Report exponentiated coefficients")		/*
		*/ option("eform")					/*
		*/

  DEFINE _x _lft
  DEFINE _y _ls
  INCLUDE _bu_factor_vars_reporting

  DEFINE _x _lft2
  DEFINE _y @
  INCLUDE _bu_coef_table_reporting
END

INCLUDE fmt_coef_table_reporting_pr
INCLUDE factor_vars_reporting_pr

PROGRAM rpt_output
BEGIN
	optionarg /hidedefault rpt.sp_level
	option rpt.ck_eform
	put " " rpt_bu_facvarsResults
	put " " rpt_bu_fmtcoefResults
END

INCLUDE max_ml

PROGRAM command
BEGIN
	put /program by_output " "
	put "xtgee "
	varlist main.vn_dv [main.vl_iv]
	if !main.vl_iv & model2.ck_nocons {
	  stopbox stop `""Suppress constant term" is selected without independent variables."'
	}
	put " " /program ifin_output
	put " " /program weights_output
	beginoptions
		/* link and family options */
		if main.rb_id_bi | main.rb_lg_bi | main.rb_lt_bi /*
			*/ | main.rb_pr_bi | main.rb_cl_bi | main.rb_pw_bi /*
			*/ | main.rb_op_bi | main.rb_rr_bi {
			put "family(binomial"
			if main.rb_sp_n {
				put " " main.sp_n
			}
			if main.rb_vn_n & main.vn_n {
				put " " main.vn_n
			}
			put ") "
		}
		if main.rb_id_nb | main.rb_lg_nb | main.rb_pw_nb /*
			*/ | main.rb_nb_nb {
			put "family(nbinomial"
			if main.en_k {
				put " " main.en_k
			}
			put ") "
		}
		option main.rb_id_ga main.rb_id_ig main.rb_id_bi /*
			*/ main.rb_id_po main.rb_id_nb main.rb_id_gm
		option main.rb_lg_ga main.rb_lg_ig main.rb_lg_bi /*
			*/ main.rb_lg_po main.rb_lg_nb main.rb_lg_gm
		option main.rb_lt_bi main.rb_pr_bi main.rb_cl_bi
		option main.rb_pw_ga main.rb_pw_ig main.rb_pw_bi /*
			*/ main.rb_pw_po main.rb_pw_nb main.rb_pw_gm
		option main.rb_nb_nb
		option main.rb_rr_ga main.rb_rr_bi /*
			*/ main.rb_rr_po main.rb_rr_gm
		if main.rb_rr_ga {
			put " " "link(reciprocal)"
		}
		if main.rb_pw_ga | main.rb_pw_ig | main.rb_pw_bi /*
		*/ | main.rb_pw_po | main.rb_pw_nb | main.rb_pw_gm {
			put " " "link(power"
			if main.en_power {
				put " " main.en_power
			}
			put ") "
		}
		if main.rb_op_bi {
			put " " "link(opower"
			if main.en_power {
				put " " main.en_power
			}
			put ") "
		}

		optionarg model2.vn_expose
		optionarg model2.vn_offset
		option model2.ck_nocons
		option model2.ck_force
		option model2.ck_asis

		put " " /program xtgee_common_output
		put " " /program se_output
		put " " /program rpt_output
		put " " /program max_output
	endoptions
END

