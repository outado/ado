{smcl}
{* *! version 1.0.3  22jul2013}{...}
{viewerdialog "predict" "dialog xtologit"}{...}
{vieweralsosee "[XT] xtologit postestimation" "mansection XT xtologitpostestimation"}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[XT] xtologit" "help xtologit"}{...}
{viewerjumpto "Description" "xtologit postestimation##description"}{...}
{viewerjumpto "Syntax for predict" "xtologit postestimation##syntax_predict"}{...}
{viewerjumpto "Menu for predict" "xtologit postestimation##menu_predict"}{...}
{viewerjumpto "Options for predict" "xtologit postestimation##options_predict"}{...}
{viewerjumpto "Example" "xtologit postestimation##example"}{...}
{title:Title}

{p2colset 5 37 39 2}{...}
{p2col :{manlink XT xtologit postestimation} {hline 2}}Postestimation tools for xtologit{p_end}
{p2colreset}{...}


{marker description}{...}
{title:Description}

{pstd}
The following postestimation commands are available after {cmd:xtologit}:

{synoptset 17}{...}
{p2coldent :Command}Description{p_end}
{synoptline}
INCLUDE help post_contrast
INCLUDE help post_estatic
INCLUDE help post_estatsum
INCLUDE help post_estatvce
INCLUDE help post_estimates
INCLUDE help post_hausman
INCLUDE help post_lincom
INCLUDE help post_lrtest
INCLUDE help post_margins
INCLUDE help post_marginsplot
INCLUDE help post_nlcom
{synopt :{helpb xtologit postestimation##predict:predict}}predictions, residuals, influence statistics, and other diagnostic measures{p_end}
INCLUDE help post_predictnl
INCLUDE help post_pwcompare
INCLUDE help post_test
INCLUDE help post_testnl
{synoptline}
{p2colreset}{...}


{marker syntax_predict}{...}
{marker predict}{...}
{title:Syntax for predict}

{p 8 16 2}
{cmd:predict}
{dtype}
{c -(}{it:stub}{cmd:*} | {newvar} | {it:{help newvarlist}}{c )-}
{ifin}
[{cmd:,} {it:statistic}
{opt o:utcome(outcome)} {opt nooff:set}]

{synoptset 17 tabbed}{...}
{synopthdr :statistic}
{synoptline}
{syntab:Main}
{synopt :{opt xb}}linear prediction; the default{p_end}
{synopt :{opt pu0}}probability of the specified outcome ({cmd:outcome()}) assuming that the random effect is zero{p_end}
{synopt :{opt stdp}}standard error of the linear prediction{p_end}
{synoptline}
{p2colreset}{...}
{p 4 6 2}
If you do not specify {cmd:outcome()}, {cmd:pu0} (with one new variable
specified) assumes {cmd:outcome(#1)}.{p_end}
{p 4 6 2}
You specify one or k new variables with {cmd:pu0}, where {it:k} is the number
of outcomes.{p_end}
{p 4 6 2}
You specify one new variable with {cmd:xb} and {cmd:stdp}.{p_end}
INCLUDE help esample


INCLUDE help menu_predict


{marker options_predict}{...}
{title:Options for predict}

{dlgtab:Main}

{phang}
{opt xb}, the default, calculates the linear prediction.

{phang}
{opt pu0} calculates predicted probabilities, assuming that the random effect
for that observation's panel is zero.

{pmore}
You specify one or {it:k} new variables, where {it:k} is the number of
categories of the dependent variable.  If you specify the {cmd:outcome()}
option, the probabilities will be predicted for the requested outcome only, in
which case, you specify only one new variable.  If you specify only one new
variable and do not specify {cmd:outcome()}, {cmd:outcome(#1)} is assumed.

{phang}
{opt stdp} calculates the standard error of the linear prediction.

{phang}
{opt outcome(outcome)} specifies the outcome for which the predicted
probabilities are to be calculated.  {opt outcome()} should contain either one
value of the dependent variable or one of {opt #1}, {opt #2}, {it:...}, with
{opt #1} meaning the first category of the dependent variable, {opt #2}
meaning the second category, etc.

{phang}
{opt nooffset} is relevant only if you specified {opth offset(varname)} for
{cmd:xtologit}.  This option modifies the calculations made by {cmd:predict} so
that they ignore the offset variable; the linear prediction is treated as xb
rather than xb + offset.


{marker example}{...}
{title:Example}

{pstd}Setup{p_end}
{phang2}{cmd:. webuse tvsfpors}{p_end}
{phang2}{cmd:. xtset school}{p_end}
{phang2}{cmd:. xtologit thk prethk cc##tv}{p_end}

{pstd}Compute predicted probabilities, assuming random effect is zero{p_end}
{phang2}{cmd:. predict prob*, pu0}{p_end}
