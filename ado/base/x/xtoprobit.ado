*! version 1.0.1  14may2013
program xtoprobit, eclass byable(onecall) prop(xt)
	version 13
	
	if _by() {
		local by "by `_byvars'`_byrc0':"
	}
	
	`by' _vce_parserun xtoprobit, panel : `0'
	if "`s(exit)'" != "" {
		ereturn local cmdline `"xtoprobit `0'"'
		exit
	}
	
	if replay() {
		if "`e(cmd)'" != "xtoprobit" {
			error 301
		}
		if _by() {
			error 190
		}
		_xtordinal_display `0'
		exit
	}
	`by' _xtordinal oprobit `0'
end

