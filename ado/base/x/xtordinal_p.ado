*! version 1.0.1  01may2013
program xtordinal_p, eclass
	version 13
	
	if !inlist("`e(cmd)'","xtologit","xtoprobit") {
		di "{err}last estimates not found"
		exit 198
	}
	
	syntax  anything 				///
		[if] [in] [, 				///
		pu0					///
		xb					///
		stdp					///
		noOFFset				///
		Outcome(passthru)			///
		]
	
	local pred `xb' `stdp' `pu0'
	if `=`:list sizeof pred'' > 1 {
		di "{err}only one of {bf:xb}, {bf:stdp}, {bf:pu0} can be specified"
		exit 198
	}
	
	if "`pu0'" != "" local pred pr
	if "`pred'" == "" {
		di "{txt}(option xb assumed; linear prediction)"
		local pred xb
	}
	if inlist("`pred'","xb","stdp") & "`outcome'"!="" {
		di "{err}option {bf:outcome()} not allowed with {bf:`pred'}"
		exit 198
	}
	
	gsem_p `anything' `if' `in' , `pred' `offset' `outcome' fixedonly
		
end
exit

