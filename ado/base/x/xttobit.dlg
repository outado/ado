/*
  xttobit

*!  VERSION 1.3.8  02may2014

  keyword:  eclass

*/

VERSION 12.0

INCLUDE _std_large
DEFINE _dlght 350
INCLUDE header

HELP hlp1, view("help xttobit")
RESET res1

SCRIPT PREINIT
BEGIN
	script se_createAsOIMJknifeBstrap
END

SCRIPT POSTINIT
BEGIN
	script se_setCluster_off
	script se_setTitleSE
END

SCRIPT show_xtset
BEGIN
	create CHILD xtset
END

DIALOG main, label("xttobit - Random-effects tobit models") tabtitle("Model")
BEGIN
  BUTTON   bu_xtset	_xsetbu	_top	_setbuwd .,		///
	onpush(script show_xtset)				///
	label("Panel settings...")

  TEXT tx_dv 		_lft	_topph	_vnwd	.,			///
	label("Dependent variable:")
  VARNAME vn_dv		@ 	_ss	@	.,			///
	ts								///
	numeric								///
	label("Dependent variable")

  TEXT tx_iv		_vlx	_topph	160	.,			///
	label("Independent variables:")
  VARLIST vl_iv		@	_ss	_vlwd	.,			///
	fv ts								///
	allowcat							///
	numeric								///
	label("Independent variables")

  CHECKBOX ck_nocons @         _ms       @         .,			/*
		*/ label("Suppress constant term")			/*
		*/ option("noconstant")					/*
		*/

  GROUPBOX gb_censor _lft      _ls       _iwd      _ht4h,		/*
*/ label("At least one censoring limit or variable must be specified")	/*
		*/
  TEXT     tx_llarg  _indent   _ss       _cwd2     .,			/*
		*/ label("Left-censoring limit/variable:")		/*
		*/
  DEFINE y @y
  VARNAME  vn_ll     @         _ss       _en14wd   .,			/*
		*/ label("Left-censoring limit/variable")		/*
		*/ option("ll")						/*
		*/ numeric						/*
		*/
  TEXT     tx_ularg  _lft2     y         _cwd2     .,			/*
		*/ label("Right-censoring limit/variable:")		/*
		*/
  VARNAME  vn_ul     @         _ss       _en14wd   .,			/*
		*/ label("Right-censoring limit/variable")		/*
		*/ option("ul")						/*
		*/ numeric						/*
		*/

  GROUPBOX gb_opts   _lft      _xxls     _iwd      _ht12,		/*
		*/ label("Options")					/*
		*/

  TEXT     tx_offset _ilft     _ss       _cwd2     .,			/*
		*/ label("Offset variable:")				/*
		*/
  VARNAME  vn_offset @         _ss       _vnwd     .,			/*
		*/ label("Offset variable")				/*
		*/ option("offset")					/*
		*/ numeric						/*
		*/

  DEFINE _x _ilft
  DEFINE _y _ls
  DEFINE _cx _ilw80
  DEFINE _bux _islw80
  INCLUDE _constraints

  DEFINE _x _ilft
  DEFINE _xw _ibwd
  INCLUDE _ck_collinear

END

INCLUDE _constraints_sc 
INCLUDE byifin
INCLUDE weights_i
INCLUDE se

SCRIPT rpt_POSTINIT
BEGIN
	create STRING rpt_bu_fmtcoefResults
	program rpt_bu_fmtcoef_ckResults
	create STRING rpt_bu_facvarsResults
        program rpt_bu_facvars_ckResults
END

DIALOG rpt, tabtitle("Reporting")
BEGIN
  DEFINE _x _lft
  DEFINE _cx _spr2
  DEFINE _y _top
  INCLUDE _sp_level

  GROUPBOX gb_tests    _lft        +35      _iwd         _ht5h,		///
  		label("Additional test statistics")
  CHECKBOX ck_tobit    _ilft       _ms      _ibwd        .,		///
		label("Perform likelihood-ratio test comparing model against pooled tobit model")		///
		option("tobit")
  CHECKBOX ck_noskip   _ilft       _ms      _ibwd        .,		///
		label("Perform likelihood-ratio test")			///
		option(noskip)

  DEFINE _x _lft
  DEFINE _y _xxls
  DEFINE _cx _iwd
  INCLUDE _nocnsreport

  DEFINE _x _lft
  DEFINE _y _ls
  DEFINE _cx _iwd
  INCLUDE _report_columns

  DEFINE _x _lft
  DEFINE _y _ls
  INCLUDE _bu_factor_vars_reporting

  DEFINE _x _lft2
  DEFINE _y @
  INCLUDE _bu_coef_table_reporting
END

INCLUDE fmt_coef_table_reporting_pr
INCLUDE factor_vars_reporting_pr

INCLUDE intopts_re
INCLUDE max_ml

PROGRAM command
BEGIN
	put /program by_output
	put "xttobit "
	varlist main.vn_dv [main.vl_iv]
	put " " /program ifin_output
	put " " /program weights_output
	beginoptions
		option main.ck_nocons

		if ! main.vn_ll & ! main.vn_ul {
			stopbox stop				/*
			*/ `"At least one censoring limit"'	/*
			*/ `""Left" or "Right" must be specified"'
		}
		optionarg main.vn_ll
		optionarg main.vn_ul
		optionarg main.vn_offset
		INCLUDE _constraints_main_pr
		option main.ck_collinear

		put " " /program se_output
		optionarg /hidedefault rpt.sp_level
		option rpt.ck_tobit
		option rpt.ck_noskip
		INCLUDE _nocnsreport_pr
		INCLUDE _report_columns_pr
		put " " rpt_bu_facvarsResults
		put " " rpt_bu_fmtcoefResults
		put " " /program intopts_re_output
		put " " /program max_output
	endoptions
END
